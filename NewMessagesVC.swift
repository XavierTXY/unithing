//
//  NewMessagesVC.swift
//  UniThing
//
//  Created by Xavier TanXY on 19/12/16.
//  Copyright © 2016 Xavier TanXY. All rights reserved.
//

import UIKit
import Firebase
    import ViewAnimator


class NewMessagesVC: UITableViewController {

    private let animations = [AnimationType.from(direction: .bottom, offset: 30.0)]

    let cellId = "cellId"
    var users = [User]()
    
    var emptyView: EmptyConfessionView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        navigationController?.title = "New Message"
        
        navigationItem.leftBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "cancel"), style: .plain, target: self, action: #selector(handleCancel))
//            UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(handleCancel))
//        self.tableView.register(NewMsgCell.self, forCellReuseIdentifier: "NewMsgCell")
       tableView.register(UserCell.self, forCellReuseIdentifier: cellId)
       fetchFriends()
        tableView.tableFooterView = UIView()
        tableView.separatorStyle = .none
        self.navigationController?.navigationBar.tintColor = PURPLE
        self.navigationController?.navigationBar.titleTextAttributes = [ NSForegroundColorAttributeName: PURPLE]
        
        
        emptyView = EmptyConfessionView.instanceFromNib() as! EmptyConfessionView
        
        emptyView.frame = self.tableView.bounds
        emptyView.iconImg.image = #imageLiteral(resourceName: "EmptyFriend")
        emptyView.titleLbl.text = "You have no friends"
        emptyView.detailLbl.text = "Start following people today!"
        self.tableView.addSubview(emptyView)
        self.hideEmptyView()
    }


    func showEmptyView() {
        emptyView.isHidden = false
        self.tableView.bringSubview(toFront: emptyView)
        
    }
    
    func hideEmptyView() {
        emptyView.isHidden = true
        self.tableView.sendSubview(toBack: emptyView)
        
    }
    func handleCancel() {
        dismiss(animated: true, completion: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
    }
    
    func fetchFriends() {
        
        DataService.ds.REF_USERS.child((Auth.auth().currentUser?.uid)!).child("following").observeSingleEvent(of: .value, with: { (snapshot) in


            if let snapshots = snapshot.children.allObjects as? [DataSnapshot] {
                
                
                for snap in snapshots {
                    
                    if snap.key != "key" {
                        DataService.ds.REF_USERS.child(snap.key).observeSingleEvent(of: .value, with: { (snapshot2) in
                            if let dict = snapshot2.value as? Dictionary<String, AnyObject> {
                                let user = User(userKey: snap.key, userData: dict)
                                self.users.append(user)
                                //                                self.tableView.reloadData()
                                self.attemptReloadTable()
                            }
                            //
                        })
                    }
                    
                }
                
                //
                //
                //
            }
            self.attemptReloadTable()

        })

    }
    
    func attemptReloadTable() {
        self.tableView.reloadData()
        UIView.animate(views: self.tableView.visibleCells, animations: animations, completion: {
        })
        
        if users.count == 0 {
            self.showEmptyView()
        } else {
            self.hideEmptyView()
        }
    }
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return users.count
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: cellId, for: indexPath) as! UserCell
        
        let user = users[indexPath.row]
        cell.textLabel?.text = user.name
        cell.detailTextLabel?.text = "\(user.universityShort) - \(user.course)"
        
        if user.profilePicUrl != NO_PIC {
            cell.profileImageView.loadImageUsingCacheWithUrlStringWithoutIndex(imageUrl: (user.profilePicUrl))
            cell.initialLbl.isHidden = true
        } else {
            cell.initialLbl.isHidden = false
            var i = String((user.userName[(user.userName.startIndex)])).capitalized
            cell.initialLbl.text = i
            cell.profileImageView.backgroundColor = ColorHelper().pickColor(alphabet: Character(i))
        }
        
        return cell
    }
    
    var messagesVC: MessagesVC?
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
      
        
        dismiss(animated: true) {
            let user = self.users[indexPath.row]
            self.messagesVC?.showChatControllerForUser(user: user)
            
        
        }
        
    }

}
