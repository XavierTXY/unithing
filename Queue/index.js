const Queue = require('firebase-queue');

var firebase = require('firebase').initializeApp({
  serviceAccount: "/xaviertanxy/Desktop/UniThing/Keys/unithing-3a985-firebase-crashreporting-hixqh-59b79657fe.json",
  databaseURL: "https://unithing-3a985.firebaseio.com/"
}, 'Queue');

var queueRef = firebase.database().ref('firebase-queue/login-queue');
//var queueRef2 = firebase.database().ref('firebase-queue/delogin-queue');
var accountRef = firebase.database().ref('firebase-queue/accounts');

var queue = new Queue(queueRef, {sanitize: false}, function(data, progress, resolve, reject) {


	return accountRef.child(data.user.uid).set(data.user)
		.then( function() {

			return queueRef.child(data._id).remove();
		})
		.then(resolve)
		.catch(reject);
});

// var queue2 = new Queue(queueRef2, {sanitize: false}, function(data, progress, resolve, reject) {


// 	return accountRef.child(data.user.uid).set(data.user)
// 		.then( function() {

// 			return queueRef.child(data._id).remove();
// 		})
// 		.then(resolve)
// 		.catch(reject);
// });

module.exports = queue;	
// module.exports = queue2;		