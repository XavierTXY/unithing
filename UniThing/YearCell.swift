//
//  YearCell.swift
//  UniThing
//
//  Created by Xavier TanXY on 30/11/16.
//  Copyright © 2016 Xavier TanXY. All rights reserved.
//

import UIKit

class YearCell: UITableViewCell {

    @IBOutlet weak var yearLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    func configureCell(year: String) {
        yearLabel.text = year
    }

}
