//
//  YearVC.swift
//  UniThing
//
//  Created by Xavier TanXY on 30/11/16.
//  Copyright © 2016 Xavier TanXY. All rights reserved.
//

import UIKit
import ViewAnimator
class YearVC: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    var uni: University?
    var email: String?
    var name: String?
    var password: String?
    var selectedCourse: String!
    var course: String!
    var gender: String?
    var provider: String?
    var uid: String?
    
    var years = [String]()
    var selectedYear: String!
    
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var tableView: UITableView!
    
    private let animations = [AnimationType.from(direction: .bottom, offset: 30.0)]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let barItem = UIBarButtonItem(image: #imageLiteral(resourceName: "Back Chevron"), style: .plain, target: self, action: "backTapped")
        barItem.tintColor = UIColor.white
        self.navigationItem.setLeftBarButton(barItem, animated: true)
        self.navigationItem.setRightBarButton(nil, animated: true)
        
        tableView.delegate = self
        tableView.dataSource = self
        tableView.tableFooterView = UIView()
        
        self.navigationItem.title = "Entry Year";
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title:"", style:.plain, target:nil, action:nil)
        
        loadYear()
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        //UIView.animate(withDuration: 0.3, delay: 0.1, options: [], animations: { self.mainView.alpha = 1.0 }, completion: nil)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
       // self.mainView.alpha = 0.0
    }
    
    func backTapped() {
        self.navigationController?.popViewController(animated: true)
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        UIView.animate(views: self.tableView.visibleCells, animations: self.animations, completion: {
        })
        return years.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if let cell = tableView.dequeueReusableCell(withIdentifier: "YearCell") as? YearCell {
            
            var year = years[indexPath.row]
            
            cell.configureCell(year: year)
            
            return cell
            
        } else {
            
            return YearCell()
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        selectedYear = years[indexPath.row]
        
        
        
        performSegue(withIdentifier: "SignUpVC", sender: nil)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 43;
    }

    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let destinationVC = segue.destination as? SignUpVC {
            destinationVC.uni = self.uni
            destinationVC.course = self.course
            destinationVC.year = selectedYear
            destinationVC.name = self.name
            destinationVC.email = self.email
            destinationVC.password = self.password
            destinationVC.gender = self.gender
            destinationVC.provider = self.provider
            destinationVC.uid = self.uid
            
            
        }
    }
    
    func loadYear() {
        years = ["2005", "2006", "2007", "2008", "2009", "2010", "2011", "2012", "2013", "2014", "2015", "2016", "2017", "2018"]

    }
    
    
    
}
