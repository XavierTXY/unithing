//
//  ViewController.swift
//  UniThing
//
//  Created by Xavier TanXY on 23/11/16.
//  Copyright © 2016 Xavier TanXY. All rights reserved.
//

import UIKit
import Firebase
import FBSDKLoginKit
import SVProgressHUD
import PopupDialog

class MainVC: UIViewController {

   
    @IBOutlet weak var mainView: UIView!
    

    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupNavBar()
        

        
        
        UIApplication.shared.statusBarStyle = .lightContent
        SVProgressHUD.setDefaultStyle(SVProgressHUDStyle.custom)
        SVProgressHUD.setBackgroundColor(UIColor(red: 255/255, green: 255/255, blue: 255/255, alpha: 1.0))
        SVProgressHUD.setRingThickness(3)

        
        
        var needTutorial = UserDefaults.standard.value(forKey: "tutorial") as? Bool
//        needTutorial = true
        if( needTutorial != false ) {
            self.performSegue(withIdentifier: "TutorialVC", sender: nil)
            UserDefaults.standard.setValue(false, forKey: "tutorial")
        }
        
        

        
        
       
    }
    
    override func viewDidAppear(_ animated: Bool) {
        UIView.animate(withDuration: 0.5, delay: 0.2, options: [], animations: { self.mainView.alpha = 1.0 }, completion: nil)
        
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        mainView.alpha = 0.0
        
        
    }
    


    
    
    @IBAction func fbLoginTapped(_ sender: Any) {

        let facebookLogin = FBSDKLoginManager()
        
//        if #available(iOS 11.0, *) {
//            facebookLogin.loginBehavior = .native
//        } else {
//            facebookLogin.loginBehavior = .native
//        }
        facebookLogin.loginBehavior = .native
        
        facebookLogin.logIn(withReadPermissions: ["email"], from: self) { (result, error) in
            
            
            if error != nil {
                print("fucking \(error.debugDescription)")
            } else if result?.isCancelled == true {
                print("User cancelled fb auth")
            } else {
                SVProgressHUD.show()
                UIApplication.shared.beginIgnoringInteractionEvents()
                print("Xavier: Successfully logged in with fb")
                let credential = FacebookAuthProvider.credential(withAccessToken: FBSDKAccessToken.current().tokenString)
                self.firebaseAuth(credential)
              
                
            }
        }
    }
    
    func firebaseAuth(_ credential: AuthCredential) {
        Auth.auth().signIn(with: credential, completion: { (user, error) in
            if error != nil {
                print("unable to auth with fire base - \(error)")
                SVProgressHUD.showError(withStatus: "\(error)")
                UIApplication.shared.endIgnoringInteractionEvents()
            } else {
                print("Xavier: successful auth with firebase")
                if let user = user {
                    let userData = ["provider": credential.provider, "email": user.email, "OS": DataService.ds.OS]
                    //let userName = ["name": user.uid]
             
                    self.provider = credential.provider
                    self.email = user.email
                    
                    DataService.ds.createFirebaseDBUserWithPartial(uid: user.uid, userData: userData as! Dictionary<String, String>)
                    
                    self.checkUserProfile(id: user.uid)

                }
            }
        })
    }
    
    var provider: String?
    var email: String?
    var name: String?
    var gender: String?
    
    
    func checkUserProfile(id: String) {
    
        let _ = DataService.ds.REF_USERS.child(id).child("course").observeSingleEvent(of: .value, with: { (snapshot) in
            
            let data = snapshot.value as? String
            
            if data == nil {
                if((FBSDKAccessToken.current()) != nil) {
                    FBSDKGraphRequest(graphPath: "me", parameters: ["fields": "id, name, gender, email"]).start(completionHandler: { (connection, result, error) -> Void in
                        if (error == nil){
                           // print(result)
 
                            let data:[String:AnyObject] = result as! [String : AnyObject]
//                            self.name = data["name"] as! String
                            self.gender = data["gender"] as! String

                            
                            self.name = self.removeSpecialCharsFromString(str: data["name"] as! String)
                            if self.gender == "male" {
                                self.gender = "Male"
                            } else if self.gender == "female" {
                                self.gender = "Female"
                            }
                            

                            SVProgressHUD.dismiss()
                            UIApplication.shared.endIgnoringInteractionEvents()
                            
                            self.performSegue(withIdentifier: "UniVC", sender: nil)
                            
                        }
                    })
                }
                
                
            } else {

                SVProgressHUD.dismiss()
                UIApplication.shared.endIgnoringInteractionEvents()
                let defaults = UserDefaults.standard
                defaults.set(id, forKey: "uid")
                
                DataService.ds.addInstanceID()
                
                VCSegue().segueVC(bundleName: "HomeSB", controllerName: "TabBarVC", vc: self)
            }
            
            
        })
        
    }
    
    
    func setupNavBar() {
        


        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title:"", style:.plain, target:nil, action: "")
        self.navigationItem.backBarButtonItem?.tintColor = UIColor.white
    }
    
 

    @IBAction func nextTapped(_ sender: Any) {
        print("Xavier hi)")
        UIView.animate(withDuration: 1.0, delay: 1.0, options: [], animations: { self.mainView.alpha = 0.0 }, completion: nil)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if let destinationVC = segue.destination as? UniVC {
            destinationVC.email = self.email
            destinationVC.name = self.name
            //destinationVC.password = password
            destinationVC.gender = self.gender
            destinationVC.provider = self.provider
            
        }
    }
    
    func removeSpecialCharsFromString(str: String) -> String {
        let chars = Set("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLKMNOPQRSTUVWXYZ1234567890_".characters)
        
        return String(str.characters.filter { chars.contains($0) })
    }
}


