//
//  ProfileVC.swift
//  UniThing
//
//  Created by Xavier TanXY on 28/6/17.
//  Copyright © 2017 Xavier TanXY. All rights reserved.
//

import UIKit
import Firebase
import SVProgressHUD
import SDWebImage
import PopupDialog

class ProfileVC: UIViewController, UITableViewDelegate, UITableViewDataSource, UIPickerViewDataSource, UIPickerViewDelegate, UITextFieldDelegate, ReloadName, ReloadCourse,UIImagePickerControllerDelegate, UINavigationControllerDelegate {

    @IBOutlet weak var tableView: UITableView!

    var details = [[String]]()
    var genderData = [String]()
    var yearData = [String]()
    var selectedData = String()
    var commentDict = [String: Comment]()

    
    var user: User?
    
    var genderPicker: UIPickerView?
    var yearPicker: UIPickerView?
    var toolBar: UIToolbar?
    
    var tf: UITextField?
    var tf2: UITextField?
    
    var dimView: UIView!
    var imagePick: UIImagePickerController!
    
//    var stretchyHeaderView:GSKStretchyHeaderView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        SVProgressHUD.setBackgroundColor(UIColor(red: 244/255, green: 244/255, blue: 244/255, alpha: 0.95))
        
        tableView.delegate = self
        tableView.dataSource = self
        
        genderPicker = UIPickerView()
        genderPicker?.delegate = self
        genderPicker?.dataSource = self
        
        yearPicker = UIPickerView()
        yearPicker?.delegate = self
        yearPicker?.dataSource = self
        
        tf?.delegate = self
        tf2?.delegate = self
        
        imagePick = UIImagePickerController()
        imagePick.allowsEditing = true
        imagePick.delegate = self
        
        self.navigationItem.title = "Profile"
        self.navigationController?.navigationBar.barTintColor = UIColor.white
        self.navigationController?.navigationBar.titleTextAttributes = [ NSFontAttributeName: UIFont(name: "HelveticaNeue-Bold", size: 16)!]
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title:"", style:.plain, target:nil, action: "")
        self.navigationController?.navigationBar.layer.borderColor = BORDER_COLOR_CGCOLOR
        self.navigationController?.navigationBar.titleTextAttributes = [ NSForegroundColorAttributeName: PURPLE]
        
        tableView.rowHeight = UITableViewAutomaticDimension
        
        dimView = UIView(frame: (UIApplication.shared.keyWindow?.frame)!)
        dimView.backgroundColor = UIColor.black
        dimView.alpha = 0.5
        
        setupDataArrays()
        loadPicker()
        


  
    }
    
    func reloadNameFromChangeDetailVC(name: String) {
        user?.name = name
        self.tableView.reloadData()
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        
        var retVal = 1
        
        if pickerView == genderPicker {
            retVal = genderData.count
        } else if pickerView == yearPicker {
            retVal = yearData.count
        }
        
        return retVal
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
       
        if pickerView == genderPicker {
            return genderData[row]
        } else if pickerView == yearPicker {
            return yearData[row]
        }
        
        return ""
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if pickerView == genderPicker {
            selectedData = genderData[row]
            print(genderData[row])
        } else if pickerView == yearPicker {
            selectedData = yearData[row]
            print(yearData[row])
        }
        
    }
    
    
    
    
    func loadPicker() {
        toolBar = UIToolbar()
        toolBar?.barStyle = UIBarStyle.default
        toolBar?.sizeToFit()
        
        tf = UITextField()
        self.view.addSubview(tf!)
        
        tf2 = UITextField()
        self.view.addSubview(tf2!)
        
        let doneButton = UIBarButtonItem(title: "Save", style: UIBarButtonItemStyle.plain, target: self, action: Selector(("saveData")))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: UIBarButtonItemStyle.plain, target: self, action: Selector(("cancelPicker")))
        
        toolBar?.setItems([cancelButton, spaceButton, doneButton], animated: false)
        toolBar?.isUserInteractionEnabled = true
        
        genderPicker?.backgroundColor = UIColor.white
        tf?.inputView = self.genderPicker
        tf?.inputAccessoryView = toolBar

        
        yearPicker?.backgroundColor = UIColor.white
        tf2?.inputView = self.yearPicker
        tf2?.inputAccessoryView = toolBar
        
        
    }
    
    func displayAlert() {
        self.dimView.removeFromSuperview()
        
        let popup = PopupDialog(title: "You are only allowed to change your gender 3 times", message: "\(self.user!.genderCount) remainder left", image: nil)
        
        let buttonOne = CancelButton(title: "Cancel") {
          
        }
        
        let buttonTwo = DefaultButton(title: "Change", dismissOnTap: true) {
            SVProgressHUD.show()
            
            UIApplication.shared.keyWindow?.addSubview(self.dimView)
            self.tf?.resignFirstResponder()
            
            DataService.ds.REF_USERS.child((Auth.auth().currentUser?.uid)!).updateChildValues(["gender": self.selectedData]) { (error, ref) in
                
                if error == nil {
                    
                    let count = self.user!.genderCount
                    DataService.ds.REF_USERS.child((Auth.auth().currentUser?.uid)!).updateChildValues(["genderCount": "\(count - 1)"], withCompletionBlock: { (error2, ref) in
                        
                        if error2 == nil {
                            
                            self.user?.gender = self.selectedData
                            self.user?.genderCount = (self.user?.genderCount)! - 1
                            print("cuurent counr now \(self.user?.genderCount)")
                            self.updateGenderInDB(newGender: self.selectedData)
                            
                            
                        } else {
                            SVProgressHUD.showError(withStatus: "Can't Save")
                            self.dimView.removeFromSuperview()
                        }
                        
                        
                    })
                    
                    
                } else {
                    SVProgressHUD.showError(withStatus: "Can't Save")
                    self.dimView.removeFromSuperview()
                }
            }
        }
        
        popup.addButtons([buttonOne,buttonTwo])
        popup.buttonAlignment = .horizontal
        
        // Present dialog
        self.present(popup, animated: true, completion: nil)
        
//        let alertController = UIAlertController(title: "You are only allowed to change your gender 3 times", message: "\(self.user!.genderCount) remainder left", preferredStyle: .alert)
//
//        alertController.addAction(UIAlertAction(title: "Cancel", style: .cancel) { (action:UIAlertAction!) in
//
//        })
        
//
//        alertController.addAction(UIAlertAction(title: "Change", style: .default) { (action:UIAlertAction!) in
//
//
//        })
//
//        self.present(alertController, animated: true, completion: nil)
    }
    
    func updateGenderInDB(newGender: String) {
        let ref = DB_BASE
        
        var uid = (Auth.auth().currentUser?.uid)!
        var updateObj = [String:AnyObject]()
        
        //update comment
        DataService.ds.REF_User_Comment.child(uid).observeSingleEvent(of: .value, with: { (snapshot) in
            
            if let snapshots = snapshot.children.allObjects as? [DataSnapshot] {
                
                for snap in snapshots {
                    
                    
                    if let commentDict = snap.value as? Dictionary<String, AnyObject> {
                        
                        
                        let key = snap.key
                        let comment = Comment(commentKey: key, postData: commentDict)
                        
                        
                        
                        updateObj["confession-comments/\(comment.confessionKey)/\(comment.commentKey)/authorGender"] = newGender as AnyObject
                        
                        
                        
                    }
                }
                
                //update confession
                DataService.ds.REF_User_Confession.child(uid).observeSingleEvent(of: .value, with: { (snapshot) in
                    
                    if let snapshots = snapshot.children.allObjects as? [DataSnapshot] {
                        
                        for snap in snapshots {
                            
                            
                            if let confessionDict = snap.value as? Dictionary<String, AnyObject> {
                                
                                
                                let key = snap.key
                                let confession = Confession(confessionKey: key, postData: confessionDict)
                                
                                
                                updateObj["user-confession/\(uid)/\(confession.confessionKey)/authorGender"] = newGender as AnyObject
                                updateObj["uni-confession/\(confession.university)/\(confession.confessionKey)/authorGender"] = newGender as AnyObject
                                updateObj["confessions/\(confession.confessionKey)/authorGender"] = newGender as AnyObject
                                
                                if !confession.hashDict.isEmpty {
                                    
                                    for hash in confession.hashDict {
                                        updateObj["hashtag/\(hash.key)/\(confession.confessionKey)/authorGender"] = newGender as AnyObject
                                        
                                    }
                                    
                                }
                                
                                
                                
                                
                                
                            }
                        }
                        
                        //update here
                        ref.updateChildValues(updateObj)
                        self.dimView.removeFromSuperview()
                        self.tableView.reloadData()
                        SVProgressHUD.dismiss()
                        
                    }
                    
                    
                })
                
            }
            
            
        })
    }
    
//    func changeGenderInAllConfession(newGender: String, updateObj: [String:AnyObject]) {
//        let ref = DB_BASE
//
//        var uid = (Auth.auth().currentUser?.uid)!
////        var updateObj = [String:AnyObject]()
//
//    }
//
    
    func saveData() {
        
        
        if (tf?.isFirstResponder)! {
            print("Save gender")
            
            if self.user?.gender == selectedData {
                print("does not change ")
                self.dimView.removeFromSuperview()
                self.tf?.resignFirstResponder()
            } else {
                
                if (self.user?.genderCount)! <= 0 {
                    self.dimView.removeFromSuperview()
                    self.tf?.resignFirstResponder()
                    ErrorAlert().createAlert(title: "Could not perform action", msg: "You can't change your gender anymore", object: self)
                } else {
                    
                    displayAlert()

                    
                }
                

            }


            
            
        } else if (tf2?.isFirstResponder)! {
            print("Save eyar")
            
            DataService.ds.REF_USERS.child((Auth.auth().currentUser?.uid)!).updateChildValues(["year": selectedData]) { (error, ref) in
                
                if error == nil {
                    self.user?.year = self.selectedData
                    self.dimView.removeFromSuperview()
                    self.tf2?.resignFirstResponder()
                    self.tableView.reloadData()
                    SVProgressHUD.dismiss()
                } else {
                    SVProgressHUD.showError(withStatus: "Can't Save")
                    self.dimView.removeFromSuperview()
                    self.tf2?.resignFirstResponder()
                }
            }
            
        }
        
    
    }
    
    
    
    
    
    func cancelPicker() {
        self.dimView.removeFromSuperview()

        tf?.resignFirstResponder()
            tf2?.resignFirstResponder()
//        if (tf?.isFirstResponder)! {
//            tf?.resignFirstResponder()
//        } else if (tf2?.isFirstResponder)! {
//            tf2?.resignFirstResponder()
//        }
        
        
    }

    func setupDataArrays() {
        details = [["UserName", "Name", "Email"] , ["Gender", "Entry Year", "University", "Course"] ]
        genderData = ["Male", "Female"]
        yearData = ["2005", "2006", "2007", "2008", "2009", "2010", "2011", "2012", "2013", "2014", "2015", "2016", "2017", "2018"]
    }
    

    func numberOfSections(in tableView: UITableView) -> Int {
        return details.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
//        if indexPath.section == 0 && indexPath.row == 0 {
//            return 85
//        } else {
            return 45
//        }
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return details[section].count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var detail = details[indexPath.section][indexPath.row]

        if indexPath.section == 0 && indexPath.row == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "UserNameCell") as? UserNameCell
            cell?.configureCell(detail: detail, user: self.user!)
            return cell!

            //use detials cell
        } else if indexPath.section == 0 && indexPath.row == 2 {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "UserNameCell") as? UserNameCell
            cell?.configureCell(detail: detail, user: self.user!)
            return cell!
            
        } else {
        
        
            let cell = tableView.dequeueReusableCell(withIdentifier: "ProfileDetailsCell") as? ProfileDetailsCell
            cell?.configureCell(detail: detail, user: self.user!)
            return cell!
            //use user name cell
        }
        
  
    }
    
    var uni_course = 0
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        

        if indexPath.section == 0 && indexPath.row == 1 {
            print("touch name")
            performSegue(withIdentifier: "ChangeDetailVC", sender: nil)
        } else if indexPath.section == 1 && indexPath.row == 0 {
            print("touch gender")
            genderPicker?.selectRow(0, inComponent: 0, animated: true)
            selectedData = genderData[0]
            UIApplication.shared.keyWindow?.addSubview(self.dimView)
            tf?.becomeFirstResponder()
        } else if indexPath.section == 1 && indexPath.row == 1 {
            print("touch year")
            yearPicker?.selectRow(yearData.count - 1, inComponent: 0, animated: true)
            selectedData = yearData[yearData.count - 1]
            UIApplication.shared.keyWindow?.addSubview(self.dimView)
            tf2?.becomeFirstResponder()
        } else if indexPath.section == 1 && indexPath.row == 2 {
        print("touch uni")
            uni_course = 0
            performSegue(withIdentifier: "UniCourseVC", sender: nil)
        } else if indexPath.section == 1 && indexPath.row == 3 {
            uni_course = 1
            print("touch course")
            performSegue(withIdentifier: "UniCourseVC", sender: nil)
        }
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let destinationVC = segue.destination as? ChangeDetailVC {
            destinationVC.delegate = self
            destinationVC.name = self.user?.name
        }
        
        if let destinationVC = segue.destination as? UniCourseVC {
   
            //destinationVC.name = self.user?.name
            destinationVC.option = uni_course
            destinationVC.user = self.user
            destinationVC.delegate = self
        }
    }
    
    
    func reload_Uni_Course_FromCourseVC(courseName: String,uniName: String, uniShortName: String) {
        self.user?.universityShort = uniShortName
        self.user?.university = uniName
        self.user?.course = courseName
        self.tableView.reloadData()
    }
    
    func reloadCourseFromUniCourseVC(courseName: String) {
        print("hiii")
        self.user?.course = courseName
        self.tableView.reloadData()
    }
    
    func addProfilePic() {
        SVProgressHUD.show()
        self.tableView.isUserInteractionEnabled = false
        
        let cell = tableView.cellForRow(at: IndexPath(row: 0, section: 0)) as! ProfilePicCell
        
        if cell.profilePic.image == #imageLiteral(resourceName: "ProfilePic") {
            self.postToFirebase(imgUrl: NO_PIC)
        } else {
            if let img = cell.profilePic.image  {
                if let imgData = UIImageJPEGRepresentation(img, 0.1) {
                    
                    let imgUid = NSUUID().uuidString
                    let metaData = StorageMetadata()
                    metaData.contentType = "image/jpeg"
                    
                    DataService.ds.REF_PROFILE_PIC.child(imgUid).putData(imgData, metadata: metaData) { (metaData, error) in
                        if error != nil {
                            print("Xavier: upload image to firebase failed")
                            print(error.debugDescription)
                        } else {
                            print("Xavier: successfuly upload img to firebase")
                            let downloadUrl = metaData?.downloadURL()?.absoluteString
                            if let url = downloadUrl {
                                self.postToFirebase(imgUrl: url)
                            }
                            
                        }
                        
                    }
                }
                
            }
        }
    }
    
    func postToFirebase(imgUrl: String) {
        SVProgressHUD.dismiss()
        self.user?.profilePicUrl = imgUrl
        self.tableView.isUserInteractionEnabled = true
        let profilePicUrl = ["profilePicUrl": imgUrl]
        DataService.ds.REF_USERS.child((Auth.auth().currentUser?.uid)!).updateChildValues(profilePicUrl)
        changeAllComments(newImgUrl: imgUrl)
        
        
        

    }
    
    func changeAllComments(newImgUrl: String) {
        let ref = DB_BASE

        var uid = (Auth.auth().currentUser?.uid)!
        var updateObj = [String:AnyObject]()
            DataService.ds.REF_User_Comment.child(uid).observeSingleEvent(of: .value, with: { (snapshot) in
                
                if let snapshots = snapshot.children.allObjects as? [DataSnapshot] {
                    
                    for snap in snapshots {
                        
                        
                        if let commentDict = snap.value as? Dictionary<String, AnyObject> {
                            
                            
                            let key = snap.key
                            let comment = Comment(commentKey: key, postData: commentDict)
                            
                            
           
                            updateObj["confession-comments/\(comment.confessionKey)/\(comment.commentKey)/profilePicUrl"] = newImgUrl as AnyObject
                            
                            
                            
                        }
                    }
                    
                    //update here
                    ref.updateChildValues(updateObj)
                    
                }
                
                
            })
    }
    
    func addPicTapped() {
        let alert = UIAlertController()
 
        let cell = tableView.cellForRow(at: IndexPath(row: 0, section: 0)) as! ProfilePicCell
        if cell.profilePic.image == #imageLiteral(resourceName: "ProfilePic") {
            
            alert.addAction(UIAlertAction(title: "Camera", style: UIAlertActionStyle.default, handler:{ (UIAlertAction)in
                self.imagePick.sourceType = UIImagePickerControllerSourceType.camera;
                self.present(self.imagePick, animated: true, completion: nil)
            }))
            
            alert.addAction(UIAlertAction(title: "Photo library", style: UIAlertActionStyle.default, handler:{ (UIAlertAction)in
                self.imagePick.sourceType = UIImagePickerControllerSourceType.photoLibrary;
                self.present(self.imagePick, animated: true, completion: nil)
            }))
            
            alert.addAction(UIAlertAction(title: "Cancel", style: .cancel) { (action:UIAlertAction!) in
                
            })
            
            self.present(alert, animated: true, completion: nil)
            
        } else {
            
            alert.addAction(UIAlertAction(title: "Choose another picture", style: UIAlertActionStyle.default, handler:{ (UIAlertAction)in
                self.present(self.imagePick, animated: true, completion: nil)
            }))
            
            alert.addAction(UIAlertAction(title: "Remove current profile picture", style: .destructive , handler:{ (UIAlertAction)in
                cell.profilePic.image = #imageLiteral(resourceName: "ProfilePic")
                SVProgressHUD.show()
                self.deleteImageInStorage()
                self.postToFirebase(imgUrl: NO_PIC)
                
            }))
            
            alert.addAction(UIAlertAction(title: "Cancel", style: .cancel) { (action:UIAlertAction!) in
                
            })
            
            self.present(alert, animated: true, completion: nil)
        }
        
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : AnyObject]) {

        
        if let editedImage = info[UIImagePickerControllerEditedImage] as? UIImage {

            if let image = info[UIImagePickerControllerEditedImage] as? UIImage {
                let cell = tableView.cellForRow(at: IndexPath(row: 0, section: 0)) as! ProfilePicCell
                cell.profilePic.image = image
                
                addProfilePic()
            }

        }
        
        /// if the request successfully done just dismiss
        imagePick.dismiss(animated: true, completion: nil)
        
    }
    
    func deleteImageInStorage() {
        
        var imgUrl = (self.user?.profilePicUrl)!
        SDImageCache.shared().removeImage(forKey: (imgUrl as NSString) as String!)
        
        if self.user?.profilePicUrl != "NoPicture" {
            let ref = Storage.storage().reference(forURL: imgUrl)
            ref.delete(completion: { (error) in
                if error != nil {
                    print("there is an error deleting picture in fb storage")
                } else {
                    print("delete picture successfully in fb storage")
                }
            })
        }
    }

}
