//
//  Notification.swift
//  UniThing
//
//  Created by Xavier TanXY on 6/8/17.
//  Copyright © 2017 Xavier TanXY. All rights reserved.
//

import Foundation

class Notification {
    
    private var _notificationKey: String!
    private var _from: String!
    private var _userName: String!
    private var _type: String!
    private var _confessionKey: String?
    private var _commentKey: String?
    private var _time: NSNumber!
    private var _profilePicUrl: String!
    private var _seen: Bool!
    private var _replyToCommentContent: String!
    private var _likedCommentContent: String!
    private var _fromGender: String!
    
    var notificationKey: String {
        return _notificationKey
    }
    
    var from: String {
        return _from
    }
    
    var userName: String {
        return _userName
    }
    
    var type: String {
        return _type
    }
    
    var confessionKey: String {
        return _confessionKey!
    }
    
    var commentKey: String {
        return _commentKey!
    }
    var profilePicUrl: String {
        return _profilePicUrl
    }
    
    var time: NSNumber {
        return _time
    }
    
    var seen: Bool {
        set(newSeen) {
           _seen = newSeen
        }
        
        get {
            return _seen
        }
        
    }
    
    var replyToCommentContent: String {
        return _replyToCommentContent
    }
    
    var likedCommentContent: String {
        return _likedCommentContent
        
    }
    
    var fromGender: String {
        return _fromGender
        
    }
    
    init(notificationKey: String, notificationData: Dictionary<String, AnyObject>) {
        
        self._notificationKey = notificationKey
        
        if let from = notificationData["from"] as? String {
            self._from = from
        }
        
        if let userName = notificationData["userName"] as? String {
            self._userName = userName
        }
        
        if let type = notificationData["type"] as? String {
            self._type = type
        }
        
        if let confessionKey = notificationData["confessionKey"] as? String {
            self._confessionKey = confessionKey
        }
        
        if let commentKey = notificationData["commentKey"] as? String {
            self._commentKey = commentKey
        }
        
        if let time = notificationData["time"] as? NSNumber {
            self._time = time
        }
        
        if let profilePicUrl = notificationData["profilePicUrl"] as? String {
            self._profilePicUrl = profilePicUrl
        }
        
        if let seen = notificationData["seen"] as? Bool {
            self._seen = seen
        }
        
        if let commentContent = notificationData["replyToCommentContent"] as? String {
            self._replyToCommentContent = commentContent
        }
        
        if let likedContent = notificationData["likedCommentContent"] as? String {
            self._likedCommentContent = likedContent
        }
        
        if let fG = notificationData["fromGender"] as? String {
            self._fromGender = fG
        }
    }
}
