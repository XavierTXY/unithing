//
//  NotificationVC.swift
//  UniThing
//
//  Created by Xavier TanXY on 5/8/17.
//  Copyright © 2017 Xavier TanXY. All rights reserved.
//

import UIKit
import Firebase
import FirebaseDatabase
import SVProgressHUD
import Whisper
import ViewAnimator
import PageMenu

class NotificationVC: UIViewController, UITableViewDelegate, UITableViewDataSource, CAPSPageMenuDelegate {

    @IBOutlet weak var tableView: UITableView!
    
    let NUMBER_NOTIFICATION_ADD_EACH_TIME = 11
    var refreshControl: UIRefreshControl!
    var actInd: UIActivityIndicatorView!
    
    var notifications = [Notification]()
    var notificationDict = [String: Notification]()
    
    var totalNotifications: Int!
    var currentNumberOfNotifications: Int!
    var timer: Timer?
    var user: User?
    
    var headerView: NotificationView?
    
    var emptyView: EmptyNotificationView!
    var animateTableView = true
    var firstTime = true
    
    var selectedUserID: String!
    
    var pageMenu : CAPSPageMenu!
    var searchConfessionVC: SearchConfessionVC!
    var searchHashVC: SearchHashVC!
    
    private let animations = [AnimationType.from(direction: .bottom, offset: 30.0)]
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.tableView.clipsToBounds = true
        UIApplication.shared.statusBarStyle = .default
        NotificationCenter.default.addObserver(self, selector: #selector(SomeNotificationAct), name: NSNotification.Name(rawValue: "SomeNotification"), object: nil)
        SVProgressHUD.setDefaultStyle(SVProgressHUDStyle.custom)
        SVProgressHUD.setBackgroundColor(UIColor(red: 255/255, green: 255/255, blue: 255/255, alpha: 1.0))
        SVProgressHUD.setRingThickness(3)
        SVProgressHUD.show()
        
        currentNumberOfNotifications = NUMBER_NOTIFICATION_ADD_EACH_TIME
        
        tableView.delegate = self
        tableView.dataSource = self
        tableView.tableFooterView = UIView()
//        tableView.estimatedRowHeight = 88
//        tableView.rowHeight = UITableViewAutomaticDimension
        
        self.navigationItem.title = "Activities"
        
        self.navigationController?.navigationBar.barTintColor = UIColor.white
        self.navigationController?.navigationBar.titleTextAttributes = [ NSFontAttributeName: UIFont(name: "HelveticaNeue-Bold", size: 16)!]

     
        
        self.navigationController?.navigationBar.layer.borderColor = BORDER_COLOR_CGCOLOR
        
        self.navigationController?.navigationBar.tintColor = PURPLE
        
        let backImage = #imageLiteral(resourceName: "Back Chevron")
        self.navigationController?.navigationBar.backIndicatorImage = backImage
        self.navigationController?.navigationBar.backIndicatorTransitionMaskImage = backImage
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title:"", style:.plain, target:nil, action: "")
        
        self.tabBarController?.tabBar.barTintColor = UIColor.white
        
        self.navigationController?.navigationBar.titleTextAttributes = [ NSForegroundColorAttributeName: PURPLE]
        
        refreshControl = UIRefreshControl()
        refreshControl.backgroundColor = UIColor.clear
        refreshControl.tintColor = UIColor.lightGray
        refreshControl.addTarget(self, action: "refresh", for: UIControlEvents.valueChanged)
        tableView.addSubview(refreshControl)
        

        
        getUserDetails()

        
        
        initFooter()
        
        emptyView = EmptyNotificationView.instanceFromNib() as! EmptyNotificationView
        emptyView.frame = self.tableView.bounds
        
        self.tableView.addSubview(emptyView)
        
        self.hideEmptyView()
        

        
        // Array to keep track of controllers in page menu
        var controllerArray : [UIViewController] = []
        
        searchConfessionVC = SearchConfessionVC(nibName: "SearchConfessionVC", bundle: nil)
        searchConfessionVC.parentNavigationController = self.navigationController
//        searchConfessionVC.searchVC = self
        searchConfessionVC.title = "Confessions"
        searchHashVC = SearchHashVC(nibName: "SearchHashVC", bundle: nil)
        searchHashVC.title = "Hashtags"
//        searchHashVC.searchVC = self
        searchHashVC.parentNavigationController = self.navigationController
        controllerArray.append(searchConfessionVC)
        controllerArray.append(searchHashVC)
        
        // Customize page menu to your liking (optional) or use default settings by sending nil for 'options' in the init
        // Example:
        var parameters: [CAPSPageMenuOption] = [
            .menuItemSeparatorWidth(4.3),
            .scrollMenuBackgroundColor(UIColor.white),
            .menuItemSeparatorPercentageHeight(0.1),
            .viewBackgroundColor(UIColor(red: 247.0/255.0, green: 247.0/255.0, blue: 247.0/255.0, alpha: 1.0)),
            .bottomMenuHairlineColor(UIColor(red: 20.0/255.0, green: 20.0/255.0, blue: 20.0/255.0, alpha: 0.1)),
            .selectionIndicatorColor(PURPLE),
            .menuMargin(20.0),
            .menuHeight(40.0),
            .selectedMenuItemLabelColor(PURPLE),
            .unselectedMenuItemLabelColor(UIColor(red: 40.0/255.0, green: 40.0/255.0, blue: 40.0/255.0, alpha: 1.0)),
            .menuItemFont(UIFont(name: "HelveticaNeue-Medium", size: 14.0)!),
            .useMenuLikeSegmentedControl(true),
            .menuItemSeparatorRoundEdges(true),
            .selectionIndicatorHeight(2.0)
            
        ]
        
        
        let navheight = (navigationController?.navigationBar.frame.size.height ?? 0) + UIApplication.shared.statusBarFrame.size.height
        let frame = CGRect(x: 0, y: navheight, width: view.frame.width, height: view.frame.height-navheight)
        pageMenu = CAPSPageMenu(viewControllers: controllerArray ,frame: frame, pageMenuOptions: parameters)
        
        pageMenu.delegate = self
        

    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        UIApplication.shared.statusBarStyle = .default
        UIApplication.shared.applicationIconBadgeNumber = 0
        self.slideMenuController()?.removeLeftGestures()
        fetchNotification {
            SVProgressHUD.dismiss()
            self.clearDisplayBadge()
            
        }

        displayNoInternetConnection()
        
        
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationController?.view.backgroundColor = .clear
    }
    
    func clearDisplayBadge() {
        
        let ref = DataService.ds.REF_USERS.child((Auth.auth().currentUser!.uid))
        var updateObj = [String:AnyObject]()
        updateObj["/notiCount"] = 0 as AnyObject
        ref.updateChildValues(updateObj)
        
        
        if let tabItems = self.tabBarController?.tabBar.items as NSArray!
        {
            // In this case we want to modify the badge number of the third tab:
            let tabItem = tabItems[3] as! UITabBarItem
            tabItem.badgeValue = nil
            
        }
        
    }

    
    func displayNoInternetConnection() {
        if !DataService.ds.connectedToNetwork() {
            
            let murmur = Murmur(title: "No Internet Connection")
            
            
            // Show and hide a message after delay
            Whisper.show(whistle: murmur, action: .show(0.5))
            
            // Present a permanent status bar message
            Whisper.show(whistle: murmur, action: .present)
            
            // Hide a message
            Whisper.hide(whistleAfter: 3)
            
            
        }
    }
    
    func SomeNotificationAct(notification: NSNotification){
        DispatchQueue.main.async() {
            //self.performSegueWithIdentifier("NotificationView", sender: self)
            print("come")
            self.fetchNotification {
                SVProgressHUD.dismiss()
                
            }
            
        }
    }
    
    func showEmptyView() {
        emptyView.isHidden = false
        self.tableView.bringSubview(toFront: emptyView)
    }
    
    func hideEmptyView() {
        emptyView.isHidden = true
        self.tableView.sendSubview(toBack: emptyView)
        
    }

    
    func getUserDetails() {
        DataService.ds.REF_USERS.child((Auth.auth().currentUser?.uid)!).observe(.value, with: { (snapshot) in
            if let snapshots = snapshot.children.allObjects as? [DataSnapshot] {
                //print("\(snapshot.value)")
                
                if let userDict = snapshot.value as? Dictionary<String, AnyObject> {
                    let key = snapshot.key
                    self.user = User(userKey: key, userData: userDict)
                }
                
            }
            
        })
    }
    
    func initFooter() {
        
        actInd = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.gray)
        actInd.frame = CGRect(x: 0, y: 0, width: 320, height: 40)
        actInd.hidesWhenStopped = true
        //        self.tableView.tableFooterView = actInd
        
        
    }
    
    func refresh() {
        self.tableView.isUserInteractionEnabled = false
        //displayNoInternetConnection()
        Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(reloadDataWhenPulled), userInfo: nil, repeats: false)
        
    }
    
    func reloadDataWhenPulled() {
        
        if self.refreshControl.isRefreshing {
            
            // reloadTable()
            //  self.tableView.reloadData()
            fetchNotification {
                self.refreshControl.endRefreshing()
                self.tableView.isUserInteractionEnabled = true
            }
            // self.refreshControl.endRefreshing()
            
            
            
        }
        
    }
    
    func getTotalNotifications(completion: @escaping () -> ()) {
        var currentUserID = (Auth.auth().currentUser?.uid)!
        
        
        DataService.ds.REF_NOTIFICATION.child(currentUserID).observeSingleEvent(of: .value, with: { (snapshot) in
 
            if let snapshots = snapshot.children.allObjects as? [DataSnapshot] {
                self.totalNotifications = snapshots.count
 
            }
            
            completion()
        })
    }
    
    func fetchNotification(completion: @escaping () -> ()) {
        getTotalNotifications {
            var currentUserID = (Auth.auth().currentUser?.uid)!
            
            

            DataService.ds.REF_NOTIFICATION.child(currentUserID).queryLimited(toLast: UInt(self.currentNumberOfNotifications)).observeSingleEvent(of: .value, with: { (snapshot) in
                //observeSingleEvent(of: .value, with: { (snapshot) in
                self.notificationDict.removeAll()
                self.notifications = []
                
                if let snapshots = snapshot.children.allObjects as? [DataSnapshot] {
                    
                    if snapshots.count == 0 {
                        self.reloadAndSortTable()
                        print("empty")
                    } else {
                        for snap in snapshots {
                            //print("SNAP: \(snap)")
                            if let notificationDict = snap.value as? Dictionary<String, AnyObject> {
                                let key = snap.key
                                var notification = Notification(notificationKey: key, notificationData: notificationDict)
                                
                                self.notificationDict[key] = notification
                                print(notificationDict)
                               
                            }
                        }
                        self.reloadAndSortTable()
                    }
                    
                }
                
                completion()
            })
        }

    }
    
    func reloadAndSortTable() {
        
        if totalNotifications == 0 {
            self.showEmptyView()
        } else {
            self.hideEmptyView()
        }
        self.notifications = Array(self.notificationDict.values)
        
        self.notifications.sort(by: { (n1, n2) -> Bool in
            
            return (n1.time.intValue) > (n2.time.intValue)
        })
        
        print(notifications.count)
        self.tableView.reloadData()
        
        if firstTime {
            if animateTableView {
                UIView.animate(views: tableView.visibleCells, animations: animations, completion: {
                })
            }
        }
        
        firstTime = false
        animateTableView = true
    }
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return notifications.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 88.0
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var noti = notifications[indexPath.row]
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "NotificationCell") as? NotificationCell
        
        if noti.userName == "One Student" {
            if noti.fromGender == MALE {
                cell?.profilePic.image = #imageLiteral(resourceName: "ProfilePicMale")
            } else if noti.fromGender == FEMALE {
                cell?.profilePic.image = #imageLiteral(resourceName: "ProfilePicFemale")
            }
            
        } else {
            if noti.profilePicUrl != "NoPicture" {
                cell?.profilePic.loadImageUsingCacheWithUrlString(imageUrl: noti.profilePicUrl, tv: self.tableView, index: indexPath)
                cell?.initialLbl.isHidden = true
            } else {
                //default image
//                cell?.profilePic.image = #imageLiteral(resourceName: "ProfilePic")
                var iniName = String((noti.userName[(noti.userName.startIndex)])).capitalized
                cell?.initialLbl.text = iniName
                cell?.profilePic.backgroundColor = ColorHelper().pickColor(alphabet: Character(iniName))
                cell?.initialLbl.isHidden = false
                cell?.profilePic.image = nil
            }
        }


        cell?.configureCell(notification: noti)
        return cell!
    }
    
    var selectedConfessionId: String!
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        var updateObj = [String:AnyObject]()
        
        var noti = notifications[indexPath.row]
        selectedConfessionId = noti.confessionKey
        


            
        
        DataService.ds.REF_NOTIFICATION.child((Auth.auth().currentUser?.uid)!).child(noti.notificationKey).observeSingleEvent(of: .value, with: { (snapshot) in
            if snapshot.exists() {
                
                if noti.type == "commentLike" {
                    DataService.ds.REF_COMMENTS.child(noti.commentKey).observeSingleEvent(of: .value, with: { (commentSnapshot) in
                        if commentSnapshot.exists() {
                            DataService.ds.REF_CONFESSIONS.child(noti.confessionKey).observeSingleEvent(of: .value, with: { (confessionSnapshot) in
                                if confessionSnapshot.exists() {
                                    if !noti.seen {
                                        var ref = DataService.ds.REF_NOTIFICATION.child((Auth.auth().currentUser?.uid)!).child(noti.notificationKey)
                                        
                                        updateObj["/seen"] = true as AnyObject
                                        ref.updateChildValues(updateObj)
                                        
                                        noti.seen = true
                                        self.tableView.reloadData()
                                    }
                                    
                                    self.performSegue(withIdentifier: "CommentVC", sender: nil)
                                } else {
                                    ErrorAlert().createAlert(title: "Something went wrong", msg: "Could not perform action", object: self)
                                }
                            })
                        } else {
                            ErrorAlert().createAlert(title: "Something went wrong", msg: "Could not perform action", object: self)
                        }
                    })
                } else if noti.type == "follow" {
                    self.selectedUserID = noti.from.trimmingCharacters(in: .whitespaces)
                    if !noti.seen {
                        var ref = DataService.ds.REF_NOTIFICATION.child((Auth.auth().currentUser?.uid)!).child(noti.notificationKey)
                        
                        updateObj["/seen"] = true as AnyObject
                        ref.updateChildValues(updateObj)
                        
                        noti.seen = true
                        self.tableView.reloadData()
                    }
                    self.performSegue(withIdentifier: "OtherProfileVC", sender: nil)
                } else {
                    DataService.ds.REF_CONFESSIONS.child(noti.confessionKey).observeSingleEvent(of: .value, with: { (snapshot) in
                        if snapshot.exists() {
                            if !noti.seen {
                                var ref = DataService.ds.REF_NOTIFICATION.child((Auth.auth().currentUser?.uid)!).child(noti.notificationKey)
                                
                                updateObj["/seen"] = true as AnyObject
                                ref.updateChildValues(updateObj)
                                
                                noti.seen = true
                                self.tableView.reloadData()
                            }
                            
                            self.performSegue(withIdentifier: "Comment3VC", sender: nil)
                        } else {
                            ErrorAlert().createAlert(title: "Something went wrong", msg: "Could not perform action", object: self)
                        }
                    })
                }

            } else {
                ErrorAlert().createAlert(title: "Something went wrong", msg: "Could not perform action", object: self)
            }
        })
            

//        
//        if noti.type == "like" {
//            DataService.ds.REF_CONFESSIONS.child(noti.confessionKey).observeSingleEvent(of: .value, with: { (snapshot) in
//                if snapshot.exists() {
//                    if !noti.seen {
//                        var ref = DataService.ds.REF_NOTIFICATION.child((FIRAuth.auth()?.currentUser?.uid)!).child(noti.notificationKey)
//                        
//                        updateObj["/seen"] = true as AnyObject
//                        ref.updateChildValues(updateObj)
//                        
//                        noti.seen = true
//                        self.tableView.reloadData()
//                    }
//                    
//                    self.performSegue(withIdentifier: "CommentVC", sender: nil)
//                } else {
//                    ErrorAlert().createAlert(title: "Something went wrong", msg: "Could not perform action", object: self)
//                }
//            })
//        } else {
//            
//            DataService.ds.REF_NOTIFICATION.child((FIRAuth.auth()?.currentUser?.uid)!).child(noti.notificationKey).observeSingleEvent(of: .value, with: { (snapshot) in
//                if snapshot.exists() {
//                    if !noti.seen {
//                        var ref = DataService.ds.REF_NOTIFICATION.child((FIRAuth.auth()?.currentUser?.uid)!).child(noti.notificationKey)
//                        
//                        updateObj["/seen"] = true as AnyObject
//                        ref.updateChildValues(updateObj)
//                        
//                        noti.seen = true
//                        self.tableView.reloadData()
//                    }
//                    
//                    self.performSegue(withIdentifier: "CommentVC", sender: nil)
//                } else {
//                    ErrorAlert().createAlert(title: "Something went wrong", msg: "Could not perform action", object: self)
//                }
//                
//            })
//        }
        


        
     
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if indexPath.row == self.notifications.count - 1 {
            print("in the end")
            
            // self.tableView.isUserInteractionEnabled = false
            self.tableView.tableFooterView = actInd
            self.actInd.startAnimating()
            // self.tableView.reloadData()
            //displayNoInternetConnection()
            if totalNotifications > notifications.count {
                self.timer = Timer.scheduledTimer(timeInterval: 0.5, target: self, selector: #selector(self.addMoreNotifications), userInfo: nil, repeats: false)
                
                
            } else {
                self.tableView.isUserInteractionEnabled = true
                self.actInd.stopAnimating()
                self.tableView.tableFooterView = UIView()
            }
            

            
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        headerView = Bundle.main.loadNibNamed("NotificationView", owner: self, options: nil)?.first as! NotificationView
        
        return headerView
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 44
    }
    
    func addMoreNotifications() {
        animateTableView = false
        currentNumberOfNotifications = currentNumberOfNotifications + NUMBER_NOTIFICATION_ADD_EACH_TIME
        fetchNotification {
            if self.actInd.isAnimating {
                self.actInd.stopAnimating()
            }
            self.tableView.tableFooterView = UIView()
        }
    }
    

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let destinationVC = segue.destination as? Comment3VC {
            
            destinationVC.confessionId = self.selectedConfessionId
//            destinationVC.delegate = self

//            destinationVC.passReloadCommentConfessionDelegate = self
            destinationVC.uniName = self.user?.university
            destinationVC.user = self.user
            
        }
        
        if let destinationVC = segue.destination as? OtherProfileVC {
            
            destinationVC.userID = self.selectedUserID
            
        }
    }



}
