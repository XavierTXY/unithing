//
//  SearchVC.swift
//  UniThing
//
//  Created by Xavier TanXY on 1/7/17.
//  Copyright © 2017 Xavier TanXY. All rights reserved.
//

import UIKit
import Firebase
import FirebaseDatabase
import BRYXBanner
import Whisper
import AlgoliaSearch
import PageMenu

class SearchVC: UIViewController, UITableViewDelegate, UITableViewDataSource, UISearchResultsUpdating, UISearchBarDelegate, CAPSPageMenuDelegate {

    var pageMenu : CAPSPageMenu!
    @IBOutlet weak var tableView: UITableView!
//    @IBOutlet weak var collectionView: UICollectionView!
//    @IBOutlet weak var confessionCollectionView: UICollectionView!
//    @IBOutlet weak var topHashLbl: UILabel!
//    @IBOutlet weak var scrollView: UIScrollView!
    
    
    let SEARCH_CONFESSION = 0
    let SEARCH_HASH = 1
    var searchConfessionVC: SearchConfessionVC!
    var searchHashVC: SearchHashVC!
    
    var searchTypeMode: Int!
    
    var inSearchMode = false
    var searchController: UISearchController!
    
    
    var filteredHash = [String]()
    var hashArray = [String]()
    var hashDict = [String:String]()
    
    var confessionsToShow = [Confession]()
    
    var uniName: String?
    var emptyView: LoadingSearchVCView!
    
    var historyArray = [String]()
    var historyDict = [String:String]()
    var hashCountDict = Dictionary<String, UInt>()
    
    var topConfessionDict = [String:Confession]()
    var topConfessionArray = [Confession]()

    var filteredHashCountDict = [String: Hash]()
    var filteredHashCountArray = [Hash]()
    var colors = [UIColor]()
    
    var selectedConfessionID: String!
    var listener : DatabaseHandle!
    var refreshControl: UIRefreshControl!
    
    var client: Client!
    var index: Index!
    var query = Query()
    var searchId = 0
    var displayedSearchId = -1
    var loadedPage: UInt = 0
    var nbPages: UInt = 0
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        client = Client(appID: "PARE4ZML5L", apiKey: "e2171c98cc65ce47a1f6f75f19b06b3d")
        index = client.index(withName: "contacts")
        query.hitsPerPage = 6
        
        
        
        searchTypeMode = SEARCH_CONFESSION

        
        // Do any additional setup after loading the view.
        
        tableView.delegate = self
        tableView.dataSource = self
//        collectionView.delegate = self
//        collectionView.dataSource = self
//        confessionCollectionView.delegate = self
//        confessionCollectionView.dataSource = self
        
//        refreshControl = UIRefreshControl()
//        refreshControl.backgroundColor = UIColor.clear
//        refreshControl.tintColor = UIColor.lightGray
//        refreshControl.addTarget(self, action: "refresh", for: UIControlEvents.valueChanged)
//        tableView.addSubview(refreshControl)
        
        self.navigationItem.title = "Search"
        self.tableView.tableFooterView = UIView()
        
        
        configureSearchController()
        
  
        
        self.navigationController?.navigationBar.barTintColor =  UIColor.white
        self.navigationController?.navigationBar.tintColor = PURPLE
        
        
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title:"", style:.plain, target:nil, action: "")
        let backImage = #imageLiteral(resourceName: "Back Chevron")
        self.navigationController?.navigationBar.backIndicatorImage = backImage
        self.navigationController?.navigationBar.backIndicatorTransitionMaskImage = backImage

       
        
//        for _ in 0...4 {
//            colors.append(getRandomColor())
//        }
//        
//        getUniCCount()
//        fetchTopConfession()
        
       

        
        // Array to keep track of controllers in page menu
        var controllerArray : [UIViewController] = []
        
        // Create variables for all view controllers you want to put in the
        // page menu, initialize them, and add each to the controller array.
        // (Can be any UIViewController subclass)
        // Make sure the title property of all view controllers is set
        // Example:
        searchConfessionVC = SearchConfessionVC(nibName: "SearchConfessionVC", bundle: nil)
        searchConfessionVC.parentNavigationController = self.navigationController
        searchConfessionVC.searchVC = self
        searchConfessionVC.title = "Confessions"
        searchHashVC = SearchHashVC(nibName: "SearchHashVC", bundle: nil)
        searchHashVC.title = "Hashtags"
        searchHashVC.searchVC = self
        searchHashVC.parentNavigationController = self.navigationController
        controllerArray.append(searchConfessionVC)
        controllerArray.append(searchHashVC)
        
        // Customize page menu to your liking (optional) or use default settings by sending nil for 'options' in the init
        // Example:
        var parameters: [CAPSPageMenuOption] = [
            .menuItemSeparatorWidth(4.3),
            .scrollMenuBackgroundColor(UIColor.white),
            .menuItemSeparatorPercentageHeight(0.1),
            .viewBackgroundColor(UIColor(red: 247.0/255.0, green: 247.0/255.0, blue: 247.0/255.0, alpha: 1.0)),
            .bottomMenuHairlineColor(UIColor(red: 20.0/255.0, green: 20.0/255.0, blue: 20.0/255.0, alpha: 0.1)),
            .selectionIndicatorColor(PURPLE),
            .menuMargin(20.0),
            .menuHeight(40.0),
            .selectedMenuItemLabelColor(PURPLE),
            .unselectedMenuItemLabelColor(UIColor(red: 40.0/255.0, green: 40.0/255.0, blue: 40.0/255.0, alpha: 1.0)),
            .menuItemFont(UIFont(name: "HelveticaNeue-Medium", size: 14.0)!),
            .useMenuLikeSegmentedControl(true),
            .menuItemSeparatorRoundEdges(true),
            .selectionIndicatorHeight(2.0)
            
        ]
        
        
        let navheight = (searchController?.searchBar.frame.size.height ?? 0) + UIApplication.shared.statusBarFrame.size.height
        let frame = CGRect(x: 0, y: navheight, width: view.frame.width, height: view.frame.height-navheight)
//        pageMenu = CAPSPageMenu(viewControllers: subControllers, frame: frame, pageMenuOptions: menuParameters)
        
//        self.view.addSubview(pageMenu!.view)
        
        // Initialize page menu with controller array, frame, and optional parameters
//        pageMenu = CAPSPageMenu(viewControllers: controllerArray ,frame: CGRect(x:0.0, y:(self.searchController?.searchBar.frame.maxY)!+2, width:UIScreen.main.bounds.size.width, height:UIScreen.main.bounds.size.height-(self.navigationController?.navigationBar.frame.maxY)!-5), pageMenuOptions: parameters)
//        pageMenu = CAPSPageMenu(viewControllers: controllerArray ,frame: CGRect(x:0.0, y:(self.searchController?.searchBar.frame.maxY)!+20, width:UIScreen.main.bounds.size.width, height:UIScreen.main.bounds.size.height-(self.navigationController?.navigationBar.frame.maxY)!), pageMenuOptions: parameters)
        pageMenu = CAPSPageMenu(viewControllers: controllerArray ,frame: frame, pageMenuOptions: parameters)
        
         pageMenu.delegate = self
        
        // Lastly add page menu as subview of base view controller view
        // or use pageMenu controller in you view hierachy as desired
        
        
        emptyView = LoadingSearchVCView.instanceFromNib() as! LoadingSearchVCView
        
        emptyView.frame = self.tableView.bounds
        self.tableView.addSubview(emptyView)
        self.showEmptyView()
        
        self.view.addSubview(pageMenu!.view)
        
        
//
        
    }
    
    func didMoveToPage(_ controller: UIViewController, index: Int){
        
        if index == SEARCH_CONFESSION {
            searchTypeMode = SEARCH_CONFESSION
            self.searchConfessionVC.actIndicator.startAnimating()
        } else if index == SEARCH_HASH {
            searchTypeMode = SEARCH_HASH
             self.searchHashVC.actIndicator.startAnimating()
        }
        
        NSObject.cancelPreviousPerformRequests(withTarget: self, selector: #selector(self.updateResults(seachController:)), object: nil)
        self.perform(#selector(self.updateResults(seachController:)), with: nil, afterDelay: 0.5)
        print("did move to \(index)")
    }
    
    func showEmptyView() {
        emptyView.isHidden = false
        self.tableView.bringSubview(toFront: emptyView)
        pageMenu.view.isHidden = true
    }

    func hideEmptyView() {
        emptyView.isHidden = true
        self.tableView.sendSubview(toBack: emptyView)
        pageMenu.view.isHidden = false
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
//        self.tableView.isHidden = true
//        self.collectionView.isHidden = false
//        self.scrollView.isHidden = false
//        self.topHashLbl.isHidden = false
        self.slideMenuController()?.removeLeftGestures()
        UIApplication.shared.statusBarStyle = .default
        displayNoInternetConnection()

        
    }
    
    private func getUniCCount() {
        self.filteredHashCountDict.removeAll()
        self.filteredHashCountArray = []
//        self.collectionView.reloadData()
//        DataService.ds.REF_UniConfessionCount.observeSingleEvent(of: .value, with: { (snapshot) in
//
//
//            if let uniConfessionCountDict = snapshot.value as? Dictionary<String, Int> {
//
//                print(uniConfessionCountDict)
//                let greatestHue = uniConfessionCountDict.max { a, b in a.value < b.value }
//                print(greatestHue)
//            }
//
//
//
//
//        })
        
//        DataService.ds.REF_HASHTAG.observeSingleEvent(of: .value, with: { (snapshot) in
//
//
//            if let snapshots = snapshot.children.allObjects as? [DataSnapshot] {
////                self.totalConfession = snapshots.count
//
//                for s in snapshots {
////                    print(s.childrenCount)
////                    print(s.key)
//                    self.hashCountDict[s.key] = s.childrenCount
//
//                }
//
//                print("before \(self.hashCountDict.count)")
//
//                if self.hashCountDict.count != 0 {
//                    if !(self.hashCountDict.count > 5) {
//
//                        print("after \(self.hashCountDict.count)")
//                        let (key, count) = self.getNextTop()
//                        self.addToFilteredHashArray(key: key, count: count)
//
//                        if self.hashCountDict.count == 1 {
//                            print("even ehre")
//                            let (key2, count2) = self.getNextTop()
//                            self.addToFilteredHashArray(key: key2, count: count2)
//
//                        } else if self.hashCountDict.count == 2 {
//
//                            let (key3, count3) = self.getNextTop()
//                            let (key4, count4) = self.getNextTop()
//                            self.addToFilteredHashArray(key: key3, count: count3)
//                            self.addToFilteredHashArray(key: key4, count: count4)
//                        } else if self.hashCountDict.count == 3 {
//
//                            let (key3, count3) = self.getNextTop()
//                            let (key4, count4) = self.getNextTop()
//                            let (key5, count5) = self.getNextTop()
//                            self.addToFilteredHashArray(key: key3, count: count3)
//                            self.addToFilteredHashArray(key: key4, count: count4)
//                            self.addToFilteredHashArray(key: key5, count: count5)
//                        } else if self.hashCountDict.count == 4 {
//
//                            let (key3, count3) = self.getNextTop()
//                            let (key4, count4) = self.getNextTop()
//                            let (key5, count5) = self.getNextTop()
//                            let (key6, count6) = self.getNextTop()
//                            self.addToFilteredHashArray(key: key3, count: count3)
//                            self.addToFilteredHashArray(key: key4, count: count4)
//                            self.addToFilteredHashArray(key: key5, count: count5)
//                            self.addToFilteredHashArray(key: key6, count: count6)
//                        }
//
//                    } else {
//
//                        let (key, count) = self.getNextTop()
//                        let (key2, count2) = self.getNextTop()
//                        let (key3, count3) = self.getNextTop()
//                        let (key4, count4) = self.getNextTop()
//                        let (key5, count5) = self.getNextTop()
//                        self.addToFilteredHashArray(key: key, count: count)
//                        self.addToFilteredHashArray(key: key2, count: count2)
//                        self.addToFilteredHashArray(key: key3, count: count3)
//                        self.addToFilteredHashArray(key: key4, count: count4)
//                        self.addToFilteredHashArray(key: key5, count: count5)
//
//
//                    }
//
//                    self.filteredHashCountArray = Array(self.filteredHashCountDict.values)
//                    self.filteredHashCountArray.sort(by: { (c1, c2) -> Bool in
//
//                        return (c1.hashCount) > (c2.hashCount)
//                    })
//                }
//
//                self.collectionView.reloadData()
//            }
//
//
//
//        })


        
    }
    
    func addToFilteredHashArray(key: String, count: UInt) {
        
        var h = Hash(name: key, count: count)
        print("add to dict \(h.hashName)")
//        filteredHashCountArrayDict.append(h)
//        print("final \(h.hashName)")
//        print(filteredHashCountArrayDict)
        filteredHashCountDict[key] = h
    }
    
    func getNextTop() -> (String, UInt){
        
    
        var greatestHue = hashCountDict.max { a, b in a.value < b.value }
//        print(greatestHue)
        var count = hashCountDict[(greatestHue?.key)!]
        var key = (greatestHue?.key)
        hashCountDict.removeValue(forKey: (greatestHue?.key)!)
        
        print("get next top \(key)")
        return (key!, count!)
    }
    
    func displayNoInternetConnection() {
        if !DataService.ds.connectedToNetwork() {
            
            let murmur = Murmur(title: "No Internet Connection")
            
            
            // Show and hide a message after delay
            Whisper.show(whistle: murmur, action: .show(0.5))
            
            // Present a permanent status bar message
            Whisper.show(whistle: murmur, action: .present)
            
            // Hide a message
            Whisper.hide(whistleAfter: 3)
            
            
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
//        DataService.ds.REF_CONFESSIONS.removeObserver(withHandle: self.listener)
        
        
    }
    
    func refresh() {
//        self.tableView.isUserInteractionEnabled = false
        displayNoInternetConnection()
        DataService.ds.REF_CONFESSIONS.removeObserver(withHandle: self.listener)
        

        
        
        Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(reloadDataWhenPulled), userInfo: nil, repeats: false)
        
    }
    
    func reloadDataWhenPulled() {
        
        if self.refreshControl.isRefreshing {

            getUniCCount()
            fetchTopConfession()
            
            
        }
        
    }
    
    func fetchTopConfession() {
        self.topConfessionDict.removeAll()
        self.topConfessionArray = []
        self.attemptToReloadConfession()
        

        self.listener = DataService.ds.REF_CONFESSIONS.queryOrdered(byChild: "likes").queryLimited(toLast: 8).observe(.childAdded, with: { (snapshot) in
//            print("haha")
//            print(snapshot)
//            print(snapshot.key)
            print("CHILD ADDED")
            if snapshot != nil {
                let confessionDict = snapshot.value as? Dictionary<String, AnyObject>
                let key = snapshot.key
                //            print("KEY \(confessionDict)")
                
                let confession = Confession(confessionKey: key, postData: confessionDict!)
                self.topConfessionArray.append(confession)
                self.attemptToReloadConfession()
            }


        })
        
        
    }
    
    func attemptToReloadConfession() {
//        self.topConfessionArray = Array(self.topConfessionDict.values)
        //
//        self.topConfessionArray.sort(by: { (c1, c2) -> Bool in
//
//            return (c1.likes) < (c2.likes)
//        })
//
//        self.topConfessionArray.reverse()
//        self.confessionCollectionView.reloadData()
        
        if self.refreshControl.isRefreshing {
            self.refreshControl.endRefreshing()
        }
//        print("in array \(topConfessionArray)")
    }
    
    func fetchConfession(keyword: String) {
        
        self.searchConfessionVC.confessions = []
        self.searchConfessionVC.confessionDict.removeAll()
        
        query.query = keyword
        
        let curSearchId = searchId
        
        index.search(query, completionHandler: { (content, error) -> Void in
            
            if (curSearchId <= self.displayedSearchId) || (error != nil) {
                return // Newest query already displayed or error
            }
            
            self.displayedSearchId = curSearchId
            self.loadedPage = 0 // Reset loaded page
    
                
            guard let hits = content!["hits"] as? [[String: AnyObject]] else { return }
            guard let nbPages = content!["nbPages"] as? UInt else { return }
            self.nbPages = nbPages
            
            for hit in hits {
                print("hi \(hit["objectID"]) -  \(hit["caption"])")
                let key = hit["objectID"] as! String
                let confession = Confession(confessionKey: key, postData: hit)
                if confession.confessionKey != "DISCUSSION" {
                    self.searchConfessionVC.confessionDict[key] = confession
                }
                
                
            }
            
            self.searchConfessionVC.attemptToReloadTable()
            
        })
        self.searchId += 1
    }
    
    func loadMoreConfession() {
        
        if loadedPage + 1 >= nbPages {
            return // All pages already loaded
        }

        let nextQuery = Query(copy: query)

        nextQuery.page = loadedPage + 1

        index.search(nextQuery, completionHandler: { (content, error) -> Void in

            if (nextQuery.query != self.query.query) || (error != nil) {
                return // Query has changed
            }

            self.loadedPage = nextQuery.page!

            guard let hits = content!["hits"] as? [[String: AnyObject]] else { return }

            for hit in hits {
                print("hi \(hit["objectID"]) -  \(hit["caption"])")

                let key = hit["objectID"] as! String
                let confession = Confession(confessionKey: key, postData: hit)
                self.searchConfessionVC.confessionDict[key] = confession
            }

            

        })

        self.searchConfessionVC.attemptToReloadTable()

    }
    func fetchHash(hash: String) {
        
        self.searchHashVC.hashDict.removeAll()
        self.searchHashVC.hashArray = []
        DataService.ds.REF_HASHTAG.queryOrderedByKey().queryStarting(atValue: hash).queryEnding(atValue: hash+"\u{f8ff}").observeSingleEvent(of: .value, with: { (snapshot) in
            //.queryEqual(toValue: hash).observeSingleEvent(of: .value, with: { (snapshot) in


            if let snapshots = snapshot.children.allObjects as? [DataSnapshot] {
            
                let numHash = snapshots.count
                
                if snapshots.count != 0 {
                    for snap in snapshots {
                        print("SNAP: \(snap.key)")
                        
                        let h = Hash(name: snap.key, count: UInt(snap.childrenCount))
                        //                    self.hashDict[snap.key] = snap.key
                        self.searchHashVC.hashDict[snap.key] = h
                        self.searchHashVC.reloadTable()

                    }
                } else {
                     self.searchHashVC.reloadTable()
                }


            }

        })
    }
    
    
    func configureSearchController() {
        self.definesPresentationContext = true
        
        // Initialize and perform a minimum configuration to the search controller.
        searchController = UISearchController(searchResultsController: nil)
        searchController.searchResultsUpdater = self
        searchController.dimsBackgroundDuringPresentation = false
        searchController.hidesNavigationBarDuringPresentation = false
        searchController.searchBar.placeholder = "Search..."
        searchController.searchBar.delegate = self
        searchController.searchBar.tintColor = UIColor.darkGray
        searchController.searchBar.backgroundColor = UIColor.white
        searchController.searchBar.layer.borderWidth = 1
        searchController.searchBar.layer.borderColor = UIColor.white.cgColor
        
//        searchController.searchBar.setBackgroundImage(#imageLiteral(resourceName: "NavBarBG"), for: .any, barMetrics: .default)
//        self.navigationController?.navigationBar.setBackgroundImage(#imageLiteral(resourceName: "NavBarBG"), for: .default)
//        self.navigationController?.navigationBar.shadowImage = UIImage()
//        self.navigationController?.navigationBar.isTranslucent = true
//        self.navigationController?.view.backgroundColor = .clear
        
        
        for subView in searchController.searchBar.subviews {
            
            for subViewOne in subView.subviews {
                
                if let textField = subViewOne as? UITextField {
                    
                    subViewOne.backgroundColor = UIColor(red: 241/255, green: 242/255, blue: 243/255, alpha: 1)
                    
                    //use the code below if you want to change the color of placeholder
//                    let textFieldInsideUISearchBarLabel = textField.value(forKey: "placeholderLabel") as? UILabel
//                    textFieldInsideUISearchBarLabel?.textColor = UIColor.blue
                }
            }
        }
        
        searchController.searchBar.sizeToFit()
        
        
        
        // Place the search bar view to the tableview headerview.
        //tableView.tableHeaderView = searchController.searchBar
        self.navigationItem.titleView = searchController.searchBar
        
        
    }
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        print("1")
        
//        self.collectionView.isHidden = true
//        self.scrollView.isHidden = true
//        self.tableView.isHidden = false
//        self.topHashLbl.isHidden = true
        self.hideEmptyView()
        inSearchMode = true
        tableView.reloadData()
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        print("5")
        inSearchMode = true
//        searchController.searchBar.isLoading = true
        
        if searchTypeMode == SEARCH_CONFESSION {
            searchConfessionVC.hide()
        } else if searchTypeMode == SEARCH_HASH {
            searchHashVC.hide()
        }
        
        
        NSObject.cancelPreviousPerformRequests(withTarget: self, selector: #selector(self.updateResults(seachController:)), object: nil)
        self.perform(#selector(self.updateResults(seachController:)), with: nil, afterDelay: 0.5)
    }
    
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        print("2")
        self.showEmptyView()
        inSearchMode = false
        
//        self.collectionView.isHidden = false
//        self.scrollView.isHidden = false
//        self.tableView.isHidden = true
//        self.topHashLbl.isHidden = false
        
//        hashDict.removeAll()
//        hashArray.removeAll()
//        filteredHash.removeAll()
        
        moveToHistory(reloadTable: true)
//        tableView.reloadData()
    }
    
    
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        print("3")
        
        searchController.dismiss(animated: true, completion: nil)
//        let searchString = searchController.searchBar.text
//        if !inSearchMode {
//            inSearchMode = true
//            if searchString != "" {
//                fetchHash(hash: searchString!)
//                
//            }
//            // print(searchString)
//            
//            self.timer = Timer.scheduledTimer(timeInterval: 0.8, target: self, selector: #selector(lol), userInfo: nil, repeats: false)
////            tableView.reloadData()
//        }
        inSearchMode = false
        self.timer = Timer.scheduledTimer(timeInterval: 0.8, target: self, selector: #selector(filterHashArray), userInfo: nil, repeats: false)
       // searchController.searchBar.resignFirstResponder()
    }
    
    
    var timer: Timer?
    var searchString: String?
    var sController: UISearchController?
    
    
    func updateResults(seachController: UISearchController) {
        //        self.collectionView.isHidden = true
        //        self.tableView.isHidden = false
        //        self.topHashLbl.isHidden = true
        //
        searchString = searchController.searchBar.text
        searchString = searchString?.lowercased()
        sController = searchController
        
        if searchString != "" {
            
            if searchTypeMode == SEARCH_CONFESSION {
                fetchConfession(keyword: searchString!)
            } else if searchTypeMode == SEARCH_HASH {
                fetchHash(hash: searchString!)
            }
            
        } else {
            
            if searchTypeMode == SEARCH_CONFESSION {
                self.searchConfessionVC.confessions = []
                self.searchConfessionVC.confessionDict.removeAll()
                self.searchConfessionVC.tableView.reloadData()
                self.searchConfessionVC.actIndicator.stopAnimating()
            } else if searchTypeMode == SEARCH_HASH {
//                moveToHistory(reloadTable: false)
                self.searchHashVC.hashArray = []
                self.searchHashVC.hashDict.removeAll()
                self.searchHashVC.tableView.reloadData()
                self.searchHashVC.actIndicator.stopAnimating()
            }
            
            print("empty now")
            
        }
        // print(searchString)
        
        self.timer = Timer.scheduledTimer(timeInterval: 0.8, target: self, selector: #selector(filterHashArray), userInfo: nil, repeats: false)
    }
    
    func updateSearchResults(for searchController: UISearchController) {
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if !filteredHash.isEmpty {
            return "Searching"
        } else if !hashArray.isEmpty  {
            return "Search History"
        }
        
        return ""
        
    }
    
    func filterHashArray() {
    
        //Filter the data array and get only those countries that match the search text.
        filteredHash = hashArray.filter({ (hash) -> Bool in
            //let uniText: NSString = university.name as NSString
            
           // hashArray.removeAll()
            //hashDict.removeAll()
            return hash.lowercased().contains((searchString?.lowercased())!)
            
        })
        
        // Reload the tableview.
        sController?.searchBar.isLoading = false
        tableView.reloadData()
    }
    
    
    func attemptToReload() {
        
        self.hashArray = Array(self.hashDict.values)
       
        if hashArray.count == 0 {
            self.showEmptyView()
        } else {
            self.hideEmptyView()
        }
        
        tableView.reloadData()
    }
    
    func attemptToReloadWithoutOverride() {
        
        if hashArray.count == 0 {
            self.showEmptyView()
        } else {
            self.hideEmptyView()
        }
        
        tableView.reloadData()
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        var count = 0
        if inSearchMode && searchController.searchBar.text != "" {

            print("display filer")
            count = filteredHash.count
            
        } else {

   
            count = hashArray.count
        }
        
        return count
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
       
        var hash = String()
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "SearchCell") as? SearchCell
        
        if inSearchMode && searchController.searchBar.text != "" {

            print("display filer")
            hash = filteredHash[indexPath.row]
        } else {

     
            hash = hashArray[indexPath.row]
        }
        
        cell?.label.text = hash
        // print(cell?.label.text)
        return cell!
        
        
    }
    
//    func numberOfSections(in collectionView: UICollectionView) -> Int {
//        if collectionView == self.collectionView {
//            return 1
//        } else if collectionView == self.confessionCollectionView {
//            return 1
//        } else {
//            return 1
//        }
//
//    }
    
//    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
//
//        if collectionView == self.collectionView {
//            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "SearchCollectionCell", for: indexPath) as! SearchCollectionCell
//            cell.configureCell(hash: filteredHashCountArray[indexPath.row], color: colors[indexPath.row])
//            print(filteredHashCountArray[indexPath.row].hashName)
//            return cell
//        } else if collectionView == self.confessionCollectionView {
//
//            var count = topConfessionArray.count - 1
//            var newIdx = count - indexPath.row
//            let confession = topConfessionArray[newIdx]
//
//            if confession.imageUrl == "NoPicture" {
//                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "TopConfessionCollectionCell", for: indexPath) as! TopConfessionCollectionCell
//                cell.configureCell(confession: confession)
//                return cell
//            } else {
//                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "TopConfessionPicCollectionCell", for: indexPath) as! TopConfessionPicCollectionCell
//                cell.confessionPic.image = nil
//                cell.confessionPic.loadImageUsingCacheWithUrlStringCollection(imageUrl: confession.imageUrl!, cv: self.confessionCollectionView, index: indexPath)
//                return cell
//            }
//
//
//
//        } else {
//            return UICollectionViewCell()
//        }
////        TopConfessionCollectionCell
//
//    }
    
//    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
//
//        if collectionView == self.collectionView {
//            return filteredHashCountArray.count
//        } else if collectionView == self.confessionCollectionView {
////            print("HERE LALA \(topConfessionArray.count)")
//
//            return topConfessionArray.count
//        } else {
//            return 0
//        }
//
//    }
    
//
//    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
//
//        if collectionView == self.collectionView {
//            selectedHash = filteredHashCountArray[indexPath.row].hashName
//            saveHistory(selectedHash: selectedHash!)
//            sController?.isActive = false
//            performSegue(withIdentifier: "HashtagVC", sender: nil)
//
//        } else if collectionView == self.confessionCollectionView {
//            var count = topConfessionArray.count - 1
//            var newIdx = count - indexPath.row
//            let confession = topConfessionArray[newIdx]
//            self.selectedConfessionID = confession.confessionKey
//            confession.postRef.observeSingleEvent(of: .value, with: { (snapshot) in
//                if snapshot.exists() {
//                    self.performSegue(withIdentifier: "Comment3VC", sender: nil)
//                } else {
//                    ErrorAlert().createAlert(title: "Something went wrong", msg: "Could not perform action", object: self)
//                }
//            })
//        }
//
//
//    }
    
    var selectedHash: String?
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
       // inSearchMode = false

        
        if inSearchMode && searchController.searchBar.text != "" {
           // print(filteredHash[indexPath.row])

            selectedHash = filteredHash[indexPath.row]
//            historyArray.append(selectedHash!)
            
            saveHistory(selectedHash: selectedHash!)
            


            sController?.isActive = false
            performSegue(withIdentifier: "HashtagVC", sender: nil)
        
        } else {

            selectedHash = hashArray[indexPath.row]
           // historyArray.append(selectedHash!)
            saveHistory(selectedHash: selectedHash!)
            

            sController?.isActive = false
            performSegue(withIdentifier: "HashtagVC", sender: nil)
        }

        inSearchMode = false
        
            
        
    }
    
    func moveToHashtagVC(hash: Hash) {
        selectedHash = hash.hashName
        
        if self.searchController.searchBar.isFirstResponder {
            self.searchController.searchBar.resignFirstResponder()
        }
//        sController?.isActive = false
        inSearchMode = false
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
            self.performSegue(withIdentifier: "HashtagVC", sender: nil)
        }
        
        
    }
    
    func moveToConfessionVC(confession: Confession) {
        self.selectedConfessionID = confession.confessionKey
        
        if self.searchController.searchBar.isFirstResponder {
            self.searchController.searchBar.resignFirstResponder()
        }
//        sController?.isActive = false
        inSearchMode = false
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
            self.performSegue(withIdentifier: "Comment3VC", sender: nil)
        }
        
    }
    
    func getRandomColor() -> UIColor{
        //Generate between 0 to 1
        let red:CGFloat = CGFloat(drand48())
        let green:CGFloat = CGFloat(drand48())
        let blue:CGFloat = CGFloat(drand48())
        
        return UIColor(red:red, green: green, blue: blue, alpha: 1.0)
    }
    
    func moveToHistory(reloadTable: Bool) {
        hashDict.removeAll()
        hashArray.removeAll()
        filteredHash.removeAll()
        
        for h in historyDict {
            hashArray.append(h.value)
        }
        
        if reloadTable {
           attemptToReloadWithoutOverride()
        }

    }
    
    func saveHistory(selectedHash: String) {
        hashDict.removeAll()
        hashArray.removeAll()
        filteredHash.removeAll()
        
        historyDict[selectedHash] = selectedHash
//        historyArray.append(selectedHash)
       // hashArray.
    
        for h in historyDict {
            hashArray.append(h.value)
        }
        
        attemptToReloadWithoutOverride()
        
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if let destinationVC = segue.destination as? HashtagVC {
            destinationVC.hashtag = self.selectedHash
            destinationVC.uniName = self.uniName
//            destinationVC.currentConfession = self.currentConfession
//            destinationVC.delegate = self
//            
        }
        
        if let destinationVC = segue.destination as? Comment3VC {

            destinationVC.confessionId = self.selectedConfessionID
//            destinationVC.delegate = self
//            destinationVC.passReloadCommentConfessionDelegate = self
//            destinationVC.uniName = self.uniName
            destinationVC.user = DataService.ds.currentUser
            
        }
    }



}

