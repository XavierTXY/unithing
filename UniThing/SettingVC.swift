//
//  SettingVC.swift
//  UniThing
//
//  Created by Xavier TanXY on 1/7/17.
//  Copyright © 2017 Xavier TanXY. All rights reserved.
//

import UIKit
import SVProgressHUD
import Firebase
import PopupDialog

class SettingVC: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var tableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()

        tableView.delegate = self
        tableView.dataSource = self
        tableView.tableFooterView = UIView()
        
        self.navigationItem.title = "Settings"
        
        
        self.navigationController?.navigationBar.barTintColor = UIColor.white
        self.navigationController?.navigationBar.titleTextAttributes = [ NSFontAttributeName: UIFont(name: "HelveticaNeue-Bold", size: 16)!]
        self.navigationController?.navigationBar.layer.borderColor = BORDER_COLOR_CGCOLOR
        //elf.navigationController?.navigationBar.titleTextAttributes = [ NSForegroundColorAttributeName: PURPLE]

        
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return "Account"
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SettingCell") as? SettingCell
        cell?.selectionStyle = .none
        cell?.label.text = "Change Password"
        return cell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        presentAlert()
      
    }
    

    
    func presentAlert() {
        
    
        
        let user = Auth.auth().currentUser
      
        
    
        let alertController = UIAlertController(title: "Password", message: "Please enter your current password:", preferredStyle: .alert)
        let alertController2 = UIAlertController(title: "Password", message: "Please enter your new password:", preferredStyle: .alert)

        
        let confirmAction = UIAlertAction(title: "Confirm", style: .default) { (_) in
            if let field = alertController.textFields?[0] {
                let currentPassword = field.text
                let credential = EmailAuthProvider.credential(withEmail: (user?.email)!, password: currentPassword!)
                
                user?.reauthenticate(with: credential, completion: { (error) in
                    if error != nil{
                        ErrorAlert().createAlert(title: "Incorrect Password", msg: "Your current password is incorrect", object: self)
                    }else{
                        
                        let confirmChange = UIAlertAction(title: "Change", style: .default) { (_) in
                            if let field = alertController2.textFields?[0] {
                                let newPassword = field.text
                                
                                if (newPassword?.characters.count)! >= 6 {
                                    Auth.auth().currentUser?.updatePassword(to: newPassword!) { (error) in
                                        
                                        if error != nil {
                                            ErrorAlert().createAlert(title: "Error in changing", msg: "Could not perfrom action", object: self)
                                        } else {
                                            print("done changing")
                                            SVProgressHUD.showSuccess(withStatus: "Done!")
                                            
                                        }
                                    }
                                } else {
                                    ErrorAlert().createAlert(title: "Invalid Format", msg: "Password must be at least 6 characters", object: self)
                                }


                            }
                        }
                    
                        alertController2.addAction(confirmChange)
                        self.present(alertController2, animated: true, completion: nil)
                    }
                })

                
            } else {
                // user did not fill field
            }
        }
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { (_) in }
        
        alertController.addTextField { (textField) in
            textField.placeholder = "Current password"
            textField.isSecureTextEntry = true
        }
        
        alertController2.addTextField { (textField) in
            textField.placeholder = "New password"
            textField.isSecureTextEntry = true
        }
        
    
        
        alertController.addAction(confirmAction)
        alertController.addAction(cancelAction)
        alertController2.addAction(cancelAction)
        
        self.present(alertController, animated: true, completion: nil)
    }


}
