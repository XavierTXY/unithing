//
//  Hash.swift
//  UniThing
//
//  Created by XavierTanXY on 1/4/18.
//  Copyright © 2018 Xavier TanXY. All rights reserved.
//

import Foundation

class Hash {
    
    private var _hashName: String!
    private var _hashCount: UInt!

    
    var hashName: String {
        return _hashName
    }
    
    var hashCount: UInt {
        return _hashCount
    }
    

    
    init(name: String, count: UInt) {
        self._hashName = name
        self._hashCount = count


    }
}
