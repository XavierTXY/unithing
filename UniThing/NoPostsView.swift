//
//  NoPostsView.swift
//  UniThing
//
//  Created by XavierTanXY on 12/3/18.
//  Copyright © 2018 Xavier TanXY. All rights reserved.
//

import Foundation
import UIKit

class NoPostsView: UIView {
    
    
    class func instanceFromNib() -> UIView {
        return UINib(nibName: "NoPostsView", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! UIView
    }

}


