//
//  Interaction.swift
//  UniThing
//
//  Created by Xavier TanXY on 30/11/16.
//  Copyright © 2016 Xavier TanXY. All rights reserved.
//

import Foundation
import UIKit
import SVProgressHUD

class Interaction {
    
    
    func enableInteraction() {
        
        
        UIApplication.shared.endIgnoringInteractionEvents()
        SVProgressHUD.dismiss()
    }
    
    /*
    func enable2(image: UIImageView) {
        image.isHidden = true
        UIApplication.shared.endIgnoringInteractionEvents()
        SVProgressHUD.dismiss()
    }*/
    
    /*
    func disable2(msg: String, image: UIImageView) {
        image.isHidden = false
        UIApplication.shared.beginIgnoringInteractionEvents()
        SVProgressHUD.show(withStatus: msg)
    }*/
    
    func disableInteraction(msg: String) {
        
        UIApplication.shared.beginIgnoringInteractionEvents()
        SVProgressHUD.show(withStatus: msg)
    }
    
    func showSuccess() {
        SVProgressHUD.showSuccess(withStatus: "Done")
        UIApplication.shared.endIgnoringInteractionEvents()
        SVProgressHUD.dismiss(withDelay: 1)
    }
}
