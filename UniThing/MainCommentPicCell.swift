//
//  MainCommentPicCell.swift
//  UniThing
//
//  Created by Xavier TanXY on 11/5/17.
//  Copyright © 2017 Xavier TanXY. All rights reserved.
//

import UIKit
import Firebase
import FirebaseDatabase
import ActiveLabel

class MainCommentPicCell: UITableViewCell, UIScrollViewDelegate {


    @IBOutlet weak var dateLabel: UILabel!
   
    @IBOutlet weak var confessionPic: UIImageView!
    @IBOutlet weak var captionLabel: ActiveLabel!
    
    @IBOutlet weak var facultyLbl: UILabel!
    
    @IBOutlet weak var mainView: UIView!
    
//    @IBOutlet weak var likeImage: UIImageView!
    @IBOutlet weak var likeBtn: UIButton!
    @IBOutlet weak var likeLabel: UILabel!
    @IBOutlet weak var commentImage: UIImageView!
    @IBOutlet weak var commentLabel: UILabel!
    @IBOutlet weak var profilePic: UIImageView!
    @IBOutlet weak var anonymousLabel: UILabel!
    
    @IBOutlet weak var categoryLbl: UILabel!
    
    @IBOutlet weak var likeStack: UIStackView!
    var commentVC: Comment3VC?
 
    var confession: Confession!
    var likesRef: DatabaseReference!
    var newImageView: UIImageView!
    var scrollView: UIScrollView!
    var profileTap: UITapGestureRecognizer!
    var profileTap2: UITapGestureRecognizer!
    var initialLbl: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
//        let likeTap = UITapGestureRecognizer(target: self, action: #selector(likeTapped))
//        likeTap.numberOfTapsRequired = 1
//        likeImage.addGestureRecognizer(likeTap)
//        likeImage.isUserInteractionEnabled = true
        
        let commentTap = UITapGestureRecognizer(target: self, action: #selector(commentTapped))
        commentTap.numberOfTapsRequired = 1
        commentImage.addGestureRecognizer(commentTap)
        commentImage.isUserInteractionEnabled = true
        

        let likerTap = UITapGestureRecognizer(target: self, action: #selector(likersTapped))
        likerTap.numberOfTapsRequired = 1
        likeStack.addGestureRecognizer(likerTap)
        likeStack.isUserInteractionEnabled = true

        
        profileTap = UITapGestureRecognizer(target: self, action: #selector(profileTapped))
        profileTap.numberOfTapsRequired = 1
        profilePic.addGestureRecognizer(profileTap)
        
        profileTap2 = UITapGestureRecognizer(target: self, action: #selector(profileTapped))
        profileTap2.numberOfTapsRequired = 1
        anonymousLabel.addGestureRecognizer(profileTap2)
        
        scrollView = UIScrollView()
        scrollView.minimumZoomScale = 1.0
        scrollView.maximumZoomScale = 6.0
        scrollView.delegate = self;
        
        
        // UITapGestureRecognizer(target: self, action: "tapHandler:")
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(tapHandler))
        
        self.confessionPic.addGestureRecognizer(tapGestureRecognizer)
        //tapGestureRecognizer.cancelsTouchesInView = false
        self.confessionPic.isUserInteractionEnabled = true
        
        
        confessionPic.translatesAutoresizingMaskIntoConstraints = false
        profilePic.layer.cornerRadius = profilePic.frame.height / 2
        confessionPic.layer.masksToBounds = true
        
        initialLbl = UILabel()
        initialLbl.frame.size = CGSize(width: 50, height: 50)
        initialLbl.font = UIFont(name: "HelveticaNeue-Medium", size: 20.0)
        initialLbl.textColor = UIColor.white
        initialLbl.textAlignment = NSTextAlignment.center
        initialLbl.center = profilePic.center
        profilePic.addSubview(initialLbl)
        
        
    }
    
    func tapHandler(sender: UITapGestureRecognizer) {
        
        print("TAPPED")
        
        if let imageView = sender.view as? UIImageView {
            self.commentVC?.performZoomInForStartingImageView(startingImageView: imageView)
        }
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func configureCell(confession: Confession) {
        
        anonymousLabel.isUserInteractionEnabled = true
        profilePic.isUserInteractionEnabled = true
        initialLbl.isHidden = true
        
        self.confession = confession
        likesRef = DataService.ds.REF_USERS.child((Auth.auth().currentUser?.uid)!).child("likes").child(confession.confessionKey)
        
        
        captionLabel.enabledTypes = [.hashtag, .url]
        captionLabel.customize { label in
            label.hashtagColor = UIColor(red: 85.0/255, green: 172.0/255, blue: 238.0/255, alpha: 1)
            
        }
        captionLabel.text = confession.caption
        // facultyLabel.text = confession.faculty
        
        if let c = confession.category {
            categoryLbl.text = c.uppercased()
        } else {
            categoryLbl.text = "UNDEFINED"
        }
        
        if (confession.anonymous) != nil {
            
            if confession.anonymous! {
                anonymousLabel.isUserInteractionEnabled = false
                profilePic.isUserInteractionEnabled = false
                
                if confession.authorGender == MALE {
                    profilePic.image = UIImage(named: "male")
                } else if confession.authorGender == FEMALE {
                    profilePic.image = UIImage(named: "female")
                }
                
                self.anonymousLabel.text = "Student"
                
            } else {
                
                if confession.authorProfilePicUrl == NO_PIC {
//                    profilePic.image = #imageLiteral(resourceName: "ProfilePic")
                    var iniName = String((confession.authorUserName![(confession.authorUserName?.startIndex)!])).capitalized
                    initialLbl.text = iniName
                    profilePic.backgroundColor = ColorHelper().pickColor(alphabet: Character(iniName))
                    initialLbl.isHidden = false
                    profilePic.image = nil
                } else {
                    profilePic.loadImageUsingCacheWithUrlStringWithoutIndex(imageUrl: confession.authorProfilePicUrl!)
                }
                
                self.anonymousLabel.text = confession.authorUserName!
            }
            
        } else {
            if confession.authorGender == MALE {
                profilePic.image = UIImage(named: "male")
            } else if confession.authorGender == FEMALE {
                profilePic.image = UIImage(named: "female")
            }
            
            self.anonymousLabel.text = "Student"
            
            profilePic.isUserInteractionEnabled = false
            anonymousLabel.isUserInteractionEnabled = false
        }
        
//        if confession.author == Auth.auth().currentUser?.uid {
//            self.anonymousLabel.text = "Student"
//        }
        
        let seconds = confession.time.doubleValue
        let timesStampDate = NSDate(timeIntervalSince1970: seconds)
        var timeAgo:String =  DateHelper().timeAgoSinceDate(timesStampDate as Date, currentDate: NSDate() as Date, numericDates: true)
        
        var dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "HH:mm"
        // timeLabel.text = dateFormatter.string(from: timesStampDate as Date)
        
        dateFormatter.dateFormat = "dd, MMM"
        // dateLabel.text = dateFormatter.string(from: timesStampDate as Date)
        
//        if confession.authorUni != confession.university {
            var shortUni = DataService.ds.convertUniNameToShort(uniName: confession.authorUni)
//            dateLabel.text = "\(shortUni) - \(confession.faculty)・\(timeAgo)"
//        } else {
            dateLabel.text = "\(timeAgo)"
        facultyLbl.text = "\(shortUni) - \(confession.faculty)"
//        }
        
        
        self.likeLabel.text = "\(self.confession.likes)"
        commentLabel.text = "\(confession.comments)"
        
        
        let exist = confession.usersLike[(Auth.auth().currentUser?.uid)!]
        
        if exist != nil{
//            self.likeImage.image = UIImage(named: "NewLoveFilled")
            self.likeBtn.setImage(#imageLiteral(resourceName: "NewLoveFilled"), for: .normal)
        } else {
//            self.likeImage.image = UIImage(named: "NewLove")
            self.likeBtn.setImage(#imageLiteral(resourceName: "NewLove"), for: .normal)
        }
        
        
        
    }
    
    func enableTouch() {
        self.likeBtn.isUserInteractionEnabled = true
        
    }
    
    var timer: Timer?

    @IBAction func likeBtnTapped(_ sender: Any) {
        self.likeBtn.pulsate()
        self.likeBtn.isUserInteractionEnabled = false
        self.timer = Timer.scheduledTimer(timeInterval: 2.0, target: self, selector: #selector(enableTouch), userInfo: nil, repeats: false)
        self.confession.postRef.observeSingleEvent(of: .value, with: { (snapshot) in
            if snapshot.exists() {
                
                
                
                let exist = self.confession.usersLike[(Auth.auth().currentUser?.uid)!]
                
                if exist == nil {
                    //                    self.likeImage.image = UIImage(named: "NewLoveFilled")
                    self.likeBtn.setImage(#imageLiteral(resourceName: "NewLoveFilled"), for: .normal)
                    self.likeLabel.text = "\(self.confession.likes + 1)"
                    
                    self.confession.adjustLikes(addLike: true, uid: (Auth.auth().currentUser?.uid)! )
                    self.confession.likes = self.confession.likes + 1
                    self.commentVC?.reloadLike(confession: self.confession, reloadLike: true)
                } else {
                    
                    //                    self.likeImage.image = UIImage(named: "NewLove")
                    self.likeBtn.setImage(#imageLiteral(resourceName: "NewLove"), for: .normal)
                    if self.confession.likes != 0 {
                        self.likeLabel.text = "\(self.confession.likes - 1)"
                    }
                    
                    self.confession.adjustLikes(addLike: false, uid: (Auth.auth().currentUser?.uid)! )
                    
                    self.commentVC?.reloadLike(confession: self.confession, reloadLike: false)
                }
                
                
            } else {
                ErrorAlert().createAlert(title: "Something went wrong", msg: "Could not perform action", object: self.parentViewController!)
            }
        })
    }
    
    func likeTapped(_ sender: Any) {

    }
    
    func likersTapped(_ sender: UITapGestureRecognizer) {
        if self.confession.likes != 0 {
            commentVC?.likersTapped(isComment: false, commentID: "")
        }
        
    }
    
    func commentTapped(_ sender: UITapGestureRecognizer) {
        print("Tapp comment")
//        self.commentVC?.addComment(confessionKey: self.confession.confessionKey, confession: self.confession)
        commentVC?.showKeyboard()
    }
    
    func profileTapped(_ sender: UITapGestureRecognizer) {
        
        self.commentVC?.showProfile(authorID: self.confession.author)
        
    }
    
    

}
