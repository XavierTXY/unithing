//
//  GradientView.swift
//  UniThing
//
//  Created by XavierTanXY on 21/5/18.
//  Copyright © 2018 Xavier TanXY. All rights reserved.
//

import Foundation
import UIKit

class GradientView: UIView {
    var FirstColor: UIColor = UIColor.clear {
        didSet {
            updateView()
        }
    }
    
    var SecondColor: UIColor = UIColor.clear {
        didSet {
            updateView()
        }
    }
    
    override class var layerClass: AnyClass {
        get {
            return CAGradientLayer.self
        }
    }
    
    func updateView() {
        let layer = self.layer as! CAGradientLayer
        layer.colors = [FirstColor.cgColor, SecondColor.cgColor]
    }
}
