//
//  ConfessionCell.swift
//  UniThing
//
//  Created by Xavier TanXY on 6/12/16.
//  Copyright © 2016 Xavier TanXY. All rights reserved.
//

import UIKit
import Firebase
import FirebaseDatabase
import SVProgressHUD
import ActiveLabel

class ConfessionCell: UITableViewCell {

    @IBOutlet weak var facultyLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var categoryLbl: UILabel!
    
    @IBOutlet weak var label: ActiveLabel!
    
    @IBOutlet weak var nameLbl: UILabel!
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var profilePic: CircleView!
 

    @IBOutlet weak var likeLabel: UILabel!
//    @IBOutlet weak var likeImage: UIImageView!
    @IBOutlet weak var likeBtn: UIButton!
    @IBOutlet weak var commentLabel: UILabel!
    @IBOutlet weak var commentImage: UIImageView!
    
    
    var confession: Confession!
    var likesRef: DatabaseReference!
    var confessionVC: ConfessionVC?
    var hashtagVC: HashtagVC?
    var selfConfessionVC: SelfConfessionVC?
    var otherProfileVC: OtherProfileVC?
    var totalComments: Int?
    var initialLbl: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
       

        
//        let likeTap = UITapGestureRecognizer(target: self, action: #selector(likeTapped))
//        likeTap.numberOfTapsRequired = 1
//        likeImage.addGestureRecognizer(likeTap)
//        likeImage.isUserInteractionEnabled = true
        
        let commentTap = UITapGestureRecognizer(target: self, action: #selector(commentTapped))
        commentTap.numberOfTapsRequired = 1
        commentImage.addGestureRecognizer(commentTap)
        commentImage.isUserInteractionEnabled = true
        
        label.enabledTypes = [.hashtag, .url]
        label.handleHashtagTap { hashtag in
            print("Success. You just tapped the \(hashtag) hashtag")
            self.confessionVC?.hashTap( hashtag: hashtag, confession: self.confession )
            self.hashtagVC?.hashTap( hashtag: hashtag )
        }
        
        label.customize { label in
            label.hashtagColor = UIColor(red: 85.0/255, green: 172.0/255, blue: 238.0/255, alpha: 1)
            
        }
        
        profilePic.layer.cornerRadius = profilePic.frame.height / 2
        
        initialLbl = UILabel()
        initialLbl.frame.size = CGSize(width: 50, height: 50)
        initialLbl.font = UIFont(name: "HelveticaNeue-Medium", size: 20.0)
        initialLbl.textColor = UIColor.white
        initialLbl.textAlignment = NSTextAlignment.center
        initialLbl.center = profilePic.center
        profilePic.addSubview(initialLbl)

        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
   
    
    func configureCell(confession: Confession) {
        
        self.confession = confession
        initialLbl.isHidden = true
        likesRef = DataService.ds.REF_USERS.child((Auth.auth().currentUser?.uid)!).child("likes").child(confession.confessionKey)
        



        label.text = confession.caption
        
        
   
        
//        let seconds = confession.time.doubleValue
//        let timesStampDate = NSDate(timeIntervalSince1970: seconds)
//        var timeAgo:String =  DateHelper().timeAgoSinceDate(timesStampDate as Date, currentDate: NSDate() as Date, numericDates: true)
//       
//        var dateFormatter = DateFormatter()
//        //dateFormatter.dateFormat = "HH:mm"
//
//        
//        dateFormatter.dateFormat = "dd, MMM"
//        dateLabel.text = "\(confession.faculty)・\(timeAgo)"
       
        likeLabel.text = "\(confession.likes)"
        commentLabel.text = "\(confession.comments)"
        
        if let c = confession.category {
            categoryLbl.text = c.uppercased()
        } else {
            categoryLbl.text = "UNDEFINED"
        }
        
        
        if (confession.anonymous) != nil {
        
            if confession.anonymous! {
                
                if confession.authorGender == MALE {
                    profilePic.image = UIImage(named: "male")
                } else if confession.authorGender == FEMALE {
                    profilePic.image = UIImage(named: "female")
                }
                
                nameLbl.text = "Student"
                
            } else {
                
                if confession.authorProfilePicUrl == NO_PIC {

                    var iniName = String((confession.authorUserName![(confession.authorUserName?.startIndex)!])).capitalized
                    initialLbl.text = iniName
                    profilePic.backgroundColor = ColorHelper().pickColor(alphabet: Character(iniName))
                    initialLbl.isHidden = false
                    profilePic.image = nil
                } else {
                    profilePic.loadImageUsingCacheWithUrlStringWithoutIndex(imageUrl: confession.authorProfilePicUrl!)
                }
                
                nameLbl.text = confession.authorUserName!
            }
            
        } else {
            if confession.authorGender == MALE {
                profilePic.image = UIImage(named: "male")
            } else if confession.authorGender == FEMALE {
                profilePic.image = UIImage(named: "female")
            }
            
            nameLbl.text = "Student"
        }
        

        
        let exist = confession.usersLike[(Auth.auth().currentUser?.uid)!]
        
        if exist != nil{
//            self.likeImage.image = UIImage(named: "NewLoveFilled")
            likeBtn.setImage(#imageLiteral(resourceName: "NewLoveFilled"), for: .normal)
        } else {
//            self.likeImage.image = UIImage(named: "NewLove")
            likeBtn.setImage(#imageLiteral(resourceName: "NewLove"), for: .normal)
        }
//        likesRef.observeSingleEvent(of: .value, with: { (snapshot) in
//            
//            /* if the user havent like the photo, the like is empty*/
//            if let _ = snapshot.value as? NSNull {
//                self.likeImage.image = UIImage(named: "empty love")
//            } else {
//                self.likeImage.image = UIImage(named: "love")
//            }
//            
//        })
    }

    @IBAction func optionsTapped(_ sender: Any) {
        
        let alert = UIAlertController()
        
        alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel, handler:{ (UIAlertAction)in
            print("User click Dismiss button")
            
        }))
        
        if confession.author == (Auth.auth().currentUser!.uid) {
            alert.addAction(UIAlertAction(title: "Delete", style: .destructive , handler:{ (UIAlertAction)in
                self.displayAlert()
            }))
            
        }
        
        parentViewController?.present(alert, animated: true, completion: nil)
    }
    
    func displayAlert() {
        let alertController = UIAlertController(title: "Are you sure?", message: "", preferredStyle: .alert)
        
        alertController.addAction(UIAlertAction(title: "Cancel", style: .cancel) { (action:UIAlertAction!) in
            // println("you have pressed the Cancel button");
        })
        
        alertController.addAction(UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction!) in
           
            self.confessionVC?.deleteConfession(confessionKey: self.confession.confessionKey)

        })
        
        parentViewController?.present(alertController, animated: true, completion: nil)
        
    }
    
    func enableTouch() {
//        self.likeImage.isUserInteractionEnabled = true
        self.likeBtn.isUserInteractionEnabled = true
    }
    var timer: Timer?
    
    @IBAction func likeBtnTapped(_ sender: Any) {
        self.likeBtn.pulsate()
        
        //        sender.isEnabled = false
        //        self.likeImage.isUserInteractionEnabled = false
        self.likeBtn.isUserInteractionEnabled = false
        self.timer = Timer.scheduledTimer(timeInterval: 2.0, target: self, selector: #selector(enableTouch), userInfo: nil, repeats: false)
        
        
        self.confession.postRef.observeSingleEvent(of: .value, with: { (snapshot) in
            if snapshot.exists() {
                
                
                
                let exist = self.confession.usersLike[(Auth.auth().currentUser?.uid)!]
                
                if exist == nil {
                    //                        self.confession.addUser(uid: (FIRAuth.auth()?.currentUser?.uid)!)
                    //                        self.likeImage.image = UIImage(named: "NewLoveFilled")
                    self.likeBtn.setImage(#imageLiteral(resourceName: "NewLoveFilled"), for: .normal)
                    self.likeLabel.text = "\(self.confession.likes + 1)"
                    
                    
                    //self.confession.likes = self.confession.likes + 1
                    
                    self.confessionVC?.reloadConfesionInSearchVC(confession: self.confession, reloadLike: true)
                    self.hashtagVC?.reloadLike(confession: self.confession, reloadLike: true)
                    self.selfConfessionVC?.reloadLike(confession: self.confession, reloadLike: true)
                    self.otherProfileVC?.reloadLike(confession: self.confession, reloadLike: true)
                    self.confession.adjustLikes(addLike: true, uid: (Auth.auth().currentUser?.uid)! )
                    
                    
                } else {
                    //                        self.confession.removeUser(uid: (FIRAuth.auth()?.currentUser?.uid)!)
                    
                    //                        self.likeImage.image = UIImage(named: "NewLove")
                    self.likeBtn.setImage(#imageLiteral(resourceName: "NewLove"), for: .normal)
                    if self.confession.likes != 0 {
                        self.likeLabel.text = "\(self.confession.likes - 1)"
                    }
                    
                    
                    self.confessionVC?.reloadConfesionInSearchVC(confession: self.confession, reloadLike: false)
                    self.hashtagVC?.reloadLike(confession: self.confession, reloadLike: false)
                    self.selfConfessionVC?.reloadLike(confession: self.confession, reloadLike: false)
                    self.otherProfileVC?.reloadLike(confession: self.confession, reloadLike: false)
                    self.confession.adjustLikes(addLike: false, uid: (Auth.auth().currentUser?.uid)! )
                    
                }
                
                
                
                
            } else {
                ErrorAlert().createAlert(title: "Something went wrong", msg: "Could not perform action", object: self.parentViewController!)
                
            }
        })

    }
    
    func likeTapped(_ sender: UITapGestureRecognizer) {
        
 
        
 
    }

    func commentTapped(_ sender: UITapGestureRecognizer) {
        
        self.confession.postRef.observeSingleEvent(of: .value, with: { (snapshot) in
            if snapshot.exists() {
                self.confessionVC?.addComment(confessionKey: self.confession.confessionKey, confession: self.confession)
            } else {
                ErrorAlert().createAlert(title: "Something went wrong", msg: "Could not perform action", object: self.parentViewController!)
            }
        })
        
    }
    

    
    
}
