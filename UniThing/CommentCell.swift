//
//  CommentCell.swift
//  UniThing
//
//  Created by Xavier TanXY on 18/12/16.
//  Copyright © 2016 Xavier TanXY. All rights reserved.
//

import UIKit
import Firebase
import FirebaseDatabase
import ActiveLabel

class CommentCell: UITableViewCell {


    @IBOutlet weak var commentLabel: ActiveLabel!


    @IBOutlet weak var profilePic: CircleImageView!
    @IBOutlet weak var facultyLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var loveStack: UIStackView!

    @IBOutlet weak var likeActionLabel: UILabel!
    @IBOutlet weak var likeLabel: UILabel!
    @IBOutlet weak var anonymousLabel: UILabel!
    @IBOutlet weak var genderPic: UIImageView!
    @IBOutlet weak var levelLbl: UILabel!
    @IBOutlet weak var bottomStack: UIStackView!
    @IBOutlet weak var likeBtnStack: UIStackView!
    
    var commentVC: Comment3VC?
    var likesRef: DatabaseReference!
    var comment: Comment!
    var likeTap: UITapGestureRecognizer!
    var profileTap: UITapGestureRecognizer!
    var profileTap2: UITapGestureRecognizer!
    var optionTap: UITapGestureRecognizer!
    var state: String?
    
    var initialLbl: UILabel!

    
  
    override func awakeFromNib() {
        super.awakeFromNib()
        likeTap = UITapGestureRecognizer(target: self, action: #selector(likeTapped))
        likeTap?.numberOfTapsRequired = 1
        likeActionLabel.addGestureRecognizer(likeTap!)
        likeActionLabel.isUserInteractionEnabled = true
        
        
        profileTap = UITapGestureRecognizer(target: self, action: #selector(profileTapped))
        profileTap.numberOfTapsRequired = 1
        profilePic.addGestureRecognizer(profileTap)
        profileTap2 = UITapGestureRecognizer(target: self, action: #selector(profileTapped))
        profileTap2.numberOfTapsRequired = 1
        anonymousLabel.addGestureRecognizer(profileTap2)
        
        let likerTap = UITapGestureRecognizer(target: self, action: #selector(likersTapped))
        likerTap.numberOfTapsRequired = 1
        loveStack.addGestureRecognizer(likerTap)
        loveStack.isUserInteractionEnabled = true

        
        commentLabel.enabledTypes = [.hashtag, .mention]
        
        commentLabel.customize { commentLabel in
            commentLabel.hashtagColor = UIColor(red: 85.0/255, green: 172.0/255, blue: 238.0/255, alpha: 1)
            commentLabel.mentionColor = UIColor(red: 85.0/255, green: 172.0/255, blue: 238.0/255, alpha: 1)
            
        }
        
        initialLbl = UILabel()
        initialLbl.frame.size = CGSize(width: 50, height: 50)
        initialLbl.font = UIFont(name: "HelveticaNeue-Medium", size: 20.0)
        initialLbl.textColor = UIColor.white
        initialLbl.textAlignment = NSTextAlignment.center
        initialLbl.center = profilePic.center
        profilePic.addSubview(initialLbl)
        
        initialLbl.isHidden = true
        
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func configureCell(comment: Comment, confession: Confession, levelID: Int) {
        
        initialLbl.isEnabled = true
        profilePic.isUserInteractionEnabled = true
        anonymousLabel.isUserInteractionEnabled = true
        
        self.comment = comment

        likesRef = DataService.ds.REF_USERS.child((Auth.auth().currentUser?.uid)!).child("likes").child(comment.commentKey)
        
        //self.anonymousLabel.text = "Anonymous (\(comment.anonymousId))"
        
//        if comment.author == FIRAuth.auth()?.currentUser?.uid {
//         self.anonymousLabel.text = comment.userName
//        } else if comment.author == confession.author {
//            self.anonymousLabel.text = "Student"
//            
//        } else {
//            //self.anonymousLabel.text = "Student (\(comment.anonymousId))"
//        }
   

        
        if comment.author == confession.author {
            
            if (confession.anonymous) != nil {
                
                if confession.anonymous! {
                    self.anonymousLabel.text = "Student"
                    profilePic.isUserInteractionEnabled = false
                    anonymousLabel.isUserInteractionEnabled = false
                } else {
                    
                    self.anonymousLabel.text = comment.userName
                    profilePic.isUserInteractionEnabled = true
                    anonymousLabel.isUserInteractionEnabled = true
                    
                    if comment.profilePicUrl != "NoPicture" {
                        profilePic.loadImageUsingCacheWithUrlStringWithoutIndex(imageUrl: comment.profilePicUrl)
                    } else {
                        //default picture
                        //                                cell.profilePic.image = UIImage(named: "ProfilePic")
                        var iniName = String((comment.userName[(comment.userName.startIndex)])).capitalized
                        initialLbl.text = iniName
                        profilePic.backgroundColor = ColorHelper().pickColor(alphabet: Character(iniName))
                        initialLbl.isHidden = false
                        profilePic.image = nil
                        
                    }
                    
                }
            } else {
                self.anonymousLabel.text = "Student"
                profilePic.isUserInteractionEnabled = false
                anonymousLabel.isUserInteractionEnabled = false
            }
            
            if comment.deleted {
                profilePic.isUserInteractionEnabled = false
                anonymousLabel.isUserInteractionEnabled = false
                profilePic.image = #imageLiteral(resourceName: "ProfilePic")
                initialLbl.isHidden = true
            }


        } else {
             if comment.anonymous {
            
                profilePic.isUserInteractionEnabled = false
                anonymousLabel.isUserInteractionEnabled = false
                
                if comment.author == Auth.auth().currentUser?.uid {
                    self.anonymousLabel.text = "Anonymous Student (You)"
                } else {
                    self.anonymousLabel.text = "Anonymous Student"
                }
                
             } else {

                self.anonymousLabel.text = comment.userName

            }
            
        }
       
        let exist = comment.usersLike[(Auth.auth().currentUser?.uid)!]
        
        if exist != nil{
            self.likeActionLabel.text = "Unlike"
        } else {
           self.likeActionLabel.text = "Like"
        }
      
      

        
        

        
        if comment.deleted {
            profilePic.isUserInteractionEnabled = false
            anonymousLabel.isUserInteractionEnabled = false
            
            self.anonymousLabel.text = "Anonymous Student"
            loveStack.isHidden = true
            likeBtnStack.isHidden = true
        } else {
            loveStack.isHidden = false
            likeBtnStack.isHidden = false
            
            if comment.likes != 0 {
                loveStack.isHidden = false
                likeLabel.text = "\(comment.likes)"
            } else {
                loveStack.isHidden = true
            }
        }
        
//        facultyLabel.text = comment.authorFaculty
        levelLbl.text = "L\(levelID+1)"


        let seconds = comment.time.doubleValue
        let timesStampDate = NSDate(timeIntervalSince1970: seconds)
        var timeAgo:String =  DateHelper().timeAgoSinceDate(timesStampDate as Date, currentDate: NSDate() as Date, numericDates: true)
        dateLabel.text = "\(timeAgo)"


        
    }
    
    func enableTouch() {
        self.likeActionLabel.isUserInteractionEnabled = true
    }
    
    func likeOperation() {
        
        comment?.commentRef.observeSingleEvent(of: .value, with: { (snapshot) in
            if snapshot.exists() {

                let exist = self.comment.usersLike[(Auth.auth().currentUser?.uid)!]
                    
                    
                    /* if the user havent like the photo, the like is empty*/
                    //  if let _ = snapshot.value as? NSNull {
                    if exist == nil {
                        self.loveStack.isHidden = false
                        self.likeLabel.text = "\((self.comment?.likes)! + 1)"
                        self.commentVC?.recursiveReloadPreviousVC(newComment: self.comment, addCom: true)
                        self.comment.adjustLikes(addLike: true, uid: (Auth.auth().currentUser?.uid)! )
                       
                        
                        
                        self.likeActionLabel.text = "Unlike"
                        
                    } else {
                        
                        self.likeLabel.text = "\(self.comment.likes - 1)"
                        if self.comment.likes - 1 == 0 {
                            self.loveStack.isHidden = true
                        }
                        
                        self.commentVC?.recursiveReloadPreviousVC(newComment: self.comment, addCom: false)
                        self.comment.adjustLikes(addLike: false, uid: (Auth.auth().currentUser?.uid)! )
                        
                        
                        self.likeActionLabel.text = "Like"
                        
                        
                        
                    }
                    
                
                
                
                
            } else {
                ErrorAlert().createAlert(title: "Something went wrong", msg: "Could not perform action", object: self.parentViewController!)
            }
        })
//        comment?.commentRef.observeSingleEvent(of: .value, with: { (snapshot) in
//            if snapshot.exists() {
//
//                self.likesRef?.observeSingleEvent(of: .value, with: { (snapshot) in
//
//
//
//                    let exist = self.comment.usersLike[(FIRAuth.auth()?.currentUser?.uid)!]
//
//
//                    /* if the user havent like the photo, the like is empty*/
//                    //  if let _ = snapshot.value as? NSNull {
//                    if exist == nil {
//                        self.loveStack.isHidden = false
//                        self.likeLabel.text = "\((self.comment?.likes)! + 1)"
//                        self.comment.adjustLikes(addLike: true, uid: (FIRAuth.auth()?.currentUser?.uid)! )
//
//
//                        self.likeActionLabel.text = "Unlike"
//
//                    } else {
//
//                        self.likeLabel.text = "\(self.comment.likes - 1)"
//                        if self.comment.likes - 1 == 0 {
//                            self.loveStack.isHidden = true
//                        }
//
//                        self.comment.adjustLikes(addLike: false, uid: (FIRAuth.auth()?.currentUser?.uid)! )
//
//
//                        self.likeActionLabel.text = "Like"
//
//
//
//                    }
//
//                })
//
//
//
//            } else {
//                ErrorAlert().createAlert(title: "Something went wrong", msg: "Could not perform action", object: self.parentViewController!)
//
//               if self.likeActionLabel.text == "Unlike"{
//                    if self.comment.likes - 1 <= 0 {
//                        self.likeLabel.text = "0"
//                    } else {
//                        self.likeLabel.text = "\(self.comment.likes - 1)"
//                    }
//                    self.loveStack.isHidden = true
//                    self.likeActionLabel.text = "Like"
//                }
//            }
//        })
    }
    
    var timer: Timer?

    
    func likeTapped(_ sender: UITapGestureRecognizer) {
        
        self.likeActionLabel.isUserInteractionEnabled = false
        
        self.timer = Timer.scheduledTimer(timeInterval: 2.0, target: self, selector: #selector(enableTouch), userInfo: nil, repeats: false)
        //self.timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(likeOperation), userInfo: nil, repeats: false)
        
        likeOperation()
        
        
    }
    
    func profileTapped(_ sender: UITapGestureRecognizer) {
        
        self.commentVC?.showProfile(authorID: self.comment.author)

        
    }
    
    func likersTapped(_ sender: UITapGestureRecognizer) {
        
         if self.comment.likes != 0 {
            commentVC?.likersTapped(isComment: true, commentID: self.comment.commentKey)
        }
        
    }
    

        
    
        

}
