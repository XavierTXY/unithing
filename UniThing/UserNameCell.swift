//
//  UserNameCell.swift
//  UniThing
//
//  Created by Xavier TanXY on 28/6/17.
//  Copyright © 2017 Xavier TanXY. All rights reserved.
//

import UIKit

class UserNameCell: UITableViewCell {

    @IBOutlet weak var label: UILabel!
    @IBOutlet weak var detailLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    
    func configureCell(detail: String, user: User) {
        detailLabel.text = detail
        
        var userD = String()
        
        switch detail {
        case "UserName":
            userD = user.userName
            label.text = userD
            break
        case "Email":
            userD = user.email
            label.text = userD
            break
        default:
            label.text = detail
            detailLabel.text = "Version"
            break
        }
        
        
    }
}
