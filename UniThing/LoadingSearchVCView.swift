//
//  LoadingSearchVCView.swift
//  UniThing
//
//  Created by Xavier TanXY on 5/7/17.
//  Copyright © 2017 Xavier TanXY. All rights reserved.
//

import UIKit

class LoadingSearchVCView: UIView {

    @IBOutlet weak var view: UIView!
    @IBOutlet weak var label: UILabel!
    class func instanceFromNib() -> UIView {
        return UINib(nibName: "LoadingSearchVCView", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! UIView
    }

}
