//
//  MoreCell.swift
//  UniThing
//
//  Created by Xavier TanXY on 28/6/17.
//  Copyright © 2017 Xavier TanXY. All rights reserved.
//

import UIKit

class MoreCell: UITableViewCell {


    @IBOutlet weak var label: UILabel!
    @IBOutlet weak var morePic: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func configureCell(labelName: String) {
        label.text = labelName
//        
//        if labelName == "Log out" {
//            morePic.image = #imageLiteral(resourceName: "logout")
//        } else if labelName == "Settings" {
//            morePic.image = #imageLiteral(resourceName: "setting")
//        } else if labelName == "Rate Us" {
//            morePic.image = #imageLiteral(resourceName: "rate")
//        } else if labelName == "My Post" {
//            morePic.image = #imageLiteral(resourceName: "posts")
//        }
        
        if labelName == "Log out" {
            label.textColor = UIColor.red
            self.accessoryType = .none
            label.textAlignment = .center
        }
    }

}
