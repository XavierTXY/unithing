//
//  CustomImageView.swift
//  UniThing
//
//  Created by Xavier TanXY on 11/12/16.
//  Copyright © 2016 Xavier TanXY. All rights reserved.
//

import Foundation
import UIKit
import SVProgressHUD

class CustomImageView: UIImageView {
    
  
    
    override func layoutSubviews() {
        layer.cornerRadius = 2
        clipsToBounds = true
        
    }
}
