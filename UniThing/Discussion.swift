//
//  Discussion.swift
//  UniThing
//
//  Created by XavierTanXY on 9/5/18.
//  Copyright © 2018 Xavier TanXY. All rights reserved.
//


import Foundation
import UIKit
import Firebase
import FirebaseDatabase

class Discussion {
    private var _caption: String!
    private var _topic: String!
    // private var _imageUrl: String!
    var imageUrl: String?
    private var _likes: Int!
    private var _comments: Int!
    private var _discussionKey: String!
    private var _time: NSNumber!
    private var _usersLike: [String: Bool]!
    private var _usersComment: [String: Bool]!
    private var _postRef: DatabaseReference!
    private var _visible: Bool!
    
    var topic: String {
        return _topic
    }
    
    var caption: String {
        return _caption
    }


    var likes: Int {
        get {
            return _likes
        }
        
        set(newLikes) {
            if newLikes < 0 {
                _likes = 0
            } else {
                if _likes >= 0 {
                    _likes = newLikes
                }
            }
            
            
        }
        
    }
    
    var comments: Int {
        get {
            return _comments
        }
        
        set(newComments) {
            _comments = newComments
        }
        
    }
    
    var discussionKey: String {
        return _discussionKey
    }
    
    var time: NSNumber {
        return _time
    }
    
    var postRef: DatabaseReference {
        return _postRef
    }
    
    var usersLike: [String: Bool] {
        return _usersLike
    }
    
    var usersComment: [String: Bool] {
        return _usersComment
    }
    
    var visible: Bool{
        return _visible
    }
    
    init(disKey: String, postData: Dictionary<String, AnyObject>) {
        
        
        self._discussionKey = disKey
        
        if let caption = postData["caption"] as? String{
            self._caption = caption
        }
        
        if let t = postData["topic"] as? String{
            self._topic = t
        }
        
        self.imageUrl = postData["imageUrl"] as! String?
        
        if let likes = postData["likes"] as? Int {
            self._likes = likes
        }
        
        if let comments = postData["comments"] as? Int {
            self._comments = comments
        }
        
        if let time = postData["time"] as? NSNumber {
            self._time = time
        }
        
        if let v = postData["visible"] as? Bool {
            self._visible = v
        }

        
        if let userLike = postData["userLike"] as? [String: Bool] {
            self._usersLike = userLike
        }
        
        if let userComment = postData["userComment"] as? [String: Bool] {
            self._usersComment = userComment
        }

        
        
//        _postRef = DataService.ds.REF_Discussion.child(_discussionKey)
    }
    
    func addUser(uid: String) {
        _usersLike[uid] = true
        //true
    }
    
    func removeUser(uid: String) {
        _usersLike.removeValue(forKey: uid)
        
    }
    
    func addCommentUser(uid: String) {
        _usersComment[uid] = true
    }
    
    func removeCommentUser(uid: String) {
        _usersComment.removeValue(forKey: uid)
    }
    
    func adjustLikes(addLike: Bool, uid: String) {
//        
//        let ref2 = DataService.ds.REF_Discussion.child(self.discussionKey)
//        
//        ref2.runTransactionBlock({ (currentData) -> TransactionResult in
//            
//            if var discussionDict = currentData.value as? Dictionary<String, AnyObject> {
//                
//                //var confessionDict = currentData.value as? Dictionary<String, AnyObject>
//                
//                
//                
//                var userLike = discussionDict["userLike"] as? [String: Bool]
//                if addLike {
//                    userLike?[uid] = true
//                    self.addUser(uid: uid)
//                    
//                } else {
//                    // userLike.removeValue(forKey: uid)
//                    userLike?.removeValue(forKey: uid)
//                    self.removeUser(uid: uid)
//                }
//                
//                discussionDict["userLike"] = userLike as AnyObject
//                
//                var likes = discussionDict["likes"] as? Int
//                
//                if likes != nil {
//                    if addLike {
//                        likes = likes! + 1
//                        //                        self.addNotification()
//                    } else {
//                        if likes != 0 {
//                            likes = likes! - 1
//                            //self.removeNotification()
//                        }
//                    }
//                }
//                
//                
//                discussionDict["likes"] = likes as AnyObject
//                
//                
//                currentData.value = discussionDict
//                print("scueess")
//                return TransactionResult.success(withValue: currentData)
//            }
//            
//            return TransactionResult.success(withValue: currentData)
//        }) { (error, commited, snap) in
//            
//            if commited {
//                
//                var discussionDict = snap?.value as? Dictionary<String, AnyObject>
//                
//                //                let likeToBeUpdated = snap!.value
//                if let likeToBeUpdated = discussionDict?["likes"] {
//                    self._likes = likeToBeUpdated as! Int
//                    
//                    
//                    var updateObj = [String:AnyObject]()
//                    let ref = DB_BASE
//                    
//                    if addLike {
//                        
//
//                        
//                        self.addUser(uid: uid)
//
//                        
//                        updateObj["/discussion/\(self.discussionKey)/likes"] = likeToBeUpdated as AnyObject
//                        updateObj["/discussion/\(self.discussionKey)/userLike/\(uid)"] = true as AnyObject
//                        ref.updateChildValues(updateObj)
//                        
//                        
//                    } else {
//                        
//                        self.removeUser(uid: uid)
//                        
//                        updateObj["/discussion/\(self.discussionKey)/likes"] = likeToBeUpdated as AnyObject
//                        updateObj["/discussion/\(self.discussionKey)/userLike/\(uid)"] = NSNull()
//                        ref.updateChildValues(updateObj)
//                        
//                        
//                    }
//                }
//                
//                
//            } else {
//                //call error callback function if you want
//                
//                print("fail")
//            }
//            
//        }
        
    }
    
    func addCommentNotification(commentKey: String, comment: Comment,completion: @escaping () -> ()) {
        var updateObj = [String:AnyObject]()
        let ref = DB_BASE
        var notificationID: DatabaseReference!
        
        DataService.ds.REF_USERS.child((Auth.auth().currentUser?.uid)!).observeSingleEvent(of: .value, with: { (snapshot) in
            if let snapshots = snapshot.children.allObjects as? [DataSnapshot] {
                if let userDict = snapshot.value as? Dictionary<String, AnyObject> {
                    let key = snapshot.key
                    var user = User(userKey: key, userData: userDict)
                    
                    let timestamp = NSNumber(integerLiteral: Int(NSDate().timeIntervalSince1970))
                    
                    
                    var type: String!
                    var notificationValue: Dictionary<String, AnyObject>!
                    
                        notificationID = DataService.ds.REF_NOTIFICATION.child(comment.replyTo).childByAutoId()
                        type = "reply"
                        
                        updateObj["/FOR_REPLY_DELETION/\(self.discussionKey)/\(comment.replyTo)/\(notificationID.key)"] = true as AnyObject
                        
//                        self.incrementUserNotiCount(authorID: comment.replyTo)
                    
                        
                        if comment.anonymous {
                            notificationValue = ["from" : "\((Auth.auth().currentUser?.uid)!) " as AnyObject, "type" : type as AnyObject, "userName" : "One Student" as AnyObject, "discussionKey" : self.discussionKey as AnyObject, "time" : timestamp as AnyObject, "profilePicUrl" : user.profilePicUrl as AnyObject, "seen" : false as AnyObject, "replyToCommentContent": "\((comment.replyCommentContent))" as AnyObject, "fromGender": comment.authorGender as AnyObject]
                        } else {
                            notificationValue = ["from" : "\((Auth.auth().currentUser?.uid)!) " as AnyObject, "type" : type as AnyObject, "userName" : user.userName as AnyObject, "discussionKey" : self.discussionKey as AnyObject, "time" : timestamp as AnyObject, "profilePicUrl" : user.profilePicUrl as AnyObject, "seen" : false as AnyObject, "replyToCommentContent": "\((comment.replyCommentContent))" as AnyObject,"fromGender": comment.authorGender as AnyObject]
                        }
                        
                        
                        
                        notificationID.updateChildValues(notificationValue)
                        
                        
                        updateObj["/discussion-notification/\(self.discussionKey)/\(notificationID.key)"] = true as AnyObject
                        updateObj["commenter-commentID-notiID/\((Auth.auth().currentUser?.uid)!)/\(commentKey)/\(notificationID.key)"] = true as AnyObject
                        updateObj["discussion-commenter-commentID/\(self.discussionKey)/\((Auth.auth().currentUser?.uid)!)/\(commentKey)"] = true as AnyObject
                        ref.updateChildValues(updateObj)
                        completion()
                     
                    
                }
                
            }
            
        })
    }
    
    func removeCommentNotification(commentKey: String, comment: Comment,completion: @escaping () -> ()) {
        
        var updateObj = [String:AnyObject]()
        let ref = DB_BASE
        var notificationID: DatabaseReference!
        
        var uid = comment.author
        
        
//        decreaseUserNotiCount(authorID: self.author)
        
        //to be done
        DB_BASE.child("comment-liker").child(comment.commentKey).observeSingleEvent(of: .value, with: { (snapshot) in
            if let snapshots = snapshot.children.allObjects as? [DataSnapshot] {
                for snap in snapshots {
                    print("yiweri")
                    print(snap.key)
                    var commentLiker = snap.key
                    updateObj["/liker-comment-notification/\(commentLiker)/\(comment.commentKey)"] = NSNull()
                    
                }
                
            }
        })
        
        DB_BASE.child("comment-commentAuthor-notification").child(comment.commentKey).child(comment.author).child("notifications").observeSingleEvent(of: .value, with: { (snapshot) in
            if let snapshots = snapshot.children.allObjects as? [DataSnapshot] {
                for snap in snapshots {
                    print("yiweri")
                    print(snap.key)
                    var notiID = snap.key
                    
                    updateObj["/notification/\(comment.author)/\(notiID)"] = NSNull()
                    //var commentLiker = snap.key
                    
                }
                
            }
            
        })
        
        updateObj["/comment-commentAuthor-notification/\(comment.commentKey)"] = NSNull()
        updateObj["/comment-liker/\(comment.commentKey)"] = NSNull()
        
        
        // if self.author != FIRAuth.auth()?.currentUser?.uid {
        
        DataService.ds.REF_COMMENTER_COMMENT_NOTIFICATION.child(uid).child(commentKey).observeSingleEvent(of: .value, with: { (snapshot) in
            print("wtf")
            //print(snap)
            //var notiID = snap.value
            if let snapshots = snapshot.children.allObjects as? [DataSnapshot] {
                
                for snap in snapshots {
                    updateObj["/notification/\(uid)/\(snap.key)"] = NSNull()
                    updateObj["commenter-commentID-notiID/\(uid)/\(commentKey)/\(snap.key)"] = NSNull()
                    updateObj["/discussion-notification/\(self.discussionKey)/\(snap.key)"] = NSNull()
                    
                }
                
                
                
                DB_BASE.child("commenter-commentID-notiID").child(uid).observeSingleEvent(of: .value, with: { (snap) in
                    if !snap.exists() {
                        updateObj["discussion-commenter-commentID/\(self.discussionKey)/\(uid)"] = NSNull()
                        ref.updateChildValues(updateObj)
                        completion()
                        
                    }
                    
                })
                
                ref.updateChildValues(updateObj)
                completion()
                
                
                
            }
            
        })
        
    }
    
    func adjustComments(addComment: Bool, uid: String, commentKey: String, comment: Comment) {
//        let ref = DataService.ds.REF_Discussion.child(_discussionKey)
//        
//        ref.runTransactionBlock({ (currentData) -> TransactionResult in
//            var disDict = currentData.value as? Dictionary<String,AnyObject>
//            //value of the counter before an update
//            //            var value = currentData.value as? Int
//            //var value = confessionDict?["comments"] as? Int
//            
//            var userComment = disDict?["userComment"] as? [String: Bool]
//            
//            if addComment {
//                userComment?[uid] = true
//                self.addCommentUser(uid: uid)
//            } else {
//                userComment?.removeValue(forKey: uid)
//                self.removeCommentUser(uid: uid)
//            }
//            
//            disDict?["userComment"] = userComment as AnyObject
//            var comments = disDict?["comments"] as? Int
//            //checking for nil data is very important when using
//            //transactional writes
//            if comments == nil {
//                comments = 0
//            }
//            
//            if addComment {
//                comments = comments! + 1
//            } else {
//                
//                //                if comments != 0 {
//                //                    comments = comments! - 1
//                //
//                //                }
//                
//            }
//            
//            disDict?["comments"] = comments as AnyObject
//            currentData.value = disDict
//            
//            //actual update
//            
//            return TransactionResult.success(withValue: currentData)
//            
//        }) { (error, commited, snap) in
//            
//            if commited {
//                print("commite")
//                var disDict = snap?.value as? Dictionary<String,AnyObject>
//                //let commentsToBeUpdated = snap!.value
//                let commentsToBeUpdated = disDict?["comments"]
//                self._comments = commentsToBeUpdated as! Int
//                
//                var updateObj = [String:AnyObject]()
//                let ref = DB_BASE
//                
//                
//                if addComment {
//                    self.addCommentNotification(commentKey: commentKey, comment: comment) {
//                        self.addCommentUser(uid: uid)
//
//                            
//                        }
//                        
//                        
//                        
//                        
//                        updateObj["/discussion/\(self.discussionKey)/comments"] = commentsToBeUpdated as AnyObject
//                        updateObj["/discussion/\(self.discussionKey)/userComment/\(uid)"] = true as AnyObject
//
//                        
//                        ref.updateChildValues(updateObj)
//                    
//                    
//                    
//                    
//                    
//                    
//                } else {
//                    
//                    self.removeCommentNotification(commentKey: commentKey, comment: comment) {
//                        self.removeCommentUser(uid: uid)
//
//
//
//                        updateObj["FOR_REPLY_DELETION/\(self.discussionKey)/\(comment.replyTo)"] = NSNull()
//                        updateObj["/discussion/\(self.discussionKey)/comments"] = commentsToBeUpdated as AnyObject
//                        updateObj["/discussion/\(self.discussionKey)/userComment/\(uid)"] = NSNull()
//                        ref.updateChildValues(updateObj)
//                    }
//                    
//                    
//                }
//                    
//                    
//            
//            
//            } else {
//                
//                print("fail")
//            }
//        }
        
        
    }
    
}

