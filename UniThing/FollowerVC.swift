//
//  FollowerVC.swift
//  UniThing
//
//  Created by XavierTanXY on 24/5/18.
//  Copyright © 2018 Xavier TanXY. All rights reserved.
//

import UIKit
import Firebase
import PopupDialog

class FollowerCell: UITableViewCell {
    
    @IBOutlet weak var nameLbl: UILabel!
    @IBOutlet weak var detailLbl: UILabel!
    @IBOutlet weak var initialLbl: UILabel!
    @IBOutlet weak var outerViewLbl: UIView!
    @IBOutlet weak var profilePicImg: UIImageView!
    @IBOutlet weak var followBtn: UIButton!
    var followerVC: FollowerVC!
    var user: User!
    var owner: User!
    var follow: Bool!
    
    override func awakeFromNib() {
        profilePicImg.layer.cornerRadius = profilePicImg.frame.height / 2
        profilePicImg.clipsToBounds = true
        outerViewLbl.layer.cornerRadius = outerViewLbl.frame.height / 2
        outerViewLbl.clipsToBounds = true
    }
    
    func configureCell(user: User) {
        self.user = user
//        self.owner = owner
        nameLbl.text = user.userName
        detailLbl.text = "\(user.universityShort) - \(user.course)"
        profilePicImg.image = nil
        
        if user.profilePicUrl != NO_PIC {
            profilePicImg.loadImageUsingCacheWithUrlStringWithoutIndex(imageUrl: (user.profilePicUrl))
            initialLbl.isHidden = true
        } else {
            initialLbl.isHidden = false
            var i = String((user.userName[(user.userName.startIndex)])).capitalized
            initialLbl.text = i
            profilePicImg.backgroundColor = ColorHelper().pickColor(alphabet: Character(i))
        }
        
        if DataService.ds.currentUser.isFollowing(userID: user.userKey!) {
            follow = true

            changeFollowLbl(follow: follow)

        } else {
             follow = false
            changeFollowLbl(follow: follow)
        }

        
        
    }
    
    func changeFollowLbl(follow: Bool) {
        
        if follow {
            let attributedString = NSMutableAttributedString(string: "FOLLOWING")
            
            let firstAttributes:[String:Any] = [NSForegroundColorAttributeName: PURPLE]
            attributedString.addAttributes(firstAttributes, range: NSRange(location: 0, length: 9))
            
            followBtn.setAttributedTitle(attributedString , for: .normal)
            
            followBtn.setBackgroundImage(#imageLiteral(resourceName: "RecWhiteFollowBtn"), for: .normal)
        } else {
            let attributedString = NSMutableAttributedString(string: "FOLLOW")
            
            let firstAttributes:[String:Any] = [NSForegroundColorAttributeName: UIColor.white]
            attributedString.addAttributes(firstAttributes, range: NSRange(location: 0, length: 6))
            
            
            followBtn.setAttributedTitle(attributedString, for: .normal)
            followBtn.setBackgroundImage(#imageLiteral(resourceName: "RecFollowBtn"), for: .normal)
        }
    }
    
    @IBAction func followTap(_ sender: Any) {
        

        followTapped(user: self.user)


    }
    
    func followTapped(user: User) {
        self.followBtn.pulsate()
        
        if DataService.ds.currentUser.userKey == user.userKey {
            let title = "Didn't you notice something?"
            let message = "This is you!"
            
            let popup = PopupDialog(title: title, message: message, image: nil)
            
            
            let buttonTwo = DefaultButton(title: "OK", dismissOnTap: true) {
                
            }
            
            popup.addButtons([buttonTwo])
            popup.buttonAlignment = .horizontal
            
            followerVC.present(popup, animated: true, completion: nil)
            
        } else {
            if !DataService.ds.currentUser.isFollowing(userID: user.userKey!) {
                sendFollowNoti(user: user)
            } else {
                //                self.performSegue(withIdentifier: "NewChatLogVC", sender: nil)
                self.displayUnfollowAlert(user: user)
            }
        }
        
        
    }
    
    func removeFollowNoti(user: User) {
        var updateObj = [String:AnyObject]()
        let ref = DB_BASE
        DataService.ds.REF_User_Following_Notification.child((Auth.auth().currentUser?.uid)!).child(user.userKey!).observeSingleEvent(of: .value, with: { (snapshot) in
            
            if let notiDict = snapshot.value as? Dictionary<String, AnyObject> {
                
                updateObj["/notification/\(user.userKey!)/\(notiDict["notificationID"]!)"] = NSNull() as AnyObject
                
                ref.updateChildValues(updateObj)
                
                DataService.ds.currentUser.adjustFollowing(userID: user.userKey!, add: false)
                user.adjustFollower(userID: DataService.ds.currentUser.userKey!, add: false)
                
                //                self.setupUserDetails()
                self.followerVC.reloadUser(user: user)
                self.changeFollowLbl(follow: false)
                self.followerVC.tableView.reloadData()
                
            }
        })
    }
    
    func sendFollowNoti(user: User) {
        
        
        var updateObj = [String:AnyObject]()
        let ref = DB_BASE
        
        let timestamp = NSNumber(integerLiteral: Int(NSDate().timeIntervalSince1970))
        var notifcationID = DataService.ds.REF_NOTIFICATION.child((user.userKey)!).childByAutoId()
        var notificationValue: Dictionary<String, AnyObject> = ["from" : "\((Auth.auth().currentUser?.uid)!) " as AnyObject, "type" : "follow" as AnyObject, "userName" : DataService.ds.currentUser.userName as AnyObject, "confessionKey" : "" as AnyObject, "time" : timestamp as AnyObject, "profilePicUrl" : DataService.ds.currentUser.profilePicUrl as AnyObject, "seen" : false as AnyObject]
        
        
        updateObj["/user-following-notification/\((Auth.auth().currentUser?.uid)!)/\(user.userKey!)"] = ["notificationID" : notifcationID.key ] as AnyObject
        
        notifcationID.updateChildValues(notificationValue)
        ref.updateChildValues(updateObj)
        
        DataService.ds.currentUser.adjustFollowing(userID: user.userKey!, add: true)
        user.adjustFollower(userID: DataService.ds.currentUser.userKey!, add: true)
        
        
        //        self.setupUserDetails()
        followerVC.reloadUser(user: user)
        self.changeFollowLbl(follow: true)

        
        
        
        //        followBtn.setTitle("MESSAGE", for: .normal)
        //        followBtn.setBackgroundImage(#imageLiteral(resourceName: "MessageBtn"), for: .normal)
        //        followBtn.setTitleColor(UIColor.white, for: .normal)
        
    }
    
    func displayUnfollowAlert(user: User) {
        let title = "Unfollow"
        let message = "Are you sure you want to unfollow this student?"
        
        let popup = PopupDialog(title: title, message: message, image: nil)
        
        let buttonOne = CancelButton(title: "Cancel", dismissOnTap: true) {
            
        }
        
        let buttonTwo = DefaultButton(title: "Unfollow", dismissOnTap: true) {
            self.removeFollowNoti(user: user)
        }
        
        popup.addButtons([buttonOne,buttonTwo])
        popup.buttonAlignment = .horizontal
        
        followerVC.present(popup, animated: true, completion: nil)
        
    }
    
    
}

class FollowerVC: UIViewController, UITableViewDataSource, UITableViewDelegate {

    @IBOutlet weak var tableView: UITableView!
    
    var users = [User]()
    var userDict = [String: User]()
    var userVisiting: User!
    var mode: String!
    var selectedUserID: String!
    var confessionID: String!
    var commentID: String!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.tableFooterView = UIView()
        
        if mode == FOLLOWING {
            self.navigationItem.title = "Following"
            fetchFriends(mode: mode)
        } else if mode == FOLLOWER {
            self.navigationItem.title = "Follower"
            fetchFriends(mode: mode)
        } else {
            self.navigationItem.title = "Likes"
            fetchLikers()
        }
        
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.tableView.reloadData()
        self.navigationController?.navigationBar.tintColor = PURPLE
        self.navigationController?.navigationBar.titleTextAttributes = [ NSForegroundColorAttributeName: PURPLE]
        UIApplication.shared.statusBarStyle = .default
    }

    func fetchLikers() {
        
        if mode == "CONFESSION" {
            DataService.ds.REF_CONFESSIONS.child(self.confessionID).child(LIKER).observeSingleEvent(of: .value, with: { (snapshot) in
                
                
                if let snapshots = snapshot.children.allObjects as? [DataSnapshot] {
                    for snap in snapshots {
                        
                        if snap.key != "key" {
                            DataService.ds.REF_USERS.child(snap.key).observeSingleEvent(of: .value, with: { (snapshot2) in
                                if let dict = snapshot2.value as? Dictionary<String, AnyObject> {
                                    let user = User(userKey: snap.key, userData: dict)
                                    self.userDict[user.userKey!] = user
                                    
                                    self.attemptReloadTable()
                                }
                                
                            })
                        }
                        
                    }
                    
                }
            })
        } else if mode == "COMMENT" {
            DataService.ds.REF_COMMENTS.child(self.commentID).child(LIKER).observeSingleEvent(of: .value, with: { (snapshot) in
                
                
                if let snapshots = snapshot.children.allObjects as? [DataSnapshot] {
                    for snap in snapshots {
                        
                        if snap.key != "key" {
                            DataService.ds.REF_USERS.child(snap.key).observeSingleEvent(of: .value, with: { (snapshot2) in
                                if let dict = snapshot2.value as? Dictionary<String, AnyObject> {
                                    let user = User(userKey: snap.key, userData: dict)
                                    self.userDict[user.userKey!] = user
                                    
                                    self.attemptReloadTable()
                                }
                                
                            })
                        }
                        
                    }
                    
                }
            })
        }

    }
    
    func fetchFriends(mode: String) {
        
        DataService.ds.REF_USERS.child(self.userVisiting.userKey!).child(mode).observeSingleEvent(of: .value, with: { (snapshot) in
            
            
            if let snapshots = snapshot.children.allObjects as? [DataSnapshot] {
                for snap in snapshots {
                    
                    if snap.key != "key" {
                        DataService.ds.REF_USERS.child(snap.key).observeSingleEvent(of: .value, with: { (snapshot2) in
                            if let dict = snapshot2.value as? Dictionary<String, AnyObject> {
                                let user = User(userKey: snap.key, userData: dict)
                                self.userDict[user.userKey!] = user
                                
//                                self.users.append(user)
                                
                                self.attemptReloadTable()
                            }
      
                        })
                    }
                    
                }
                
                //
                //
                //
            }
        })
        
    }
    
    func attemptReloadTable() {
        
        self.users = Array(self.userDict.values)
        self.tableView.reloadData()
//        UIView.animate(views: self.tableView.visibleCells, animations: animations, completion: {
//        })
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return users.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "FollowerCell", for: indexPath) as! FollowerCell
        cell.selectionStyle = .none
        let user = users[indexPath.row]
        cell.followerVC = self
        cell.configureCell(user: user)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.selectedUserID = users[indexPath.row].userKey!
        self.performSegue(withIdentifier: "OtherProfileVC", sender: nil)
    }

    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let destinationVC = segue.destination as? OtherProfileVC {
            destinationVC.userID = self.selectedUserID

        }
    }
    
    func followTap(user: User) {
        if DataService.ds.currentUser.userKey == user.userKey {
            let title = "Didn't you notice something?"
            let message = "This is you!"
            
            let popup = PopupDialog(title: title, message: message, image: nil)
            
            
            let buttonTwo = DefaultButton(title: "OK", dismissOnTap: true) {
                
            }
            
            popup.addButtons([buttonTwo])
            popup.buttonAlignment = .horizontal
            
            self.present(popup, animated: true, completion: nil)
            
        } else {
            if !DataService.ds.currentUser.isFollowing(userID: user.userKey!) {
                sendFollowNoti(user: user)
            } else {
//                self.performSegue(withIdentifier: "NewChatLogVC", sender: nil)
                 self.displayUnfollowAlert(user: user)
            }
        }

        
    }
    
   
    func displayUnfollowAlert(user: User) {
        let title = "Unfollow"
        let message = "Are you sure you want to unfollow this student?"
        
        let popup = PopupDialog(title: title, message: message, image: nil)
        
        let buttonOne = CancelButton(title: "Cancel", dismissOnTap: true) {
            
        }
        
        let buttonTwo = DefaultButton(title: "Unfollow", dismissOnTap: true) {
            self.removeFollowNoti(user: user)
        }
        
        popup.addButtons([buttonOne,buttonTwo])
        popup.buttonAlignment = .horizontal
        
        self.present(popup, animated: true, completion: nil)
        
    }

    func removeFollowNoti(user: User) {
        var updateObj = [String:AnyObject]()
        let ref = DB_BASE
        DataService.ds.REF_User_Following_Notification.child((Auth.auth().currentUser?.uid)!).child(user.userKey!).observeSingleEvent(of: .value, with: { (snapshot) in

            if let notiDict = snapshot.value as? Dictionary<String, AnyObject> {

                updateObj["/notification/\(user.userKey!)/\(notiDict["notificationID"]!)"] = NSNull() as AnyObject

                ref.updateChildValues(updateObj)

                DataService.ds.currentUser.adjustFollowing(userID: user.userKey!, add: false)
                user.adjustFollower(userID: DataService.ds.currentUser.userKey!, add: false)

//                self.setupUserDetails()
                self.reloadUser(user: user)

            }
        })
    }

    func sendFollowNoti(user: User) {
        
        
        var updateObj = [String:AnyObject]()
        let ref = DB_BASE
        
        let timestamp = NSNumber(integerLiteral: Int(NSDate().timeIntervalSince1970))
        var notifcationID = DataService.ds.REF_NOTIFICATION.child((user.userKey)!).childByAutoId()
        var notificationValue: Dictionary<String, AnyObject> = ["from" : "\((Auth.auth().currentUser?.uid)!) " as AnyObject, "type" : "follow" as AnyObject, "userName" : DataService.ds.currentUser.userName as AnyObject, "confessionKey" : "" as AnyObject, "time" : timestamp as AnyObject, "profilePicUrl" : DataService.ds.currentUser.profilePicUrl as AnyObject, "seen" : false as AnyObject]
        
        
        updateObj["/user-following-notification/\((Auth.auth().currentUser?.uid)!)/\(user.userKey!)"] = ["notificationID" : notifcationID.key ] as AnyObject
        
        notifcationID.updateChildValues(notificationValue)
        ref.updateChildValues(updateObj)
        
        DataService.ds.currentUser.adjustFollowing(userID: user.userKey!, add: true)
        user.adjustFollower(userID: DataService.ds.currentUser.userKey!, add: true)
        
        
//        self.setupUserDetails()
        self.reloadUser(user: user)

        
        
        //        followBtn.setTitle("MESSAGE", for: .normal)
        //        followBtn.setBackgroundImage(#imageLiteral(resourceName: "MessageBtn"), for: .normal)
        //        followBtn.setTitleColor(UIColor.white, for: .normal)
        
    }
    

    
    func reloadAllUsers(vc: UINavigationController, user: User) {
        for vc2 in vc.viewControllers {
            
            
            if vc2 is OtherProfileVC {
                let profileVC = vc2 as! OtherProfileVC
                profileVC.user = user
                profileVC.setupUserDetails()
                
            }
            
            if vc2 is FollowerVC {
                let fVC = vc2 as! FollowerVC
                
                if fVC.userDict[user.userKey!] != nil {
                    fVC.userDict[user.userKey!] = user
                    fVC.attemptReloadTable()
                    
                }
                
            }
            
        }
        
    }
    
    func reloadUser(user: User) {
        let vc = self.tabBarController?.viewControllers?[0] as! UINavigationController
        reloadAllUsers(vc: vc, user: user)
        
        let vc2 = self.tabBarController?.viewControllers?[1] as! UINavigationController
        reloadAllUsers(vc: vc2, user: user)
        
        let vc3 = self.tabBarController?.viewControllers?[3] as! UINavigationController
        reloadAllUsers(vc: vc3, user: user)
        
        let vc4 = self.tabBarController?.viewControllers?[4] as! UINavigationController
        reloadAllUsers(vc: vc4, user: user)
    }
}
