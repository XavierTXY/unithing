//
//  EmptyConfessionView.swift
//  UniThing
//
//  Created by Xavier TanXY on 7/8/17.
//  Copyright © 2017 Xavier TanXY. All rights reserved.
//

import UIKit

class EmptyConfessionView: UIView {

    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var iconImg: UIImageView!
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var detailLbl: UILabel!
    //    override init(frame: CGRect) {
//        super.init(frame: frame)
//        xibSetup()
//    }
//    
//    required init?(coder aDecoder: NSCoder) {
//        super.init(coder: aDecoder)
//        xibSetup()
//    }
//    
//    func xibSetup() {
//        contentView = loadViewFromNib()
//        
//        // use bounds not frame or it'll be offset
//        contentView!.frame = bounds
//        
//        // Make the view stretch with containing view
//        contentView!.autoresizingMask = [UIViewAutoresizing.flexibleWidth, UIViewAutoresizing.flexibleHeight]
//        
//        // Adding custom subview on top of our view (over any custom drawing > see note below)
//        addSubview(contentView!)
//    }
//    
//    func loadViewFromNib() -> UIView! {
//        
//        let bundle = Bundle(for: type(of: self))
//        //let bundle = Bundle(forClass: type(of: self))
//        let nib = UINib(nibName: String(describing: type(of: self)), bundle: bundle)
//        let view = nib.instantiate(withOwner: self, options: nil)[0] as! UIView
//        
//        return view
//    }
    class func instanceFromNib() -> UIView {
        return UINib(nibName: "EmptyConfessionView", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! UIView
    }

}
