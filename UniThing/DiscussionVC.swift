//
//  DiscussionVC.swift
//  UniThing
//
//  Created by XavierTanXY on 11/5/18.
//  Copyright © 2018 Xavier TanXY. All rights reserved.
//

import UIKit
import Firebase
import SVProgressHUD
import PopupDialog
import Whisper

class DiscussionVC: UIViewController {

//
//    lazy var inputContainerView: ChatInputContainerDiscussion = {
//
//        let chatInputContainerView = ChatInputContainerDiscussion(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: 55))
//
//        chatInputContainerView.discussionVC = self
//
//        return chatInputContainerView
//
//
//    }()
//
//    @IBOutlet weak var tableView: UITableView!
//    var discussion: Discussion!
//
//    var commments = [Comment]()
//    var commentIDs = [String]()
//    var commentDict = [String: Comment]()
//    var totalComments: Int!
//    var user: User!
//
//    var replyAnonymously = false
//    var refreshControl: UIRefreshControl!
//
//    var numberOfObjectsinArray: Int = 12
//    var actInd: UIActivityIndicatorView!
//
//    override func viewDidLoad() {
//        super.viewDidLoad()
//
//        self.navigationItem.title = "Discussion"
//
//        tableView.delegate = self
//        tableView.dataSource = self
//        // Do any additional setup after loading the view.
//
//        tableView.keyboardDismissMode = .interactive
//        tableView.estimatedRowHeight = 200
//        tableView.rowHeight = UITableViewAutomaticDimension
//        tableView.estimatedSectionHeaderHeight = 450
//        tableView.sectionHeaderHeight = UITableViewAutomaticDimension
//
//        refreshControl = UIRefreshControl()
//        refreshControl?.backgroundColor = UIColor.clear
//        refreshControl?.tintColor = UIColor.lightGray
//        refreshControl?.addTarget(self, action: "refresh", for: UIControlEvents.valueChanged)
//        tableView.addSubview(refreshControl!)
//
//        initFooter()
//        fetchDiscussion()
//        fetchDiscussionComment()
//        print(self.discussion.caption)
//    }
    
//    func displayEmptyTxtFieldAlert() {
//        ErrorAlert().createAlert(title: "Reminder", msg: "Comment can't be blank", object: self)
//
//    }
//
//
//    func anonymousModeTapped(){
//        print("you become anon")
//
//
//
//            inputContainerView.anonBtn.pulsate()
//            if replyAnonymously {
//
//                inputContainerView.anonBtn.setImage(#imageLiteral(resourceName: "anonyModeOff"), for: .normal)
//                replyAnonymously = false
//            } else {
//                inputContainerView.anonBtn.setImage(#imageLiteral(resourceName: "anonyModeOn"), for: .normal)
//                var murmur = Murmur(title: "Replying Anonymously")
//                murmur.backgroundColor = UIColor(red: 18/255, green: 102/255, blue: 215/255, alpha: 1)
//                murmur.titleColor = UIColor.white
//
//                // Show and hide a message after delay
//                Whisper.show(whistle: murmur, action: .show(1.5))
//                replyAnonymously = true
//            }
//
//
//
//
//
//    }
//
//    var dict = [String: Bool]()
//
//    func commentSend()  {
//        self.discussion?.postRef.observeSingleEvent(of: .value, with: { (snapshot) in
//            if snapshot.exists() {
//
//                self.dict["key"] = true
//                self.createComment()
//
//
//
//            } else {
//                ErrorAlert().createAlert(title: "Something went wrong", msg: "Could not perform action", object: self)
//                //                self.enableSendButton()
//            }
//        })
//    }
//
//    var commentValues: Dictionary<String, AnyObject>?
//
//    func createComment() {
//
////        var uid = (Auth.auth().currentUser?.uid)!
////        let timestamp = NSNumber(integerLiteral: Int(NSDate().timeIntervalSince1970))
////        var anonymousID = 0
////        var commentors: Int?
////
////
////        self.commentValues = ["comment": self.inputContainerView.inputTextField.text as AnyObject, "likes": 0 as AnyObject, "author": Auth.auth().currentUser?.uid as AnyObject, "userName": self.user.userName as AnyObject, "time": timestamp, "confessionKey": self.confessionId as AnyObject, "authorGender": self.user.gender as AnyObject, "authorFaculty": self.user.course as AnyObject, "userLike": self.dict as AnyObject, "profilePicUrl" : self.user.profilePicUrl as AnyObject, "replyTo": "" as AnyObject , "replyCommentContent": "" as AnyObject, "deleted": false as AnyObject, "anonymous": replyAnonymously as AnyObject, "authorUniversity": self.user.university as AnyObject, "university": self.confession?.university as AnyObject]
////
////        let firebaseComment = DataService.ds.REF_CONFESSION_COMMENTS.childByAutoId()
////
////        if let values = self.commentValues {
////
////            let comment = Comment(commentKey: firebaseComment.key, postData: values)
////
////            DataService.ds.REF_CONFESSION_COMMENTS.child(self.confessionId!).child(firebaseComment.key).updateChildValues(values)
////            DataService.ds.REF_User_Comment.child((Auth.auth().currentUser?.uid)!).child(firebaseComment.key).updateChildValues(values)
////            DataService.ds.REF_COMMENTS.child(firebaseComment.key).updateChildValues(values)
////
////
////            self.discussion?.comments = (self.confession?.comments)! + 1
////            self.discussion?.adjustComments(addComment: true, uid: (Auth.auth().currentUser?.uid)!, commentKey: firebaseComment.key, comment: comment)
////            //NotificationCenter.default.post(name: NSNotification.Name(rawValue: "reloadAddComment"), object: nil)
////
////
////            self.commments.append(comment)
////            self.commentDict[firebaseComment.key] = comment
////
////
////
////            self.recursiveReloadPreviousVC(addCom: true)
////
////
////            self.inputContainerView.inputTextField.text = nil
////            //            self.disableSendButton()
////
////
////            self.inputContainerView.inputTextField.resignFirstResponder()
////
////            self.tableView.reloadData()
////            self.deleteMode = DELETE_MODE_AFTER_POSTED
////        }
//    }
//
//    func initFooter() {
//
//        actInd = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.gray)
//        actInd.frame = CGRect(x: 0, y: 0, width: 320, height: 35)
//        actInd.hidesWhenStopped = true
//        self.tableView.tableFooterView = actInd
//
//
//    }
//
//    func fetchDiscussion() {
//
//        DataService.ds.REF_Discussion.observeSingleEvent(of: .value, with: { (snapshot) in
//
//            if snapshot.exists() {
//                if let snapshots = snapshot.children.allObjects as? [DataSnapshot] {
//                    for snap in snapshots {
//                        //print("SNAP: \(snap)")
//                        if let disDict = snap.value as? Dictionary<String, AnyObject> {
//
//
//                            let key = snap.key
//
//
//                            let d = Discussion(disKey: key, postData: disDict)
//
//
//                                self.discussion = d
//
//
//
//
//                        }
//                    }
//
//                }
//            }
//
//        })
//    }
//
//
//    func fetchDiscussionComment() {
//        getTotalDiscussionComments()
//
//        DataService.ds.REF_Discussion_Comments.child((self.discussion?.discussionKey)!).queryLimited(toFirst: UInt(self.numberOfObjectsinArray)).observeSingleEvent(of: .value, with: { (snapshot) in
//            // DataService.ds.REF_COMMENTS.queryOrdered(byChild: "confessionKey").queryEqual(toValue: self.confessionId!).queryLimited(toFirst: UInt(self.numberOfObjectsinArray)).observeSingleEvent(of: .value, with: { (snapshot) in
//
//            self.commments = []
//            self.commentIDs = []
//            self.commentDict.removeAll()
//            //self.currentComments = []
//
//
//            if let snapshots = snapshot.children.allObjects as? [DataSnapshot] {
//
//                for snap in snapshots {
//
//
//                    if let commentDict = snap.value as? Dictionary<String, AnyObject> {
//
//
//                        let key = snap.key
//                        let comment = Comment(commentKey: key, postData: commentDict)
//                        //
//                        //                        if( comment.anonymous ) {
//                        //                            self.anonyCommentDict[key] = comment
//                        //                        } else {
//                        self.commentDict[key] = comment
//                        //                        }
//
//
//
//
//
//
//
//                    }
//                }
//
//                self.reloadTable()
//
//            }
//
//
//        })
//
//        DataService.ds.REF_COMMENTS.observe(.childRemoved, with: { (snapshot) in
//
//            self.commentDict.removeValue(forKey: snapshot.key)
//            self.totalComments = self.totalComments - 1
//
//        })
//    }
//
//    func getTotalDiscussionComments() {
//        DataService.ds.REF_Discussion.child((self.discussion?.discussionKey)!).child("comments").observeSingleEvent(of: .value, with: { (snapshot) in
//
//
//            //if let snapshots = snapshot.children.allObjects as? [FIRDataSnapshot] {
//
//            self.totalComments = snapshot.value as! Int
//            if self.totalComments == 0 {
//                self.refreshControl?.endRefreshing()
//                self.commments.removeAll()
//                self.commentDict.removeAll()
//                self.reloadTable()
//
//            }
//            //}
//
//        })
//    }
//
//    func refresh() {
//        print("refresh")
//        self.tableView.isUserInteractionEnabled = false
//        Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(reloadDataWhenPulled), userInfo: nil, repeats: false)
//
//
//    }
//
//    func fetchDatas() {
//
//        //        if checkExisted() {
//        //
//        //            fetchConfession()
//        //            fetchComments()
//        //        } else {
//        //            print("what")
//        //        }
//        //
//        checkExisted()
//
//
//
//    }
//
//    func reloadDataWhenPulled() {
//
//        if (self.refreshControl?.isRefreshing)! {
//
//            // reloadTable()
//            //  self.tableView.reloadData()
//            fetchDatas()
//            // self.refreshControl.endRefreshing()
//
//            self.tableView.isUserInteractionEnabled = true
//
//        }
//
//        //        if (self.refreshControl?.isRefreshing)! {
//        //
//        //            fetchDatas()
//        //            if refreshControl?.isRefreshing {
//        //                //self.refreshControl?.endRefreshing()
//        //            }
//        //
//        ////            if self.commments.count == 0 {
//        ////                self.refreshControl?.endRefreshing()
//        ////
//        ////            }
//        //            self.tableView.isUserInteractionEnabled = true
//        //        }
//
//    }
//
//    func reloadTable() {
//
//
//
//        self.commments = Array(self.commentDict.values)
//
//        self.commments.sort(by: { (c1, c2) -> Bool in
//
//            return (c1.time.intValue) < (c2.time.intValue)
//        })
//
//        //Put all the keys into a separate arrays
//        for com in commments {
//            print("in order \(com.commentKey)")
//            commentIDs.append(com.commentKey)
//        }
//
//        if actInd.isAnimating {
//            self.actInd.stopAnimating()
//        }
//
//
//
//
//        if (refreshControl?.isRefreshing)! {
//            self.refreshControl?.endRefreshing()
//        }
//
//        self.tableView.isUserInteractionEnabled = true
//        self.tableView.reloadData()
//        SVProgressHUD.dismiss()
//
//    }
//
//    func checkExisted() -> Bool {
//        var exist = false
//        self.discussion?.postRef.observeSingleEvent(of: .value, with: { (snapshot) in
//            if snapshot.exists() {
//                exist = true
//                self.fetchDiscussion()
//                self.fetchDiscussionComment()
//            } else {
//
//                let popup = PopupDialog(title: "Unvailable Discussion", message: "Could not perform action", image: nil)
//
//
//                let buttonTwo = CancelButton(title: "OK") {
//                    if (self.refreshControl?.isRefreshing)! {
//                        self.refreshControl?.endRefreshing()
//                    }
//
//                    self.dismiss(animated: true, completion: nil)
//
//                }
//
//                popup.addButtons([buttonTwo])
//                popup.buttonAlignment = .horizontal
//
//                // Present dialog
//                self.present(popup, animated: true, completion: nil)
//
//                //                let alert = UIAlertController(title: "Unvailable Confession", message: "Could not perform action", preferredStyle: .alert)
//                //                //alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
//                //                alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: { (action) in
//                //                    if (self.refreshControl?.isRefreshing)! {
//                //                        self.refreshControl?.endRefreshing()
//                //                    }
//                //
//                //                    self.dismiss(animated: true, completion: nil)
//                //
//                //                }))
//                //
//                //                self.present(alert, animated: true, completion: nil)
//
//                //ErrorAlert().createAlert(title: "Something went wrong", msg: "Could not perform action", object: self)
//
//
//
//            }
//        })
//
//
//        return exist
//    }
//
//    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//        return commments.count
//    }
//
//    func numberOfSections(in tableView: UITableView) -> Int {
//        return 1
//    }
//
//    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
//        return UITableViewCell()
//    }
//
//    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
//        let cell = tableView.dequeueReusableCell(withIdentifier: "DiscussionCell") as? DiscussionCell
////        print(self.discussion.caption)
//        cell?.configureCell(dis: self.discussion)
//        return cell
//
//    }
//
//    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//
//    }

}
