
import Foundation
//
//  DataService.swift
//  dev-social
//
//  Created by Xhien Yi Tan on 6/08/2016.
//  Copyright © 2016 Xavier TanXY. All rights reserved.
//

import Foundation
import Firebase
import FirebaseDatabase
import SystemConfiguration
import FBSDKLoginKit

//import SwiftKeychainWrapper


let DB_BASE = Database.database().reference()
let STORAGE_BASE = Storage.storage().reference()
//let CURRENT_USER_UID = (FIRAuth.auth()?.currentUser!.uid)! Everytime it creates the same uid when logging in and out



class DataService {
    
    static let ds = DataService()
    
    //DB References
    var uniArray = [University]()
    var states = [[University]]()
    var courseArray = [String]()
    var categoryArray = [String]()
    var uniArrayWithoutTop = [University]()
    var currentVisitingUni: String!
    var OS: String!
    
    private var _REF_BASE = DB_BASE
    private var _REF_USERS = DB_BASE.child("users")
    private var _REF_UNIVERSITIES = DB_BASE.child("universities")
    private var _REF_CONFESSIONS = DB_BASE.child("confessions")
    private var _REF_COMMENTS = DB_BASE.child("comments")
    private var _REF_MESSAGES = DB_BASE.child("messages")
    private var _REF_REPORTS = DB_BASE.child("reports")
    private var _REF_COMMENT_REPORTS = DB_BASE.child("comment-reports")
    private var _REF_CONFESSION_REPORTS = DB_BASE.child("confession-reports")
    private var _REF_USER_MESSAGES = DB_BASE.child("user-messages")
    private var _REF_CONFESSION_COMMENTS = DB_BASE.child("confession-comments")
    private var _REF_HASHTAG = DB_BASE.child("hashtag")
    private var _REF_FEEDBACK = DB_BASE.child("feedback")
    private var _REF_NOTIFICATION = DB_BASE.child("notification")
    private var _REF_CONFESSION_NOTIFICATION = DB_BASE.child("confession-notification")
    private var _REF_COMMENTER_COMMENT_NOTIFICATION = DB_BASE.child("commenter-commentID-notiID")
       
    private var _REF_MISSING_UNI = DB_BASE.child("missing-uni")
    private var _REF_MISSING_COURSE = DB_BASE.child("missing-course")
    
    var REF_UserBlock_Uni_Confession = DB_BASE.child("userBlock-uni-confession")
    var REF_UserBlock_PartnerID = DB_BASE.child("userBlock-partnerID")
    var REF_LIKER_CONFESSION_NOTIFICATION = DB_BASE.child("liker-confession-notification")
    var REF_LIKER_COMMENT_NOTIFICATION = DB_BASE.child("liker-comment-notification")
    var REF_CONFESSION_LIKER = DB_BASE.child("confession-liker")
    var REF_UniConfession = DB_BASE.child("uni-confession")
    var REF_UniConfessionCount = DB_BASE.child("uni-confessionCount")
    var REF_User_Confession = DB_BASE.child("user-confession")
    var REF_User_Comment = DB_BASE.child("user-comment")
    var REF_Version = DB_BASE.child("version")
    var REF_Update = DB_BASE.child("update")
    var REF_Category = DB_BASE.child("category")
    var REF_User_Friend = DB_BASE.child("user-friend")
    var PUSH_TOKEN = ""
    
    
    var REF_University_Info = DB_BASE.child("university-info")
    var REF_Course_Info = DB_BASE.child("course-info")
    var REF_Category_Info = DB_BASE.child("category-info")
    var REF_Message_Report = DB_BASE.child("message-report")
    var REF_User_Report = DB_BASE.child("user-report")
    var REF_User_Following_Notification = DB_BASE.child("user-following-notification")
    var currentUser: User!
    
    var REF_Confessions_Discussion = DB_BASE.child("confessions").child("DISCUSSION")
    var REF_Discussion_Comments = DB_BASE.child("discussion-comments")
    
    //Storage reference
    private var _REF_PROFILE_PIC = STORAGE_BASE.child("profilePic")
    private var _REF_CONFESSION_PIC = STORAGE_BASE.child("confessionPic")
    private var _REF_MESSAGE_PIC = STORAGE_BASE.child("messagePic")
    
    var REF_BASE: DatabaseReference {
        return _REF_BASE
    }
    
    var REF_USERS: DatabaseReference {
        return _REF_USERS
    }
    
    var REF_UNIVERSITIES: DatabaseReference {
        return _REF_UNIVERSITIES
    }
    
    var REF_PROFILE_PIC : StorageReference {
        return _REF_PROFILE_PIC
    }
    
    var REF_CONFESSION_PIC : StorageReference {
        return _REF_CONFESSION_PIC
    }
    
    var REF_MESSAGE_PIC : StorageReference {
        return _REF_MESSAGE_PIC
    }
    
    var REF_REPORTS : DatabaseReference {
        return _REF_REPORTS
    }
    
    var REF_CONFESSIONS: DatabaseReference {
        return _REF_CONFESSIONS
    }
    
    var REF_COMMENTS: DatabaseReference {
        return _REF_COMMENTS
    }
    
    var REF_MESSAGES: DatabaseReference {
        return _REF_MESSAGES
    }
    
    var REF_USER_MESSAGES: DatabaseReference {
        return _REF_USER_MESSAGES
    }
    
    var REF_CONFESSION_COMMENTS: DatabaseReference {
        return _REF_CONFESSION_COMMENTS
    }
    
    var REF_COMMENT_REPORTS: DatabaseReference {
        return _REF_COMMENT_REPORTS
    }
    
    var REF_CONFESSION_REPORTS: DatabaseReference {
        return _REF_CONFESSION_REPORTS
    }
    
    var REF_HASHTAG: DatabaseReference {
        return _REF_HASHTAG
    }
    
    var REF_FEEDBACK: DatabaseReference {
        return _REF_FEEDBACK
    }
    
    var REF_MISSING_UNI: DatabaseReference {
        return _REF_MISSING_UNI
    }

    var REF_MISSING_COURSE: DatabaseReference {
        return _REF_MISSING_COURSE
    }
    
    var REF_NOTIFICATION : DatabaseReference {
        return _REF_NOTIFICATION
    }
    
    var REF_CONFESSION_NOTIFICATION : DatabaseReference {
        return _REF_CONFESSION_NOTIFICATION
    }
    
    var REF_COMMENTER_COMMENT_NOTIFICATION : DatabaseReference {
        return _REF_COMMENTER_COMMENT_NOTIFICATION
    }
    
    
    
    func createFirebaseDBUserWithPartial(uid: String, userData: Dictionary<String,String>) {
        
        REF_USERS.child(uid).updateChildValues(userData)
        
    }
    

    func createFirebaseDBUserWithFB(uid: String, userProvider: String, userEmail: String, name: String, userName: String, uni: String, uniShort: String, year: String, course: String, gender: String, completion: @escaping () -> ()){
        
        if((FBSDKAccessToken.current()) != nil) {
            FBSDKGraphRequest(graphPath: "me", parameters: ["fields": "id, name, gender, email"]).start(completionHandler: { (connection, result, error) -> Void in
                if (error == nil){
                    // print(result)
                    let data:[String:AnyObject] = result as! [String : AnyObject]
                    
                    let pictureURL = "https://graph.facebook.com/\(data["id"]!)/picture?type=large&return_ssl_resources=1"
                    // var imgURLString = "http://graph.facebook.com/\(data["id"])/picture?type=large" //type=normal
                    var imgURL = URL(string: pictureURL)
                    var imageData = try! Data(contentsOf: imgURL!)
                    var image = UIImage(data: imageData)
                    
                    // profilePicUrl = ["profilePicUrl": "\(imgURL)"]
                    
                    if let img = image  {
                        if let imgData = UIImageJPEGRepresentation(img, 0.1) {
                            
                            let imgUid = NSUUID().uuidString
                            let metaData = StorageMetadata()
                            metaData.contentType = "image/jpeg"
                            
                          
                            DataService.ds.REF_PROFILE_PIC.child(imgUid).putData(imgData, metadata: metaData) { (metaData, error) in
                                if error != nil {
                                    print("Xavier: upload image to firebase failed")
                                    print(error.debugDescription)
                                } else {
                                    print("Xavier: successfuly upload img to firebase")
                                    let downloadUrl = metaData?.downloadURL()?.absoluteString
                                    if let url = downloadUrl {
                                        
                                        var updateObj = [String:AnyObject]()
                                        let ref = DataService.ds.REF_USERS.child(uid)
                                        
                                        updateObj["/provider"] = userProvider as AnyObject
                                        updateObj["/email"] = userEmail as AnyObject
                                        updateObj["/name"] = name as AnyObject
                                        updateObj["/userName"] = userName as AnyObject
                                        updateObj["/university"] = uni as AnyObject
                                        updateObj["/universityShort"] = uniShort as AnyObject
                                        updateObj["/course"] = course as AnyObject
                                        updateObj["/year"] = year as AnyObject
                                        updateObj["/gender"] = gender as AnyObject
                                        updateObj["/genderCount"] = DEFAULT_COUNT as AnyObject
                                        updateObj["/uniCount"] = DEFAULT_COUNT as AnyObject
                                        updateObj["/courseCount"] = DEFAULT_COUNT as AnyObject
                                        updateObj["/profilePicUrl"] = url as AnyObject
                                        updateObj["/notiCount"] = 0 as AnyObject
                                        updateObj["/OS"] = DataService.ds.OS as AnyObject

                                        
                                        updateObj["/pushToken"] = self.PUSH_TOKEN as AnyObject
                                        
                                        
                                        let timestamp = NSNumber(integerLiteral: Int(NSDate().timeIntervalSince1970))
                                        updateObj["/creationDate"] = timestamp as AnyObject
                                        
                                        var dict = [String: Bool]()
                                        dict["key"] = true
                                        
                                        
                                        updateObj["/follower"] = dict as AnyObject
                                        updateObj["/following"] = dict as AnyObject
                                        updateObj["/msgCount"] = dict as AnyObject
                                        
                                        ref.updateChildValues(updateObj)
                                        
                                        let defaults = UserDefaults.standard
                                        defaults.set(uid, forKey: "uid")
                                        completion()
                                    }
                                    
                                }
                                
                            }
                        }
                        
                    }
                    
                }
            })
            
        }
        
        
        
    }
    
    func createFirebaseUser(uid: String, userProvider: String, userEmail: String, name: String, userName: String, uni: String, uniShort: String, year: String, course: String, gender: String,  completion: @escaping () -> ()) {

        var updateObj = [String:AnyObject]()
        let ref = DataService.ds.REF_USERS.child(uid)
        
        updateObj["/provider"] = userProvider as AnyObject
        updateObj["/email"] = userEmail as AnyObject
        updateObj["/name"] = name as AnyObject
        updateObj["/userName"] = userName as AnyObject
        updateObj["/university"] = uni as AnyObject
        updateObj["/universityShort"] = uniShort as AnyObject
        updateObj["/course"] = course as AnyObject
        updateObj["/year"] = year as AnyObject
        updateObj["/gender"] = gender as AnyObject
        updateObj["/genderCount"] = DEFAULT_COUNT as AnyObject
        updateObj["/uniCount"] = DEFAULT_COUNT as AnyObject
        updateObj["/courseCount"] = DEFAULT_COUNT as AnyObject
        updateObj["/profilePicUrl"] = NO_PIC as AnyObject
        updateObj["/notiCount"] = 0 as AnyObject
        updateObj["/OS"] = DataService.ds.OS as AnyObject

        
        updateObj["/pushToken"] = self.PUSH_TOKEN as AnyObject
        
        let timestamp = NSNumber(integerLiteral: Int(NSDate().timeIntervalSince1970))
        updateObj["/creationDate"] = timestamp as AnyObject
        
        var dict = [String: Bool]()
        dict["key"] = true

        
        updateObj["/follower"] = dict as AnyObject
        updateObj["/following"] = dict as AnyObject
        updateObj["/msgCount"] = dict as AnyObject
        
        ref.updateChildValues(updateObj)
        
        let defaults = UserDefaults.standard
        defaults.set(uid, forKey: "uid")
        
        completion()

    }
    
    func create(uid: String, userProvider: Dictionary<String,String>, userEmail: Dictionary<String,String>){
        REF_USERS.child(uid).updateChildValues(userProvider)
        REF_USERS.child(uid).updateChildValues(userEmail)
    }
    
    func removeInstanceID() {
        let ref = DataService.ds.REF_USERS.child((Auth.auth().currentUser?.uid)!)
        var updateObj = ["/pushToken":""]
        ref.updateChildValues(updateObj)
    }
    
    func addInstanceID() {
        let ref = DataService.ds.REF_USERS.child((Auth.auth().currentUser?.uid)!)
        var updateObj = ["/pushToken":"\(self.PUSH_TOKEN)"]
        ref.updateChildValues(updateObj)
    }
    
    func convertUniNameToShort(uniName: String) -> String {
        var retVal = ""
        
        for uni in uniArray {
            if uni.name == uniName {
                retVal = uni.shortName
                break
            }
        }
        
        return retVal
    }
   
    func updateUniversity(uid: Dictionary<String,Bool>, uni: University) {
        
//        var uniDict = [String: String]()
//        uniDict["shortName"] = uni.shortName
        let id = uni.name
      //  REF_UNIVERSITIES.child(id).updateChildValues(uniDict)
        REF_UNIVERSITIES.child(id).child("users").updateChildValues(uid)
    }
    
    func connectedToNetwork() -> Bool {
        
        var zeroAddress = sockaddr_in()
        zeroAddress.sin_len = UInt8(MemoryLayout<sockaddr_in>.size)
        zeroAddress.sin_family = sa_family_t(AF_INET)
        
        guard let defaultRouteReachability = withUnsafePointer(to: &zeroAddress, {
            $0.withMemoryRebound(to: sockaddr.self, capacity: 1) {
                SCNetworkReachabilityCreateWithAddress(nil, $0)
            }
        }) else {
            return false
        }
        
        var flags: SCNetworkReachabilityFlags = []
        if !SCNetworkReachabilityGetFlags(defaultRouteReachability, &flags) {
            return false
        }
        
        let isReachable = flags.contains(.reachable)
        let needsConnection = flags.contains(.connectionRequired)
        
        return (isReachable && !needsConnection)
    }
    
    
    
    
    
    
    
}
