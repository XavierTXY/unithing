//
//  SelfConfessionVC.swift
//  UniThing
//
//  Created by Xavier TanXY on 1/7/17.
//  Copyright © 2017 Xavier TanXY. All rights reserved.
//

import UIKit
import Firebase
import SVProgressHUD
import FirebaseDatabase
import SDWebImage
import PopupDialog

class SelfConfessionVC: UIViewController, UITableViewDelegate, UITableViewDataSource,UIImagePickerControllerDelegate, UINavigationControllerDelegate, UITabBarControllerDelegate {

    
    
    @IBOutlet weak var tableView: UITableView!
    
    var refreshControl: UIRefreshControl!
    var actInd: UIActivityIndicatorView!
//    var headerInd: UIActivityIndicatorView!
//    var initialLbl: UILabel!
    var imagePick: UIImagePickerController!
    var confessions = [Confession]()
    var confessionDict = [String: Confession]()
    var timer: Timer?
    
    var isNavHiding = false
    
    
//    var stretchyHeaderView: GSKStretchyHeaderView!
    
    var user: User?
    
    var emptyView: NoPostsView!
    var scrollToTop: Bool!
    
    var firstTime: Bool!
    
    @IBOutlet weak var profilePic: UIImageView!
    @IBOutlet weak var initialLbl: UILabel!
    @IBOutlet weak var displayNameLbl: UILabel!
    @IBOutlet weak var userNameLbl: UILabel!
    @IBOutlet weak var detailsLbl: UILabel!
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var outerView: UIView!
    
    @IBOutlet weak var numFollowerLbl: UILabel!
    @IBOutlet weak var numFollowingLbl: UILabel!
    @IBOutlet weak var numPostLbl: UILabel!
    
    @IBOutlet weak var followingStack: UIStackView!
    @IBOutlet weak var followerStack: UIStackView!
    override func viewDidLoad() {
        super.viewDidLoad()

//        SVProgressHUD.show()
        
        firstTime = true
        
        let rightBarItem = UIBarButtonItem(image: #imageLiteral(resourceName: "settings"), style: .plain, target: self, action: "settingTapped")
//        rightBarItem.tintColor = UIColor(red: 0/255, green: 105/255, blue: 192/255, alpha: 1)
        self.navigationItem.setRightBarButton(rightBarItem, animated: true)
        
        scrollToTop = true
        
        imagePick = UIImagePickerController()
        imagePick.allowsEditing = true
        imagePick.delegate = self
        
        tableView.delegate = self
        tableView.dataSource = self
        tableView.tableFooterView = UIView()
        tableView.estimatedRowHeight = 400
        tableView.rowHeight = UITableViewAutomaticDimension
    
        
        refreshControl = UIRefreshControl()
        refreshControl.backgroundColor = UIColor.clear
        refreshControl.tintColor = PURPLE
        refreshControl.addTarget(self, action: "refresh", for: UIControlEvents.valueChanged)
        //refreshControl.beginRefreshing()
        tableView.addSubview(refreshControl)
        
        self.navigationItem.title = ""
        self.navigationController?.navigationBar.barTintColor = UIColor.white
        self.navigationController?.navigationBar.titleTextAttributes = [ NSFontAttributeName: UIFont(name: "HelveticaNeue-Bold", size: 16)!]
        self.navigationController?.navigationBar.layer.borderColor = BORDER_COLOR_CGCOLOR
        self.navigationController?.navigationBar.tintColor = UIColor.white
        
        self.navigationController?.navigationBar.titleTextAttributes = [ NSForegroundColorAttributeName: UIColor.white]
        
        
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationController?.view.backgroundColor = .clear
        
        
//        emptyView = EmptyConfessionView.instanceFromNib() as! EmptyConfessionView
        emptyView = NoPostsView.instanceFromNib() as! NoPostsView
        emptyView.frame = self.tableView.bounds
//        self.tableView.addSubview(emptyView)
//        self.showEmptyView()

        self.hideEmptyView()
        

        
        
        getUserDetails()

        
//        let nibViews = Bundle.main.loadNibNamed("Header", owner: self, options: nil)
//        self.stretchyHeaderView = nibViews?.first as! GSKStretchyHeaderView
//        self.stretchyHeaderView.minimumContentHeight = 150 // you can replace the navigation bar with a stretchy header view
//        self.stretchyHeaderView.maximumContentHeight = 250
//        headerInd =  self.stretchyHeaderView.viewWithTag(6) as! UIActivityIndicatorView
//        self.tableView.addSubview(self.stretchyHeaderView)
        
        let ppTap = UITapGestureRecognizer(target: self, action: #selector(profilePicTapped))
        ppTap.numberOfTapsRequired = 1
        profilePic.addGestureRecognizer(ppTap)
        profilePic.isUserInteractionEnabled = true
        
        profilePic.layer.cornerRadius = profilePic.frame.height / 2
        profilePic.clipsToBounds = true
        outerView.layer.cornerRadius = outerView.frame.height / 2
        outerView.clipsToBounds = true
        
        initFooter()
        fetchData()
        
        if #available(iOS 11.0, *) {
            self.tableView.contentInsetAdjustmentBehavior = .never
        }
        
        let following = UITapGestureRecognizer(target: self, action: #selector(followingTapped))
        let follower = UITapGestureRecognizer(target: self, action: #selector(followerTapped))
        following.numberOfTapsRequired = 1
        follower.numberOfTapsRequired = 1
        self.followerStack.addGestureRecognizer(follower)
        self.followerStack.isUserInteractionEnabled = true
        self.followingStack.addGestureRecognizer(following)
        self.followingStack.isUserInteractionEnabled = true

     
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.slideMenuController()?.removeLeftGestures()
//self.navigationController?.navigationBar.isTranslucent = true
        UIApplication.shared.statusBarStyle = .lightContent
        tabBarController?.delegate = self
        getUserDetails()
        
        if !firstTime {
            attemptToReloadTable()
        }
        
        self.navigationController?.navigationBar.tintColor = UIColor.white
        
        self.navigationController?.navigationBar.titleTextAttributes = [ NSForegroundColorAttributeName: UIColor.white]
        


//        fetchData()
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        

    }
    
    override func viewWillDisappear(_ animated: Bool) {
        
        super.viewWillDisappear(true)
//        self.navigationController?.navigationBar.isTranslucent = false
        scrollToTop = false

    }
    
    var mode: String!
    func followingTapped() {
        
        if numFollowingLbl.text != "0" {
            if isNavHiding {
                unhideNav()
            }
            
            mode = FOLLOWING
            performSegue(withIdentifier: "FollowerVC", sender: nil)
        }

    }
    
    func followerTapped() {
        
        if numFollowerLbl.text != "0" {
            if isNavHiding {
                unhideNav()
            }
            
            mode = FOLLOWER
            performSegue(withIdentifier: "FollowerVC", sender: nil)
        }

    }
    func tabBarController(_ tabBarController: UITabBarController, didSelect viewController: UIViewController) {
        if self.tabBarController?.selectedIndex == 3 {
            if scrollToTop {
                self.tableView.scrollRectToVisible(CGRect(x: 0, y: 0, width: 1, height: 1), animated: true)
            } else {
                scrollToTop = true
            }
            
            
        }
    
        
    }
    
    func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
        if(velocity.y>0) {
            //Code will work without the animation block.I am using animation block incase if you want to set any delay to it.
            UIView.animate(withDuration: 2.5, delay: 0, options: UIViewAnimationOptions(), animations: {
                self.hideNav()
                //                self.navigationController?.setToolbarHidden(true, animated: true)
                print("Hide")
            }, completion: nil)
            
        } else {
            UIView.animate(withDuration: 2.5, delay: 0, options: UIViewAnimationOptions(), animations: {
                self.unhideNav()
                //                self.navigationController?.setToolbarHidden(false, animated: true)
                print("Unhide")
            }, completion: nil)
        }
    }
    
    func hideNav() {
        isNavHiding = true
        titleLbl.isHidden = true
        self.navigationController?.setNavigationBarHidden(true, animated: true)
    }
    
    func unhideNav() {
        isNavHiding = false
        titleLbl.isHidden = false
        self.navigationController?.setNavigationBarHidden(false, animated: true)
    }

    func settingTapped() {
        if isNavHiding {
            unhideNav()
        }
        
        performSegue(withIdentifier: "MoreVC", sender: nil)
        
    }
    
    func reloadDeletedConfessionFromPreviousVC(confession: Confession) {

        self.totalConfession = self.totalConfession! - 1
        confessionDict.removeValue(forKey: confession.confessionKey)
        attemptToReloadTable()
//
//        self.tableView.isUserInteractionEnabled = false
//        self.timer = Timer.scheduledTimer(timeInterval: 0.5, target: self, selector: #selector(avoidUserMultipleTap), userInfo: nil, repeats: false)
        
    }
    
    func getUserDetails() {
        DataService.ds.REF_USERS.child((Auth.auth().currentUser?.uid)!).observeSingleEvent(of: .value, with: { (snapshot) in

            if let snapshots = snapshot.children.allObjects as? [DataSnapshot] {
                //print("\(snapshot.value)")
                print("user details changed")
                
                if let userDict = snapshot.value as? Dictionary<String, AnyObject> {
                    let key = snapshot.key
                    self.user = User(userKey: key, userData: userDict)
                    self.setupUserDetails()
                    
                    let defaults = UserDefaults.standard
                    defaults.set(self.user?.userName, forKey: "userName")
                    defaults.set(self.user?.name, forKey: "name")
                    defaults.set(self.user?.profilePicUrl, forKey: "profilePicUrl")
                    
                    // self.imgUrl = self.user?.profilePicUrl
                    defaults.synchronize()
                    
                }
                

            }
            
        })
    }
    
    
    func showInitial() {
        profilePic.image = nil
        
        profilePic.backgroundColor = ColorHelper().pickColor(alphabet: Character(initialLbl.text!))
        initialLbl.isHidden = false
    }
    
    func hideInitial() {
        profilePic.backgroundColor = UIColor.clear
        initialLbl.isHidden = true
    }
    
    func setupUserDetails() {
        

        
        displayNameLbl.text = user?.name
        userNameLbl.text = "@\((user?.userName)!)"
        
        detailsLbl.text = "\(user!.universityShort) - \(user!.course)"
        initialLbl.text = String((user?.userName[(user?.userName.startIndex)!])!).capitalized
        
        if ( (self.user?.following.count)! - 1 ) >= 0 {
            numFollowingLbl.text = "\((self.user?.following.count)! - 1)"
        } else {
            numFollowingLbl.text = "0"
        }
        
        if ( (self.user?.follower.count)! - 1 ) >= 0 {
            numFollowerLbl.text = "\((self.user?.follower.count)! - 1)"
        } else {
            numFollowerLbl.text = "0"
        }
        
        if let profilePicUrl = self.user?.profilePicUrl {

            let url = profilePicUrl as! String
            if url != NO_PIC {
                hideInitial()
                profilePic.loadImageUsingCacheWithUrlStringWithoutIndex(imageUrl: url)
            } else {

                showInitial()


            }

        }
    }
    
//    func setupUserDetails() {
//        var displayNameLbl =  self.stretchyHeaderView.viewWithTag(1) as! UILabel
//        displayNameLbl.text = user?.name
//
//
//        let outerView =  self.stretchyHeaderView.viewWithTag(7) as! UIView
//        outerView.clipsToBounds = false
//        outerView.layer.shadowColor = UIColor.black.cgColor
//        outerView.layer.shadowOpacity = 0.2
//        outerView.layer.shadowOffset = CGSize.zero
//        outerView.layer.cornerRadius = outerView.frame.height / 2
//        outerView.layer.shadowRadius = 3
//        outerView.layer.shadowPath = UIBezierPath(roundedRect: outerView.bounds, cornerRadius: outerView.frame.height / 2).cgPath
//
////        let myImage = UIImageView(frame: outerView.bounds)
////        myImage.clipsToBounds = true
////        myImage.layer.cornerRadius = 10
//
//
//        profilePic =  self.stretchyHeaderView.viewWithTag(2) as! UIImageView
////        profilePic.frame = outerView.bounds
//        profilePic.layer.cornerRadius = profilePic.frame.height / 2
//        profilePic.clipsToBounds = true
//        let likeTap = UITapGestureRecognizer(target: self, action: #selector(profilePicTapped))
//        likeTap.numberOfTapsRequired = 1
//        profilePic.addGestureRecognizer(likeTap)
//        profilePic.isUserInteractionEnabled = true
//
//        var userNameLbl =  self.stretchyHeaderView.viewWithTag(9) as! UILabel
//        userNameLbl.text = "@\((user?.userName)!)"
//
//        initialLbl =  self.stretchyHeaderView.viewWithTag(8) as! UILabel
//        initialLbl.text = String((user?.userName[(user?.userName.startIndex)!])!).capitalized
//
//        if let profilePicUrl = self.user?.profilePicUrl {
//
//            let url = profilePicUrl as! String
//            if url != NO_PIC {
////                profilePic.isHidden = false
//                hideInitial()
//                profilePic.loadImageUsingCacheWithUrlStringWithoutIndex(imageUrl: url)
//            } else {
//
//                showInitial()
//
//
//            }
//
//        }
////        outerView.addSubview(profilePic)
//
//
//
//        var uniLbl =  self.stretchyHeaderView.viewWithTag(3) as! UILabel
//        uniLbl.text = "\(user!.universityShort) - "
//        var courseLbl =  self.stretchyHeaderView.viewWithTag(4) as! UILabel
//        courseLbl.text = user?.course
//
//
//
////        var sexImg =  self.stretchyHeaderView.viewWithTag(5) as! UIImageView
////
////        if user?.gender == MALE {
////            sexImg.image = UIImage(named: "male")
////        } else if user?.gender == FEMALE {
////            sexImg.image = UIImage(named: "female")
////        }
//    }
    
    func profilePicTapped(_ sender: UITapGestureRecognizer) {
        
        let alert = UIAlertController()
        
        if profilePic.image == nil {
            
            alert.addAction(UIAlertAction(title: "Camera", style: UIAlertActionStyle.default, handler:{ (UIAlertAction)in
                self.imagePick.sourceType = UIImagePickerControllerSourceType.camera;
                self.present(self.imagePick, animated: true, completion: nil)
            }))

            alert.addAction(UIAlertAction(title: "Photo library", style: UIAlertActionStyle.default, handler:{ (UIAlertAction)in
                self.imagePick.sourceType = UIImagePickerControllerSourceType.photoLibrary;
                self.present(self.imagePick, animated: true, completion: nil)
            }))

            alert.addAction(UIAlertAction(title: "Cancel", style: .cancel) { (action:UIAlertAction!) in

            })

            self.present(alert, animated: true, completion: nil)
            
            

            
        } else {
            
            alert.addAction(UIAlertAction(title: "Choose another picture", style: UIAlertActionStyle.default, handler:{ (UIAlertAction)in
                self.present(self.imagePick, animated: true, completion: nil)
            }))
            
            alert.addAction(UIAlertAction(title: "Remove current profile picture", style: .destructive , handler:{ (UIAlertAction)in
                self.showInitial()
                SVProgressHUD.show()
                self.deleteImageInStorage()
                self.postToFirebase(imgUrl: NO_PIC)
                
            }))
            
            alert.addAction(UIAlertAction(title: "Cancel", style: .cancel) { (action:UIAlertAction!) in
                
            })
            
            self.present(alert, animated: true, completion: nil)
        }
        
    
    }
    
    func postToFirebase(imgUrl: String) {
//        SVProgressHUD.dismiss()
        self.user?.profilePicUrl = imgUrl
        
        if imgUrl != NO_PIC {
            hideInitial()
            self.profilePic.loadImageUsingCacheWithUrlStringWithoutIndex(imageUrl: imgUrl)
        } else {
            showInitial()
        }
        self.tableView.isUserInteractionEnabled = true
        let profilePicUrl = ["profilePicUrl": imgUrl]
        DataService.ds.REF_USERS.child((Auth.auth().currentUser?.uid)!).updateChildValues(profilePicUrl)
//        changeAllComments(newImgUrl: imgUrl)
        updatePicInDB(newImgUrl: imgUrl)
        
        
        
        
    }
    
    func updatePicInDB(newImgUrl: String) {
        let ref = DB_BASE
        
        var uid = (Auth.auth().currentUser?.uid)!
        var updateObj = [String:AnyObject]()
        
        //update comment
        DataService.ds.REF_User_Comment.child(uid).observeSingleEvent(of: .value, with: { (snapshot) in
            
            if let snapshots = snapshot.children.allObjects as? [DataSnapshot] {
                
                for snap in snapshots {
                    
                    
                    if let commentDict = snap.value as? Dictionary<String, AnyObject> {
                        
                        
                        let key = snap.key
                        let comment = Comment(commentKey: key, postData: commentDict)
                        
                        
                        
                        updateObj["confession-comments/\(comment.confessionKey)/\(comment.commentKey)/profilePicUrl"] = newImgUrl as AnyObject
                        
                        
                        
                    }
                }
                
                //update confession
                DataService.ds.REF_User_Confession.child(uid).observeSingleEvent(of: .value, with: { (snapshot) in
                    
                    if let snapshots = snapshot.children.allObjects as? [DataSnapshot] {
                        
                        for snap in snapshots {
                            
                            
                            if let confessionDict = snap.value as? Dictionary<String, AnyObject> {
                                
                                
                                let key = snap.key
                                let confession = Confession(confessionKey: key, postData: confessionDict)
                                
                                
                                updateObj["user-confession/\(uid)/\(confession.confessionKey)/authorProfilePicUrl"] = newImgUrl as AnyObject
                                updateObj["uni-confession/\((confession.university))/\(confession.confessionKey)/authorProfilePicUrl"] = newImgUrl as AnyObject
                                updateObj["confessions/\(confession.confessionKey)/authorProfilePicUrl"] = newImgUrl as AnyObject
                                
                                if let c = confession.category {
                                    updateObj["category/\(c)/\(confession.confessionKey)/authorProfilePicUrl"] = newImgUrl as AnyObject
                                }
                                
                                if !confession.hashDict.isEmpty {
                                    
                                    for hash in confession.hashDict {
                                        updateObj["hashtag/\(hash.key)/\(confession.confessionKey)/authorProfilePicUrl"] = newImgUrl as AnyObject
                                        
                                    }
                                    
                                }
                                
                                
                                
                                
                                
                            }
                        }
                        
                        //update here
                        ref.updateChildValues(updateObj)
                        SVProgressHUD.dismiss()
                        
                    }
                    
                    
                })
                
            }
            
            
        })
    }
    
    func changeAllComments(newImgUrl: String) {
        let ref = DB_BASE
        
        var uid = (Auth.auth().currentUser?.uid)!
        var updateObj = [String:AnyObject]()
        DataService.ds.REF_User_Comment.child(uid).observeSingleEvent(of: .value, with: { (snapshot) in
            
            if let snapshots = snapshot.children.allObjects as? [DataSnapshot] {
                
                for snap in snapshots {
                    
                    
                    if let commentDict = snap.value as? Dictionary<String, AnyObject> {
                        
                        
                        let key = snap.key
                        let comment = Comment(commentKey: key, postData: commentDict)
                        
                        
                        
                        updateObj["confession-comments/\(comment.confessionKey)/\(comment.commentKey)/profilePicUrl"] = newImgUrl as AnyObject
                        
                        
                        
                    }
                }
                
                //update here
                ref.updateChildValues(updateObj)
                
            }
            
            
        })
    }
    
    func addProfilePic() {
        SVProgressHUD.show()
        
        if profilePic.image == nil {
            self.postToFirebase(imgUrl: NO_PIC)
        } else {
            if let img = profilePic.image  {
                if let imgData = UIImageJPEGRepresentation(img, 0.1) {
                    
                    let imgUid = NSUUID().uuidString
                    let metaData = StorageMetadata()
                    metaData.contentType = "image/jpeg"
                    
                    DataService.ds.REF_PROFILE_PIC.child(imgUid).putData(imgData, metadata: metaData) { (metaData, error) in
                        if error != nil {
                            print("Xavier: upload image to firebase failed")
                            print(error.debugDescription)
                        } else {
                            print("Xavier: successfuly upload img to firebase")
                            let downloadUrl = metaData?.downloadURL()?.absoluteString
                            if let url = downloadUrl {
                                self.postToFirebase(imgUrl: url)
                            }
                            
                        }
                        
                    }
                }
                
            }
        }
    }
    
    func deleteImageInStorage() {
        
        var imgUrl = (self.user?.profilePicUrl)!
        SDImageCache.shared().removeImage(forKey: (imgUrl as NSString) as String!)
        
        if self.user?.profilePicUrl != "NoPicture" {
            let ref = Storage.storage().reference(forURL: imgUrl)
            ref.delete(completion: { (error) in
                if error != nil {
                    print("there is an error deleting picture in fb storage")
                } else {
                    print("delete picture successfully in fb storage")
                }
            })
        }
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : AnyObject]) {
        
        
//        if let editedImage = info[UIImagePickerControllerEditedImage] as? UIImage {
        
            if let image = info[UIImagePickerControllerEditedImage] as? UIImage {
                profilePic.image = image
                
                addProfilePic()
            }
            
//        }
        
        /// if the request successfully done just dismiss
        imagePick.dismiss(animated: true, completion: nil)
        
    }
    
    func showEmptyView() {
//        emptyView.isHidden = false
//        self.tableView.bringSubview(toFront: emptyView)
//        emptyConfessionView.isHidden = false

    }

    func hideEmptyView() {
//        emptyView.isHidden = true
//        self.tableView.sendSubview(toBack: emptyView)
//        emptyConfessionView.isHidden = true
        
    }

    func initFooter() {
        
        actInd = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.gray)
        actInd.frame = CGRect(x: 0, y: 0, width: 320, height: 40)
        actInd.hidesWhenStopped = true
        //        self.tableView.tableFooterView = actInd
    }
    
    
   // var uniName = "Australian National University"
    var numberOfObjectsinArray = 6
    func fetchData() {
        
//        headerInd.isHidden = false
//        headerInd.startAnimating()
        
        getTotalConfession()
        

            
        DataService.ds.REF_User_Confession.child((Auth.auth().currentUser?.uid)!).queryLimited(toLast: UInt(self.numberOfObjectsinArray)).observeSingleEvent(of: .value, with: { (snapshot) in
                self.confessionDict.removeAll()
                self.confessions = []
                
            if let snapshots = snapshot.children.allObjects as? [DataSnapshot] {
                    for snap in snapshots {
                        //print("SNAP: \(snap)")
                        if let confessionDict = snap.value as? Dictionary<String, AnyObject> {
                            let key = snap.key
                            let confession = Confession(confessionKey: key, postData: confessionDict)
                            
                            //self.confessions.append(confession)
                            
                            
                            self.confessionDict[key] = confession
                            
                            
                            self.attemptToReloadTable()
                            
                            
                        }
                    }
                    
                }
                
                
                
                
            })
            
//            DataService.ds.REF_CONFESSIONS.queryOrdered(byChild: "university").queryEqual(toValue: self.uniName).observe(.childRemoved, with: { (snapshot) in
//                
//                self.confessionDict.removeValue(forKey: snapshot.key)
//                self.totalConfession = self.totalConfession! - 1
//                
//                
//            })
        

    }
    
    var totalConfession: Int?
    
    private func getTotalConfession() {
        
    
        DataService.ds.REF_User_Confession.child((Auth.auth().currentUser?.uid)!).observeSingleEvent(of: .value, with: { (snapshot) in
            //.observe(.value, with: { (snapshot) in
            //
            
            if let snapshots = snapshot.children.allObjects as? [DataSnapshot] {
                self.totalConfession = snapshots.count
                print("total confession \(self.totalConfession)")
                self.numPostLbl.text = "\(self.totalConfession!)"
               
                if self.totalConfession == 0 {
                    self.refreshControl.endRefreshing()
                    self.confessions.removeAll()
                    self.confessionDict.removeAll()
                    self.reloadAndSortTable()
                }
                
            }
            
            
            SVProgressHUD.dismiss()
            
            
            
        })
       

    }
    
    func attemptToReloadTable() {
        //      self.timer?.invalidate()
        
        //        self.timer = Timer.scheduledTimer(timeInterval: 0.5, target: self, selector: #selector(reloadAndSortTable), userInfo: nil, repeats: false)
        //
        firstTime = false
        reloadAndSortTable()
    }
    
    func reloadAndSortTable() {
        // self.refreshControl.endRefreshing()
        
        self.confessions = Array(self.confessionDict.values)
        //
        self.confessions.sort(by: { (c1, c2) -> Bool in
            
            return (c1.time.intValue) > (c2.time.intValue)
        })
        
//
//        if headerInd != nil {
//            if headerInd.isAnimating {
//                headerInd.isHidden = true
//                headerInd.stopAnimating()
//            }
//        }

        
        if refreshControl != nil {
            if refreshControl.isRefreshing {
                refreshControl.endRefreshing()
            }
        }

        if actInd != nil {
            if actInd.isAnimating {
                actInd.stopAnimating()
            }
        }

        
        if tableView != nil {
            self.tableView.reloadData()
            self.tableView.isUserInteractionEnabled = true
        }

        self.tabBarController?.tabBar.isUserInteractionEnabled = true
        
        if self.confessions.count == 0 {
//            self.showEmptyView()
        } else {
//            self.hideEmptyView()
        }
        
        SVProgressHUD.dismiss()
        
    }

    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return confessions.count + 1
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "DetailCell") as? DetailCell
        
        if indexPath.row == confessions.count  {
            var confession = Confession()
//            print((self.user?.creationDate)!)
            
            if let time = self.user?.creationDate {
                cell?.configureCell(confession: confession, time:time , last: true)
            } else {
                cell?.configureCell(confession: confession, time:0.0 , last: true)
            }
            
            return cell!
        } else {
            if indexPath.row <= confessions.count - 1 {
                var confession = confessions[indexPath.row]
                cell?.configureCell(confession: confession, time: 0.0, last: false)
                return cell!
            } else {
                return UITableViewCell()
            }

        }

//        //let gender = confession.authorGender
//        let seconds = confession.time.doubleValue
//        let timesStampDate = NSDate(timeIntervalSince1970: seconds)
//        var timeAgo:String =  DateHelper().timeAgoSinceDate(timesStampDate as Date, currentDate: NSDate() as Date, numericDates: true)
//
//        var dateFormatter = DateFormatter()
//
//        dateFormatter.dateFormat = "dd, MMM"
//
//
//        //  if !self.refreshControl.isRefreshing {
//
//        if confession.imageUrl == NO_PIC {
//            let cell = tableView.dequeueReusableCell(withIdentifier: "DetailCell") as? DetailCell
////            cell?.selfConfessionVC = self
////            cell?.configureCell(confession: confession)
////            cell?.dateLabel.text = "\(confession.faculty)・\(timeAgo)"
//
//
//            return cell!
//        }
//        else {
//
//
//            let cell2 = tableView.dequeueReusableCell(withIdentifier: "ConfessionPicCell") as? ConfessionPicCell
//
//            cell2?.confessionPic.image = nil
//
//
//
//            cell2?.confessionPic.loadImageUsingCacheWithUrlString(imageUrl: confession.imageUrl!, tv: self.tableView, index: indexPath)
//
//
//
//            cell2?.configureCell(confession: confession)
//            cell2?.dateLabel.text = "\(confession.faculty)・\(timeAgo)"
//            //                cell2?.confessionPic.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 200)
//
////
////            if gender == MALE {
////                cell2?.profilePic.image = UIImage(named: "male")
////            } else if gender == FEMALE {
////                cell2?.profilePic.image = UIImage(named: "female")
////            }
//
//
//
//
//            cell2?.selfConfessionVC = self
//            return cell2!
//        }
    }
    
    var currentConfessionId: String!
    var currentConfession: Confession?
    var currentIndex: Int?
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if isNavHiding {
            unhideNav()
        }
        
        if indexPath.row != confessions.count {
            var newIdx = indexPath.row
            self.currentConfessionId  = confessions[newIdx].confessionKey
            self.currentConfession = confessions[newIdx]
            currentIndex = newIdx
            
            self.currentConfession?.postRef.observeSingleEvent(of: .value, with: { (snapshot) in
                if snapshot.exists() {
                    
                    self.performSegue(withIdentifier: "Comment3VC", sender: nil)
                } else {
                    ErrorAlert().createAlert(title: "Something went wrong", msg: "Could not perform action", object: self)
                }
            })
        }

    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if indexPath.row == self.confessions.count - 1 {
            print("in the end")
            
            // self.tableView.isUserInteractionEnabled = false
            self.tableView.tableFooterView = actInd
            self.actInd.startAnimating()
            // self.tableView.reloadData()
            if totalConfession! > confessions.count {
                self.timer = Timer.scheduledTimer(timeInterval: 0.5, target: self, selector: #selector(self.addMoreObjects), userInfo: nil, repeats: false)
                
                
            } else {
                self.tableView.isUserInteractionEnabled = true
                self.actInd.stopAnimating()
                self.tableView.tableFooterView = UIView()
            }
            
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let destinationVC = segue.destination as? Comment3VC {
            
//            destinationVC.confession = self.currentConfession!
            destinationVC.confessionId = self.currentConfessionId!
           // destinationVC.delegate = self
            //            destinationVC.passDeletedConfessionDelegate = self
           // destinationVC.passReloadCommentConfessionDelegate = self
            destinationVC.uniName = self.user?.university

            
        }
        
        if let destinationVC = segue.destination as? MoreVC {
            
            destinationVC.user = self.user
//            destinationVC.selfVC = self
            
        }
        
        if let destinationVC = segue.destination as? FollowerVC {
            destinationVC.mode = self.mode
            destinationVC.userVisiting = DataService.ds.currentUser
            
            
        }
    }
    
    func reloadView(confession: Confession) {
        confessionDict[confession.confessionKey] = confession
        attemptToReloadTable()
    }
    
    func reloadLike(confession: Confession, reloadLike: Bool) {
        
        let vc = self.tabBarController?.viewControllers?[0] as! UINavigationController
        
        for vc2 in vc.viewControllers {
            if vc2 is ConfessionVC {
                let confessionVC = vc2 as! ConfessionVC
                if confessionVC.currentVisitingUni.name == confession.university {
                  confessionVC.reloadLikeFromHashtagVC(confession: confession, reloadLike: reloadLike)
                } else {
                    if confessionVC.confessionDict[confession.confessionKey] != nil {
                        confessionVC.reloadLikeFromHashtagVC(confession: confession, reloadLike: reloadLike)
                    }
                }
                
            }
            
            if vc2 is HashtagVC {
                let hashVC = vc2 as! HashtagVC
                if self.user?.university == confession.university {
                    hashVC.confessionDict[confession.confessionKey] = confession
                    hashVC.attemptToReloadTable()
                }

                
            }
        }
        
        /* Reload search vc's hash vc confession from comment vc */
        let navVC = self.tabBarController?.viewControllers?[1] as! UINavigationController
        
        for vcs in navVC.viewControllers {
            
            if vcs is HashtagVC {
                print("reload in hashtag22")
                let hashVC = vcs as! HashtagVC
                if self.user?.university == confession.university {
                    hashVC.confessionDict[confession.confessionKey] = confession
                    hashVC.attemptToReloadTable()
                }

            }
            
        }
    }
    
    func reload() {
        self.tableView.reloadData()
    }
    
    func reloadCommentFromPreviousVC(confession: Confession) {
        
        confessionDict[confession.confessionKey] = confession
        attemptToReloadTable()
        
    }
    
    func refresh() {
        
//        headerInd.isHidden = false
//        headerInd.startAnimating()
        
        let ref = DataService.ds.REF_USERS.child((Auth.auth().currentUser?.uid)!).child("university")
        ref.removeAllObservers()
        
        // self.tableView.isScrollEnabled = false
        self.tableView.isUserInteractionEnabled = false
        
        Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(reloadDataWhenPulled), userInfo: nil, repeats: false)
        
    }
    
    func reloadDataWhenPulled() {
        
        if self.refreshControl.isRefreshing {
            
            // reloadTable()
            //  self.tableView.reloadData()
            getUserDetails()
            fetchData()
            // self.refreshControl.endRefreshing()
            
            
            
        }
        
    }
    
    func addMoreObjects() {
        
        numberOfObjectsinArray += 6
        DataService.ds.REF_User_Confession.child((Auth.auth().currentUser?.uid)!).queryLimited(toLast: UInt(self.numberOfObjectsinArray)).observeSingleEvent(of: .value, with: { (snapshot) in
      //  DataService.ds.REF_CONFESSIONS.queryOrdered(byChild: "university").queryEqual(toValue: self.uniName).queryLimited(toLast: UInt(numberOfObjectsinArray)).observeSingleEvent(of: .value, with: { (snapshot) in
            
            
            if let snapshots = snapshot.children.allObjects as? [DataSnapshot] {
                for snap in snapshots {
                    //print("SNAP: \(snap)")
                    if let confessionDict = snap.value as? Dictionary<String, AnyObject> {
                        let key = snap.key
                        let confession = Confession(confessionKey: key, postData: confessionDict)
                        // print(confession.caption)
                        
                        self.confessionDict[key] = confession
                        
                        
                        self.attemptToReloadTable()
                        
                        
                        
                    }
                }
                
            }
            
            
            
            
            self.actInd.stopAnimating()
            //  self.tableView.tableFooterView? = UIView()
            
            
        })
    }
    
    var startingImgView: UIImageView?
    var startingFrame: CGRect?
    var blackBlackgroundView: UIView?
    
    func performZoomInForStartingImageView(startingImageView: UIImageView) {
        print("zoom in")
        
        startingFrame = startingImageView.superview?.convert(startingImageView.frame, to: nil)
        startingImgView = startingImageView
        startingImgView?.isHidden = true
        
        let zoomingImageView = UIImageView(frame: startingFrame!)
        zoomingImageView.contentMode = .scaleAspectFit
        zoomingImageView.clipsToBounds = true
        zoomingImageView.image = startingImageView.image
        zoomingImageView.isUserInteractionEnabled = true
        zoomingImageView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(handleZoomOut)))
        
        
        
        if let keyWindow = UIApplication.shared.keyWindow {
            blackBlackgroundView = UIView(frame: keyWindow.frame)
            blackBlackgroundView?.backgroundColor = UIColor.black
            blackBlackgroundView?.alpha = 0
            keyWindow.addSubview(blackBlackgroundView!)
            keyWindow.addSubview(zoomingImageView)
            
            
            UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
                self.blackBlackgroundView?.alpha = 1
                //self.inputContainerView.alpha = 0
                let height = (self.startingFrame?.height)! / (self.startingFrame?.width)! * keyWindow.frame.width
                
                //zoomingImageView.frame = CGRect(x: 0, y: 0, width: keyWindow.frame.width, height: height)
                zoomingImageView.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: UIScreen.main.bounds.size.height)
                
                zoomingImageView.center = keyWindow.center
                self.navigationController?.isNavigationBarHidden = true
                
                
            }, completion: { (comepleted: Bool) in
                
            })
        }
        
    }
    
    func handleZoomOut(sender: UITapGestureRecognizer) {
        print("sup2")
        navigationController?.isNavigationBarHidden = false
        
        
        //        if let zoomOutImageView = sender.view {
        //            zoomOutImageView.layer.cornerRadius = 2
        //            zoomOutImageView.contentMode = .scaleAspectFill
        //            zoomOutImageView.clipsToBounds = true
        //
        ////            UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
        ////                zoomOutImageView.frame = self.startingFrame!
        ////                self.blackBlackgroundView?.alpha = 0
        ////            }, completion: { (comepleted: Bool) in
        ////                self.startingImgView?.isHidden = false
        ////                zoomOutImageView.removeFromSuperview()
        ////            })
        //
        //
        //            UIView.animate(withDuration: 0.5, animations: {
        //                zoomOutImageView.frame = self.startingFrame!
        //                self.blackBlackgroundView?.alpha = 0
        //            }, completion: { (comepleted: Bool) in
        //                self.startingImgView?.isHidden = false
        //                zoomOutImageView.removeFromSuperview()
        //            })
        //
        //
        //        }
        
        if let zoomOutImageView = sender.view {
            zoomOutImageView.layer.cornerRadius = 2
            zoomOutImageView.contentMode = .scaleAspectFill
            zoomOutImageView.clipsToBounds = true
            
            UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: [], animations: {
                zoomOutImageView.frame = self.startingFrame!
                self.blackBlackgroundView?.alpha = 0
                //  self.inputContainerView.alpha = 1
                
            }, completion: { (comepleted: Bool) in
                self.startingImgView?.isHidden = false
                zoomOutImageView.removeFromSuperview()
            })
            
            
            
        }
        
        
    }
    
    override var prefersStatusBarHidden: Bool {
        return navigationController?.isNavigationBarHidden == true
    }
    
    override var preferredStatusBarUpdateAnimation: UIStatusBarAnimation {
        return UIStatusBarAnimation.none
    }
    


}
