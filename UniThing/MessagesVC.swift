//
//  MessagesVC.swift
//  UniThing
//
//  Created by Xavier TanXY on 19/12/16.
//  Copyright © 2016 Xavier TanXY. All rights reserved.
//

import UIKit
import Firebase
import PopupDialog
import SVProgressHUD
import ViewAnimator


class MessagesVC: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var tableView: UITableView!
    private let animations = [AnimationType.from(direction: .bottom, offset: 30.0)]
    var messages = [MessageModel]()
    var messagesDictionary = [String: MessageModel]()
    var blockDict = [String: Bool]()
    
    var users = [User]()
    
    var timer: Timer?
    var firstTime = true
    var emptyView: EmptyConfessionView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.delegate = self
        tableView.dataSource = self
        
        let image = #imageLiteral(resourceName: "Add")
        navigationItem.rightBarButtonItem = UIBarButtonItem(image: image, style: .plain, target: self, action: #selector(handleNewMessage))
        navigationItem.rightBarButtonItem?.tintColor = PURPLE
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title:"", style:.plain, target:nil, action: "")
        
        fetchBlockList()
        observeUserMessages()
        
        tableView.allowsMultipleSelectionDuringEditing = true
        
        
        tableView.tableFooterView = UIView()
        self.navigationItem.title = "Messages"
        
        self.navigationController?.navigationBar.barTintColor = UIColor.white
        self.navigationController?.navigationBar.titleTextAttributes = [ NSFontAttributeName: UIFont(name: "HelveticaNeue-Bold", size: 16)!]
        self.navigationController?.navigationBar.layer.borderColor = BORDER_COLOR_CGCOLOR
        
        self.navigationController?.navigationBar.tintColor = PURPLE
        self.navigationController?.navigationBar.titleTextAttributes = [ NSForegroundColorAttributeName: PURPLE]
        
        
        let longPressGesture:UILongPressGestureRecognizer = UILongPressGestureRecognizer(target: self, action: #selector(self.handleLongPress(_:)))
        longPressGesture.minimumPressDuration = 0.5
        self.tableView.addGestureRecognizer(longPressGesture)
        
        emptyView = EmptyConfessionView.instanceFromNib() as! EmptyConfessionView
        
        emptyView.frame = self.tableView.bounds
        emptyView.iconImg.image = #imageLiteral(resourceName: "EmptyChat")
        emptyView.titleLbl.text = "No Messages"
        emptyView.detailLbl.text = "Start chatting now!"
        self.tableView.addSubview(emptyView)
        self.hideEmptyView()
        
     
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.slideMenuController()?.removeLeftGestures()
        UIApplication.shared.statusBarStyle = .default
//        self.tabBarController?.tabBar.isHidden = false
    }
    

    
    func showEmptyView() {
        emptyView.isHidden = false
        self.tableView.bringSubview(toFront: emptyView)
        
    }
    
    func hideEmptyView() {
        emptyView.isHidden = true
        self.tableView.sendSubview(toBack: emptyView)
        
    }
    
    func fetchBlockList() {
        let currentUid = Auth.auth().currentUser?.uid
        DataService.ds.REF_UserBlock_PartnerID.child(currentUid!).observe(.value, with: { (snapshot) in
            self.blockDict.removeAll()
            if let snapshots = snapshot.children.allObjects as? [DataSnapshot] {
                for snap in snapshots {
                    
                    //print("SNAP: \(snap)")
                    if let confessionDict = snap.value as? Dictionary<String, AnyObject> {
                        
                        
                        
                        let key = snap.key
                        self.blockDict[key] = true
                        self.attemptToReloadTable()
                        
                    }
                }
               
            }
        })
    }
    
    func handleLongPress(_ longPressGestureRecognizer: UILongPressGestureRecognizer) {
        
        if longPressGestureRecognizer.state == UIGestureRecognizerState.began {
            
            let touchPoint = longPressGestureRecognizer.location(in: self.tableView)
            if let indexPath = tableView.indexPathForRow(at: touchPoint) {
                self.optionTapped(index: indexPath.row)
                
            }
        }
    }
    
    func optionTapped(index: Int) {

        
        let alert = UIAlertController()
        
        alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel, handler:{ (UIAlertAction)in
            
        }))
        
        var partnerID = messages[index].chatPartnerId()
        if self.blockDict[partnerID!] != nil {
            alert.addAction(UIAlertAction(title: "Unblock", style: UIAlertActionStyle.default, handler:{ (UIAlertAction)in
                self.displayUnblockAlert(index: index)
            }))
        } else {
            alert.addAction(UIAlertAction(title: "Block", style: UIAlertActionStyle.default, handler:{ (UIAlertAction)in
                self.displayBlockAlert(index: index)
            }))
            
        }

        alert.addAction(UIAlertAction(title: "Report", style: .destructive , handler:{ (UIAlertAction)in
            self.displayReportAlert(index: index)
        }))
        
        alert.addAction(UIAlertAction(title: "Delete", style: .destructive , handler:{ (UIAlertAction)in
            self.showDeleteOption(index: index)
        }))
        
        
        
        self.present(alert, animated: true, completion: nil)
    }
    
    func displayUnblockAlert(index: Int) {
        let popup = PopupDialog(title: "Warning", message: "Do you want to unblock?", image: nil)
        
        var partnerID = messages[index].chatPartnerId()
        let buttonOne = DefaultButton(title: "Unblock") {
            var updateObj = [String:AnyObject]()
            let ref = DB_BASE
            updateObj["/userBlock-partnerID/\((Auth.auth().currentUser?.uid)!)/\(partnerID!)/blocked"] = NSNull()
            ref.updateChildValues(updateObj)
            
            SVProgressHUD.setMinimumDismissTimeInterval(0.5)
            SVProgressHUD.showSuccess(withStatus: "Unblocked!")
            
        }
        
        let buttonTwo = CancelButton(title: "Cancel") {
            
        }
        
        popup.addButtons([buttonTwo,buttonOne])
        popup.buttonAlignment = .horizontal
        
        // Present dialog
        self.present(popup, animated: true, completion: nil)
    }
    
    func displayBlockAlert(index: Int) {
        let popup = PopupDialog(title: "Warning", message: "Are you sure you want to block?", image: nil)

        var partnerID = messages[index].chatPartnerId()
        let buttonOne = DestructiveButton(title: "Block") {
            var updateObj = [String:AnyObject]()
            let ref = DB_BASE
            updateObj["/userBlock-partnerID/\((Auth.auth().currentUser?.uid)!)/\(partnerID!)/blocked"] = true as AnyObject
            ref.updateChildValues(updateObj)
            
            SVProgressHUD.setMinimumDismissTimeInterval(0.5)
            SVProgressHUD.showSuccess(withStatus: "Blocked!")

        }

        let buttonTwo = CancelButton(title: "Cancel") {

        }

        popup.addButtons([buttonTwo,buttonOne])
        popup.buttonAlignment = .horizontal

        // Present dialog
        self.present(popup, animated: true, completion: nil)

        
    }
    
    func displayReportAlert(index: Int) {
        
        let alert = UIAlertController()
        
        
        alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel, handler:{ (UIAlertAction)in
            
        }))
        
        alert.addAction(UIAlertAction(title: "Report Spam", style: UIAlertActionStyle.default, handler:{ (UIAlertAction)in
            self.submitReport(index: index, reason: "Spam")
            
        }))
        
        alert.addAction(UIAlertAction(title: "Report Inappropriate", style: UIAlertActionStyle.default, handler:{ (UIAlertAction)in
            self.submitReport(index: index,reason: "Inappropriate")
            
        }))
        
        alert.addAction(UIAlertAction(title: "Report Bullying", style: UIAlertActionStyle.default, handler:{ (UIAlertAction)in
            self.submitReport(index: index,reason: "Bullying")
            
        }))
        
        self.present(alert, animated: true, completion: nil)
    }
    
    func submitReport(index: Int,reason: String) {
        
        
        
        let firebaseReport = DataService.ds.REF_Message_Report.child((Auth.auth().currentUser?.uid)!).childByAutoId()
        
        var parterID = self.messages[index].chatPartnerId()
        let reportValues: Dictionary<String, AnyObject> = ["reporter": Auth.auth().currentUser?.uid as AnyObject, "Reason": reason as AnyObject, "ChatWith": parterID as AnyObject]
        
        firebaseReport.updateChildValues(reportValues)
        
        
        SVProgressHUD.setMinimumDismissTimeInterval(0.5)
        SVProgressHUD.showSuccess(withStatus: "Done!")
        
        
        
        
    }
    
//    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
//        return true
//    }
//
//    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
//        print("here")
//        showDeleteOption(index: indexPath.row)
//
//    }

    func showDeleteOption(index: Int) {
        let popup = PopupDialog(title: "Warning", message: "Are you sure you want to delete?", image: nil)
        
        let buttonOne = DestructiveButton(title: "Delete") {
            if let currentUserUid = Auth.auth().currentUser?.uid {
                
                let msg = self.messages[index]
                if let chatPartnerId = msg.chatPartnerId() {
                    DataService.ds.REF_USER_MESSAGES.child(currentUserUid).child(chatPartnerId).removeValue(completionBlock: { (error, ref) in
                        
                        if error != nil {
                            print("failed to delete messg", error)
                            return
                        }
                        self.messagesDictionary.removeValue(forKey: chatPartnerId)
                        
                        self.attemptToReloadTable()
                        
                    })
                }
                
            }
        }
        
        let buttonTwo = CancelButton(title: "Cancel") {
            
        }
        
        popup.addButtons([buttonTwo,buttonOne])
        popup.buttonAlignment = .horizontal
        
        // Present dialog
        self.present(popup, animated: true, completion: nil)
        
    }
    
    func observeUserMessages() {
        
        let uid = Auth.auth().currentUser?.uid
        let ref = DataService.ds.REF_USER_MESSAGES.child(uid!)

        
        
              ref.observe(.childAdded, with: {(snapshot) in
                
                let userId = snapshot.key
                let userRef = DataService.ds.REF_USER_MESSAGES.child(uid!).child(userId)
               
                    userRef.observe(.childAdded, with: { (snapshot) in
                        // print(snapshot)
                        let msgId = snapshot.key
                        
                        self.fetchMessageWithMessageId(msgId: msgId)
                        
                    })
                

        
              })
        
                ref.observe(.childRemoved, with: { (snapshot) in
                    
                    print("here333")
                    self.messagesDictionary.removeValue(forKey: snapshot.key)
//                    print(self.messagesDictionary)
                    self.attemptToReloadTable()

        
                   
        
                })



        self.attemptToReloadTable()
    }
    
    private func fetchMessageWithMessageId(msgId: String) {
        
        let msgRef = DataService.ds.REF_MESSAGES.child(msgId)
        
        msgRef.observeSingleEvent(of: .value, with: { (snapshot) in
            print("1")
            if let dictionary = snapshot.value as? [String: Any] {

                let message = MessageModel(key: snapshot.key, dictionary: dictionary)
                
//self.messages.append(message)

                if let chatPartnerId = message.chatPartnerId() {
                    self.messagesDictionary[chatPartnerId] = message
//                     self.messages.append(message)

                }
                
                self.attemptToReloadTable()

                
                
            }
            
        })
    }
    
    func attemptToReloadTable() {
        self.timer?.invalidate()
        
        self.timer = Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(self.reloadTable), userInfo: nil, repeats: false)
//        reloadTable()
    }
    
     func reloadTable() {
   
        self.messages = Array(self.messagesDictionary.values)

        self.messages.sort(by: { (m1, m2) -> Bool in

            return (m1.timeStamp?.intValue)! > (m2.timeStamp?.intValue)!
        })
        self.tableView.reloadData()
        

        if firstTime {
            UIView.animate(views: self.tableView.visibleCells, animations: animations, completion: {
            })
        
        }
        
        firstTime = false
        
        if messages.count == 0 {
            self.showEmptyView()
        } else {
            self.hideEmptyView()
        }
    }
    
    func handleNewMessage() {
        let newMessageVC = NewMessagesVC()
        newMessageVC.messagesVC = self
        let navController = UINavigationController(rootViewController: newMessageVC)
        present(navController, animated: true, completion: nil)
//        self.performSegue(withIdentifier: "NewMessages2VC", sender: nil)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let destinationVC = segue.destination as? NewChatLogVC {
            destinationVC.user = self.selectedUser

            
        }
        
        
    }
    var chatLogVC: ChatLogVC!
    var selectedUser: User!
    
    func showChatControllerForUser(user: User) {
//        chatLogVC = ChatLogVC(collectionViewLayout: UICollectionViewFlowLayout())
//        chatLogVC.user = user
        selectedUser = user
        goToChat()
//        self.present(chatLogVC, animated: true, completion: nil)
//        self.timer = Timer.scheduledTimer(timeInterval: 0.5, target: self, selector: #selector(goToChat), userInfo: nil, repeats: false)
        
    }
    
    func goToChat() {
//        self.navigationController?.pushViewController(self.chatLogVC, animated: true)
        performSegue(withIdentifier: "NewChatLogVC", sender: nil)
        
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return messages.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let chatPartnerId: String?
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "MessageCell") as? MessageCell
        cell?.selectionStyle = .none
        let msg = messages[indexPath.row]
        
        
        
        if let id = msg.chatPartnerId() {
            DataService.ds.REF_USERS.child(id).observeSingleEvent(of: .value, with: { (snapshot) in
                
                if let dict = snapshot.value as? Dictionary<String, AnyObject> {
                    let partner = User(userKey: id, userData: dict)
//                    let toName = dictionary["name"] as? String
                    cell?.configureCell(message: msg, toName: partner.userName, url: partner.profilePicUrl)
                }
            })
        }
        
        
        return cell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let msg = messages[indexPath.row]
        msg.toSeen = true
        self.tableView.reloadData()
        let partnerId = msg.chatPartnerId()
        let ref = DataService.ds.REF_USERS.child(partnerId!)
        
        clearDisplayBadge()
        DataService.ds.currentUser.adjustMsgCount(addCount: false, fromId: partnerId!)
        
        ref.observeSingleEvent(of: .value, with: { (snapshot) in
            
//            if let snapshots = snapshot.children.allObjects as? [FIRDataSnapshot] {
//                for snap in snapshots.reversed() {
//                    //print("SNAP: \(snap)")
//                    if let userDict = snap.value as? Dictionary<String, AnyObject> {
//                        let key = snap.key
//                        let user = User(userKey: key, userData: userDict)
//                        self.showChatControllerForUser(user: user)
//
//                    }
//                }
//                
//            }

            
            if let userDict = snapshot.value as? Dictionary<String, AnyObject> {
                
                let user = User(userKey: partnerId!, userData: userDict)
//                user.userKey = partnerId
//                user.setValuesForKeys(userDict)
                print(user.userKey)
                self.showChatControllerForUser(user: user)
                //print(userDict)
                // print(userDict["email"])
                //self.users.append(user)
                
            }
           
            
        })
    }
    
    func clearDisplayBadge() {
        
        
        if let tabItems = self.tabBarController?.tabBar.items as NSArray!
        {
            // In this case we want to modify the badge number of the third tab:
            let tabItem = tabItems[2] as! UITabBarItem
            if tabItem.badgeValue?.count != 0 {
                
                if( tabItem.badgeValue != nil) {
                    if ((tabItem.badgeValue?.count)! - 1) == 0 {
                        tabItem.badgeValue = nil
                    } else {
                        tabItem.badgeValue = "\((tabItem.badgeValue?.count)! - 1)"
                    }
                }

                
            }
            
            
        }
        
    }


    
}
