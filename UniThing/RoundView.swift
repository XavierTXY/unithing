//
//  RoundView.swift
//  UniThing
//
//  Created by Xavier TanXY on 7/12/16.
//  Copyright © 2016 Xavier TanXY. All rights reserved.
//

import Foundation
import UIKit

class RoundView: UIView {
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        //layer.shadowColor = UIColor(red: SHADOW_GRAY, green: SHADOW_GRAY, blue: SHADOW_GRAY, alpha: 0.6).cgColor
        
//        layer.shadowOpacity = 0.5
//        layer.shadowRadius = 3.0
//
//        layer.shadowOffset = CGSize(width: 1.0, height: 1.0)
//        layer.cornerRadius = 1.0
//
//        clipsToBounds = false
//        layer.shadowColor = UIColor.black.cgColor
//        layer.shadowOpacity = 0.5
//        layer.shadowOffset = CGSize.zero
//        layer.cornerRadius = 5
//        layer.shadowRadius = 1
//        layer.shadowPath = UIBezierPath(roundedRect: self.bounds, cornerRadius: 10.0).cgPath
     
//        layer.cornerRadius = 5
//        layer.masksToBounds = false
//        layer.shadowColor = UIColor.black.cgColor
//        layer.shadowOpacity = 0.15
//        layer.shadowOffset = CGSize.zero
//        layer.shadowRadius = 15.0
        
//        self.backgroundColor = UIColor.white
        layer.cornerRadius = 5.0
        layer.masksToBounds = false
        layer.shadowColor = UIColor.black.withAlphaComponent(0.2).cgColor
        layer.shadowOffset = CGSize(width:0, height: 0)
        layer.shadowOpacity = 0.4
        
        
        //E6ECF0 table view and content view

//        layer.shadowPath = UIBezierPath(roundedRect: self.bounds, cornerRadius: 30.0).cgPath
//        layer.shouldRasterize = true
       

    }
}
