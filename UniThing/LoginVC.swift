//
//  LoginVC.swift
//  UniThing
//
//  Created by Xavier TanXY on 3/12/16.
//  Copyright © 2016 Xavier TanXY. All rights reserved.
//

import UIKit
import Firebase
import SVProgressHUD

class LoginVC: UIViewController, UITextFieldDelegate {

    @IBOutlet weak var emailTxtField: UITextField!
    @IBOutlet weak var passwordTxtField: UITextField!
   // @IBOutlet weak var coverImage: UIImageView!
    @IBOutlet weak var loginBtn: UIButton!
    
    @IBOutlet weak var mainView: UIView!
    
    var email: String!
    var password: String!
    var provider: String!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
//        let leftBarItem = UIBarButtonItem(image: #imageLiteral(resourceName: "Back Chevron"), style: .plain, target: self, action: "backTapped")
        
//        let rightBarItem = UIBarButtonItem(title: "Forgot", style: .done, target: self, action: "forgotPassTapped")
//        leftBarItem.tintColor = UIColor.white
//        rightBarItem.tintColor = UIColor.white
//        self.navigationItem.setLeftBarButton(leftBarItem, animated: true)
//        self.navigationItem.setRightBarButton(rightBarItem, animated: true)
        
        if #available(iOS 11.0, *) {
            self.navigationController?.navigationBar.prefersLargeTitles = true
            self.navigationController?.navigationBar.largeTitleTextAttributes = [ NSForegroundColorAttributeName: UIColor.white]
        } else {
            // Fallback on earlier versions
        }
        
        self.navigationItem.title = "Login"
        
        self.navigationController?.navigationBar.titleTextAttributes = [ NSFontAttributeName: UIFont(name: "HelveticaNeue-Bold", size: 16)!]
        self.navigationController?.navigationBar.titleTextAttributes = [ NSForegroundColorAttributeName: UIColor.white]
        

        emailTxtField.delegate = self
        passwordTxtField.delegate = self
        
        emailTxtField.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        passwordTxtField.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)

        emailTxtField.becomeFirstResponder()
        self.loginBtn.isEnabled = false
    }
    
    override func viewDidAppear(_ animated: Bool) {
        UIView.animate(withDuration: 0.5, delay: 0.2, options: [], animations: { self.mainView.alpha = 1.0 }, completion: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        mainView.alpha = 0.0
    }
    
    func backTapped() {
        self.navigationController?.popViewController(animated: false)
    }
    
    func forgotPassTapped() {
        
    }
    

    func textFieldDidChange(_ textField: UITextField) {
        
        if (passwordTxtField.text?.characters.count)! >= 6 && emailTxtField.text?.range(of: "@") != nil {
            self.loginBtn.setBackgroundImage(#imageLiteral(resourceName: "Log In Btn"), for: .normal)
            self.loginBtn.isEnabled = true
            
        } else {
            self.loginBtn.setBackgroundImage(#imageLiteral(resourceName: "LoginBtnGray"), for: .normal)
            self.loginBtn.isEnabled = false
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        // textField.resignFirstResponder()
        if textField == emailTxtField {
            textField.resignFirstResponder()
            passwordTxtField.becomeFirstResponder()
        } else if textField == passwordTxtField {
            textField.resignFirstResponder()
            //self.loginTapped(Any)
            
        }
        
        return true
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        print("yoyo")
        self.view.endEditing(true)
    }
    
    func resignAllResponder() {
        emailTxtField.resignFirstResponder()
        passwordTxtField.resignFirstResponder()
    }
    
    @IBAction func loginTapped(_ sender: Any) {
        
        
        
        self.resignAllResponder()
        Interaction().disableInteraction(msg: "Logging in")
     
        
        if let email = emailTxtField.text, let pwd = passwordTxtField.text , (email.characters.count > 0 && pwd.characters.count > 0) {
//            FIRAuth.auth()?.signIn(withEmail: email, password: pwd, completion: { (user, error) in
//                if error != nil {
//                    
//                    let err = error as? NSError
//                    print(err?.code)
//                    print(err.debugDescription)
//                    
//                    if err?.code == USER_NOT_FOUND {
//                        ErrorAlert().show(title: "User Not Found", msg: "Please check your email correctly", object: self)
//                    } else if err?.code == WRONG_PASSWORD {
//                        ErrorAlert().show(title: "Wrong Password", msg: "Please check your password correctly", object: self)
//                    } else if err?.code == INVALID_EMAIL {
//                        ErrorAlert().show(title: "Error Signing In", msg: "Please check your email or password correctly", object: self)
//                    }
//                    
//                    
//                    
//                   
//                } else {
//                    Interaction().enableInteraction()
//                    print("Xavier: email user authed with Firebase")
//                    if let user = user {
//                        //self.checkUserProfile(id: user.uid)
//                        print("Xavier UID \(user.uid)")
//                        print("Xavier current user \(FIRAuth.auth()?.currentUser?.uid)")
//                        
//                        let defaults = UserDefaults.standard
//                        defaults.set(user.uid, forKey: "uid")
//                        DataService.ds.addInstanceID()
//                        
//                        VCSegue().segueVC(bundleName: "HomeSB", controllerName: "TabBarVC", vc: self)
//                        
//                    }
//                    
//                }
////                self.coverImage.isHidden = true
//            
//            })
            Auth.auth().signIn(withEmail: email, password: pwd) { (user, error) in
                self.email = email
                self.password = pwd
                self.provider = FIREBASE
                
                if error != nil {
                    Interaction().enableInteraction()
                    ErrorAlert().show(title: "Error", msg: "\((error?.localizedDescription)!)", object: self)
                } else {
                    if let user = user {
                        if user.isEmailVerified {
                            
                            var uid = (Auth.auth().currentUser?.uid)!
                            DataService.ds.REF_USERS.child(uid).observeSingleEvent(of: .value, with: { (snapshot) in
                                if snapshot.exists() {
                                    
                                    let defaults = UserDefaults.standard
                                    defaults.set(user.uid, forKey: "uid")
                                    
                                    if let snapshots = snapshot.children.allObjects as? [DataSnapshot] {
                                        //print("\(snapshot.value)")
                                        
                                        if let userDict = snapshot.value as? Dictionary<String, AnyObject> {
                                            let key = snapshot.key
                                            var currentUser = User(userKey: key, userData: userDict)
                                            DataService.ds.currentUser = currentUser
                                            defaults.set(currentUser.universityShort, forKey: "uniShort")

                                            
                                        }
                                    }
                                    
                                    Interaction().enableInteraction()

                                    
                                    DataService.ds.addInstanceID()
            
                                    VCSegue().segueVC(bundleName: "HomeSB", controllerName: "TabBarVC", vc: self)

                                } else {
                                    Interaction().enableInteraction()
                                    self.performSegue(withIdentifier: "DetailVC", sender: nil)
                                }
                            })
                            
                            
                        } else {
                            Interaction().enableInteraction()
                            self.performSegue(withIdentifier: "VerificationVC", sender: nil)
                        }
                        
                    }
                }
            }
        } else {
            Interaction().enableInteraction()
            ErrorAlert().show(title: "Username and Password Required", msg: "You must enter both a username and a password", object: self)
//            self.coverImage.isHidden = true
        }
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if let destinationVC = segue.destination as? VerificationVC {
            destinationVC.email = self.email
            destinationVC.password = self.password
            destinationVC.provider = self.provider
            
        }
        
        if let destinationVC = segue.destination as? DetailVC {
            destinationVC.email = self.email
            destinationVC.password = self.password
            destinationVC.provider = self.provider
            
        }
    }

}
