//
//  Social.swift
//  UniThing
//
//  Created by XavierTanXY on 4/6/18.
//  Copyright © 2018 Xavier TanXY. All rights reserved.
//

import Foundation
import CoreLocation
import MapKit

class Social {
    private var _name: String!
    private var _desc: String!
    private var _time: NSNumber!
    private var _organiserUsername: String!
    private var _organiserID: String!
    private var _type: String!
    private var _students: [String: Bool]!
    private var _latitude: Double!
    private var _longitude: Double!
    private var _distance: Double!
    private var _address: String!
    
    
    var name: String {
    
        get {
            return _name
        }
    }
    
    var desc: String {
        
        return _desc
    }
    
    var time: NSNumber {
        return _time
    }
    
    var organiserUsername: String {
        return _organiserUsername
    }
    
    var organiserID: String {
        return _organiserID
    }
    
    var students: [String: Bool] {
        
        get {
            return _students
        }
        
    }
    
    var address: String {
        return _address
    }
    
    init(keyName: String, social: Dictionary<String, AnyObject>) {
        
        self._name = keyName
        
        if let desc = social["desc"] as? String{
            self._desc = desc
        }
        
        if let organiserUsername = social["organiserUsername"] as? String{
            self._organiserUsername = organiserUsername
        }
        
        if let organiserID = social["organiserID"] as? String{
            self._organiserID = organiserID
        }
        
        if let desc = social["desc"] as? String{
            self._desc = desc
        }
        
        if let students = social["students"] as? [String: Bool] {
            self._students = students
        }
        
        if let time = social["time"] as? NSNumber{
            self._time = time
        }

        
    }
    
}
