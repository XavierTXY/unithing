//
//  ConfessionVC.swift
//  UniThing
//
//  Created by Xavier TanXY on 4/12/16.
//  Copyright © 2016 Xavier TanXY. All rights reserved.
//

import UIKit
import Firebase
import FirebaseDatabase
import FirebaseInstanceID
import FirebaseMessaging
import SVProgressHUD
import BRYXBanner
import Whisper
import DropDown
import Popover
import BubbleTransition
import ViewAnimator


class ConfessionVC: UIViewController, UITableViewDelegate, UITableViewDataSource, PassConfessionProtocol, ReloadLikeProtocol, ReloadLikeFromHashtagVCProtocol, ReloadCommentFromPreviousVCProtocal, UIPopoverPresentationControllerDelegate, UITabBarControllerDelegate, UIViewControllerTransitioningDelegate {

    @IBOutlet weak var addBtn: UIButton!
    @IBOutlet weak var loadingScreen: UIImageView!
    @IBOutlet weak var tableView: UITableView!
    var uniTableView: UITableView!
    var refreshControl: UIRefreshControl!

    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var topView: UIView!
    
    private let animations = [AnimationType.from(direction: .bottom, offset: 30.0)]
    
    var confessions = [Confession]()
    var confessionDict = [String: Confession]()
    var allConfessionDict = [String: Confession]()
    var allConfessionArray = [Confession]()
    var mainConfession: Confession?

    var uniName: String!
  //  var uniNameShort: String!
    var gender: String?
    var currentConfessionId: String!
    var currentConfession: Confession?

    
    var elements = [Confession]()
    var hiddenPosts = [String]()
    var totalConfession: Int!
    var totalHiddenConfession: Int!
    var numberOfObjectsinArray: Int = 10
    var actInd: UIActivityIndicatorView!
    
    var startingFrame: CGRect?
    var blackBlackgroundView: UIView?
    
    var timer: Timer?
    var currentIndex: Int?
    
    var dimView: UIView!
    var commentVC: Comment3VC?
    var user: User?
    var dropDown = DropDown()
//    var universities = [String]()
    let button =  UIButton(type: .custom)
    
    var scrollToTop: Bool!
    //static var confessionImageCache: NSCache<NSString, UIImage> = NSCache()

    var emptyView: EmptyConfessionView!
    
//    var universities = [String]()
//    var universitiesShort = [String]()
    var universities = [University]()
    var ACT = [University]()
    var NATIONAL = [University]()
    var NSW = [University]()
    var NT = [University]()
    var QLD = [University]()
    var WA = [University]()
    var TAS = [University]()
    var VIC = [University]()
    var SA = [University]()
    var TOP = [University]()
    
    var states = [[University]]()
    
    var selectedUni = University()
    var changedUni = false
    var currentVisitingUni = University()
    var firstTimeLoadCurrentVisitingUni = true
    
    var fetchCategory = false
    var category = "None"
    
    var popover: Popover!
    var popoverOptions: [PopoverOption] = [
        .type(.down)
//        .blackOverlayColor(UIColor(white: 0.0, alpha: 0.6))
    ]
    
    var courses = [String]()
    var listener : DatabaseHandle!
    var first = true
    var isNavHiding = false
    
    var animateTableView = true
    

    override func viewDidLoad() {
        super.viewDidLoad()
      // addRedDotAtTabBarItemIndex(index: 0)
        

        

        if #available(iOS 11.0, *) {
            self.navigationController?.navigationBar.prefersLargeTitles = false
        } else {
            // Fallback on earlier versions
        }
        

        
        button.frame = CGRect(x: 0, y: 0, width: 120, height: 40)
        button.backgroundColor = UIColor.clear
        button.setTitleColor(UIColor.white, for: .normal)
        button.titleLabel?.font = UIFont(name: "HelveticaNeue-Bold", size: 16)!
//        button.setTitle("UNI", for: .normal)
        
//        button.imageView?.contentMode = .scaleAspectFill
//        button.setImage(#imageLiteral(resourceName: "optionArrow"), for: .normal)
        
        button.titleEdgeInsets = UIEdgeInsetsMake(0.0, 0.0, 0.0, 4.0)
        button.contentHorizontalAlignment = .center
        button.semanticContentAttribute = .forceRightToLeft
        button.addTarget(self, action: #selector(self.titleWasTapped), for: .touchUpInside)
        button.isHidden = true
        button.isUserInteractionEnabled = false

        self.navigationItem.titleView = button
        
        
        
        scrollToTop = true

        
        SVProgressHUD.setDefaultStyle(SVProgressHUDStyle.custom)
        SVProgressHUD.setMinimumDismissTimeInterval(1.0)

        SVProgressHUD.setBackgroundColor(UIColor(red: 255/255, green: 255/255, blue: 255/255, alpha: 1.0))
        SVProgressHUD.setRingThickness(3)
        //SVProgressHUD.setForegroundColor(PURPLE)
        SVProgressHUD.setFont(UIFont(name: "HelveticaNeue-Medium", size: 16)!)
//        SVProgressHUD.show(withStatus: "Loading")
//        SVProgressHUD.show()


        refreshControl = UIRefreshControl()
        refreshControl.backgroundColor = UIColor.clear
        refreshControl.tintColor = UIColor.white
        refreshControl.addTarget(self, action: "refresh", for: UIControlEvents.valueChanged)
        tableView.addSubview(refreshControl)
       
//        dimView = UIView(frame: (UIApplication.shared.keyWindow?.frame)!)
//        dimView.backgroundColor = UIColor.black
//        dimView.alpha = 0.5
       
   
//
//        let rightBarItem = UIBarButtonItem(image: #imageLiteral(resourceName: "Add"), style: .plain, target: self, action: "addConfessionTapped")
//        rightBarItem.tintColor = UIColor.white
//        self.navigationItem.setRightBarButton(rightBarItem, animated: true)
        
        let leftItem = UIBarButtonItem(image: #imageLiteral(resourceName: "SlideMenuIcon"), style: .plain, target: self, action: "slideMenuTapped")
        leftItem.tintColor = UIColor.white
        self.navigationItem.setLeftBarButton(leftItem, animated: true)
        
        self.navigationItem.title = "Confession"
        

//        self.navigationController?.navigationBar.barTintColor = UIColor.white
        self.navigationController?.navigationBar.titleTextAttributes = [ NSFontAttributeName: UIFont(name: "HelveticaNeue-Bold", size: 16)!]
        self.navigationController?.navigationBar.layer.borderColor = BORDER_COLOR_CGCOLOR
        
//        self.navigationController?.navigationBar.tintColor = UIColor.white
        self.navigationController?.navigationBar.titleTextAttributes = [ NSForegroundColorAttributeName: UIColor.white]
        
        let backImage = #imageLiteral(resourceName: "Back Chevron")
        self.navigationController?.navigationBar.backIndicatorImage = backImage
        self.navigationController?.navigationBar.backIndicatorTransitionMaskImage = backImage
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title:"", style:.plain, target:nil, action: "")
        
        UITabBarItem.appearance().setTitleTextAttributes([NSFontAttributeName: UIFont(name: "HelveticaNeue", size: 11)!], for: .normal)


        
        
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationController?.view.backgroundColor = .clear
        

        //self.tabBarController?.tabBar.barTintColor = UIColor(red: 244/255, green: 244/255, blue: 244/255, alpha: 0.5)
       // self.tabBarController?.tabBar.items?[0].selectedImage = UIImage(named: "confessionTabSelected")
        
//        self.tabBarController!.tabBar.layer.borderWidth = 0.50
//
//        self.tabBarController!.tabBar.layer.borderColor = UIColor.clear.cgColor
//        self.tabBarController?.tabBar.clipsToBounds = true

        

        

        self.tabBarController?.tabBar.isUserInteractionEnabled = false
        self.tableView.isUserInteractionEnabled = false
        self.navigationController?.navigationBar.isUserInteractionEnabled = false
        
        
        self.tabBarController?.tabBar.shadowImage = #imageLiteral(resourceName: "shadowImage")
        self.tabBarController?.tabBar.backgroundImage = #imageLiteral(resourceName: "opacImage")
        



//        self.navigationController?.hidesBarsOnSwipe = true


        
//        tableView.estimatedSectionHeaderHeight = 400
//        tableView.sectionHeaderHeight = UITableViewAutomaticDimension
        tableView.delegate = self
        tableView.dataSource = self
        tableView.tableFooterView = UIView()
    
        
        //tableView.allowsSelection = false
        
//        tabBarController?.delegate = self
        tableView.estimatedRowHeight = 450
        
        tableView.rowHeight = UITableViewAutomaticDimension
//        self.tableView.contentInset = UIEdgeInsetsMake(50, 0, 0, 0)


//        self.user = DataService.ds.currentUser
//        var uniShort = UserDefaults.standard.string(forKey: "uniShort")
//        self.button.setTitle("\(uniShort)", for: .normal)
        

        

//        NotificationCenter.default.addObserver(self, selector: #selector(reloadAddComment),name:NSNotification.Name(rawValue: "reloadAddComment"), object: nil)
       // NotificationCenter.default.addObserver(self, selector: #selector(reloadMinusComment),name:NSNotification.Name(rawValue: "reloadMinusComment"), object: nil)
//        NotificationCenter.default.addObserver(self, selector: #selector(reloadAddLike),name:NSNotification.Name(rawValue: "reloadAddLike"), object: nil)
//        NotificationCenter.default.addObserver(self, selector: #selector(reloadMinusLike),name:NSNotification.Name(rawValue: "reloadMinusLike"), object: nil)
        

        emptyView = EmptyConfessionView.instanceFromNib() as! EmptyConfessionView
    
        emptyView.frame = self.tableView.bounds
        self.tableView.addSubview(emptyView)
        self.hideEmptyView()
        
        self.fetchCourses()
//        self.fetchDiscussion(completion: )
//        self.parseUniversityCSV()
        
        self.getUserDetails {
            if self.first {
//                var shortName = DataService.ds.convertUniNameToShort(uniName: self.currentVisitingUni.name)
//                UserDefaults.standard.setValue(shortName, forKey: "uniShort")
                var uni = UserDefaults.standard.value(forKey: "uniShort") as? String
                
                if uni != nil && uni != ""{
                    self.button.setTitle("\(uni!)", for: .normal)
                } else {
                    self.button.setTitle("\(DataService.ds.convertUniNameToShort(uniName: self.currentVisitingUni.name))", for: .normal)
                }
                
                self.fetchData(changeUni: self.changedUni)
                self.first = false
            }
        }
        
        initFooter()


        
        
        
        
        
        
        
    }
    
    func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
        if(velocity.y>0) {
            //Code will work without the animation block.I am using animation block incase if you want to set any delay to it.
//           self.addBtn.isHidden = true
            UIView.animate(withDuration: 0.1, animations: {
                self.addBtn.alpha = 0.0
            })
            
            UIView.animate(withDuration: 2.5, delay: 0, options: UIViewAnimationOptions(), animations: {
                self.hideNav()
     
//                self.navigationController?.setToolbarHidden(true, animated: true)
                print("Hide")
            }, completion: nil)
            
        } else {
//            self.addBtn.isHidden = false
            UIView.animate(withDuration: 0.1, animations: {
                self.addBtn.alpha = 1.0
            })
            
            UIView.animate(withDuration: 2.5, delay: 0, options: UIViewAnimationOptions(), animations: {
                self.unhideNav()
//                self.navigationController?.setToolbarHidden(false, animated: true)
                print("Unhide")
            }, completion: nil)
        }
    }
    
    func hideNav() {
        isNavHiding = true
        
        self.navigationController?.setNavigationBarHidden(true, animated: true)
    }
    
    func unhideNav() {
        isNavHiding = false
        self.navigationController?.setNavigationBarHidden(false, animated: true)
    }
    func fetchDiscussion(completion: @escaping () -> ()) {
//        self.discussion = nil
        
        DataService.ds.REF_Confessions_Discussion.observeSingleEvent(of: .value, with: { (snapshot) in

            if snapshot.exists() {
                
                if let disDict = snapshot.value as? Dictionary<String, AnyObject> {
                    print(disDict)
                    
                    self.mainConfession = Confession(confessionKey: snapshot.key, postData: disDict)
                    completion()
                }
                
            } else {
                self.mainConfession = nil
                completion()
            }

        })
    }
    
    func titleWasTapped() {
        print("tspped")
        
        
//        dropDown.show()
//        let startPoint = CGPoint(x: (self.navigationController?.navigationBar.frame.width)!/2, y: 55)
//        self.popover.show(uniTableView, point: startPoint)
        
//        var startPoint = self.button.convert((self.button.center), to: self.view)
//        let startPoint = CGPoint(x: self.navigationItem.titleView!.frame.origin.x, y: self.navigationItem.titleView!.frame.origin.y)
        let aView = UIView(frame: CGRect(x: 0, y: 0, width: self.view.frame.width-20, height: self.tableView.frame.height - 49 ))
        aView.layer.cornerRadius = 15.0
        aView.clipsToBounds = true
        uniTableView = UITableView(frame: CGRect(x: 0, y: 10, width: self.view.frame.width, height: aView.frame.height))
        uniTableView.delegate = self
        uniTableView.dataSource = self
        uniTableView.isScrollEnabled = true
        self.uniTableView.register(UINib(nibName: "DropDownUniCell", bundle: nil), forCellReuseIdentifier: "DropDownUniCell")
        self.uniTableView.rowHeight = 40.0
        self.uniTableView.reloadData()
        
        self.popover = Popover(options: self.popoverOptions)
        self.popover.willShowHandler = {
//            print("willShowHandler \(self.selectedUni.name)")
//            self.universities.index(where:  { (selectedUni) -> Bool in
//
//            })
            

            
            

            var (section, index) = self.findUni(uniName: self.currentVisitingUni.name)
            print("section \(section) and index \(index)")
            

            self.uniTableView.selectRow(at:  IndexPath(row: index, section: section), animated: false, scrollPosition: .none)
        }



        uniTableView.clipsToBounds = true
        aView.addSubview(uniTableView)

        popover.cornerRadius = 15.0
//        popover.show(aView, point: startPoint)
        popover.show(aView, fromView: self.navigationItem.titleView!)

    }
    
    func findUni(uniName: String) -> (Int, Int) {
        var index = 0
        var section = 0
        var found = false
        
        for uniArray in self.states {
            index = 0
            
            for uni in uniArray {
                if uni.name == uniName {
                    found = true
                    break
                }
                
                index = index + 1
            }
            
            if found {
                break
            } else {
                section = section + 1
            }
            
            
            
        }

        
        return ( section, index)
        

    }
    
    func getUserDetails(completion: @escaping () -> ()) {
        
        if let currentVersion = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String {
            let ref = DataService.ds.REF_USERS.child((Auth.auth().currentUser?.uid)!)
            var updateObj = [String:AnyObject]()
            DataService.ds.OS  = currentVersion
            updateObj["/OS"] = DataService.ds.OS as AnyObject
//            updateObj["/lastLogin"] = DataService.ds.OS as AnyObject
            
            let timestamp = NSNumber(integerLiteral: Int(NSDate().timeIntervalSince1970))
            let seconds = timestamp.doubleValue
            let timesStampDate = NSDate(timeIntervalSince1970: seconds)
            var dateFormatter = DateFormatter()
            
            dateFormatter.dateFormat = "EEEE, MMM d, h:mm a"
            updateObj["/lastLogin"] = dateFormatter.string(from: timesStampDate as Date) as AnyObject
            
            ref.updateChildValues(updateObj)
            
        }

        
        
        self.listener = DataService.ds.REF_USERS.child((Auth.auth().currentUser?.uid)!).observe(.value, with: { (snapshot) in
            if let snapshots = snapshot.children.allObjects as? [DataSnapshot] {
                print("detect user changes")
                
                if let userDict = snapshot.value as? Dictionary<String, AnyObject> {
                    let key = snapshot.key
                    self.user = User(userKey: key, userData: userDict)
                    DataService.ds.currentUser = self.user
                    
           
                    self.selectedUni.name = (self.user?.university)!
                    if self.firstTimeLoadCurrentVisitingUni {
                        self.currentVisitingUni.name = (self.user?.university)!
                        DataService.ds.currentVisitingUni = self.currentVisitingUni.name
                        self.firstTimeLoadCurrentVisitingUni = false
                    }
                    
                    print("CHANGED \(self.selectedUni.name) in get user details")
                    
                    
                    
//                    if self.firstTimeLoadCurrentVisitingUni {
//                        self.currentVisitingUni.name = (self.user?.university)!
//                        self.firstTimeLoadCurrentVisitingUni = false
//                        self.button.setTitle("\((self.user?.universityShort)!)", for: .normal)
//                    } else {
//                        
//                        if self.selectedUni.name != self.currentVisitingUni.name {
//                            
//                        } else {
//                            
//                            self.button.setTitle("\((self.user?.universityShort)!)", for: .normal)
//                        }
//                    }
                    

                   
                    
                    
                    if ((self.user?.msgCount.count)! - 1) != 0 {
                        self.setMsgBadgeNumber(user: self.user!)
                    }
                    
                    self.button.isHidden = false
                    if self.user?.notiCount != 0 {
                        self.setBadgeNumber(user: self.user!)
                    }
                    
                    completion()

                }
            }
            
        })
    }
    
    func setBadgeNumber(user: User) {
        if let tabItems = self.tabBarController?.tabBar.items as NSArray!
        {
            // In this case we want to modify the badge number of the third tab:
            let tabItem = tabItems[3] as! UITabBarItem
            tabItem.badgeValue = "\(user.notiCount)"
            
        }
    }
    
    func setMsgBadgeNumber(user: User) {
        if let tabItems = self.tabBarController?.tabBar.items as NSArray!
        {
            // In this case we want to modify the badge number of the third tab:
            let tabItem = tabItems[2] as! UITabBarItem
            tabItem.badgeValue = "\(user.msgCount.count - 1)"
            
        }
    }
    
    func showEmptyView() {
        emptyView.isHidden = false
        self.tableView.bringSubview(toFront: emptyView)

    }
    
    func hideEmptyView() {
        emptyView.isHidden = true
        self.tableView.sendSubview(toBack: emptyView)
     
    }
    
    func fetchCourses() {
        
        
        DataService.ds.REF_Course_Info.observeSingleEvent(of: .value, with: { (snapshot) in
            
            if let snapshots = snapshot.children.allObjects as? [DataSnapshot] {
                for snap in snapshots {
                    
                    if let uniDict = snap.value as? Dictionary<String, AnyObject> {
                        
                        
                        let courseName = snap.key
//                        print(courseName)
                        self.courses.append(courseName)
                    }
                }
                DataService.ds.courseArray = self.courses
            }
        })
    }
    
    func parseUniversityCSV() {

        var trending = University(id: 00, name: "Trending", shortName: "Trending", state: "TOP")
        var latest = University(id: 01, name: "Latest", shortName: "Latest", state: "TOP")

        TOP.append(trending)
        TOP.append(latest)

        
        DataService.ds.REF_University_Info.observeSingleEvent(of: .value, with: { (snapshot) in

            if let snapshots = snapshot.children.allObjects as? [DataSnapshot] {
                for snap in snapshots {

                    if let uniDict = snap.value as? Dictionary<String, AnyObject> {


                        let uniName = snap.key
                        let uni = University(keyName: uniName, uni: uniDict)

                        self.universities.append(uni)

                        switch uni.state {
                            case "ACT":
                                self.ACT.append(uni)
                                break
                            case "NATIONAL":
                                self.NATIONAL.append(uni)
                                break
                            case "NSW":
                                self.NSW.append(uni)
                                break
                            case "NT":
                                self.NT.append(uni)
                                break
                            case "QLD":
                                self.QLD.append(uni)
                                break
                            case "WA":
                                self.WA.append(uni)
                                break
                            case "TAS":
                                self.TAS.append(uni)
                                break
                            case "VIC":
                                self.VIC.append(uni)
                                break
                            case "SA":
                                self.SA.append(uni)
                                break
                            default:
                                break
                        }

                }
            }
                self.states.append(self.TOP)
                self.states.append(self.ACT)
                self.states.append(self.NATIONAL)
                self.states.append(self.NSW)
                self.states.append(self.NT)
                self.states.append(self.QLD)
                self.states.append(self.SA)
                self.states.append(self.TAS)
                self.states.append(self.VIC)
                self.states.append(self.WA)
                
                

                DataService.ds.uniArray = self.universities
                DataService.ds.states = self.states

            }

        })
    }


    func displayNoInternetConnection() {
        if !DataService.ds.connectedToNetwork() {
//            let banner = Banner(title: "No Internet Connection", subtitle: "Check your internet connection", image: nil, backgroundColor: UIColor(red:229.0/255.0, green:115.0/255.0, blue:115.0/255.0, alpha:1.0))
//            SVProgressHUD.dismiss()
//            if self.refreshControl.isRefreshing {
//                self.refreshControl.endRefreshing()
//            }
//
//            if self.actInd.isAnimating {
//                self.actInd.stopAnimating()
//            }
//
//            banner.dismissesOnTap = true
//            banner.show(duration: 2.0)
            
            let murmur = Murmur(title: "No Internet Connection")
            
            
            // Show and hide a message after delay
            Whisper.show(whistle: murmur, action: .show(0.5))
            
            // Present a permanent status bar message
            Whisper.show(whistle: murmur, action: .present)
            
            // Hide a message
            Whisper.hide(whistleAfter: 3)
            
            self.tabBarController?.tabBar.isUserInteractionEnabled = false
            self.navigationController?.navigationBar.isUserInteractionEnabled = false
            self.tableView.isUserInteractionEnabled = true
            
        }
    }
    
    func removeTabbarItemsText() {
        if let items = self.tabBarController?.tabBar.items {
            for item in items {
                item.title = ""
                item.imageInsets = UIEdgeInsetsMake(6, 0, -6, 0);
            }
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
       // navigationController?.hidesBarsOnSwipe = true
       
        
       
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear(true)

        self.slideMenuController()?.addLeftGestures()
        self.navigationController?.navigationBar.titleTextAttributes = [ NSForegroundColorAttributeName: UIColor.white]
        UIApplication.shared.statusBarStyle = .lightContent
     
        tabBarController?.delegate = self
        tabBarController?.tabBar.isHidden = false
        tabBarController?.tabBar.isTranslucent = false
        self.tableView.reloadData()
        displayNoInternetConnection()

    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
//        DataService.ds.REF_USERS.child((Auth.auth().currentUser?.uid)!).removeObserver(withHandle: self.listener)
        scrollToTop = false

    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(true)
//        self.navigationController?.navigationBar.isTranslucent = true
//        navigationController?.isNavigationBarHidden = false
//         navigationController?.hidesBarsOnSwipe = false
    }
    
  
    
//    func reloadAddComment(notification:NSNotification){
//        
//        for confession in confessions {
//            if confession.confessionKey == self.currentConfessionId {
//                confession.comments = confession.comments + 1
//                self.reloadTable()
//            }
//        }
//        
//        
//    }
    
//    func reloadMinusComment(notification:NSNotification){
//
////        for confession in confessions {
////            if confession.confessionKey == self.currentConfessionId {
////                confession.comments = confession.comments - 1
////                self.reloadTable()
////            }
////        }
////        
//        
//        print("testing")
//        self.tableView.reloadData()
//        
//    }
    
//    func reloadAddLike(notification:NSNotification){
//        print("add")
//        
//        
//        for confession in confessions {
////            if confession.confessionKey == self.currentConfessionId {
////                confession.likes = confession.likes + 1
////                self.reloadTable()
////            }
//        }
//        
//        
//    }
    
//    func reloadMinusLike(notification:NSNotification){
//        print("minus")
//        
//        
//        for confession in confessions {
////            if confession.confessionKey == self.currentConfessionId {
////                confession.likes = confession.likes - 1
////                self.reloadTable()
////            }
//        }
//        
//        
//    }
    
    var totalDisplayConfession = 0
 
    private func getTotalConfession(changeUni: Bool) {
        
//        DataService.ds.REF_USERS.child((Auth.auth().currentUser?.uid)!).child("university").observeSingleEvent(of: .value, with: { (snapshot) in
//          //  if let snapshots = snapshot.children.allObjects as? [FIRDataSnapshot] {
//              //  print(snapshot.childSnapshot(forPath: "university").value )
//            print("1")
//
//
//
//            if !(snapshot.value is NSNull) {
//
//                if changeUni {
//                    self.uniName = self.selectedUni.name
//                } else {
//                    self.uniName = snapshot.value as! String!
//                }
//
//                print("CHANGED \(self.uniName) get total confession")
//                self.selectedUni.name = self.uniName
        
        if fetchCategory {
            DataService.ds.REF_Category.child(self.category).observeSingleEvent(of: .value, with: { (snapshot) in
                
                
                if let snapshots = snapshot.children.allObjects as? [DataSnapshot] {
                    self.totalConfession = snapshots.count
                    
                    if self.mainConfession == nil {
                        self.totalConfession = self.totalConfession - self.totalHiddenConfession
                    } else {
                        self.totalConfession = self.totalConfession - self.totalHiddenConfession - 1
                    }
                    
                    if self.totalConfession < 0 {
                        self.totalConfession = 0
                    }
                    print("total confession \(self.totalConfession)")
                    
                    if self.totalConfession == 0 {
                        
                        self.refreshControl.endRefreshing()
                        self.confessions.removeAll()
                        self.confessionDict.removeAll()
                        self.reloadAndSortTable()
                        
                        if self.mainConfession == nil {
                            self.showEmptyView()
                        }
                        
                    } else {
                        self.hideEmptyView()
                    }
                    
                }
                
                
                SVProgressHUD.dismiss()
                
                
                
            })
        } else {
            if self.currentVisitingUni.name == "Latest" || self.currentVisitingUni.name == "Trending" {
                
                DataService.ds.REF_CONFESSIONS.observeSingleEvent(of: .value, with: { (snapshot) in
                    
                    
                    if let snapshots = snapshot.children.allObjects as? [DataSnapshot] {
                        self.totalConfession = snapshots.count
                        
                        if self.mainConfession == nil {
                            self.totalConfession = self.totalConfession - self.totalHiddenConfession
                        } else {
                            self.totalConfession = self.totalConfession - self.totalHiddenConfession - 1
                        }
                        
                        if self.totalConfession < 0 {
                            self.totalConfession = 0
                        }
                        print("total confession \(self.totalConfession)")
                        
                        if self.totalConfession == 0 {
                            
                            self.refreshControl.endRefreshing()
                            self.confessions.removeAll()
                            self.confessionDict.removeAll()
                            self.reloadAndSortTable()
                            
                            if self.mainConfession == nil {
                                self.showEmptyView()
                            }
                            
                        } else {
                            self.hideEmptyView()
                        }
                        
                    }
                    
                    
                    SVProgressHUD.dismiss()
                    
                    
                    
                })
                
            } else if self.currentVisitingUni.name == "Followed" {
                self.confessionDict.removeAll()
                self.confessions = []
                self.allConfessionArray = []
                self.allConfessionDict.removeAll()
                self.totalConfession = 0
                print("IN FOLLOWED")
                var totalUserRead = 0
                
        
                for (userID, following) in DataService.ds.currentUser.following {
                    print("im following \(userID)")
                    if userID != "key" {
                        DataService.ds.REF_User_Confession.child(userID).observeSingleEvent(of: .value, with: { (snapshot) in

//
                        if let snapshots = snapshot.children.allObjects as? [DataSnapshot] {

//
                            for snap in snapshots {
                                    //print("SNAP: \(snap)")
                                    if let confessionDict = snap.value as? Dictionary<String, AnyObject> {
                                        let key = snap.key
                                        let confession = Confession(confessionKey: key, postData: confessionDict)

                                        if let a = confession.anonymous {
                                            if !a {
//                                                self.totalDisplayConfession += 1
                                                self.totalConfession = self.totalConfession + 1
                                                print("IN FOLLOWED adding total confession \(self.totalConfession)")
//                                                self.allConfessionDict[confession.confessionKey] = confession
                                                self.allConfessionArray.append(confession)
                                            }
                                        }

                                    }
                                }
                                
                                if (totalUserRead % DataService.ds.currentUser.following.count) == 0 {
                                    print("IN FOLLOWED all confessiosn \(self.allConfessionArray.count)")
                                    if self.mainConfession == nil {
                                        self.totalConfession = self.totalConfession - self.totalHiddenConfession
                                    } else {
                                        self.totalConfession = self.totalConfession - self.totalHiddenConfession - 1
                                    }
                                    
                                    if self.totalConfession < 0 {
                                        self.totalConfession = 0
                                    }
                                    
                                    self.fetchFollowing()
                                }
//
                            }
//
                        })
                    }


                }
            
                
                if self.totalConfession == 0 {
                    
                    self.refreshControl.endRefreshing()
                    self.reloadAndSortTable()
                    
                    if self.mainConfession == nil {
                        self.showEmptyView()
                    }
                    
                } else {
                    self.hideEmptyView()
                }
                
            } else {
                DataService.ds.REF_UserBlock_Uni_Confession.child((Auth.auth().currentUser?.uid)!).child(self.currentVisitingUni.name).observeSingleEvent(of: .value, with: { (snapshot) in
                    
                    
                    if let snapshots = snapshot.children.allObjects as? [DataSnapshot] {
                        self.totalHiddenConfession = snapshots.count
                        print("total hidden confession \(self.totalHiddenConfession)")
                        
                        
                        
                    }
                    
                    
                    
                    
                    
                })
                
                DataService.ds.REF_UniConfession.child(self.currentVisitingUni.name).observeSingleEvent(of: .value, with: { (snapshot) in
                    
                    
                    if let snapshots = snapshot.children.allObjects as? [DataSnapshot] {
                        self.totalConfession = snapshots.count
                        self.totalConfession = self.totalConfession - self.totalHiddenConfession
                        if self.totalConfession < 0 {
                            self.totalConfession = 0
                        }
                        print("total confession \(self.totalConfession)")
                        
                        if self.totalConfession == 0 {
                            self.refreshControl.endRefreshing()
                            self.confessions.removeAll()
                            self.confessionDict.removeAll()
                            self.reloadAndSortTable()
                            
                            if self.mainConfession == nil {
                                self.showEmptyView()
                            }
                            //                        self.showEmptyView()
                        } else {
                            self.hideEmptyView()
                        }
                        
                    }
                    
                    
                    SVProgressHUD.dismiss()
                    
                    
                    
                })
            }
            
            //            }
            //
            //        })
        }
        

       
    }
    
   
    
    func addRedDotAtTabBarItemIndex(index: Int) {
        
        // for first tab
        (tabBarController!.tabBar.items!.first! as! UITabBarItem).badgeValue = "1"
        
        //for second tab
        (tabBarController!.tabBar.items![1] as! UITabBarItem).badgeValue = "2"
        
        // for last tab
        (tabBarController!.tabBar.items!.last! as! UITabBarItem).badgeValue = "final"
        
    }
    
    func fetchCategoryData() {
        self.hiddenPosts = []
        DataService.ds.REF_Category.child(self.category).queryLimited(toLast: UInt(self.numberOfObjectsinArray)).observeSingleEvent(of: .value, with: { (snapshot) in
            //  DataService.ds.REF_CONFESSIONS.queryOrdered(byChild: "university").queryEqual(toValue: self.uniName).queryLimited(toLast: UInt(self.numberOfObjectsinArray)).observeSingleEvent(of: .value, with: { (snapshot) in
            self.confessionDict.removeAll()
            self.confessions = []
            //                    self.elements = []
            
            if let snapshots = snapshot.children.allObjects as? [DataSnapshot] {
                for snap in snapshots {
                    
                    //print("SNAP: \(snap)")
                    if let confessionDict = snap.value as? Dictionary<String, AnyObject> {
                        
                        
                        
                        let key = snap.key
                        if( !self.hiddenPosts.contains(key) ) {
                            print("i didnt block \(key)")
                            let confession = Confession(confessionKey: key, postData: confessionDict)
                            
                            //self.confessions.append(confession)
                            
                            if confession.confessionKey != DISCUSSION {
                                print("adding confess to dict \(confession.confessionKey)")
                                self.confessionDict[key] = confession
                            }
                            
                            
                            
                            
                            
                        }
                        
                        
                        
                    }
                }
                self.attemptToReloadTable()
            }
            
            
            self.actInd.stopAnimating()
            
        })
    }
    
    func fetchLatest() {
        self.hiddenPosts = []
        DataService.ds.REF_CONFESSIONS.queryLimited(toLast: UInt(self.numberOfObjectsinArray)).observeSingleEvent(of: .value, with: { (snapshot) in
            //  DataService.ds.REF_CONFESSIONS.queryOrdered(byChild: "university").queryEqual(toValue: self.uniName).queryLimited(toLast: UInt(self.numberOfObjectsinArray)).observeSingleEvent(of: .value, with: { (snapshot) in
            self.confessionDict.removeAll()
            self.confessions = []
            //                    self.elements = []
            
            if let snapshots = snapshot.children.allObjects as? [DataSnapshot] {
                for snap in snapshots {
                    
                    //print("SNAP: \(snap)")
                    if let confessionDict = snap.value as? Dictionary<String, AnyObject> {
                        
                        
                        
                        let key = snap.key
                        if( !self.hiddenPosts.contains(key) ) {
                            print("i didnt block \(key)")
                            let confession = Confession(confessionKey: key, postData: confessionDict)
                            
                            //self.confessions.append(confession)
                            
                            if confession.confessionKey != DISCUSSION {
                                print("adding confess to dict \(confession.confessionKey)")
                                self.confessionDict[key] = confession
                            }

                            
                            
                            
                            
                        }
                        
                        
                        
                    }
                }
                self.attemptToReloadTable()
            }
            
            
            self.actInd.stopAnimating()
            
        })
    }
    
    func fetchTrending() {
        self.hiddenPosts = []
        DataService.ds.REF_CONFESSIONS.observeSingleEvent(of: .value, with: { (snapshot) in
            //  DataService.ds.REF_CONFESSIONS.queryOrdered(byChild: "university").queryEqual(toValue: self.uniName).queryLimited(toLast: UInt(self.numberOfObjectsinArray)).observeSingleEvent(of: .value, with: { (snapshot) in
            self.confessionDict.removeAll()
            self.confessions = []
            //                    self.elements = []
            
            if let snapshots = snapshot.children.allObjects as? [DataSnapshot] {
                for snap in snapshots {
                    //print("SNAP: \(snap)")
                    if let confessionDict = snap.value as? Dictionary<String, AnyObject> {
                        
                        
                        let key = snap.key
                        if( !self.hiddenPosts.contains(key) ) {
                            print("i didnt block \(key)")
                            let confession = Confession(confessionKey: key, postData: confessionDict)
                            
                            //self.confessions.append(confession)
                            if confession.confessionKey != DISCUSSION {
                                print("adding confess to dict \(confession.confessionKey)")
                                self.confessionDict[key] = confession
                            }
                            
                            
                            
                        }
                        
                        
                        
                    }
                }
                self.attemptToReloadTable()
                
            }
            
            
            self.actInd.stopAnimating()
            
        })
    }
    
    var totalConfessionRead = 0
    var totalAnonymousConfessionRead = 0
    var startIdx = 0
    func fetchFollowing() {
        print("IN FOLLOWED in fetch following")
        self.allConfessionArray.sort(by: { (c1, c2) -> Bool in
            
            return (c1.time.intValue) > (c2.time.intValue)
        })
        

        
        for idx in startIdx...numberOfObjectsinArray {
            if idx < allConfessionArray.count {
                let c = allConfessionArray[idx]
                confessionDict[c.confessionKey] = c
            }

        }
        
//        startIdx = numberOfObjectsinArray
        
        self.attemptToReloadTable()

    }
    
    func fetchData(changeUni: Bool) {
        fetchDiscussion{
            self.fetchRemainingData(changeUni: changeUni)
        }
        
    }
    
    func fetchRemainingData(changeUni: Bool) {
        getTotalConfession(changeUni: changeUni)
        
        
        
        

        if fetchCategory {
            self.fetchCategoryData()
        } else {
            if self.currentVisitingUni.name == "Latest" {
                self.fetchLatest()
                
                //
                //            DataService.ds.REF_CONFESSIONS.queryOrdered(byChild: "university").queryEqual(toValue: self.uniName).observe(.childRemoved, with: { (snapshot) in
                //
                //                self.confessionDict.removeValue(forKey: snapshot.key)
                //                self.totalConfession = self.totalConfession - 1
                //
                //
                //            })
                
            } else if self.currentVisitingUni.name == "Trending" {
                
                self.fetchTrending()
                
                //            DataService.ds.REF_CONFESSIONS.queryOrdered(byChild: "university").queryEqual(toValue: self.uniName).observe(.childRemoved, with: { (snapshot) in
                //
                //                self.confessionDict.removeValue(forKey: snapshot.key)
                //                self.totalConfession = self.totalConfession - 1
                //
                //
                //            })
                
            } else if self.currentVisitingUni.name == "Followed" {
//                self.fetchFollowing()
            } else {
                DataService.ds.REF_UserBlock_Uni_Confession.child((Auth.auth().currentUser?.uid)!).child(self.currentVisitingUni.name).observeSingleEvent(of: .value, with: { (snapshot) in
                    self.hiddenPosts = []
                    if let snapshots = snapshot.children.allObjects as? [DataSnapshot] {
                        for snap in snapshots {
                            if let dict = snap.value as? Dictionary<String, AnyObject> {
                                let key = snap.key as String
                                
                                self.hiddenPosts.append(key)
                                
                            }
                        }
                        
                    }
                    
                    DataService.ds.REF_UniConfession.child(self.currentVisitingUni.name).queryLimited(toLast: UInt(self.numberOfObjectsinArray)).observeSingleEvent(of: .value, with: { (snapshot) in
                        //  DataService.ds.REF_CONFESSIONS.queryOrdered(byChild: "university").queryEqual(toValue: self.uniName).queryLimited(toLast: UInt(self.numberOfObjectsinArray)).observeSingleEvent(of: .value, with: { (snapshot) in
                        self.confessionDict.removeAll()
                        self.confessions = []
                        //                    self.elements = []
                        
                        if let snapshots = snapshot.children.allObjects as? [DataSnapshot] {
                            for snap in snapshots {
                                //print("SNAP: \(snap)")
                                if let confessionDict = snap.value as? Dictionary<String, AnyObject> {
                                    
                                    
                                    let key = snap.key
                                    if( !self.hiddenPosts.contains(key) ) {
                                        print("i didnt block \(key)")
                                        let confession = Confession(confessionKey: key, postData: confessionDict)
                                        
                                        //self.confessions.append(confession)
                                        
                                        if confession.confessionKey != DISCUSSION {
                                            print("adding confess to dict \(confession.confessionKey)")
                                            self.confessionDict[key] = confession
                                        }
                                        
                                        
                                        
                                        
                                    }
                                    
                                    
                                    
                                }
                            }
                            self.attemptToReloadTable()
                            
                        }
                        
                        
                        
                        
                    })
                    
                    
                    //  }
                })
            }
            
            DataService.ds.REF_CONFESSIONS.queryOrdered(byChild: "university").queryEqual(toValue: self.uniName).observe(.childRemoved, with: { (snapshot) in
                
                self.confessionDict.removeValue(forKey: snapshot.key)
                self.totalConfession = self.totalConfession - 1
                
                
            })
        }
        
        

        
        
        

        
        
        self.tableView.isUserInteractionEnabled = true
        
        
    }
    
    func attemptToReloadTable() {
        
        
        //      self.timer?.invalidate()
        
        //        self.timer = Timer.scheduledTimer(timeInterval: 0.5, target: self, selector: #selector(reloadAndSortTable), userInfo: nil, repeats: false)
        //
        reloadAndSortTable()
    }
    
    func reloadAndSortTable() {
       // self.refreshControl.endRefreshing()
        
        self.confessions = Array(self.confessionDict.values)
//
        if self.currentVisitingUni.name == "Trending" {
            self.confessions.sort(by: { (c1, c2) -> Bool in
                
                return (c1.likes) > (c2.likes)
            })
        } else {
            self.confessions.sort(by: { (c1, c2) -> Bool in
                
                return (c1.time.intValue) > (c2.time.intValue)
            })
        }

        
//        DispatchQueue.main.async() {
//            self.tableView.reloadData()
//        }
        

//        self.actInd.stopAnimating()
//        self.tableView.tableFooterView? = UIView()


        
//        if self.loadingScreen.isHidden == false {
//            self.loadingScreen.isHidden = true
//        }
//
        if self.loadingScreen.alpha != 0.0 {
            UIView.animate(withDuration: 0.2, delay: 0.0, options:[], animations: { self.loadingScreen.alpha = 0.0 }, completion: nil)
        }

        
   
        

        if refreshControl.isRefreshing {
            refreshControl.endRefreshing()
        }
        self.tableView.isUserInteractionEnabled = true
        self.tabBarController?.tabBar.isUserInteractionEnabled = true
        self.navigationController?.navigationBar.isUserInteractionEnabled = true
        SVProgressHUD.dismiss()
        

        if self.confessions.count == 0 {
            if self.mainConfession == nil {
                self.showEmptyView()
            }
//            self.showEmptyView()
        } else {
            self.hideEmptyView()
        }
        
        
        self.tableView.reloadData()
        
        

        
        
        if animateTableView {
            UIView.animate(views: tableView.visibleCells, animations: animations, completion: {
            })
        }
        
        
        animateTableView = true

       
        
    }

    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if tableView == self.tableView {
            
            if isNavHiding {
                unhideNav()
            }
            if self.mainConfession == nil {
                self.currentConfessionId  = confessions[indexPath.row].confessionKey
                self.currentConfession = confessions[indexPath.row]
                currentIndex = indexPath.row
                
                self.currentConfession?.postRef.observeSingleEvent(of: .value, with: { (snapshot) in
                    if snapshot.exists() {
                        self.performSegue(withIdentifier: "Comment3VC", sender: nil)
                    } else {
                        ErrorAlert().createAlert(title: "Something went wrong", msg: "Could not perform action", object: self)
                    }
                })
            } else {
                
                if indexPath.row == 0 {
                    print("touch main \(self.mainConfession?.caption)")
                    
                    self.currentConfessionId  = self.mainConfession?.confessionKey
                    self.currentConfession = self.mainConfession
                    currentIndex = 0
                    
                    self.currentConfession?.postRef.observeSingleEvent(of: .value, with: { (snapshot) in
                        if snapshot.exists() {
                            self.performSegue(withIdentifier: "Comment3VC", sender: nil)
                        } else {
                            ErrorAlert().createAlert(title: "Something went wrong", msg: "Could not perform action", object: self)
                        }
                    })
                    
                } else {
                    var newIndex = indexPath.row - 1
                    self.currentConfessionId  = confessions[newIndex].confessionKey
                    self.currentConfession = confessions[newIndex]
                    currentIndex = newIndex
                    
                    self.currentConfession?.postRef.observeSingleEvent(of: .value, with: { (snapshot) in
                        if snapshot.exists() {
                            self.performSegue(withIdentifier: "Comment3VC", sender: nil)
                        } else {
                            ErrorAlert().createAlert(title: "Something went wrong", msg: "Could not perform action", object: self)
                        }
                    })
                }

            }
            

        } else if tableView == self.uniTableView {
//            popover.dismiss()
//
//
//            self.tableView.scrollRectToVisible(CGRect(x: 0, y: 0, width: 1, height: 1), animated: false)
//            var uni = states[indexPath.section][indexPath.row]
//
//
//            didSelectUni(uniName: uni.name, uniShort: uni.shortName, isCategory: false)

        }

        
    }
    
    func didSelectUni(uniName: String, uniShort: String, isCategory: Bool, cat: String) {
        SVProgressHUD.show()
        self.changedUni = true
        self.fetchCategory = isCategory
        if self.user?.university == uniName {
            self.changedUni = false
        }
        
        if isCategory {
//            self.fetchCategory = true
            self.category = cat
            self.changedUni = false
        }
        //            self.selectedUni.name = uni.name
        self.currentVisitingUni.name = uniName
        DataService.ds.currentVisitingUni = self.currentVisitingUni.name
        self.button.setTitle("\(uniShort)", for: .normal)
        self.fetchData(changeUni: self.changedUni)
    }
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        if tableView == self.uniTableView {
            return states.count
        } else {
            return 1
        }
        
    }
    
//    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
//
//        if self.discussion == nil {
//            return nil
//        } else {
//            let cell = tableView.dequeueReusableCell(withIdentifier: "DiscussionCell") as? DiscussionCell
//            cell?.configureCell(dis: self.discussion!)
//            return cell
//        }
//
//
//    }
    
//    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
//
//        if tableView == self.tableView {
//            if self.discussion == nil {
//                return 1
//            } else {
//                return 400
//            }
//        } else {
//            return 20
//        }
//
//    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        var retVal = ""
        
        if tableView == self.uniTableView {
            switch section {
            case 0:
                retVal = "Top"
                break
            case 1:
                retVal = "ACT"
                break
            case 2:
                retVal = "NATIONAL"
                break
            case 3:
                retVal = "NSW"
                break
            case 4:
                retVal = "NT"
                break
            case 5:
                retVal = "QLD"
                break
            case 6:
                retVal = "SA"
                break
            case 7:
                retVal = "TAS"
                break
            case 8:
                retVal = "VIC"
                break
            case 9:
                retVal = "WA"
                break
            default:
                break
            }
            
            
            return retVal
        } else {
            return ""
        }

    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == self.tableView {
            
            if self.mainConfession == nil {
                return confessions.count
            } else {
                return confessions.count + 1
            }
            
        } else if tableView == self.uniTableView {
            return states[section].count
        } else {
            return 1
        }
        
       // return elements.count
    }
    

    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    
        
        if tableView == self.tableView {
            
            if self.mainConfession == nil {
                
                let confession = confessions[indexPath.row]
                
                let seconds = confession.time.doubleValue
                let timesStampDate = NSDate(timeIntervalSince1970: seconds)
                var timeAgo:String =  DateHelper().timeAgoSinceDate(timesStampDate as Date, currentDate: NSDate() as Date, numericDates: true)
                

                
                if confession.imageUrl == NO_PIC {
                    
                    let cell = tableView.dequeueReusableCell(withIdentifier: "ConfessionCell") as? ConfessionCell
                    cell?.confessionVC = self
                    cell?.configureCell(confession: confession)
                    
                    
                    //                if confession.authorUni != confession.university {
                    var shortUni = DataService.ds.convertUniNameToShort(uniName: confession.authorUni)
                    
                    cell?.dateLabel.text = "\(timeAgo)"
                    cell?.facultyLabel.text = "\(shortUni) - \(confession.faculty)"
                    
                    
                    
                    //                } else {
                    //                    cell?.dateLabel.text = "\(confession.faculty)・\(timeAgo)"
                    //                }
                    //cell?.commentButton.addTarget(self, action: "commentTapped", for: .touchUpInside)
                    
                    //                if gender == MALE {
                    //                    cell?.profilePic.image = UIImage(named: "male")
                    //                } else if gender == FEMALE {
                    //                    cell?.profilePic.image = UIImage(named: "female")
                    //                }
                    return cell!
                }
                else {
                    
                    
                    let cell2 = tableView.dequeueReusableCell(withIdentifier: "ConfessionPicCell") as? ConfessionPicCell
                    
                    cell2?.confessionPic.image = nil
                    
                    
                    
                    cell2?.confessionPic.loadImageUsingCacheWithUrlString(imageUrl: confession.imageUrl!, tv: self.tableView, index: indexPath)
                    
                    
                    
                    cell2?.configureCell(confession: confession)
                    
                    //                if confession.authorUni != confession.university {
                    var shortUni = DataService.ds.convertUniNameToShort(uniName: confession.authorUni)
                    
//                    cell2?.dateLabel.text = "\(shortUni) - \(confession.faculty)・\(timeAgo)"
                    cell2?.dateLabel.text = "\(timeAgo)"
                    cell2?.facultyLabel.text = "\(shortUni) - \(confession.faculty)"
                    //                } else {
                    //                    cell2?.dateLabel.text = "\(confession.faculty)・\(timeAgo)"
                    //                }
                    
                    
                    
                    //                cell2?.confessionPic.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 200)
                    
                    
                    
                    //
                    //                if gender == MALE {
                    //                    cell2?.profilePic.image = UIImage(named: "male")
                    //                } else if gender == FEMALE {
                    //                    cell2?.profilePic.image = UIImage(named: "female")
                    //                }
                    
                    
                    
                    
                    cell2?.confessionVC = self
                    return cell2!
                }
                
                //    }
                
                
                
                //  return UITableViewCell()
            } else {
                
                if indexPath.row == 0 {
                    let cell = tableView.dequeueReusableCell(withIdentifier: "DiscussionCell") as? DiscussionCell
                    cell?.configureCell(confession: self.mainConfession!)
                    return cell!
                } else {
                    
              
                    var newIndex = indexPath.row - 1
                    let confession = confessions[newIndex]
                    
                    let seconds = confession.time.doubleValue
                    let timesStampDate = NSDate(timeIntervalSince1970: seconds)
                    var timeAgo:String =  DateHelper().timeAgoSinceDate(timesStampDate as Date, currentDate: NSDate() as Date, numericDates: true)
                    
                    var dateFormatter = DateFormatter()
                    
                    dateFormatter.dateFormat = "dd, MMM"
                    
                    if confession.imageUrl == NO_PIC {
                        
                        let cell = tableView.dequeueReusableCell(withIdentifier: "ConfessionCell") as? ConfessionCell
                        cell?.confessionVC = self
                        cell?.configureCell(confession: confession)
                        
                        
                        //                if confession.authorUni != confession.university {
                        var shortUni = DataService.ds.convertUniNameToShort(uniName: confession.authorUni)
                        
//                        cell?.dateLabel.text = "\(shortUni) - \(confession.faculty)・\(timeAgo)"
                        cell?.dateLabel.text = "\(timeAgo)"
                        cell?.facultyLabel.text = "\(shortUni) - \(confession.faculty)"
                        
                        
                        
                        //                } else {
                        //                    cell?.dateLabel.text = "\(confession.faculty)・\(timeAgo)"
                        //                }
                        //cell?.commentButton.addTarget(self, action: "commentTapped", for: .touchUpInside)
                        
                        //                if gender == MALE {
                        //                    cell?.profilePic.image = UIImage(named: "male")
                        //                } else if gender == FEMALE {
                        //                    cell?.profilePic.image = UIImage(named: "female")
                        //                }
                        return cell!
                    }
                    else {
                        
                        
                        let cell2 = tableView.dequeueReusableCell(withIdentifier: "ConfessionPicCell") as? ConfessionPicCell
                        
                        cell2?.confessionPic.image = nil
                        
                        
                        
                        cell2?.confessionPic.loadImageUsingCacheWithUrlString(imageUrl: confession.imageUrl!, tv: self.tableView, index: indexPath)
                        
                        
                        
                        cell2?.configureCell(confession: confession)
                        
                        //                if confession.authorUni != confession.university {
                        var shortUni = DataService.ds.convertUniNameToShort(uniName: confession.authorUni)
                        
//                        cell2?.dateLabel.text = "\(shortUni) - \(confession.faculty)・\(timeAgo)"
                        cell2?.dateLabel.text = "\(timeAgo)"
                        cell2?.facultyLabel.text = "\(shortUni) - \(confession.faculty)"
                        //                } else {
                        //                    cell2?.dateLabel.text = "\(confession.faculty)・\(timeAgo)"
                        //                }
                        
                        
                        
                        //                cell2?.confessionPic.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 200)
                        
                        
                        
                        //
                        //                if gender == MALE {
                        //                    cell2?.profilePic.image = UIImage(named: "male")
                        //                } else if gender == FEMALE {
                        //                    cell2?.profilePic.image = UIImage(named: "female")
                        //                }
                        
                        
                        
                        
                        cell2?.confessionVC = self
                        return cell2!
                    }
                    
                    //    }
                    
                    
                    
                    //  return UITableViewCell()
                }
            }
            

            
            

            

        
        } else if tableView == self.uniTableView {
//            let cell = UITableViewCell(style: .default, reuseIdentifier: nil)
////            cell.textLabel?.text = self.states[indexPath.section][indexPath.row].name
//            let imageView = UIImageView(frame: CGRect(x: 20, y: 0, width: 30, height: 30))
//            imageView.image = #imageLiteral(resourceName: "hash")
//            cell.addSubview(imageView)
//            var lbl = UILabel(frame: CGRect(x: 70, y: 0, width: cell.frame.width - 20, height: 50))
//            lbl.text = self.states[indexPath.section][indexPath.row].name
//            cell.addSubview(lbl)
//
//
//
//            return cell
            
            let cell : DropDownUniCell = tableView.dequeueReusableCell(withIdentifier: "DropDownUniCell") as! DropDownUniCell
            cell.configureCell(name: self.states[indexPath.section][indexPath.row].name)
            return cell
        } else {
            return UITableViewCell()
        }
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
//        print("calling in the end \(self.confessions.count) -  \(totalConfession) - \(indexPath.row)")
//        print("calling2 in the end")
        if tableView != self.uniTableView {
            
            if self.mainConfession != nil {
                

                

                if indexPath.row == self.confessions.count - 2 || (self.confessions.count - 1) - indexPath.row == (self.confessions.count - 1) {
                    print("in the end")
                    
                    // self.tableView.isUserInteractionEnabled = false
                    self.tableView.tableFooterView = actInd
                    self.actInd.startAnimating()
                    // self.tableView.reloadData()
                    displayNoInternetConnection()
                    print("toal \(totalConfession)")
                    print("count \(confessions.count)")
                    
//                    if self.currentVisitingUni.name == "Followed" {
//                        if totalConfession - 1 > confessions.count {
//
//
//                            Timer.scheduledTimer(timeInterval: 0.5, target: self, selector: #selector(self.addMoreObjects), userInfo: nil, repeats: false)
//
//
//
//
//                        } else {
//                            print("STOP")
//                            self.tableView.isUserInteractionEnabled = true
//                            self.actInd.stopAnimating()
//                            self.tableView.tableFooterView = UIView()
//                        }
//                    } else {
                        if totalConfession - 1 > confessions.count {
                            self.timer = Timer.scheduledTimer(timeInterval: 0.5, target: self, selector: #selector(self.addMoreObjects), userInfo: nil, repeats: false)
                            
                            
                        } else {
                            self.tableView.isUserInteractionEnabled = true
                            self.actInd.stopAnimating()
                            self.tableView.tableFooterView = UIView()
                        }
//                    }

                    
                }
            } else {
                
                if indexPath.row == self.confessions.count - 1 {
                    print("in the end")
                    
                    // self.tableView.isUserInteractionEnabled = false
                    self.tableView.tableFooterView = actInd
                    self.actInd.startAnimating()
                    // self.tableView.reloadData()
                    displayNoInternetConnection()
                    
//                    if self.currentVisitingUni.name == "Followed" {
//                        if totalConfession! > (self.totalConfessionRead + self.totalAnonymousConfessionRead) {
//
//                            Timer.scheduledTimer(timeInterval: 0.5, target: self, selector: #selector(self.addMoreObjects), userInfo: nil, repeats: false)
//
//
//
//
//                        } else {
//                            print("STOP")
//                            self.tableView.isUserInteractionEnabled = true
//                            self.actInd.stopAnimating()
//                            self.tableView.tableFooterView = UIView()
//                        }
//                    } else {
                        if totalConfession > confessions.count {
                            self.timer = Timer.scheduledTimer(timeInterval: 0.5, target: self, selector: #selector(self.addMoreObjects), userInfo: nil, repeats: false)
                            
                            
                        } else {
                            self.tableView.isUserInteractionEnabled = true
                            self.actInd.stopAnimating()
                            self.tableView.tableFooterView = UIView()
                        }
//                    }

                    
                }
            }

        }


        
    }
    
//    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
//
//
//        if tableView == self.uniTableView {
//            return 20
//        }
//    }
//
//    private func estimateFrameForText(text: String) -> CGRect {
//        let size = CGSize(width: 200, height: 1000)
//        let options = NSStringDrawingOptions.usesFontLeading.union(.usesLineFragmentOrigin)
//        return NSString(string: text).boundingRect(with: size, options: options, attributes: [NSFontAttributeName: UIFont.systemFont(ofSize: 16)], context: nil)
//        
//    }
    
    public func animationController(forPresented presented: UIViewController, presenting: UIViewController, source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        transition.transitionMode = .present
        transition.startingPoint = addBtn.center
        transition.bubbleColor = UIColor.white
        return transition
    }
    
    public func animationController(forDismissed dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        transition.transitionMode = .dismiss
//        transition.startingPoint = addBtn.center
        transition.bubbleColor = UIColor.white
        transition.startingPoint = CGPoint(x: addBtn.center.x, y: addBtn.center.y + 80)
    
        return transition
    }
    
    let transition = BubbleTransition()
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if let destinationVC = segue.destination as? Comment3VC {
        
//            destinationVC.confession = self.currentConfession
            destinationVC.confessionId = self.currentConfessionId
            destinationVC.delegate = self
//            destinationVC.passDeletedConfessionDelegate = self
            destinationVC.passReloadCommentConfessionDelegate = self
//            destinationVC.uniName = self.uniName
            
            if self.currentVisitingUni.name == "Trending" || self.currentVisitingUni.name == "Latest" || fetchCategory {
                destinationVC.uniName = self.user?.university
            } else {
                destinationVC.uniName = self.currentVisitingUni.name
                print("current visiting uni \(destinationVC.uniName)")
            }
            
            destinationVC.user = self.user
 
        }
        
        
        if let destinationVC = segue.destination as? HashtagVC {
            destinationVC.hashtag = self.hashtag
//            destinationVC.uniName = self.uniName
            if self.currentVisitingUni.name == "Trending" || self.currentVisitingUni.name == "Latest" || fetchCategory{
                destinationVC.uniName = self.user?.university
            } else {
                destinationVC.uniName = self.currentVisitingUni.name
            }
            
            destinationVC.currentConfession = self.currentConfession
            destinationVC.delegate = self
            
        }

        if segue.identifier == "AddConfessionVC" {
            let popVc = segue.destination
//            popVc.transitioningDelegate = self
//            popVc.modalPresentationStyle = .custom
            

            
//            popVc.popoverPresentationController?.delegate = self
            
            let pvc = storyboard?.instantiateViewController(withIdentifier: "AddConfessionVC") as! AddConfessionVC
            pvc.delegate = self
            pvc.user = self.user!
            
            if self.user?.email == ADMIN_EMAIL {
                pvc.isAdmin = true
                pvc.navigationItem.title = "New Discussion";
            } else {
                pvc.isAdmin = false
                pvc.navigationItem.title = "New Confession";
            }
            
//            pvc.uniName = self.uniName
            if self.currentVisitingUni.name == "Trending" || self.currentVisitingUni.name == "Latest" || fetchCategory {
                pvc.uniName = self.user?.university
            } else {
                pvc.uniName = self.currentVisitingUni.name
            }
            
            let navController = UINavigationController(rootViewController: pvc)
            navController.transitioningDelegate = self
            navController.modalPresentationStyle = .custom
//
//            pvc.isHeroEnabled = true
//            pvc.heroModalAnimationType = .fade
//            self.hero_replaceViewController(with: pvc)

            self.present(navController, animated: true, completion: nil)
        }
        
        
        
    }

    func initFooter() {
      
        actInd = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.gray)
        actInd.frame = CGRect(x: 0, y: 0, width: 320, height: 40)
        actInd.hidesWhenStopped = true
//        self.tableView.tableFooterView = actInd
        

    }
    

    
    func addMoreObjects() {
        
        animateTableView = false
        numberOfObjectsinArray += 10
        
        if fetchCategory {
            self.fetchCategoryData()
        } else {
            if self.currentVisitingUni.name == "Latest" {
                self.fetchLatest()
            } else if self.currentVisitingUni.name == "Trending" {
                self.fetchTrending()
            } else if self.currentVisitingUni.name == "Followed" {
                self.fetchFollowing()
            } else {
                DataService.ds.REF_UniConfession.child(self.currentVisitingUni.name).queryLimited(toLast: UInt(self.numberOfObjectsinArray)).observeSingleEvent(of: .value, with: { (snapshot) in
                    // DataService.ds.REF_CONFESSIONS.queryOrdered(byChild: "university").queryEqual(toValue: self.uniName).queryLimited(toLast: UInt(numberOfObjectsinArray)).observeSingleEvent(of: .value, with: { (snapshot) in
                    
                    
                    if let snapshots = snapshot.children.allObjects as? [DataSnapshot] {
                        for snap in snapshots {
                            //print("SNAP: \(snap)")
                            if let confessionDict = snap.value as? Dictionary<String, AnyObject> {
                                let key = snap.key
                                
                                if( !self.hiddenPosts.contains(key)) {
                                    let confession = Confession(confessionKey: key, postData: confessionDict)
                                    
                                    if confession.confessionKey != DISCUSSION {
                                        self.confessionDict[key] = confession
                                    }
                                    
                                    
                                }
                            }
                        }
                        self.attemptToReloadTable()
                    }
                    
                    
                    
                    
                    self.actInd.stopAnimating()
                    //  self.tableView.tableFooterView? = UIView()
                    
                    
                })
            }
        }
        


    }
    
    
    @IBAction func addTapped(_ sender: Any) {
        addConfessionTapped()
    }
    
    func addConfessionTapped() {
        performSegue(withIdentifier: "AddConfessionVC", sender: nil)

    }
    
    func slideMenuTapped() {
        self.slideMenuController()?.openLeft()
    }
    
    func refresh() {
        self.tableView.isUserInteractionEnabled = false
        displayNoInternetConnection()
        let ref = DataService.ds.REF_USERS.child((Auth.auth().currentUser?.uid)!).child("university")
        ref.removeAllObservers()
        
        // self.tableView.isScrollEnabled = false
        
        
        Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(reloadDataWhenPulled), userInfo: nil, repeats: false)
 
    }
    
    func reloadDataWhenPulled() {
     
        if self.refreshControl.isRefreshing {
          
           // reloadTable()
          //  self.tableView.reloadData()
            fetchData(changeUni: changedUni)
           // self.refreshControl.endRefreshing()
            
           

        }
        
    }
    
    var previousController: UIViewController?
    

    
    func tabBarController(_ tabBarController: UITabBarController, didSelect viewController: UIViewController) {
        print("tab bar tapped \(self.tabBarController?.selectedIndex)")
        print(" \(self.totalConfession )")
        
        
        if self.tabBarController?.selectedIndex == 0 {
            print("hai3")
            
            
            if scrollToTop {
                if totalConfession > 0 {
                    let indexPath = NSIndexPath(item: 0, section: 0)
                    self.tableView.scrollToRow(at: indexPath as IndexPath, at: .top, animated: true)
                }
            } else {
                scrollToTop = true
            }

        }
    


        
    }
    var startingImgView: UIImageView?
    
    func performZoomInForStartingImageView(startingImageView: UIImageView) {
        print("zoom in")
        
        startingFrame = startingImageView.superview?.convert(startingImageView.frame, to: nil)
        startingImgView = startingImageView
        startingImgView?.isHidden = true
        
        let zoomingImageView = UIImageView(frame: startingFrame!)
        zoomingImageView.contentMode = .scaleAspectFit
        zoomingImageView.clipsToBounds = true
        zoomingImageView.image = startingImageView.image
        zoomingImageView.isUserInteractionEnabled = true
        zoomingImageView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(handleZoomOut)))
        
        
        
        if let keyWindow = UIApplication.shared.keyWindow {
            blackBlackgroundView = UIView(frame: keyWindow.frame)
            blackBlackgroundView?.backgroundColor = UIColor.black
            blackBlackgroundView?.alpha = 0
            keyWindow.addSubview(blackBlackgroundView!)
            keyWindow.addSubview(zoomingImageView)
            
            
            UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
                self.blackBlackgroundView?.alpha = 1
                //self.inputContainerView.alpha = 0
                let height = (self.startingFrame?.height)! / (self.startingFrame?.width)! * keyWindow.frame.width
                
                //zoomingImageView.frame = CGRect(x: 0, y: 0, width: keyWindow.frame.width, height: height)
                zoomingImageView.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: UIScreen.main.bounds.size.height)
                
                zoomingImageView.center = keyWindow.center
                self.navigationController?.isNavigationBarHidden = true
           
                
            }, completion: { (comepleted: Bool) in
                
            })
        }
        
    }
    
    override var prefersStatusBarHidden: Bool {
        return navigationController?.isNavigationBarHidden == true
    }
    
    override var preferredStatusBarUpdateAnimation: UIStatusBarAnimation {
        return UIStatusBarAnimation.none
    }
    
    func handleZoomOut(sender: UITapGestureRecognizer) {
        print("sup2")
        navigationController?.isNavigationBarHidden = false
        
//        if let zoomOutImageView = sender.view {
//            zoomOutImageView.layer.cornerRadius = 2
//            zoomOutImageView.contentMode = .scaleAspectFill
//            zoomOutImageView.clipsToBounds = true
//            
////            UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
////                zoomOutImageView.frame = self.startingFrame!
////                self.blackBlackgroundView?.alpha = 0
////            }, completion: { (comepleted: Bool) in
////                self.startingImgView?.isHidden = false
////                zoomOutImageView.removeFromSuperview()
////            })
//            
//            
//            UIView.animate(withDuration: 0.5, animations: { 
//                zoomOutImageView.frame = self.startingFrame!
//                self.blackBlackgroundView?.alpha = 0
//            }, completion: { (comepleted: Bool) in
//                self.startingImgView?.isHidden = false
//                zoomOutImageView.removeFromSuperview()
//            })
//           
//          
//        }
        
        if let zoomOutImageView = sender.view {
            zoomOutImageView.layer.cornerRadius = 2
            zoomOutImageView.contentMode = .scaleAspectFill
            zoomOutImageView.clipsToBounds = true
            
            UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: [], animations: {
                zoomOutImageView.frame = self.startingFrame!
                self.blackBlackgroundView?.alpha = 0
              //  self.inputContainerView.alpha = 1
                
            }, completion: { (comepleted: Bool) in
                self.startingImgView?.isHidden = false
                zoomOutImageView.removeFromSuperview()
            })
            
            
            
        }
        

    }
    
    var key: String?
    
    func deleteConfession(confessionKey: String) {
        
        SVProgressHUD.show(withStatus: "Deleting")
        UIApplication.shared.keyWindow?.addSubview(self.dimView)
        
        DataService.ds.REF_CONFESSIONS.child(confessionKey).removeValue()
        DataService.ds.REF_USERS.child((Auth.auth().currentUser!.uid) ).child("likes").child(confessionKey).removeValue()
        
        DataService.ds.REF_CONFESSION_COMMENTS.child(confessionKey).observeSingleEvent(of: .value, with: { (snapshot) in
            
            
            if let snapshots = snapshot.children.allObjects as? [DataSnapshot] {
                
                for snap in snapshots {
               
                   DataService.ds.REF_COMMENTS.child(snap.key).removeValue()
                }
                
            }
            
        })
        key = confessionKey
        Timer.scheduledTimer(timeInterval: 2, target: self, selector: #selector(deleteCompletion), userInfo: nil, repeats: false)
        
        
        
        confessionDict.removeValue(forKey: confessionKey)
        totalConfession = totalConfession - 1
        attemptToReloadTable()

    }
    
    func deleteCompletion() {
        if let k = key {
            DataService.ds.REF_CONFESSION_COMMENTS.child(k).removeValue()
        }
        
        SVProgressHUD.dismiss()
        self.dimView.removeFromSuperview()
        
    }
    
    
    func sendConfessionToPreviousVC(confession: Confession) {
    
        if fetchCategory {
            if let cat = confession.category {
                if self.category == cat {
                    confessionDict[confession.confessionKey] = confession
                }
            }
            
        } else {
            
            if self.currentVisitingUni.name != "Followed" {
                confessionDict[confession.confessionKey] = confession
            }
            
        }
        
        
        attemptToReloadTable()
    }
    
    func reloadLikeFromCommentVC(confession: Confession, reloadLike: Bool) {
        
        confessionDict[confession.confessionKey] = confession
        attemptToReloadTable()
    }
    
    func reloadLikeFromHashtagVC(confession: Confession, reloadLike: Bool) {
 
        confessionDict[confession.confessionKey] = confession
        attemptToReloadTable()
    }
    
    func reloadCommentFromPreviousVC(confession: Confession, addComment: Bool, isDiscussion: Bool) {
        
        if isDiscussion {
            self.mainConfession = confession
        } else {
            if addComment {
                //  print("Reload add comment in Main")
                confessionDict[confession.confessionKey] = confession
                
                attemptToReloadTable()
                //self.reloadTable()
            } else {
                print("Reload minus comment in Main")
                self.reloadAndSortTable()
                //            for c in confessions {
                //                if c.confessionKey == confession.confessionKey {
                //                    c.comments = c.comments - 1
                //                    self.reloadTable()
                //                }
                //            }
            }
        }


    }
    
//    func version2(confession: Confession) {
//        for c in confessions {
//            if c.confessionKey == confession.confessionKey {
//                c.comments = c.comments - 1
//                self.reloadAndSortTable()
//            }
//        }
//    }
    
    func reloadTable() {
        self.tableView.reloadData()
    }
    
    func reloadDeletedConfessionFromPreviousVC(confession: Confession) {
     
        print("reload from deleted or hidden")
        print(" \(self.totalConfession )")
        self.totalConfession = self.totalConfession - 1
        confessionDict.removeValue(forKey: confession.confessionKey)
        attemptToReloadTable()
        
        self.tableView.isUserInteractionEnabled = false
        self.timer = Timer.scheduledTimer(timeInterval: 0.5, target: self, selector: #selector(avoidUserMultipleTap), userInfo: nil, repeats: false)

    }
    

    
    func avoidUserMultipleTap() {
        self.tableView.isUserInteractionEnabled = true
    }
    
    func addComment(confessionKey: String, confession: Confession) {
        
        self.currentConfessionId  = confessionKey
        self.currentConfession = confession
      
        performSegue(withIdentifier: "Comment3VC", sender: nil)
        
    }
    
    var hashtag: String?
    
    func hashTap(hashtag: String, confession: Confession) {
        
        self.hashtag = hashtag.lowercased()
        self.currentConfession = confession
        performSegue(withIdentifier: "HashtagVC", sender: nil)
    }
    
    func reloadConfesionInSearchVC(confession: Confession, reloadLike: Bool) {
        
        
        for navVC in (self.tabBarController?.viewControllers)! {
            
            let nav = navVC as! UINavigationController
            for vcs in nav.viewControllers {
                
                if vcs is HashtagVC {
                   print("reload in hashtag")
                    let hashVC = vcs as! HashtagVC
                    
                    if hashVC.confessionDict[confession.confessionKey] != nil {
                        hashVC.confessionDict[confession.confessionKey] = confession
                        hashVC.attemptToReloadTable()
                    }
                    
                    
                }
                
                if DataService.ds.currentUser.userKey == confession.author {
                    if vcs is SelfConfessionVC {
                        
                        //                    let selfVC = vcs as! SelfConfessionVC
                        if let selfVC = vcs as? SelfConfessionVC {
                            selfVC.confessionDict[confession.confessionKey] = confession
                            //                        selfVC.attemptToReloadTable()
                        }
                        
                    }
                }

                
            }
            
        }
    }

}
