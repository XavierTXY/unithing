//
//  MessageModel.swift
//  UniThing
//
//  Created by Xavier TanXY on 19/12/16.
//  Copyright © 2016 Xavier TanXY. All rights reserved.
//

import Foundation
import Firebase

class MessageModel: NSObject {

    var msgId: String?
    var fromId: String?
    var fromUserName: String?
    var toUserName: String?
    var toId: String?
    var timeStamp: NSNumber?
    var text: String?
    var imageUrl: String?
    
    var fromSeen: Bool?
    var toSeen: Bool?
    
    var imgWidth: NSNumber?
    var imgHeight: NSNumber?
    
    
    func chatPartnerId() -> String? {
        let chatPartnerId: String?
        
        if fromId == Auth.auth().currentUser?.uid {
            return toId
        } else {
            return fromId
        }
    }
    
    init(key: String, dictionary: [String: Any]) {
        super.init()
        msgId = key
        fromId = dictionary["fromId"] as? String
        toId = dictionary["toId"] as? String
        timeStamp = dictionary["timeStamp"] as? NSNumber
        text = dictionary["text"] as? String
        fromUserName = dictionary["fromUserName"] as? String
        toUserName = dictionary["toUserName"] as? String
        imageUrl = dictionary["imageUrl"] as? String
        imgWidth = dictionary["imgWidth"] as? NSNumber
        imgHeight = dictionary["imgHeight"] as? NSNumber
       
        fromSeen = dictionary["fromSeen"] as? Bool
        toSeen = dictionary["toSeen"] as? Bool
    }
}
