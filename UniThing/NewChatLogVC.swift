//
//  NewChatLogVC
//  UniThing
//
//  Created by Xavier TanXY on 19/12/16.
//  Copyright © 2016 Xavier TanXY. All rights reserved.
//

import UIKit
import Firebase
import SVProgressHUD
import PopupDialog

class NewChatLogVC: UIViewController, UITextFieldDelegate, UICollectionViewDelegateFlowLayout, UIImagePickerControllerDelegate, UINavigationControllerDelegate, UICollectionViewDelegate, UICollectionViewDataSource  {
    
    
    
    lazy var inputContainerView: ChatInputContainerViewImage = {
        
        let chatInputContainerView = ChatInputContainerViewImage(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: 50))
        chatInputContainerView.newChatLogVC = self
        return chatInputContainerView
        
        
    }()
    
    
    @IBOutlet weak var collectionView: UICollectionView!
    var user: User?
    var messages = [MessageModel]()
    var seenArray = [Bool]()
    var msgIndex = 0
    var msgDict = [String: MessageModel]()
    var originalFrame : CGRect?
    var startingFrame : CGRect?
    var blackBlackgroundView: UIView?
    var timer: Timer?
    
    var listener : DatabaseHandle!
    var blockingListener : DatabaseHandle!
    
    var numberOfObjectsinArray = 10
    var totalMsg: Int!
    static var messageImageCache: NSCache<NSString, UIImage> = NSCache()
    
    var isAppear = true
    var blocked = false
    var isBeingBlocked = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        SVProgressHUD.show()
        self.collectionView.delegate = self
        self.collectionView.dataSource = self
        
        navigationItem.title = user?.userName
        
        //        originalFrame = self.tabBarController?.tabBar.frame
        //        let height = (self.tabBarController?.tabBar.frame.height)! - 49
        //        self.tabBarController?.tabBar.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: height)
        
        collectionView?.contentInset = UIEdgeInsetsMake(25, 0, 8, 0)
        collectionView?.scrollIndicatorInsets = UIEdgeInsetsMake(0, 0, 20, 0)
        
        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        layout.minimumInteritemSpacing = 40
        layout.minimumLineSpacing = 40
        collectionView!.collectionViewLayout = layout
        
        collectionView?.backgroundColor = UIColor(red: 248/255, green: 249/255, blue: 250/255, alpha: 1)
        collectionView?.keyboardDismissMode = .interactive
        collectionView?.alwaysBounceVertical = true
        collectionView?.register(ChatMessageCell.self, forCellWithReuseIdentifier: "cellId")
        //
        //        let rightBarItem = UIBarButtonItem(image: #imageLiteral(resourceName: "option"), style: .plain, target: self, action: "optionTapped")
        //        rightBarItem.tintColor = UIColor(red: 0/255, green: 105/255, blue: 192/255, alpha: 1)
        //        self.navigationItem.setRightBarButton(rightBarItem, animated: true)
        
        
        SVProgressHUD.show()
        setupKeyboardObservers()
        
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        UIApplication.shared.statusBarStyle = .default
        self.navigationController?.navigationBar.tintColor = PURPLE
        self.navigationController?.navigationBar.titleTextAttributes = [ NSForegroundColorAttributeName: PURPLE]
        
        if isAppear {
            getTotalMessages {
                self.fetchBeingBlockedList()
                self.fetchBlockList()
                self.observeMessages()
            }
        }
        
        //        self.tabBarController?.tabBar.isHidden = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        
        let userId = (self.user?.userKey)!
        let currentUid = Auth.auth().currentUser?.uid
        
        if isAppear {
            DataService.ds.REF_USER_MESSAGES.child(currentUid!).child(userId).removeObserver(withHandle: self.listener)
            DataService.ds.REF_UserBlock_PartnerID.child((self.user?.userKey)!).child(currentUid!).removeObserver(withHandle: self.blockingListener)
        }
        
        //        self.tabBarController?.tabBar.frame = originalFrame!
    }
    
    func textFieldDidChange(_ textField: UITextField) {
        
        if (textField.text?.characters.count)! > 0 && !(textField.text?.trimmingCharacters(in: .whitespaces).isEmpty)! {
            //            enableSendButton(tf: textField)
            print("enable")
            //            inputContainerView.inputTextField.enablesReturnKeyAutomatically = true
        } else {
            print("disable")
            //            inputContainerView.inputTextField.enablesReturnKeyAutomatically = false
            //            disableSendButton(tf: textField)
        }
    }
    
    func setupKeyboardObservers() {
//        NotificationCenter.default.addObserver(self, selector: #selector(handleKeyBoardDidShow), name: NSNotification.Name.UIKeyboardDidShow, object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(handleKeyboardNotification), name: NSNotification.Name.UIKeyboardDidShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(handleKeyboardNotification2), name: NSNotification.Name.UIKeyboardDidHide, object: nil)

    }
    
    func fetchBlockList() {
        let currentUid = Auth.auth().currentUser?.uid
        DataService.ds.REF_UserBlock_PartnerID.child(currentUid!).child((self.user?.userKey)!).observeSingleEvent(of: .value, with: { (snapshot) in
            if snapshot.exists() {
                print("YOU BLOCKED THIS")
                self.blocked = true
            } else {
                print("NOT BLOCKED")
                self.blocked = false
            }
        })
    }
    
    func fetchBeingBlockedList() {
        let currentUid = Auth.auth().currentUser?.uid
        self.blockingListener = DataService.ds.REF_UserBlock_PartnerID.child((self.user?.userKey)!).child(currentUid!).observe(.value, with: { (snapshot) in
            if snapshot.exists() {
                print("KENAB LOCKED")
                self.isBeingBlocked = true
            } else {
                print("NOT KENA BLOCKED")
                self.isBeingBlocked = false
            }
        })
    }
    

    func handleKeyboardNotification(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            print("walo")
            let userInfo = notification.userInfo!
            let animationDuration: TimeInterval = (userInfo[UIKeyboardAnimationDurationUserInfoKey] as! NSNumber).doubleValue
            
            self.collectionView.contentInset = UIEdgeInsets(top: 25, left: 0, bottom: keyboardSize.height, right: 0)
            
            UIView.animate(withDuration: animationDuration, animations: {
                self.view.layoutIfNeeded()
            }) { (completed) in
                if self.messages.count > 0 {
                    print("hi there")
                    
                    let indexPath = NSIndexPath(item: self.messages.count - 1, section: 0)
                    self.collectionView?.scrollToItem(at: indexPath as IndexPath, at: .bottom, animated: true)
                    
                    
                    
                }
            }
        }
        
    }
    
    func handleKeyboardNotification2(notification: NSNotification) {
            print("walo")
            let userInfo = notification.userInfo!
            let animationDuration: TimeInterval = (userInfo[UIKeyboardAnimationDurationUserInfoKey] as! NSNumber).doubleValue
            
            self.collectionView.contentInset = UIEdgeInsets(top: 25, left: 0, bottom: 8, right: 0)
            
        UIView.animate(withDuration: animationDuration, animations: {
            self.view.layoutIfNeeded()
        }) { (completed) in

        }

        
    }
    
    
    func handleKeyBoardDidShow() {
        if messages.count > 0 {
            print("hi there")
            self.collectionView.contentInset = UIEdgeInsets(top: 8, left: 0, bottom: 400+8, right: 0)
            let indexPath = NSIndexPath(item: self.messages.count - 1, section: 0)
            self.collectionView?.scrollToItem(at: indexPath as IndexPath, at: .bottom, animated: true)
  


        }
        
    }
    
    func displayEmptyTxtFieldAlert() {
        
        
        ErrorAlert().createAlert(title: "Reminder", msg: "Message can't be blank", object: self)
        
    }
    
    //    func enableSendButton(tf: UITextField) {
    //        //        self.inputContainerView.sendButton.tintColor = UIColor(red: 33/255, green: 150/255, blue: 243/255, alpha: 1)
    //        //        self.inputContainerView.sendButton.isEnabled = true
    //        inputContainerView.inputTextField.enablesReturnKeyAutomatically = true
    //    }
    //
    //    func disableSendButton(tf: UITextField) {
    //        //        self.inputContainerView.sendButton.tintColor = UIColor(red: 158/255, green: 158/255, blue: 158/255, alpha: 0.7)
    //        //        self.inputContainerView.sendButton.isEnabled = false
    //        inputContainerView.inputTextField.enablesReturnKeyAutomatically = false
    //    }
    
    
    func getTotalMessages(completion: @escaping () -> ()) {
        let currentUid = Auth.auth().currentUser?.uid
        
        DataService.ds.REF_USER_MESSAGES.child(currentUid!).child((self.user?.userKey)!).observeSingleEvent(of: .value, with: { (snapshot) in
            
            
            if let snapshots = snapshot.children.allObjects as? [DataSnapshot] {
                self.totalMsg = snapshots.count
                
                print("total \(snapshots.count)")
                completion()
                
            }
            
            
            
        })
    }
    
    
    
    func loadMore() {
        print("LOAD MORE")
        numberOfObjectsinArray = numberOfObjectsinArray + 10
        
        let currentUid = Auth.auth().currentUser?.uid
        let ref =  DataService.ds.REF_USER_MESSAGES.child(currentUid!)
        
        let userId = (self.user?.userKey)!
        let userRef = DataService.ds.REF_USER_MESSAGES.child(currentUid!).child(userId)
        
        userRef.queryLimited(toLast: UInt(self.numberOfObjectsinArray)).observeSingleEvent(of: .value, with: { (snapshot) in
            //            self.confessions = []
            //                    self.elements = []
            
            if let snapshots = snapshot.children.allObjects as? [DataSnapshot] {
                for snap in snapshots {
                    print("SNAP: \(snap)")
                    if let dict = snap.value as? Dictionary<String, AnyObject> {
                        
                        
                        let key = snap.key
                        let message = MessageModel(key: snap.key, dictionary: dict)
                        
                        //                            let confession = Confession(confessionKey: key, postData: confessionDict)
                        
                        self.msgDict[key] = message
                        
                        
                        
                    }
                }
                self.attemptToReload()
                
            }
        })
    }
    
    func updateSeen(msg: MessageModel) {
        var updateObj = [String:AnyObject]()
        let ref = DB_BASE
        updateObj["/messages/\(msg.msgId!)/toSeen"] = true as AnyObject
        ref.updateChildValues(updateObj)
    }
    
    func observeMessages() {
        let currentUid = Auth.auth().currentUser?.uid
        let ref =  DataService.ds.REF_USER_MESSAGES.child(currentUid!)
        
        //        ref.observe(.childAdded, with: { (snapshot) in
        
        let userId = (self.user?.userKey)!
        let userRef = DataService.ds.REF_USER_MESSAGES.child(currentUid!).child(userId)
        
        self.listener = userRef.observe(.childAdded, with: { (snapshot) in
            // print(snapshot)
            let msgId = snapshot.key
            let msgRef = DataService.ds.REF_MESSAGES.child(msgId)
            
            msgRef.observeSingleEvent(of: .value, with: { (snapshot) in
                
                let dictionary = snapshot.value as? [String: Any]
                
                
                let message = MessageModel(key: snapshot.key, dictionary: dictionary!)
                if message.toId == currentUid {
                    self.updateSeen(msg: message)
                }
                
                // message.setValuesForKeys(dictionary!)
                
                if message.chatPartnerId() == self.user?.userKey {
                    print("WAD")
                    //                        self.msgDict[message.msgId!] = message
                    //                        self.attemptToReload()
                    self.messages.append(message)
                    self.collectionView?.reloadData()
//
                    if self.messages.count > 0 {
                        print("hi there 2")
                        let indexPath = NSIndexPath(item: self.messages.count - 1, section: 0)
                        self.collectionView?.scrollToItem(at: indexPath as IndexPath, at: .bottom, animated: true)
                        SVProgressHUD.dismiss()
                    }
                    
                    //
                    //                    self.timer?.invalidate()
                    //
                    //                    self.timer = Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(self.reloadTable), userInfo: nil, repeats: false)
                    //
                    
                    
                }
                
            })
            
            
            
            
        })
        SVProgressHUD.dismiss()
        //        })
        
    }
    
    func timerReload() {
        Timer.scheduledTimer(timeInterval: 0.5, target: self, selector: #selector(reloadAndSortTable), userInfo: nil, repeats: false)
    }
    
    func reloadAndSortTable() {
        self.collectionView?.reloadData()
    }
    
    func showBeingBlockAlert() {
        
        if inputContainerView.inputTextField.isFirstResponder {
            inputContainerView.inputTextField.resignFirstResponder()
        }
        let popup = PopupDialog(title: "Message Not Sent", message: "This person isn't receiving messages at this time.", image: nil)
        
        //        var partnerID = messages[index].chatPartnerId()
        let buttonOne = DefaultButton(title: "OK") {
            
        }
        
        
        popup.addButtons([buttonOne])
        popup.buttonAlignment = .horizontal
        
        // Present dialog
        self.present(popup, animated: true, completion: nil)
    }
    func showBlockAlert() {
        
        if inputContainerView.inputTextField.isFirstResponder {
            inputContainerView.inputTextField.resignFirstResponder()
        }
        let popup = PopupDialog(title: "You blocked this chat", message: "Do you want to unblock?", image: nil)
        
        //        var partnerID = messages[index].chatPartnerId()
        let buttonOne = DefaultButton(title: "Unblock") {
            var updateObj = [String:AnyObject]()
            let ref = DB_BASE
            updateObj["/userBlock-partnerID/\((Auth.auth().currentUser?.uid)!)/\((self.user?.userKey)!)/blocked"] = NSNull()
            ref.updateChildValues(updateObj)
            self.blocked = false
            SVProgressHUD.setMinimumDismissTimeInterval(0.5)
            SVProgressHUD.showSuccess(withStatus: "Unblocked!")
            
        }
        
        let buttonTwo = CancelButton(title: "Cancel") {
            
        }
        
        popup.addButtons([buttonTwo,buttonOne])
        popup.buttonAlignment = .horizontal
        
        // Present dialog
        self.present(popup, animated: true, completion: nil)
    }
    
    func attemptToReload() {
        self.messages = Array(self.msgDict.values)
        self.collectionView?.reloadData()
    }
    
    
    func resetIsAppear() {
        self.isAppear = true
    }
    func uploadImageTapped() {
        print("tapped")
        isAppear = false
        
        if self.inputContainerView.inputTextField.isFirstResponder {
            self.inputContainerView.inputTextField.resignFirstResponder()
        }
        let imgPicker = UIImagePickerController()
        
        imgPicker.allowsEditing = false
        imgPicker.delegate = self
        //        present(imgPicker, animated: true, completion: nil)
        
        let alert = UIAlertController()
        alert.addAction(UIAlertAction(title: "Camera", style: UIAlertActionStyle.default, handler:{ (UIAlertAction)in
            imgPicker.sourceType = UIImagePickerControllerSourceType.camera;
            
            self.present(imgPicker, animated: true, completion: nil)
        }))
        
        alert.addAction(UIAlertAction(title: "Photo library", style: UIAlertActionStyle.default, handler:{ (UIAlertAction)in
            imgPicker.sourceType = UIImagePickerControllerSourceType.photoLibrary;
            self.present(imgPicker, animated: true, completion: nil)
        }))
        
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel) { (action:UIAlertAction!) in
            
            Timer.scheduledTimer(timeInterval: 1.5, target: self, selector: #selector(self.resetIsAppear), userInfo: nil, repeats: false)
        })
        
        self.present(alert, animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
        Timer.scheduledTimer(timeInterval: 1.5, target: self, selector: #selector(self.resetIsAppear), userInfo: nil, repeats: false)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        dismiss(animated: true, completion: nil)
        
        if isBeingBlocked && blocked {
            showBlockAlert()
        } else if isBeingBlocked {
            showBeingBlockAlert()
        } else if blocked {
            showBlockAlert()
        } else {
            SVProgressHUD.show()
            
            var selectedImageFromPicker: UIImage?
            
            if let editedImage = info[UIImagePickerControllerEditedImage] as? UIImage {
                selectedImageFromPicker = editedImage
            } else if let originalImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
                selectedImageFromPicker = originalImage
            }
            
            if let selectedImage = selectedImageFromPicker {
                uploadImageToFirebase(selectedImage: selectedImage)
            }
            
            
        }
        
        
        
        Timer.scheduledTimer(timeInterval: 1.5, target: self, selector: #selector(self.resetIsAppear), userInfo: nil, repeats: false)
    }
    
    private func uploadImageToFirebase(selectedImage: UIImage?) {
        
        
        if let img = selectedImage {
            if let imgData = UIImageJPEGRepresentation(img, 0.2) {
                
                let imgUid = NSUUID().uuidString
                let metaData = StorageMetadata()
                metaData.contentType = "image/jpeg"
                
                DataService.ds.REF_MESSAGE_PIC.child(imgUid).putData(imgData, metadata: metaData) { (metaData, error) in
                    if error != nil {
                        print("Xavier: upload image to firebase failed")
                        print(error.debugDescription)
                    } else {
                        print("Xavier: successfuly upload img to firebase")
                        SVProgressHUD.dismiss()
                        let downloadUrl = metaData?.downloadURL()?.absoluteString
                        if let url = downloadUrl {
                            self.postToFirebase(imgUrl: url, img: img)
                            
                            
                        }
                        
                    }
                    
                }
                
                
            }
        }
        
        
    }
    
    private func postToFirebase(imgUrl: String, img: UIImage) {
        messageSendWithImageUrl(imgUrl: imgUrl, img: img)
        
    }
    
    //    override func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
    //        if indexPath.row == 0 {
    //            if numberOfObjectsinArray < totalMsg {
    //                print("load more!")
    //
    //                loadMore()
    //            }
    //
    //        }
    //    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return messages.count
    }
    //
    //    override func scrollViewDidScroll(_ scrollView: UIScrollView) {
    //        // calculates where the user is in the y-axis
    //        let offsetY = scrollView.contentOffset.y
    //        let contentHeight = scrollView.contentSize.height
    //
    //        if offsetY < contentHeight - scrollView.frame.size.height {
    //
    //            print("load more")
    //            // increments the number of the page to request
    ////            indexOfPageRequest += 1
    //
    //            // call your API for more data
    ////            loadData()
    //
    //            // tell the table view to reload with the new data
    ////            self.tableView.reloadData()
    //        }
    //    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cellId", for: indexPath) as! ChatMessageCell
        let msg = messages[indexPath.row]
        cell.newChatLogVC = self
        
        cell.textView.text = msg.text
        
        setupCell(cell: cell, msg: msg)
        
        
        if let text = msg.text {
            
            cell.bubbleWidthAnchor?.constant = estimateFrameForText(text: msg.text!).width + 27
            cell.textView.isHidden = false
        } else if msg.imageUrl != nil {
            cell.bubbleWidthAnchor?.constant = 200
            cell.textView.isHidden = true
            
        }
        
        
        return cell
    }
    
    private func setupCell(cell: ChatMessageCell, msg: MessageModel) {
        
        
        
        if self.user?.profilePicUrl != NO_PIC {
            cell.profileImageView.loadImageUsingCacheWithUrlStringWithoutIndex(imageUrl: (self.user?.profilePicUrl)!)
            cell.initialLbl.isHidden = true
        } else {
            cell.initialLbl.isHidden = false
            var i = String((self.user?.userName[(self.user?.userName.startIndex)!])!).capitalized
            cell.initialLbl.text = i
            cell.profileImageView.backgroundColor = ColorHelper().pickColor(alphabet: Character(i))
        }
        
        let seconds = msg.timeStamp?.doubleValue
        let timesStampDate = NSDate(timeIntervalSince1970: seconds!)
        var dateFormatter = DateFormatter()
        
        dateFormatter.dateFormat = "MM/dd/yyyy, HH:mm"
        cell.dateLbl.text = dateFormatter.string(from: timesStampDate as Date)
        
        
        // reset everytime for anchor problem
        cell.bubbleRightAnchor?.isActive = false
        cell.bubbleLeftAnchor?.isActive = false
        
        if msg.fromId == Auth.auth().currentUser?.uid {
            
            cell.profileImageView.isHidden = true
            
            cell.bubbleView.backgroundColor = UIColor.blue
            cell.blueImg.isHidden = false
            cell.textView.textColor = UIColor.white
            
            cell.bubbleRightAnchor?.isActive = true
            cell.bubbleLeftAnchor?.isActive = false
        } else {
            
            cell.profileImageView.isHidden = false
            cell.bubbleView.backgroundColor = UIColor(red: 235/255, green: 235/255, blue: 235/255, alpha: 1.0)
            cell.blueImg.isHidden = true
            cell.textView.textColor = UIColor.black
            cell.bubbleRightAnchor?.isActive = false
            cell.bubbleLeftAnchor?.isActive = true
            
            
        }
        
        if let msgImgUrl = msg.imageUrl {
            cell.bubbleView.backgroundColor = UIColor.clear
            cell.messageImageView.backgroundColor = UIColor.clear
            cell.messageImageView.loadImageUsingCacheWithUrlStringWithoutIndex(imageUrl: msgImgUrl)
            cell.messageImageView.isHidden = false
            
        } else {
            cell.messageImageView.isHidden = true
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        var height: CGFloat = 80
        
        let msg = messages[indexPath.row]
        if let text = msg.text {
            height = estimateFrameForText(text: text).height + 20
        } else if let msgWidth = msg.imgWidth?.floatValue, let msgHeight = msg.imgHeight?.floatValue {
            
            height = CGFloat( msgHeight / msgWidth * 200 )
        }
        return CGSize(width: self.view.frame.width, height: height)
    }
    
    private func estimateFrameForText(text: String) -> CGRect {
        let size = CGSize(width: 200, height: 1000)
        let options = NSStringDrawingOptions.usesFontLeading.union(.usesLineFragmentOrigin)
        return NSString(string: text).boundingRect(with: size, options: options, attributes: [NSFontAttributeName: UIFont(name: "HelveticaNeue", size: 16)], context: nil)
        
    }
    
    override var inputAccessoryView: UIView? {
        get {
            
            return inputContainerView
        }
    }
    
    override var canBecomeFirstResponder: Bool {
        return true
    }
    
    private func messageSendWithImageUrl(imgUrl: String, img: UIImage) {
        let values = ["imageUrl": imgUrl, "imgWidth": img.size.width, "imgHeight": img.size.height] as [String : Any]
        handleSend(values: values)
        
    }
    
    func messageSend() {
        
        if isBeingBlocked && blocked {
            showBlockAlert()
        } else if isBeingBlocked {
            showBeingBlockAlert()
        } else if blocked {
            showBlockAlert()
        } else {
            let values = ["text": inputContainerView.inputTextField.text!]
            handleSend(values: values)
        }
        
        
    }
    
    
    private func handleSend(values: [String: Any])  {
        
        let ref = DataService.ds.REF_MESSAGES
        let firebaseMessage = DataService.ds.REF_MESSAGES.childByAutoId()
        let timestamp = NSNumber(integerLiteral: Int(NSDate().timeIntervalSince1970))
        let toId = user?.userKey
        let fromId = Auth.auth().currentUser?.uid
        
        user?.adjustMsgCount(addCount: true, fromId: fromId!)
        
        var dict: [String: Any] = ["toId": toId, "fromId": fromId, "timeStamp": timestamp, "fromUserName": DataService.ds.currentUser.userName, "fromSeen": true, "toSeen": false, "toUserName": (user?.userName)! ] as [String : Any]
        
        for (key, element) in values {
            
            dict[key] = element
            print(dict)
        }
        // values.forEach({dict[$0] = $1 })
        //      firebaseMessage.updateChildValues(values)
        
        firebaseMessage.updateChildValues(dict, withCompletionBlock: {(error, ref) in
            
            if error != nil {
                print(error)
            }
            
            self.inputContainerView.inputTextField.text = nil
            
            let userMsgRef = DataService.ds.REF_USER_MESSAGES.child(fromId!).child(toId!)
            let msgId = firebaseMessage.key
            userMsgRef.updateChildValues([msgId: true])
            
            let recipientUserMsgRef = DataService.ds.REF_USER_MESSAGES.child(toId!).child(fromId!)
            recipientUserMsgRef.updateChildValues([msgId: true])
            
            
        })
    }
    
    
    var startingImgView: UIImageView?
    
    func performZoomInForStartingImageView(startingImageView: UIImageView) {
        
        if self.inputContainerView.inputTextField.isFirstResponder {
            self.inputContainerView.inputTextField.resignFirstResponder()
        }
        
        startingFrame = startingImageView.superview?.convert(startingImageView.frame, to: nil)
        startingImgView = startingImageView
        startingImgView?.isHidden = true
        
        let zoomingImageView = UIImageView(frame: startingFrame!)
        zoomingImageView.contentMode = .scaleAspectFit
        zoomingImageView.clipsToBounds = true
        zoomingImageView.image = startingImageView.image
        zoomingImageView.isUserInteractionEnabled = true
        zoomingImageView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(handleZoomOut)))
        
        
        if let keyWindow = UIApplication.shared.keyWindow {
            blackBlackgroundView = UIView(frame: keyWindow.frame)
            blackBlackgroundView?.backgroundColor = UIColor.black
            blackBlackgroundView?.alpha = 0
            keyWindow.addSubview(blackBlackgroundView!)
            keyWindow.addSubview(zoomingImageView)
            
            
            UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
                self.blackBlackgroundView?.alpha = 1
                self.inputContainerView.alpha = 0
                let height = (self.startingFrame?.height)! / (self.startingFrame?.width)! * keyWindow.frame.width
                
                //zoomingImageView.frame = CGRect(x: 0, y: 0, width: keyWindow.frame.width, height: height)
                zoomingImageView.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: UIScreen.main.bounds.size.height)
                
                zoomingImageView.center = keyWindow.center
            }, completion: { (comepleted: Bool) in
                
            })
        }
        
    }
    
    func handleZoomOut(sender: UITapGestureRecognizer) {
        if let zoomOutImageView = sender.view {
            zoomOutImageView.layer.cornerRadius = 16
            zoomOutImageView.clipsToBounds = true
            
            UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
                zoomOutImageView.frame = self.startingFrame!
                self.blackBlackgroundView?.alpha = 0
                self.inputContainerView.alpha = 1
                
            }, completion: { (comepleted: Bool) in
                self.startingImgView?.isHidden = false
                zoomOutImageView.removeFromSuperview()
                
            })
            
            
            
        }
    }
    
    
}
