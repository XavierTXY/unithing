//
//  OtherProfileVC.swift
//  UniThing
//
//  Created by XavierTanXY on 21/5/18.
//  Copyright © 2018 Xavier TanXY. All rights reserved.
//

import UIKit
import Firebase
import SVProgressHUD
import PopupDialog
import ViewAnimator


class OtherProfileVC: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var navView: UIView!
    @IBOutlet weak var tableView: UITableView!
    var commentVC: Comment3VC!
    var navText: UILabel!
    var userID: String!
    var user: User!
    var actInd: UIActivityIndicatorView!
    
    var confessions = [Confession]()
    var confessionDict = [String: Confession]()
    var firstTime: Bool!
    
    //Profile View
    @IBOutlet weak var outerView: UIView!
    @IBOutlet weak var profilePic: UIImageView!
    @IBOutlet weak var initialLbl: UILabel!
    
    @IBOutlet weak var nameLbl: UILabel!
    @IBOutlet weak var userNameLbl: UILabel!
    @IBOutlet weak var detailLbl: UILabel!
    
    @IBOutlet weak var numPostsLbl: UILabel!
    @IBOutlet weak var numFollowerLbl: UILabel!
    @IBOutlet weak var numFollowingLbl: UILabel!
    @IBOutlet weak var followingStack: UIStackView!
    @IBOutlet weak var followerStack: UIStackView!
    
    @IBOutlet weak var followBtn: UIButton!
    
    private let animations = [AnimationType.from(direction: .bottom, offset: 30.0)]
    
    
    var totalConfessionRead = 0
    var totalAnonymousConfessionRead = 0
    
    var totalDisplayConfession = 0
    
    var isFollowing = false
    var emptyView: NoPostsView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        print("IN OTHERVC \(self.userID)")
        // Do any additional setup after loading the view.
        tableView.delegate = self
        tableView.dataSource = self
        
        tableView.tableFooterView = UIView()
        tableView.estimatedRowHeight = 400
        tableView.rowHeight = UITableViewAutomaticDimension
        
        initFooter()
        self.navigationItem.title = ""
        
        if userID != (Auth.auth().currentUser!.uid) {
            let rightBarItem = UIBarButtonItem(image: #imageLiteral(resourceName: "option"), style: .plain, target: self, action: "optionTapped")
            rightBarItem.tintColor = UIColor(red: 1, green: 1, blue: 1, alpha: 1)
            self.navigationItem.setRightBarButton(rightBarItem, animated: true)
        }


        self.navigationItem.titleView = navText
        getUserDetails()
        fetchData()
        
//        let ppTap = UITapGestureRecognizer(target: self, action: #selector(profilePicTapped))
//        ppTap.numberOfTapsRequired = 1
//        profilePic.addGestureRecognizer(ppTap)
//        profilePic.isUserInteractionEnabled = true
        
        outerView.layer.cornerRadius = outerView.frame.height / 2
        outerView.clipsToBounds = true
        
        profilePic.layer.cornerRadius = profilePic.frame.height / 2
        profilePic.clipsToBounds = true
        
        let following = UITapGestureRecognizer(target: self, action: #selector(followingTapped))
        let follower = UITapGestureRecognizer(target: self, action: #selector(followerTapped))
        following.numberOfTapsRequired = 1
        follower.numberOfTapsRequired = 1
        self.followerStack.addGestureRecognizer(follower)
        self.followerStack.isUserInteractionEnabled = true
        self.followingStack.addGestureRecognizer(following)
        self.followingStack.isUserInteractionEnabled = true
        
        
        emptyView = NoPostsView.instanceFromNib() as! NoPostsView
        emptyView.frame = CGRect(x: 0, y: 385, width: self.tableView.bounds.width, height: self.tableView.bounds.height-450)
        self.tableView.addSubview(emptyView)
        self.hideEmptyView()

    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        UIApplication.shared.statusBarStyle = .lightContent
        self.navigationController?.navigationBar.tintColor = UIColor.white
        self.slideMenuController()?.removeLeftGestures()
        
        animateTable = false
        self.reloadAndSortTable()
        
        if offset != nil {
            if offset > 1 {
                offset = 1
                self.navView.backgroundColor = UIColor(red: 68/255, green: 159/255, blue: 255/255, alpha: offset)
                UIView.animate(withDuration: 0.1, delay: 0.1, options: [], animations: { UIApplication.shared.statusBarView?.alpha = 0.0
                    self.navigationItem.hidesBackButton = true
                }, completion: nil)
                
            }
        }
        
        

        


    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        
    }
    
    func showEmptyView() {
        emptyView.isHidden = false
        self.tableView.bringSubview(toFront: emptyView)
        
    }
    
    func hideEmptyView() {
        emptyView.isHidden = true
        self.tableView.sendSubview(toBack: emptyView)
        
    }
    
    func initFooter() {
        
        actInd = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.gray)
        actInd.frame = CGRect(x: 0, y: 0, width: 320, height: 40)
        actInd.hidesWhenStopped = true
        //        self.tableView.tableFooterView = actInd
    }
    
    func getUserDetails() {
        DataService.ds.REF_USERS.child(self.userID).observeSingleEvent(of: .value, with: { (snapshot) in
            
            if let snapshots = snapshot.children.allObjects as? [DataSnapshot] {
                //print("\(snapshot.value)")
                
                if let userDict = snapshot.value as? Dictionary<String, AnyObject> {
                    let key = snapshot.key
                    self.user = User(userKey: key, userData: userDict)
                    self.setupUserDetails()

                    
                }
                
                
            }
            
        })
    }
    
    func setupUserDetails() {
        

        
        
        nameLbl.text = user?.name
        userNameLbl.text = "@\((user?.userName)!)"
       

        if DataService.ds.currentUser.isFollowing(userID: self.user.userKey!) {

    
            let attributedString = NSMutableAttributedString(string: "MESSAGE")

            let firstAttributes:[String:Any] = [NSForegroundColorAttributeName: UIColor.white]
            attributedString.addAttributes(firstAttributes, range: NSRange(location: 0, length: 7))
            
            
            followBtn.setAttributedTitle(attributedString, for: .normal)
            followBtn.setBackgroundImage(#imageLiteral(resourceName: "MessageBtn"), for: .normal)

        } else {
            
            let attributedString = NSMutableAttributedString(string: "FOLLOW")
            
            let firstAttributes:[String:Any] = [NSForegroundColorAttributeName: PURPLE]
            attributedString.addAttributes(firstAttributes, range: NSRange(location: 0, length: 6))
            
            followBtn.setAttributedTitle(attributedString , for: .normal)
            
            followBtn.setBackgroundImage(#imageLiteral(resourceName: "FollowBtn"), for: .normal)

        }
        
        if ( self.user.following.count - 1 ) >= 0 {
            numFollowingLbl.text = "\(self.user.following.count - 1)"
        } else {
            numFollowingLbl.text = "0"
        }
        
        if ( self.user.follower.count - 1 ) >= 0 {
            numFollowerLbl.text = "\(self.user.follower.count - 1)"
        } else {
            numFollowerLbl.text = "0"
        }
        
        
        
        detailLbl.text = "\(user!.universityShort) - \(user!.course)"
        initialLbl.text = String((user?.userName[(user?.userName.startIndex)!])!).capitalized
        
        if let profilePicUrl = self.user?.profilePicUrl {
            
            let url = profilePicUrl as! String
            if url != NO_PIC {
                hideInitial()
                profilePic.loadImageUsingCacheWithUrlStringWithoutIndex(imageUrl: url)
            } else {
                
                showInitial()
                
                
            }
            
        }
    }
    
    var mode: String!
    func followingTapped() {
        
        if numFollowingLbl.text != "0" {
            mode = FOLLOWING
            performSegue(withIdentifier: "FollowerVC", sender: nil)
        }

    }
    
    func followerTapped() {
        if numFollowerLbl.text != "0" {
            mode = FOLLOWER
            performSegue(withIdentifier: "FollowerVC", sender: nil)
        }

    }
    
    func showInitial() {
        profilePic.image = nil
        
        profilePic.backgroundColor = ColorHelper().pickColor(alphabet: Character(initialLbl.text!))
        initialLbl.isHidden = false
    }
    
    func hideInitial() {
        profilePic.backgroundColor = UIColor.clear
        initialLbl.isHidden = true
    }
    
    
    // var uniName = "Australian National University"
    var numberOfObjectsinArray = 6
    func fetchData() {
        
        //        headerInd.isHidden = false
        //        headerInd.startAnimating()
        
        getTotalConfession()
        
        fetchConfession()

    
        
        
    }
    
    func fetchConfession() {
        
        DataService.ds.REF_User_Confession.child(self.userID).queryLimited(toLast: UInt(self.numberOfObjectsinArray)).observeSingleEvent(of: .value, with: { (snapshot) in
            self.confessionDict.removeAll()
            self.confessions = []
            
            if let snapshots = snapshot.children.allObjects as? [DataSnapshot] {
                for snap in snapshots {
                    //print("SNAP: \(snap)")
                    if let confessionDict = snap.value as? Dictionary<String, AnyObject> {
                        let key = snap.key
                        let confession = Confession(confessionKey: key, postData: confessionDict)
                        
//                        self.confessionDict[key] = confession
                        
//                        self.confessions.append(confession)
                        if let a = confession.anonymous {
                            if !a {
                                self.totalConfessionRead += 1
                                self.confessionDict[key] = confession
                            } else {
                                self.totalAnonymousConfessionRead += 1

                            }
                        }
                        
                        
                        
                        
                        
                        
                        
                    }
                }
                self.attemptToReloadTable()
                
            }
            
            
            
            
        })
    }
    
    var totalConfession: Int?
    
    private func getTotalConfession() {
        
        
        DataService.ds.REF_User_Confession.child(self.userID).observeSingleEvent(of: .value, with: { (snapshot) in
            //.observe(.value, with: { (snapshot) in

            if let snapshots = snapshot.children.allObjects as? [DataSnapshot] {
                self.totalConfession = snapshots.count
                if self.totalConfession == 0 {
                    self.confessions.removeAll()
                    self.confessionDict.removeAll()
                    self.reloadAndSortTable()
                }
    
                for snap in snapshots {
                    //print("SNAP: \(snap)")
                    if let confessionDict = snap.value as? Dictionary<String, AnyObject> {
                        let key = snap.key
                        let confession = Confession(confessionKey: key, postData: confessionDict)
                        
                        if let a = confession.anonymous {
                            if !a {
                                self.totalDisplayConfession += 1
                            }
                        }
                        
                    }
                }
                
                self.numPostsLbl.text = "\(self.totalDisplayConfession)"

                
            }

        })
        
        
    }
    
    func attemptToReloadTable() {
        //      self.timer?.invalidate()
        
        Timer.scheduledTimer(timeInterval: 0.5, target: self, selector: #selector(reloadAndSortTable), userInfo: nil, repeats: false)
        //
//        firstTime = false
//        reloadAndSortTable()
    }
    
    func reloadAndSortTable() {
        // self.refreshControl.endRefreshing()
        
        self.confessions = Array(self.confessionDict.values)
        //
        self.confessions.sort(by: { (c1, c2) -> Bool in
            
            return (c1.time.intValue) > (c2.time.intValue)
        })
        
    
        
        if actInd != nil {
            if actInd.isAnimating {
                actInd.stopAnimating()
            }
        }
        
        

        
        self.tabBarController?.tabBar.isUserInteractionEnabled = true
        
        if self.confessions.count == 0 {
            self.showEmptyView()
        } else {
            self.hideEmptyView()
        }
        
        self.tableView.reloadData()
        self.tableView.isUserInteractionEnabled = true
        SVProgressHUD.dismiss()
        
        if animateTable {
            UIView.animate(views: tableView.visibleCells, animations: animations, completion: {
            })
        }
        
        animateTable = true


        

        
    }
    
    func optionTapped() {
        let alert = UIAlertController()
        
        alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel, handler:{ (UIAlertAction)in
            print("User click Dismiss button")
            
        }))
        
        if DataService.ds.currentUser.isFollowing(userID: self.user.userKey!) {
            alert.addAction(UIAlertAction(title: "Unfollow", style: UIAlertActionStyle.destructive, handler:{ (UIAlertAction)in
                
                self.displayUnfollowAlert()
            }))
        }
        
        alert.addAction(UIAlertAction(title: "Report", style: .destructive , handler:{ (UIAlertAction)in
            self.displayReportAlert()
        }))
        
        self.present(alert, animated: true, completion: nil)
    }
    
    func displayUnfollowAlert() {
        let title = "Unfollow"
        let message = "Are you sure you want to unfollow this student?"
        
        let popup = PopupDialog(title: title, message: message, image: nil)
        
        let buttonOne = CancelButton(title: "Cancel", dismissOnTap: true) {
            
        }
        
        let buttonTwo = DefaultButton(title: "Unfollow", dismissOnTap: true) {
            self.removeFollowNoti()
        }
        
        popup.addButtons([buttonOne,buttonTwo])
        popup.buttonAlignment = .horizontal
        
        self.present(popup, animated: true, completion: nil)
        
    }
    
    func displayReportAlert() {
        
        let alert = UIAlertController()
        
        
        alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel, handler:{ (UIAlertAction)in
            
        }))
        
        alert.addAction(UIAlertAction(title: "Report Spam", style: UIAlertActionStyle.default, handler:{ (UIAlertAction)in
            self.submitReport(reason: "Spam")
            
        }))
        
        alert.addAction(UIAlertAction(title: "Report Inappropriate", style: UIAlertActionStyle.default, handler:{ (UIAlertAction)in
            self.submitReport(reason: "Inappropriate")
            
        }))
        
        alert.addAction(UIAlertAction(title: "Report Bullying", style: UIAlertActionStyle.default, handler:{ (UIAlertAction)in
            self.submitReport(reason: "Bullying")
            
        }))
        
        self.present(alert, animated: true, completion: nil)
    }
    
    func submitReport(reason: String) {

        var updateObj = [String:AnyObject]()
        let ref = DB_BASE
        updateObj["user-report/\(DataService.ds.currentUser.userKey!)/\(self.user.userKey!)/reason"] = reason as AnyObject
        
        SVProgressHUD.setMinimumDismissTimeInterval(0.5)
        SVProgressHUD.showSuccess(withStatus: "Done!")
        
        ref.updateChildValues(updateObj)
        
    }
    
    func reloadAllUsers(vc: UINavigationController, user: User) {
        for vc2 in vc.viewControllers {
   
            if vc2 is OtherProfileVC {
                let profileVC = vc2 as! OtherProfileVC
                profileVC.user = user
                profileVC.setupUserDetails()
                
            }
            
            if vc2 is FollowerVC {
                let fVC = vc2 as! FollowerVC
                
                if fVC.userDict[user.userKey!] != nil {
                    fVC.userDict[user.userKey!] = user
                    fVC.attemptReloadTable()
                    
                }
                
            }
            
        }
        
    }
    
    func reloadUser() {
        let vc = self.tabBarController?.viewControllers?[0] as! UINavigationController
        reloadAllUsers(vc: vc, user: self.user)
        
        let vc2 = self.tabBarController?.viewControllers?[1] as! UINavigationController
        reloadAllUsers(vc: vc2, user: self.user)
        
        let vc3 = self.tabBarController?.viewControllers?[3] as! UINavigationController
        reloadAllUsers(vc: vc3, user: self.user)
        
        let vc4 = self.tabBarController?.viewControllers?[4] as! UINavigationController
        reloadAllUsers(vc: vc4, user: self.user)
    }
    
    @IBAction func followTapped(_ sender: Any) {
        
        if DataService.ds.currentUser.userKey == self.user.userKey {
            let title = "Didn't you notice something?"
            let message = "This is you!"
            
            let popup = PopupDialog(title: title, message: message, image: nil)
            
            
            let buttonTwo = DefaultButton(title: "OK", dismissOnTap: true) {
                
            }
            
            popup.addButtons([buttonTwo])
            popup.buttonAlignment = .horizontal
            
            self.present(popup, animated: true, completion: nil)
            
        } else {
            if !DataService.ds.currentUser.isFollowing(userID: self.user.userKey!) {
                sendFollowNoti()
            } else {
                self.performSegue(withIdentifier: "NewChatLogVC", sender: nil)
            }
        }

    }
    
    func removeFollowNoti() {
        var updateObj = [String:AnyObject]()
        let ref = DB_BASE
    DataService.ds.REF_User_Following_Notification.child((Auth.auth().currentUser?.uid)!).child(self.user.userKey!).observeSingleEvent(of: .value, with: { (snapshot) in
            
            if let notiDict = snapshot.value as? Dictionary<String, AnyObject> {
             
                updateObj["/notification/\(self.user.userKey!)/\(notiDict["notificationID"]!)"] = NSNull() as AnyObject
                
                ref.updateChildValues(updateObj)
                
                DataService.ds.currentUser.adjustFollowing(userID: self.user.userKey!, add: false)
                self.user.adjustFollower(userID: DataService.ds.currentUser.userKey!, add: false)
                
  
//                self.followBtn.setTitle("FOLLOW", for: .normal)
//                self.followBtn.setBackgroundImage(#imageLiteral(resourceName: "FollowBtn"), for: .normal)
//                self.followBtn.setTitleColor(PURPLE, for: .normal)
                self.setupUserDetails()
                self.reloadUser()
            
            }
        })
    }
    
    func sendFollowNoti() {
        

        var updateObj = [String:AnyObject]()
        let ref = DB_BASE
        
        let timestamp = NSNumber(integerLiteral: Int(NSDate().timeIntervalSince1970))
        var notifcationID = DataService.ds.REF_NOTIFICATION.child((self.user.userKey)!).childByAutoId()
        var notificationValue: Dictionary<String, AnyObject> = ["from" : "\((Auth.auth().currentUser?.uid)!) " as AnyObject, "type" : "follow" as AnyObject, "userName" : DataService.ds.currentUser.userName as AnyObject, "confessionKey" : "" as AnyObject, "time" : timestamp as AnyObject, "profilePicUrl" : DataService.ds.currentUser.profilePicUrl as AnyObject, "seen" : false as AnyObject]
        
        
        updateObj["/user-following-notification/\((Auth.auth().currentUser?.uid)!)/\(self.user.userKey!)"] = ["notificationID" : notifcationID.key ] as AnyObject
  
        notifcationID.updateChildValues(notificationValue)
        ref.updateChildValues(updateObj)
        
        DataService.ds.currentUser.adjustFollowing(userID: self.user.userKey!, add: true)
        self.user.adjustFollower(userID: DataService.ds.currentUser.userKey!, add: true)
        
    
        self.setupUserDetails()
        self.reloadUser()
        
//        followBtn.setTitle("MESSAGE", for: .normal)
//        followBtn.setBackgroundImage(#imageLiteral(resourceName: "MessageBtn"), for: .normal)
//        followBtn.setTitleColor(UIColor.white, for: .normal)

    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return confessions.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let confession = confessions[indexPath.row]

        let seconds = confession.time.doubleValue
        let timesStampDate = NSDate(timeIntervalSince1970: seconds)
        var timeAgo:String =  DateHelper().timeAgoSinceDate(timesStampDate as Date, currentDate: NSDate() as Date, numericDates: true)



        if confession.imageUrl == NO_PIC {

            let cell = tableView.dequeueReusableCell(withIdentifier: "ConfessionCell") as? ConfessionCell
            cell?.otherProfileVC = self
            cell?.configureCell(confession: confession)


            //                if confession.authorUni != confession.university {
            var shortUni = DataService.ds.convertUniNameToShort(uniName: confession.authorUni)

            cell?.dateLabel.text = "\(timeAgo)"
            cell?.facultyLabel.text = "\(shortUni) - \(confession.faculty)"


            return cell!
        }
        else {


            let cell2 = tableView.dequeueReusableCell(withIdentifier: "ConfessionPicCell") as? ConfessionPicCell

            cell2?.confessionPic.image = nil
            cell2?.otherProfileVC = self


            cell2?.confessionPic.loadImageUsingCacheWithUrlString(imageUrl: confession.imageUrl!, tv: self.tableView, index: indexPath)



            cell2?.configureCell(confession: confession)

            var shortUni = DataService.ds.convertUniNameToShort(uniName: confession.authorUni)


            cell2?.dateLabel.text = "\(timeAgo)"
            cell2?.facultyLabel.text = "\(shortUni) - \(confession.faculty)"





            
            return cell2!
        }
        
//        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if indexPath.row == self.confessions.count - 1 {
            print("in the end")

            // self.tableView.isUserInteractionEnabled = false
            self.tableView.tableFooterView = actInd
            self.actInd.startAnimating()
            // self.tableView.reloadData()
//            displayNoInternetConnection()
//            print("total \(totalConfession)")
//            print("total read \(totalConfessionRead)")
            
//            if totalConfession! == (totalConfessionRead + confessions.count)  {
//                print("hurray")
//            }
            if totalConfession! > (self.totalConfessionRead + self.totalAnonymousConfessionRead) {

//                if numberOfObjectsinArray < totalConfession! {
                    Timer.scheduledTimer(timeInterval: 0.5, target: self, selector: #selector(self.addMoreObjects), userInfo: nil, repeats: false)
//                } else {
//                    self.tableView.isUserInteractionEnabled = true
//                    self.actInd.stopAnimating()
//                    self.tableView.tableFooterView = UIView()
//                }



            } else {
                print("STOP")
                self.tableView.isUserInteractionEnabled = true
                self.actInd.stopAnimating()
                self.tableView.tableFooterView = UIView()
            }

        }
    }
    
    func reloadAllLikes( vc: UINavigationController, confession: Confession, reloadLike: Bool) {
        for vc2 in vc.viewControllers {
            if vc2 is ConfessionVC {
                let confessionVC = vc2 as! ConfessionVC
                
                if confessionVC.confessionDict[confession.confessionKey] != nil {
                    print("YES ITS CONFESSION VC")
                    //                    confessionVC.confessions.append(confession)
                    //                    confessionVC.attemptToReloadTable()
                    confessionVC.reloadLikeFromHashtagVC(confession: confession, reloadLike: reloadLike)
                }
                
                
            }
            
            if vc2 is HashtagVC {
                let hashVC = vc2 as! HashtagVC
                if hashVC.confessionDict[confession.confessionKey] != nil {
                    hashVC.confessionDict[confession.confessionKey] = confession
                    hashVC.attemptToReloadTable()
                }
                
                
            }
            
            if vc2 is Comment3VC {
                let commentVC = vc2 as! Comment3VC
                if commentVC.confessionId == confession.confessionKey {
                    commentVC.confession = confession
                    commentVC.attemptToReloadTable()
                }
                
                
            }
            
            if vc2 is OtherProfileVC {
                let profileVC = vc2 as! OtherProfileVC
                if profileVC.confessionDict[confession.confessionKey] != nil {
                    print("welala")
                    profileVC.confessionDict[confession.confessionKey] = confession
//                    profileVC.attemptToReloadTable()
//                    self.tableView.reloadData()
                }
                
                
            }
            
        }
        
        //        /* Reload search vc's hash vc confession from comment vc */
        //        let navVC = self.tabBarController?.viewControllers?[1] as! UINavigationController
        //
        //        for vcs in navVC.viewControllers {
        //
        //            if vcs is HashtagVC {
        //                print("reload in hashtag22")
        //                let hashVC = vcs as! HashtagVC
        //                if self.user?.university == confession.university {
        //                    hashVC.confessionDict[confession.confessionKey] = confession
        //                    hashVC.attemptToReloadTable()
        //                }
        //
        //            }
        //
        //        }
    }
    
    var animateTable = true
    func reloadLike(confession: Confession, reloadLike: Bool) {
        animateTable = false
        
        let vc = self.tabBarController?.viewControllers?[0] as! UINavigationController
        reloadAllLikes(vc: vc, confession: confession, reloadLike: reloadLike)

        let vc2 = self.tabBarController?.viewControllers?[1] as! UINavigationController
        reloadAllLikes(vc: vc2, confession: confession, reloadLike: reloadLike)

        let vc3 = self.tabBarController?.viewControllers?[3] as! UINavigationController
        reloadAllLikes(vc: vc3, confession: confession, reloadLike: reloadLike)

        let vc4 = self.tabBarController?.viewControllers?[4] as! UINavigationController
        reloadAllLikes(vc: vc4, confession: confession, reloadLike: reloadLike)
        
        

    }

    var currentConfessionId: String!
    var currentConfession: Confession!
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.currentConfessionId  = confessions[indexPath.row].confessionKey
        self.currentConfession = confessions[indexPath.row]
//        currentIndex = indexPath.row

        self.currentConfession?.postRef.observeSingleEvent(of: .value, with: { (snapshot) in
            if snapshot.exists() {
                
                UIView.animate(withDuration: 0.1, delay: 0.1, options: [], animations: { UIApplication.shared.statusBarView?.alpha = 1.0
                    self.navigationItem.hidesBackButton = false
                }, completion: nil)
                
                self.performSegue(withIdentifier: "Comment3VC", sender: nil)
            } else {
                ErrorAlert().createAlert(title: "Something went wrong", msg: "Could not perform action", object: self)
            }
        })
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let destinationVC = segue.destination as? Comment3VC {

            //            destinationVC.confession = self.currentConfession
            destinationVC.confessionId = self.currentConfessionId
//            destinationVC.delegate = self
            //            destinationVC.passDeletedConfessionDelegate = self
//            destinationVC.passReloadCommentConfessionDelegate = self
            //            destinationVC.uniName = self.uniName

//            if self.currentVisitingUni.name == "Trending" || self.currentVisitingUni.name == "Latest" || fetchCategory {
//                destinationVC.uniName = self.user?.university
//            } else {
                destinationVC.uniName = DataService.ds.currentUser.university
//                print("current visiting uni \(destinationVC.uniName)")
//            }

            destinationVC.user = DataService.ds.currentUser

        }
        
        

        if let destinationVC = segue.destination as? NewChatLogVC {
            destinationVC.user = self.user
            
            
        }
        
        if let destinationVC = segue.destination as? FollowerVC {
            destinationVC.mode = self.mode
            destinationVC.userVisiting = self.user
            
            
        }
        
    }
    
    func addMoreObjects() {
        
        
        animateTable = false
        numberOfObjectsinArray+=6
//        fetchConfession()
        
        DataService.ds.REF_User_Confession.child(self.userID).queryLimited(toLast: UInt(self.numberOfObjectsinArray)).observeSingleEvent(of: .value, with: { (snapshot) in

            if let snapshots = snapshot.children.allObjects as? [DataSnapshot] {
                for snap in snapshots {
                    //print("SNAP: \(snap)")
                    if let confessionDict = snap.value as? Dictionary<String, AnyObject> {
                        let key = snap.key
                        let confession = Confession(confessionKey: key, postData: confessionDict)
                        
                        //self.confessions.append(confession)
                        print("total \(self.totalConfession)")
                        print("total read \(self.totalConfessionRead)")
                        print("total confession count read \(self.confessions.count)")
                        
//                        self.confessionDict[key] = confession

//                        self.totalConfessionRead  = self.totalConfessionRead + 1
                        if let a = confession.anonymous {
                            if !a {
                                self.totalConfessionRead += 1
                                self.confessionDict[key] = confession
                            } else {
                                self.totalAnonymousConfessionRead += 1

                            }
                        }

                        
//                        self.reloadAndSortTable()
                    }
                }
                self.attemptToReloadTable()
            }

        })
    }
    
    
    var offset: CGFloat!
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        offset = scrollView.contentOffset.y / 150
        


        if offset > 1 {
            offset = 1
//            self.navigationController?.navigationBar.tintColor = UIColor(hue: 1, saturation: offset, brightness: 1, alpha: 1)
            self.navView.backgroundColor = UIColor(red: 68/255, green: 159/255, blue: 255/255, alpha: offset)
//            navText.textColor = UIColor.white
//            self.navigationController?.navigationBar.tintColor = UIColor(red: 1, green: 1, blue: 1, alpha: offset)
            
//            self.navigationItem.hidesBackButton = true
            
//            UIApplication.shared.statusBarStyle = .default
//            UIApplication.shared.statusBarView?.isHidden = true
            
            UIView.animate(withDuration: 0.1, delay: 0.1, options: [], animations: { UIApplication.shared.statusBarView?.alpha = 0.0
                self.navigationItem.hidesBackButton = true
            }, completion: nil)
        } else {
//            self.navigationController?.navigationBar.tintColor = UIColor(hue: 1, saturation: offset, brightness: 1, alpha: 1)
            self.navView.backgroundColor = UIColor(red: 68/255, green: 159/255, blue: 255/255, alpha: offset)
//            navText.textColor = PURPLE
//           self.navigationController?.navigationBar.tintColor = UIColor(red: 1, green: 1, blue: 1, alpha: -offset)
            
            
            
            UIView.animate(withDuration: 0.1, delay: 0.1, options: [], animations: { UIApplication.shared.statusBarView?.alpha = 1.0
               self.navigationItem.hidesBackButton = false
            }, completion: nil)
            
            
            

        }
    }

}
