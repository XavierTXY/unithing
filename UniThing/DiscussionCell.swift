//
//  DiscussionCell.swift
//  UniThing
//
//  Created by XavierTanXY on 8/5/18.
//  Copyright © 2018 Xavier TanXY. All rights reserved.
//

import UIKit
import ActiveLabel
import Firebase

class DiscussionCell: UITableViewCell {
    
    @IBOutlet weak var categoryLbl: UILabel!
    @IBOutlet weak var topicLbl: ActiveLabel!
    @IBOutlet weak var discussionImgView: CircleView!
    @IBOutlet weak var likeNumLbl: UILabel!
    @IBOutlet weak var commentLbl: UILabel!
    @IBOutlet weak var likeBtn: UIButton!
    
    @IBOutlet weak var commentImage: UIImageView!
    var mainConfession: Confession!
    
    var likesRef: DatabaseReference!
//    var confessionVC: ConfessionVC?
//    var hashtagVC: HashtagVC?
//    var selfConfessionVC: SelfConfessionVC?
    var totalComments: Int?
    var initialLbl: UILabel!
    
    var vc: Comment3VC!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        
        let commentTap = UITapGestureRecognizer(target: self, action: #selector(commentTapped))
        commentTap.numberOfTapsRequired = 1
        commentImage.addGestureRecognizer(commentTap)
        commentImage.isUserInteractionEnabled = true
        
        let likerTap = UITapGestureRecognizer(target: self, action: #selector(likersTapped))
        likerTap.numberOfTapsRequired = 1
        likeNumLbl.addGestureRecognizer(likerTap)
        likeNumLbl.isUserInteractionEnabled = true

        
        topicLbl.enabledTypes = [.hashtag, .url]
        topicLbl.handleHashtagTap { hashtag in
            print("Success. You just tapped the \(hashtag) hashtag")
        }
        
        topicLbl.customize { label in
            label.hashtagColor = UIColor(red: 85.0/255, green: 172.0/255, blue: 238.0/255, alpha: 1)
            
        }
        
        categoryLbl.text = "Question of The Week"
        
    }
    
    func configureCell(confession: Confession) {
        
        self.mainConfession = confession

        likesRef = DataService.ds.REF_USERS.child((Auth.auth().currentUser?.uid)!).child("likes").child(confession.confessionKey)
        
        
        
        
        topicLbl.text = confession.caption
    
        
        likeNumLbl.text = "\(confession.likes)"
        commentLbl.text = "\(confession.comments)"
        
        
        let exist = confession.usersLike[(Auth.auth().currentUser?.uid)!]
        
        if exist != nil{
            //            self.likeImage.image = UIImage(named: "NewLoveFilled")
            likeBtn.setImage(#imageLiteral(resourceName: "NewLoveFilled"), for: .normal)
        } else {
            //            self.likeImage.image = UIImage(named: "NewLove")
            likeBtn.setImage(#imageLiteral(resourceName: "NewLove"), for: .normal)
        }
        
    }
    
    @IBAction func optionsTapped(_ sender: Any) {
        
//        let alert = UIAlertController()
//
//        alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel, handler:{ (UIAlertAction)in
//            print("User click Dismiss button")
//
//        }))
//
//        if confession.author == (Auth.auth().currentUser!.uid) {
//            alert.addAction(UIAlertAction(title: "Delete", style: .destructive , handler:{ (UIAlertAction)in
//                self.displayAlert()
//            }))
//
//        }
//
//        parentViewController?.present(alert, animated: true, completion: nil)
    }
    
//    func displayAlert() {
//        let alertController = UIAlertController(title: "Are you sure?", message: "", preferredStyle: .alert)
//
//        alertController.addAction(UIAlertAction(title: "Cancel", style: .cancel) { (action:UIAlertAction!) in
//            // println("you have pressed the Cancel button");
//        })
//
//        alertController.addAction(UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction!) in
//
//            self.confessionVC?.deleteConfession(confessionKey: self.confession.confessionKey)
//
//        })
//
//        parentViewController?.present(alertController, animated: true, completion: nil)
//
//    }
//
    func enableTouch() {
        //        self.likeImage.isUserInteractionEnabled = true
        self.likeBtn.isUserInteractionEnabled = true
    }
    var timer: Timer?
    
    @IBAction func likeBtnTapped(_ sender: Any) {
        self.likeBtn.pulsate()
        
        //        sender.isEnabled = false
        //        self.likeImage.isUserInteractionEnabled = false
        self.likeBtn.isUserInteractionEnabled = false
        self.timer = Timer.scheduledTimer(timeInterval: 2.0, target: self, selector: #selector(enableTouch), userInfo: nil, repeats: false)
        
        
        self.mainConfession.postRef.observeSingleEvent(of: .value, with: { (snapshot) in
            if snapshot.exists() {
                
                
                
                let exist = self.mainConfession.usersLike[(Auth.auth().currentUser?.uid)!]
                
                if exist == nil {
                    //                        self.confession.addUser(uid: (FIRAuth.auth()?.currentUser?.uid)!)
                    //                        self.likeImage.image = UIImage(named: "NewLoveFilled")
                    self.likeBtn.setImage(#imageLiteral(resourceName: "NewLoveFilled"), for: .normal)
                    self.likeNumLbl.text = "\(self.mainConfession.likes + 1)"
                    
                    
                    //self.confession.likes = self.confession.likes + 1
                    
//                    self.confessionVC?.reloadConfesionInSearchVC(confession: self.confession, reloadLike: true)
//                    self.hashtagVC?.reloadLike(confession: self.confession, reloadLike: true)
//                    self.selfConfessionVC?.reloadLike(confession: self.confession, reloadLike: true)
                    
                    self.mainConfession.adjustLikes(addLike: true, uid: (Auth.auth().currentUser?.uid)! )
                    self.vc?.reloadLike(confession: self.mainConfession, reloadLike: true)
                    
                    
                } else {
                    //                        self.confession.removeUser(uid: (FIRAuth.auth()?.currentUser?.uid)!)
                    
                    //                        self.likeImage.image = UIImage(named: "NewLove")
                    self.likeBtn.setImage(#imageLiteral(resourceName: "NewLove"), for: .normal)
                    if self.mainConfession.likes != 0 {
                        self.likeNumLbl.text = "\(self.mainConfession.likes - 1)"
                    }
                    
                    
//                    self.confessionVC?.reloadConfesionInSearchVC(confession: self.confession, reloadLike: false)
//                    self.hashtagVC?.reloadLike(confession: self.confession, reloadLike: false)
//                    self.selfConfessionVC?.reloadLike(confession: self.confession, reloadLike: false)
                    
                    self.mainConfession.adjustLikes(addLike: false, uid: (Auth.auth().currentUser?.uid)! )
                    self.vc?.reloadLike(confession: self.mainConfession, reloadLike: true)
                    
                }
                
                
                
                
            } else {
                ErrorAlert().createAlert(title: "Something went wrong", msg: "Could not perform action", object: self.parentViewController!)
                
            }
        })
        
    }
    
    func likeTapped(_ sender: UITapGestureRecognizer) {
        
        
        
        
    }
    
    func likersTapped(_ sender: UITapGestureRecognizer) {
        
        vc?.likersTapped(isComment: false, commentID: "")
    }
    
    func commentTapped(_ sender: UITapGestureRecognizer) {
        
        if self.vc != nil {
            self.vc.showKeyboard()
        }
        
        
    }


}
