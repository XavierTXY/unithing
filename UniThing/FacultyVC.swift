//
//  FacultyVC.swift
//  UniThing
//
//  Created by Xavier TanXY on 30/11/16.
//  Copyright © 2016 Xavier TanXY. All rights reserved.
//

import UIKit
import Firebase
import SVProgressHUD
import ViewAnimator

class FacultyVC: UIViewController, UITableViewDelegate, UITableViewDataSource {

    var email: String?
    var name: String?
    var password: String?
    var uni: University?
    var selectedCourse: String!
    var gender: String?
    var provider: String?
    var uid: String?
    var courses = [String]()
    
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var tableView: UITableView!
    private let animations = [AnimationType.from(direction: .bottom, offset: 30.0)]
    
    override func viewDidLoad() {
        super.viewDidLoad()

      

        let barItem = UIBarButtonItem(image: #imageLiteral(resourceName: "Back Chevron"), style: .plain, target: self, action: "backTapped")
        let rightBarItem = UIBarButtonItem(title: "Help", style: .plain, target: self, action: "helpTapped")
        rightBarItem.tintColor = UIColor.white
        barItem.tintColor = UIColor.white
        self.navigationItem.setLeftBarButton(barItem, animated: true)
        self.navigationItem.setRightBarButton(rightBarItem, animated: true)
        
        tableView.delegate = self
        tableView.dataSource = self
        tableView.tableFooterView = UIView()
        // Do any additional setup after loading the view.
        
        self.navigationItem.title = "Course";
        // Do any additional setup after loading the view.
        
        fetchCourses()
    }
    
    override func viewDidAppear(_ animated: Bool) {
       // UIView.animate(withDuration: 0.3, delay: 0.1, options: [], animations: { self.mainView.alpha = 1.0 }, completion: nil)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
       // self.mainView.alpha = 0.0
    }
    
    func fetchCourses() {
        SVProgressHUD.show()
        DataService.ds.REF_Course_Info.observeSingleEvent(of: .value, with: { (snapshot) in
            
            if let snapshots = snapshot.children.allObjects as? [DataSnapshot] {
                for snap in snapshots {
                    
                    if let uniDict = snap.value as? Dictionary<String, AnyObject> {
                        
                        
                        let courseName = snap.key
                        //                        print(courseName)
                        self.courses.append(courseName)
                    }
                }
                self.tableView.reloadData()
                
                UIView.animate(views: self.tableView.visibleCells, animations: self.animations, completion: {
                })
                SVProgressHUD.dismiss()
                DataService.ds.courseArray = self.courses
            }
        })
    }
    
    func backTapped() {
        self.navigationController?.popViewController(animated: true)
    }
    
    func helpTapped() {
        let alertController = UIAlertController(title: "Can't find your course?", message: "Please enter your course", preferredStyle: .alert)
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { (_) in }
        
        let confirmAction = UIAlertAction(title: "Confirm", style: .default) { (_) in
            if let field = alertController.textFields?[0] {
                DataService.ds.REF_MISSING_COURSE.childByAutoId().updateChildValues(["course": field.text])
            }
            
            let lastAlertController = UIAlertController(title: "Thank you for your feedback!", message: "We will add your course as soon as possible", preferredStyle: .alert)
            
            lastAlertController.addAction(UIAlertAction(title: "Done", style: .default) { (action:UIAlertAction!) in
                //self.dismiss(animated: true, completion: nil)
            })
            
            self.present(lastAlertController, animated: true, completion: nil)
        }
        
        alertController.addTextField { (textField) in
            textField.placeholder = "Current Course"
        }
        
        alertController.addAction(confirmAction)
        alertController.addAction(cancelAction)
        
        self.present(alertController, animated: true, completion: nil)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }

    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 43;
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return self.courses.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if let cell = tableView.dequeueReusableCell(withIdentifier: "FacultyCell") as? FacultyCell {
            
            let course: String!
            
            course = self.courses[indexPath.row]
            
            cell.configureCell(course: course)
            
            return cell
        } else {
            return FacultyCell()
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        selectedCourse = self.courses[indexPath.row]
        
        
        print(selectedCourse)
        performSegue(withIdentifier: "YearVC", sender: nil)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let destinationVC = segue.destination as? YearVC {
            destinationVC.uni = self.uni
            destinationVC.course = self.selectedCourse
            destinationVC.name = self.name
            destinationVC.email = self.email
            destinationVC.password = self.password
            destinationVC.gender = self.gender
            destinationVC.provider = self.provider
            destinationVC.uid = self.uid
            
        }
    }

}
