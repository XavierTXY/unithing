//
//  Extensions.swift
//  UniThing
//
//  Created by Xavier TanXY on 21/12/16.
//  Copyright © 2016 Xavier TanXY. All rights reserved.
//

import Foundation
import UIKit
import Firebase
import SDWebImage
import AVFoundation




let imageCache: NSCache<NSString, UIImage> = NSCache()


extension UIButton {
    
    func pulsate() {
        
        let pulse = CASpringAnimation(keyPath: "transform.scale")
        pulse.duration = 0.3
        pulse.fromValue = 0.95
        pulse.toValue = 1.0
        pulse.autoreverses = false
        pulse.repeatCount = 1
        pulse.initialVelocity = 0.5
        pulse.damping = 0.5
        
        layer.add(pulse, forKey: "pulse")
    }
    
    func flash() {
        
        let flash = CABasicAnimation(keyPath: "opacity")
        flash.duration = 0.5
        flash.fromValue = 1
        flash.toValue = 0.1
        flash.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
        flash.autoreverses = true
        flash.repeatCount = 3
        
        layer.add(flash, forKey: nil)
    }
    
    
    func shake() {
        
        let shake = CABasicAnimation(keyPath: "position")
        shake.duration = 0.1
        shake.repeatCount = 2
        shake.autoreverses = true
        
        let fromPoint = CGPoint(x: center.x - 5, y: center.y)
        let fromValue = NSValue(cgPoint: fromPoint)
        
        let toPoint = CGPoint(x: center.x + 5, y: center.y)
        let toValue = NSValue(cgPoint: toPoint)
        
        shake.fromValue = fromValue
        shake.toValue = toValue
        
        layer.add(shake, forKey: "position")
    }
}

extension UISearchBar {
    
    private var textField: UITextField? {
        return subviews.first?.subviews.flatMap { $0 as? UITextField }.first
    }
    
    private var activityIndicator: UIActivityIndicatorView? {
        return textField?.leftView?.subviews.flatMap{ $0 as? UIActivityIndicatorView }.first
    }
    
    var isLoading: Bool {
        get {
            return activityIndicator != nil
        } set {
            if newValue {
                if activityIndicator == nil {
                    let newActivityIndicator = UIActivityIndicatorView(activityIndicatorStyle: .gray)
                    newActivityIndicator.transform = CGAffineTransform(scaleX: 0.7, y: 0.7)
                    newActivityIndicator.startAnimating()
                    newActivityIndicator.backgroundColor = UIColor.white
                    textField?.leftView?.addSubview(newActivityIndicator)
                    let leftViewSize = textField?.leftView?.frame.size ?? CGSize.zero
                    newActivityIndicator.center = CGPoint(x: leftViewSize.width/2, y: leftViewSize.height/2)
                }
            } else {
                activityIndicator?.removeFromSuperview()
            }
        }
    }
}

extension UnicodeScalar {
    
    var isEmoji: Bool {
        
        switch value {
        case 0x1F600...0x1F64F, // Emoticons
        0x1F300...0x1F5FF, // Misc Symbols and Pictographs
        0x1F680...0x1F6FF, // Transport and Map
        0x2600...0x26FF,   // Misc symbols
        0x2700...0x27BF,   // Dingbats
        0xFE00...0xFE0F,   // Variation Selectors
        0x1F900...0x1F9FF,  // Supplemental Symbols and Pictographs
        65024...65039, // Variation selector
        8400...8447: // Combining Diacritical Marks for Symbols
            return true
            
        default: return false
        }
    }
    
    var isZeroWidthJoiner: Bool {
        
        return value == 8205
    }
}

extension String {
    

    func replace(target: String, withString: String) -> String
    {
        return self.replacingOccurrences(of: target, with: withString, options: NSString.CompareOptions.literal, range: nil)
    }
    
    func contains(find: String) -> Bool{
        return self.range(of: find) != nil
    }

    
    var glyphCount: Int {
        
        let richText = NSAttributedString(string: self)
        let line = CTLineCreateWithAttributedString(richText)
        return CTLineGetGlyphCount(line)
    }
    
    var isSingleEmoji: Bool {
        
        return glyphCount == 1 && containsEmoji
    }
    
    var containsEmoji: Bool {
        
        return unicodeScalars.contains { $0.isEmoji }
    }
    
    var containsOnlyEmoji: Bool {
        
        return !isEmpty
            && !unicodeScalars.contains(where: {
                !$0.isEmoji
                    && !$0.isZeroWidthJoiner
            })
    }
    
    // The next tricks are mostly to demonstrate how tricky it can be to determine emoji's
    // If anyone has suggestions how to improve this, please let me know
    var emojiString: String {
        
        return emojiScalars.map { String($0) }.reduce("", +)
    }
    
    var emojis: [String] {
        
        var scalars: [[UnicodeScalar]] = []
        var currentScalarSet: [UnicodeScalar] = []
        var previousScalar: UnicodeScalar?
        
        for scalar in emojiScalars {
            
            if let prev = previousScalar, !prev.isZeroWidthJoiner && !scalar.isZeroWidthJoiner {
                
                scalars.append(currentScalarSet)
                currentScalarSet = []
            }
            currentScalarSet.append(scalar)
            
            previousScalar = scalar
        }
        
        scalars.append(currentScalarSet)
        
        return scalars.map { $0.map{ String($0) } .reduce("", +) }
    }
    
    fileprivate var emojiScalars: [UnicodeScalar] {
        
        var chars: [UnicodeScalar] = []
        var previous: UnicodeScalar?
        for cur in unicodeScalars {
            
            if let previous = previous, previous.isZeroWidthJoiner && cur.isEmoji {
                chars.append(previous)
                chars.append(cur)
                
            } else if cur.isEmoji {
                chars.append(cur)
            }
            
            previous = cur
        }
        
        return chars
    }
}

extension UIImageView {
    
    func loadImageUsingCacheWithUrlStringWithoutIndex(imageUrl: String) {
        self.image = nil
        
        
        SDImageCache.shared().queryCacheOperation(forKey: (imageUrl as NSString) as String!, done: { (image2, data2, SDImageCacheType) in
            
            if( image2 != nil) {
                UIView.transition(with: self,
                                  duration:0.1,
                                  options: .transitionCrossDissolve,
                                  animations: {
                                    self.image = image2
                },
                                  completion: nil)
                
                print("it usese cached")
            } else {
                let ref = Storage.storage().reference(forURL: imageUrl)
                
                ref.getData(maxSize: 1 * 1024 * 1024, completion: { (data,error) in
                    if error != nil {
                        print("Xavier: unable to dowload image from fire")
                        self.image = #imageLiteral(resourceName: "ProfilePic")
                        print(error.debugDescription)
                    } else {
                        print("Xavier: image dowloaded from fire")
                        
                        // DispatchQueue.main.async(execute: { () -> Void in
                        if let imageData = data {
                            if let img = UIImage(data: imageData){
                                //
                                
                                //self.image = img
                                
                                DispatchQueue.main.async() {
                                    
                                    
//                                    let updateCell = tv .cellForRow(at: index)
//                                    if updateCell != nil {
                                        UIView.transition(with: self,
                                                          duration:0.1,
                                                          options: .transitionCrossDissolve,
                                                          animations: {
                                                            self.image = img
                                        },
                                                          completion: nil)
                                        
                                        
                                        
                                   // }
                                    
                                }
                                
                                // activityIndicatorView.stopAnimating()
                                // imageCache.setObject(img, forKey: imageUrl as NSString)
                                print("Xavier: cache saves img")
                                SDImageCache.shared().store(img, forKey: (imageUrl as NSString) as String!)
                                                                
                                
                                
                            }
                            
                        }
                        //  })
                        
                    }
                    
                })
            }
        })
    }
    
//    func ResizeImage(image: UIImage, targetSize: CGSize) -> UIImage {
//        let size = image.size
//        
//        let widthRatio  = targetSize.width  / image.size.width
//        let heightRatio = targetSize.height / image.size.height
//        
//        // Figure out what our orientation is, and use that to form the rectangle
//        var newSize: CGSize
//        if(widthRatio > heightRatio) {
//            //newSize = CGSizeMake(size.width * heightRatio, size.height * heightRatio)
//            newSize = CGSize(width: size.width * heightRatio, height: size.height * heightRatio)
//        } else {
//            //newSize = CGSizeMake(size.width * widthRatio,  size.height * widthRatio)
//            newSize = CGSize(width: size.width * widthRatio, height: size.height * widthRatio)
//        }
//        
//        // This is the rect that we've calculated out and this is what is actually used below
//        //let rect = CGRectMake(0, 0, newSize.width, newSize.height)
//        let rect = CGRect(x: 0, y: 0, width: newSize.width, height: newSize.height)
//        
//        // Actually do the resizing to the rect using the ImageContext stuff
//        UIGraphicsBeginImageContextWithOptions(newSize, false, 1.0)
//        image.draw(in: rect)
//        let newImage = UIGraphicsGetImageFromCurrentImageContext()
//        UIGraphicsEndImageContext()
//        
//        return newImage!
//    }

    func loadImageUsingCacheWithUrlStringCollection(imageUrl: String, cv: UICollectionView, index: IndexPath) {
        
        
        //
        SDImageCache.shared().queryCacheOperation(forKey: (imageUrl as NSString) as String!, done: { (image2, data3, SDImageCacheType) in
            
            if( image2 != nil) {
                
                
                self.image = image2
                
                
                print("it usese cached")
            } else {
                let ref = Storage.storage().reference(forURL: imageUrl)
                ref.getData(maxSize: 1 * 1024 * 1024, completion: { (data,error) in
                    if error != nil {
                        print("Xavier: unable to dowload image from fire")
                        print(error.debugDescription)
                        self.image = #imageLiteral(resourceName: "ProfilePic")
                    } else {
                        print("Xavier: image dowloaded from fire")
                        
                        DispatchQueue.main.async(execute: { () -> Void in
                            if let imageData = data {
                                if let img = UIImage(data: imageData){
                                    //
                                    
                                    //self.image = img
                                    
                                    DispatchQueue.main.async() {
                                        
                                  
                                        let updateCell = cv.cellForItem(at: index)
                                        if updateCell != nil {
                                            UIView.transition(with: self,
                                                              duration:0.1,
                                                              options: .transitionCrossDissolve,
                                                              animations: {
                                                                self.image = img
                                                                
                                                                
                                                                
                                            },
                                                              completion: nil)
                                            
                                            
                                            
                                        }
                                        
                                    }
                                    
                                    // activityIndicatorView.stopAnimating()
                                    // imageCache.setObject(img, forKey: imageUrl as NSString)
                                    print("Xavier: cache saves img")
                                    SDImageCache.shared().store(img, forKey: (imageUrl as NSString) as String!)
                                    
                                    
                                    
                                    
                                }
                                
                            }
                        })
                        
                    }
                    
                })
            }
        })
        
    }
    
    func loadImageUsingCacheWithUrlString(imageUrl: String, tv: UITableView, index: IndexPath) {
        

//        
            SDImageCache.shared().queryCacheOperation(forKey: (imageUrl as NSString) as String!, done: { (image2, data3, SDImageCacheType) in
                
                if( image2 != nil) {

                    
                    self.image = image2

                    
                    print("it usese cached")
                } else {
                    let ref = Storage.storage().reference(forURL: imageUrl)
                    ref.getData(maxSize: 1 * 1024 * 1024, completion: { (data,error) in
                        if error != nil {
                            print("Xavier: unable to dowload image from fire")
                            print(error.debugDescription)
                            self.image = #imageLiteral(resourceName: "ProfilePic")
                        } else {
                            print("Xavier: image dowloaded from fire")
                            
                             DispatchQueue.main.async(execute: { () -> Void in
                            if let imageData = data {
                                if let img = UIImage(data: imageData){
                                    //
                                    
                                    //self.image = img
                                    
                                    DispatchQueue.main.async() {
                                        
                                        
                                        let updateCell = tv .cellForRow(at: index)
                                        if updateCell != nil {
                                            UIView.transition(with: self,
                                                              duration:0.1,
                                                              options: .transitionCrossDissolve,
                                                              animations: {
                                                                self.image = img
                                                                
                                                          
                                                               
                                            },
                                                              completion: nil)
                                            
                                            
                                            
                                        }
                                        
                                    }
                                    
                                    // activityIndicatorView.stopAnimating()
                                   // imageCache.setObject(img, forKey: imageUrl as NSString)
                                    print("Xavier: cache saves img")
                                    SDImageCache.shared().store(img, forKey: (imageUrl as NSString) as String!)
                                    
                                    
                                    
                                    
                                }
                                
                            }
                              })
                            
                        }
                        
                    })
                }
            })
        
        }
    
    
}
