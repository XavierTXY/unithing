//
//  LoadingHashVCView.swift
//  UniThing
//
//  Created by Xavier TanXY on 2/8/17.
//  Copyright © 2017 Xavier TanXY. All rights reserved.
//

import UIKit

class LoadingHashVCView: UIView {

    class func instanceFromNib() -> UIView {
        return UINib(nibName: "LoadingHashVCView", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! UIView
    }

}
