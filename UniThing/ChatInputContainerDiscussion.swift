//
//  ChatInputContainerDiscussion.swift
//  UniThing
//
//  Created by XavierTanXY on 12/5/18.
//  Copyright © 2018 Xavier TanXY. All rights reserved.
//

import Foundation
import UIKit
import ActiveLabel

class ChatInputContainerDiscussion: UIView, UITextFieldDelegate, UITextViewDelegate {

    var discussionVC: DiscussionVC? {
        didSet {
            //            sendButton.addTarget(comment3VC, action: #selector(comment3VC?.commentSend), for: .touchUpInside)
//            anonBtn.addTarget(discussionVC, action: #selector(discussionVC?.anonymousModeTapped), for: .touchUpInside)
        }
    }
    
    lazy var inputTextField: UITextField = {
        let textField = UITextField()
        textField.placeholder = "Enter a comment..."
        textField.translatesAutoresizingMaskIntoConstraints = false
        textField.textColor = UIColor.lightGray
        textField.returnKeyType = UIReturnKeyType.send
        textField.backgroundColor = UIColor.white
        textField.font = UIFont(name: "HelveticaNeue", size: 17)
        // textField.backgroundColor = UIColor.blue
        textField.enablesReturnKeyAutomatically = true
        textField.delegate = self
        
        return textField
    }()
    
    //    lazy var inputTextView: UITextView = {
    //        let textView = UITextView()
    //        //textView.placeholder = "Enter a comment..."
    //        textView.translatesAutoresizingMaskIntoConstraints = false
    //        textView.backgroundColor = UIColor.yellow
    //        textView.textColor = UIColor.lightGray
    //        textView.sizeToFit()
    //         textView.isScrollEnabled = false
    //
    //        textView.delegate = self
    //
    //
    //
    //        return textView
    //    }()
    
    //    lazy var anonBtn: UIButton = {
    //        let btn = UIButton(type: .custom)
    //        btn.translatesAutoresizingMaskIntoConstraints = false
    //
    //
    ////        view.layer.borderColor = UIColor.clear.cgColor
    //        btn.backgroundColor = UIColor.blue
    ////        view.layer.borderWidth = 1.0
    ////        view.layer.cornerRadius = 16
    //
    //        return btn
    //
    //    }()
    
    lazy var rectangleView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        // view.backgroundColor = UIColor.red
        // view.layer.borderColor = UIColor(red: 0/255, green: 0/255, blue: 0/255, alpha: 0.1).cgColor
        
        view.layer.borderColor = UIColor.clear.cgColor
        //view.backgroundColor = UIColor.white
        view.layer.borderWidth = 1.0
        view.layer.cornerRadius = 16
        
        return view
        
    }()
    
    
    
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        if (textField.text?.characters.count)! > 0 && !(textField.text?.trimmingCharacters(in: .whitespaces).isEmpty)! {
//            self.discussionVC?.commentSend()
        } else {
            textField.resignFirstResponder()
//            self.discussionVC?.displayEmptyTxtFieldAlert()
        }
        
        return true
    }
    
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if inputTextField.textColor! == UIColor.lightGray {
            inputTextField.text = nil
            inputTextField.textColor = UIColor.black
            print("a1")
            
            //            if (textField.text?.characters.count)! > 0 && !(textField.text?.trimmingCharacters(in: .whitespaces).isEmpty)! {
            //                textField.enablesReturnKeyAutomatically = false
            //            } else {
            //                textField.enablesReturnKeyAutomatically = true
            //            }
            
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if (inputTextField.text?.isEmpty)! {
            inputTextField.text = "Enter a comment..."
            inputTextField.textColor = UIColor.lightGray
            print("A2")
            //            textField.enablesReturnKeyAutomatically = false
        }
    }
    
    
    //    let sendButton = UIButton(type: .custom)
    let anonBtn = UIButton(type: .custom)
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        translatesAutoresizingMaskIntoConstraints = false
        //        backgroundColor = UIColor(red: 250/255, green: 250/255, blue: 250/255, alpha: 1)
        
        backgroundColor = UIColor.white
        
        
        
        //        addSubview(sendButton)
        
        
        
        
        
        anonBtn.setImage(#imageLiteral(resourceName: "anonyModeOff"), for: .normal)
        //sendButton.setTitle("Post", for: .normal)
        anonBtn.translatesAutoresizingMaskIntoConstraints = false
        //        sendButton.tintColor = UIColor(red: 158/255, green: 158/255, blue: 158/255, alpha: 0.7)
        anonBtn.isEnabled = true
        addSubview(anonBtn)
        
        anonBtn.rightAnchor.constraint(equalTo: rightAnchor).isActive = true
        anonBtn.centerYAnchor.constraint(equalTo: centerYAnchor).isActive = true
        anonBtn.widthAnchor.constraint(equalToConstant: 80).isActive = true
        anonBtn.heightAnchor.constraint(equalTo: heightAnchor).isActive = true
        
        
        let seperatorLineView = UIView()
        seperatorLineView.backgroundColor = UIColor(displayP3Red: 230/255, green: 236/255, blue: 240/255, alpha: 1)
        seperatorLineView.translatesAutoresizingMaskIntoConstraints = false
        addSubview(seperatorLineView)
        
        seperatorLineView.leftAnchor.constraint(equalTo: leftAnchor).isActive = true
        seperatorLineView.topAnchor.constraint(equalTo: topAnchor).isActive = true
        seperatorLineView.widthAnchor.constraint(equalTo: widthAnchor).isActive = true
        seperatorLineView.heightAnchor.constraint(equalToConstant: 1).isActive = true
        
        addSubview(rectangleView)
        
        //        let view = UIView()
        //        view.backgroundColor = UIColor.blue
        //        addSubview(view)
        //        view.leftAnchor.constraint(equalTo: leftAnchor).isActive = true
        //        view.bottomAnchor.constraint(equalTo: bottomAnchor).isActive = true
        //        view.rightAnchor.constraint(equalTo: rightAnchor).isActive = true
        ////        view.widthAnchor.constraint(equalTo: widthAnchor).isActive = true
        //        view.heightAnchor.constraint(equalToConstant: 50).isActive = true
        
        rectangleView.addSubview(inputTextField)
        //         rectangleView.addSubview(anonBtn)
        
        
        inputTextField.rightAnchor.constraint(equalTo: rectangleView.rightAnchor, constant: -4).isActive = true
        inputTextField.leftAnchor.constraint(equalTo: rectangleView.leftAnchor, constant: 8).isActive = true
        
        inputTextField.topAnchor.constraint(equalTo: rectangleView.topAnchor).isActive = true
        inputTextField.bottomAnchor.constraint(equalTo: rectangleView.bottomAnchor).isActive = true
        // inputTextView.heightAnchor.constraint(equalTo: rectangleView.heightAnchor).isActive = true
        
        
        
        
        self.rectangleView.leftAnchor.constraint(equalTo: leftAnchor, constant: 4).isActive = true
        self.rectangleView.centerYAnchor.constraint(equalTo: centerYAnchor).isActive = true
        self.rectangleView.rightAnchor.constraint(equalTo: anonBtn.leftAnchor, constant: 8).isActive = true
        self.rectangleView.topAnchor.constraint(equalTo: seperatorLineView.bottomAnchor, constant: 4).isActive = true
        
        //        self.anonBtn.bottomAnchor.constraint(equalTo: rectangleView.topAnchor, constant: 15).isActive = true
        //        self.anonBtn.rightAnchor.constraint(equalTo: rectangleView.rightAnchor, constant: 15).isActive = true
        //        anonBtn.widthAnchor.constraint(equalToConstant: 40).isActive = true
        //        anonBtn.heightAnchor.constraint(equalToConstant: 40).isActive = true
        
        
        
        let seperatorLineView2 = UIView()
        seperatorLineView2.backgroundColor = BORDER_COLOR
        seperatorLineView2.translatesAutoresizingMaskIntoConstraints = false
        addSubview(seperatorLineView2)
        
        seperatorLineView2.leftAnchor.constraint(equalTo: leftAnchor).isActive = true
        seperatorLineView2.topAnchor.constraint(equalTo: rectangleView.bottomAnchor, constant: 4).isActive = true
        seperatorLineView2.bottomAnchor.constraint(equalTo: bottomAnchor).isActive = true
        seperatorLineView2.widthAnchor.constraint(equalTo: widthAnchor).isActive = true
        seperatorLineView2.heightAnchor.constraint(equalToConstant: 1).isActive = true
        
        
        
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    
    
    
    
    //
    //    func textViewDidChange(_ textView: UITextView) {
    //
    //        let fixedWidth = textView.frame.size.width
    //        textView.sizeThatFits(CGSize(width: fixedWidth, height: CGFloat.greatestFiniteMagnitude))
    //        let newSize = textView.sizeThatFits(CGSize(width: fixedWidth, height: CGFloat.greatestFiniteMagnitude))
    //        var newFrame = textView.frame
    //        newFrame.size = CGSize(width: max(newSize.width, fixedWidth), height: newSize.height)
    //        textView.frame = newFrame;
    //
    //        self.rectangleView.frame = textView.frame
    //
    //
    //    }
    
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        // print("!@3 ")
        return true
    }
    
    func textFieldShouldClear(_ textField: UITextField) -> Bool {
        // chatLogVC?.messageSend()
        return true
    }
    
    
}
