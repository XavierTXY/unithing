//
//  AccVC.swift
//  UniThing
//
//  Created by Xavier TanXY on 23/11/16.
//  Copyright © 2016 Xavier TanXY. All rights reserved.
//

import UIKit
import Firebase
import SVProgressHUD

class AccVC: UIViewController, UITextFieldDelegate {

    @IBOutlet weak var mainView: UIView!
    //@IBOutlet weak var coverImage: UIImageView!
    
    @IBOutlet weak var emailTxtField: UITextField!
    @IBOutlet weak var passwordTxtField: UITextField!
    @IBOutlet weak var nextBtn: UIButton!
    
    var email: String?
    var password: String?
    var provider: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        

        
//        let leftBarItem = UIBarButtonItem(image: #imageLiteral(resourceName: "Back Chevron"), style: .plain, target: self, action: "backTapped")
//        //let rightBarItem = UIBarButtonItem(title: "Next", style: .done, target: self, action: "nextTapped")
//        
//        //let rightBarItem = UIBarButtonItem(image: #imageLiteral(resourceName: "next"), style: .plain, target: self, action: "nextTapped")
//        leftBarItem.tintColor = UIColor.white
//        //rightBarItem.tintColor = UIColor.white
//        self.navigationItem.setLeftBarButton(leftBarItem, animated: true)
//        self.navigationItem.setRightBarButton(nil, animated: true)
        
        //self.navigationItem.rightBarButtonItem?.isEnabled = false
        //self.navigationItem.rightBarButtonItem?.tintColor = UIColor(red: 220/255, green: 220/255, blue: 220/255, alpha: 1)
        
        self.nextBtn.isEnabled = false
        //self.nextBtn.tintColor = UIColor(red: 220/255, green: 220/255, blue: 220/255, alpha: 1)
        
        emailTxtField.delegate = self
        passwordTxtField.delegate = self
        
        emailTxtField.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        passwordTxtField.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        
        emailTxtField.becomeFirstResponder()
        self.navigationItem.title = "Create Account"
        self.navigationController?.navigationBar.titleTextAttributes = [ NSFontAttributeName: UIFont(name: "HelveticaNeue-Bold", size: 16)!]
        self.navigationController?.navigationBar.titleTextAttributes = [ NSForegroundColorAttributeName: UIColor.white]
        if #available(iOS 11.0, *) {
            
            self.navigationController?.navigationBar.prefersLargeTitles = true
            self.navigationController?.navigationBar.largeTitleTextAttributes = [ NSForegroundColorAttributeName: UIColor.white]
        } else {
            // Fallback on earlier versions
        }

        
        
     
    }

    override func viewDidAppear(_ animated: Bool) {
        UIView.animate(withDuration: 0.5, delay: 0.2, options: [], animations: { self.mainView.alpha = 1.0 }, completion: nil)
    
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.mainView.alpha = 0.0
    }
    
    func textFieldDidChange(_ textField: UITextField) {
        
        if (passwordTxtField.text?.characters.count)! >= 6 && emailTxtField.text?.range(of: "@") != nil {
           // self.navigationItem.rightBarButtonItem?.isEnabled = true
            //self.navigationItem.rightBarButtonItem?.tintColor = UIColor.white
            
            self.nextBtn.isEnabled = true
            self.nextBtn.setBackgroundImage(#imageLiteral(resourceName: "Log In Btn"), for: .normal)
        } else {
           // self.navigationItem.rightBarButtonItem?.isEnabled = false
            //self.navigationItem.rightBarButtonItem?.tintColor = UIColor(red: 220/255, green: 220/255, blue: 220/255, alpha: 1)
            
            self.nextBtn.isEnabled = false
            //self.nextBtn.tintColor = UIColor(red: 220/255, green: 220/255, blue: 220/255, alpha: 1)
            self.nextBtn.setBackgroundImage(#imageLiteral(resourceName: "LoginBtnGray"), for: .normal)
        }
    }
    
    func backTapped() {
        self.navigationController?.popViewController(animated: false)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == emailTxtField {
            textField.resignFirstResponder()
            passwordTxtField.becomeFirstResponder()
        } else if textField == passwordTxtField {
            textField.resignFirstResponder()
            //self.nextTapped()
        }
        
        return true
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    func resignAllResponder() {
        emailTxtField.resignFirstResponder()
        passwordTxtField.resignFirstResponder()
    }

    @IBAction func nextTapped(_ sender: Any) {
        
        self.resignAllResponder()
        SVProgressHUD.show()
        
        email = self.emailTxtField.text!
        password = self.passwordTxtField.text!
        provider = FIREBASE
        
        Auth.auth().createUser(withEmail: email!, password: password!, completion: { (user, error) in
            if error == nil {
                
                print("Xavier: SUccessfuly auth with firebase")
                
                if let user = user {
                    SVProgressHUD.dismiss()
                    self.performSegue(withIdentifier: "VerificationVC", sender: nil)
                }
                
            } else {
                
                Auth.auth().signIn(withEmail: self.email!, password: self.password!) { (user, error) in
                    if error != nil {
                        SVProgressHUD.dismiss()
                        ErrorAlert().show(title: "Error", msg: "\((error?.localizedDescription)!)", object: self)
                    } else {
                        if let user = user {
                            if user.isEmailVerified {
                                
                                var uid = (Auth.auth().currentUser?.uid)!
                                DataService.ds.REF_USERS.child(uid).observeSingleEvent(of: .value, with: { (snapshot) in
                                    if snapshot.exists() {
                                        
                                        SVProgressHUD.dismiss()
                                        ErrorAlert().show(title: "Email is already in use", msg: "Please enter another email address", object: self)
                                    } else {
                                        SVProgressHUD.dismiss()
                                        self.performSegue(withIdentifier: "DetailVC", sender: nil)
                                    }
                                })
                                
                                
                            } else {
                                SVProgressHUD.dismiss()
                                self.performSegue(withIdentifier: "VerificationVC", sender: nil)
                            }

                        }
                    }
                }
                
            }

        })
        

        
//        self.resignAllResponder()
//        //        coverImage.isHidden = false
//        Interaction().disableInteraction(msg: "Loading")
//
//
//        if let pwd = passwordTxtField.text, let email = emailTxtField.text, (pwd.characters.count >= 6 && email.range(of: "@") != nil) {
//
//
//
//            
//            DataService.ds.REF_USERS.queryOrdered(byChild: "email").queryEqual(toValue: email).observeSingleEvent(of: .value, with: { (snapshot) in
//                
//                print("Xavier hi3)")
//                if ( snapshot.value is NSNull ) {
//                    print("Xavier not found)")
//                    
//                    self.email = email
//                    self.password = pwd
//                    self.provider = FIREBASE
//                    self.performSegue(withIdentifier: "DetailVC", sender: nil)
//                    Interaction().enableInteraction()
//                    
//                } else {
//                    
//                    ErrorAlert().show(title: "Email is already in use", msg: "Please enter another email address", object: self)
//                }
//                //                self.coverImage.isHidden = true
//            })
//            
//            
//            
//        } else if (passwordTxtField.text?.characters.count)! < 6 && emailTxtField.text?.range(of: "@") == nil {
//            ErrorAlert().show(title: "Invalid Details", msg: "Please enter your detials correctly", object: self)
//            //            coverImage.isHidden = true
//        } else if emailTxtField.text?.range(of: "@") == nil {
//            ErrorAlert().show(title: "Invalid Email", msg: "Please enter your email correctly", object: self)
//            //            coverImage.isHidden = true
//        } else if (passwordTxtField.text?.characters.count)! < 6{
//            ErrorAlert().show(title: "Invalid Password", msg: "Password must be at least six characters", object: self)
//            //            coverImage.isHidden = true
//        }
    }
        
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if let destinationVC = segue.destination as? DetailVC {
            destinationVC.email = self.email
            destinationVC.password = self.password
            destinationVC.provider = self.provider
            
        }
        
        if let destinationVC = segue.destination as? VerificationVC {
            destinationVC.email = self.email
            destinationVC.password = self.password
            destinationVC.provider = self.provider
            
        }
        
        
    }
    
}
