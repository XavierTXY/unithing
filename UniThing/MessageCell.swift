//
//  MessageCell.swift
//  UniThing
//
//  Created by Xavier TanXY on 19/12/16.
//  Copyright © 2016 Xavier TanXY. All rights reserved.
//

import UIKit
import Firebase

class MessageCell: UITableViewCell {

    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var notiImg: UIView!
    
    @IBOutlet weak var messageLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    
    @IBOutlet weak var initialLbl: UILabel!
    @IBOutlet weak var outerView: UIView!
    @IBOutlet weak var profilePic: UIImageView!
    
    var msg: MessageModel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        profilePic.layer.cornerRadius = profilePic.frame.height / 2
        profilePic.clipsToBounds = true
        outerView.layer.cornerRadius = profilePic.frame.height / 2
        outerView.clipsToBounds = true
        notiImg.layer.cornerRadius = notiImg.frame.height / 2
        notiImg.clipsToBounds = true
    }

    
    func configureCell(message: MessageModel, toName: String, url: String) {
        
        self.msg = message
        nameLabel.text = toName
        
        if message.fromId == (Auth.auth().currentUser?.uid)! {
            notiImg.isHidden = true
        } else {
            if message.toSeen! {
                notiImg.isHidden = true
            } else {
                notiImg.isHidden = false
            }
        }

        
        if url != NO_PIC {
            hideInitial()
            profilePic.loadImageUsingCacheWithUrlStringWithoutIndex(imageUrl: url)
        } else {
            
            showInitial()
            
            
        }
            
        
        
        if message.text == nil {
            messageLabel.text = "Sent an image"
        } else {
            messageLabel.text = message.text
        }
        

        
        
        
        if let seconds = message.timeStamp?.doubleValue {
            let timesStampDate = NSDate(timeIntervalSince1970: seconds)
            var dateFormatter = DateFormatter()
            

            let calendar = Calendar.current
            
            if calendar.isDateInToday(timesStampDate as Date) {
                dateFormatter.dateFormat = "hh:mm"
            } else {
                dateFormatter.dateFormat = "d MMM"
            }
            
            timeLabel.text = dateFormatter.string(from: timesStampDate as Date)
        }
        

    }
    
    func showInitial() {
        profilePic.image = nil
        
    
        initialLbl.text = String((nameLabel.text![(self.nameLabel.text?.startIndex)!])).capitalized
        profilePic.backgroundColor = ColorHelper().pickColor(alphabet: Character(initialLbl.text!))
        initialLbl.isHidden = false
    }
    
    func hideInitial() {
        profilePic.backgroundColor = UIColor.clear
        initialLbl.isHidden = true
    }
    

}
