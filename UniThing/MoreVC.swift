//
//  ProfileVC.swift
//  UniThing
//
//  Created by Xavier TanXY on 6/12/16.
//  Copyright © 2016 Xavier TanXY. All rights reserved.
//

import UIKit
import Firebase
import FirebaseDatabase
import SVProgressHUD
//import StoreKit
import PopupDialog

class MoreVC: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var tableView: UITableView!
    var options = [[String]]()
    var headerName = [String]()
    var user: User?
    
    var userName: String?
    var name: String?
    var selfVC: SelfConfessionVC!
    
    var alertController: UIAlertController!
    var headerView: MoreHeader!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        SVProgressHUD.setBackgroundColor(UIColor(red: 244/255, green: 244/255, blue: 244/255, alpha: 0.95))
//        SVProgressHUD.show(withStatus: "Loading")
        
        tableView.delegate = self
        tableView.dataSource = self
        self.navigationController?.navigationBar.titleTextAttributes = [ NSFontAttributeName: UIFont(name: "HelveticaNeue-Bold", size: 16)!]
        self.navigationItem.title = "More"

        self.navigationController?.navigationBar.barTintColor = UIColor.white

        self.navigationController?.navigationBar.layer.borderColor = BORDER_COLOR_CGCOLOR

        
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title:"", style:.plain, target:nil, action: "")
        let backImage = #imageLiteral(resourceName: "Back Chevron")
        self.navigationController?.navigationBar.backIndicatorImage = backImage
        self.navigationController?.navigationBar.backIndicatorTransitionMaskImage = backImage
        
        tableView.rowHeight = UITableViewAutomaticDimension


//        getUserDetails()
        setupOptions()
        setupAlert()
        
     


        


    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
//        self.navigationController?.setNavigationBarHidden(true, animated: true)
        UIApplication.shared.statusBarStyle = .default
        self.navigationController?.navigationBar.tintColor = PURPLE
        self.navigationController?.navigationBar.titleTextAttributes = [ NSForegroundColorAttributeName: PURPLE]
        
//        self.navigationController?.navigationBar.isTranslucent = false
//        self.navigationController?.view.backgroundColor = UIColor.white
    }
    
//    var timer: Timer!
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
//        self.timer = Timer.scheduledTimer(timeInterval: 0.5, target: self, selector: #selector(self.addMoreObjects), userInfo: nil, repeats: false)
        
//        self.navigationController?.navigationBar.isTranslucent = true

    }
    

    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        
    }
    

    
    func setupAlert() {
        alertController = UIAlertController(title: "Are you sure?", message: "", preferredStyle: .alert)
        
        alertController.addAction(UIAlertAction(title: "Cancel", style: .cancel) { (action:UIAlertAction!) in
            
        })
        
        
        alertController.addAction(UIAlertAction(title: "Log Out", style: .destructive) { (action:UIAlertAction!) in
            
            let defaults = UserDefaults.standard
            for key in defaults.dictionaryRepresentation().keys{
                defaults.removeObject(forKey: key.description)
            }
            
            DataService.ds.removeInstanceID()
            
            try! Auth.auth().signOut()
            
            UserDefaults.standard.setValue(false, forKey: "tutorial")
            VCSegue().segueVC(bundleName: "Main", controllerName: "NavVC", vc: self)
            
        })
    }
    
    func setupOptions() {
        options = [ ["Edit Profile" , "Change Password"] , ["Terms and Conditions", "Privacy Policy", "Version", "Feedback" , "Rate Us"], ["Log out"] ]
        headerName = ["Account", "About Us"]
    }

//    func getUserDetails() {
//        DataService.ds.REF_USERS.child((Auth.auth().currentUser?.uid)!).observe(.value, with: { (snapshot) in
//            if let snapshots = snapshot.children.allObjects as? [DataSnapshot] {
//                //print("\(snapshot.value)")
//                
//                if let userDict = snapshot.value as? Dictionary<String, AnyObject> {
//                    let key = snapshot.key
//                    self.user = User(userKey: key, userData: userDict)
//                    
//                    let defaults = UserDefaults.standard
//                    defaults.set(self.user?.userName, forKey: "userName")
//                    defaults.set(self.user?.name, forKey: "name")
//                    defaults.set(self.user?.profilePicUrl, forKey: "profilePicUrl")
//
//                   // self.imgUrl = self.user?.profilePicUrl
//                    defaults.synchronize()
//                    
//                }
//                
//                self.attemptToReloadTable()
//            }
//            
//        })
//    }

   
    func displayLogoutAlert() {

        
    
            
            

        let popup = PopupDialog(title: "Log Out", message: "Are you sure?", image: nil)

        let buttonOne = CancelButton(title: "Cancel") {

        }

        let buttonTwo = DefaultButton(title: "Log Out", dismissOnTap: true) {
        let defaults = UserDefaults.standard
        for key in defaults.dictionaryRepresentation().keys{
            defaults.removeObject(forKey: key.description)
        }

        DataService.ds.removeInstanceID()

        try! Auth.auth().signOut()

        UserDefaults.standard.setValue(false, forKey: "tutorial")
            VCSegue().segueVC(bundleName: "Main", controllerName: "NavVC", vc: self)
        }

        popup.addButtons([buttonOne,buttonTwo])
        popup.buttonAlignment = .horizontal

        // Present dialog
        self.present(popup, animated: true, completion: nil)

            
            
        

        
    }
    
    func attemptToReloadTable() {
        self.tableView.reloadData()
        SVProgressHUD.dismiss()
    }
    
//    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
//        
//        if indexPath.section == 0 && indexPath.row == 0 {
//            return 75
//        } else {
//            return 50
//        }
//        
//        
//    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
//        if indexPath.section == 0 && indexPath.row == 0 {
//            return 85
//        } else {
//            return 45
//        }
        
        return 45
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print(indexPath.row)
        
        if indexPath.row == 0 && indexPath.section == 0 {
            performSegue(withIdentifier: "ProfileVC", sender: nil)
        } else if indexPath.row == 0 && indexPath.section == 1 {
            print("tab TERM")
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let viewController = storyboard.instantiateViewController(withIdentifier :"TCVC") as! TCVC
            self.navigationController?.pushViewController(viewController, animated: true)
        } else if indexPath.row == 1 && indexPath.section == 0 {
//            displayLogoutAlert()
            
            print("tab change password")
//            presentChangePwdAlert()
            showCustomDialog()
        } else if indexPath.row == 1 && indexPath.section == 1 {
//            performSegue(withIdentifier: "SettingVC", sender: nil)
            print("tab privacy")
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let viewController = storyboard.instantiateViewController(withIdentifier :"PrivacyVC") as! PrivacyVC
            self.navigationController?.pushViewController(viewController, animated: true)
            
        } else if indexPath.row == 3 && indexPath.section == 1 {
//            performSegue(withIdentifier: "SelfConfessionVC", sender: nil)
            print("tab feedback")
            performSegue(withIdentifier: "FeedbackVC", sender: nil)
        } else if indexPath.row == 4 && indexPath.section == 1 {
//            SKStoreReviewController.requestReview()
            if let reviewURL = URL(string: "https://itunes.apple.com/us/app/unithing/id1302177232?ls=1&mt=8"), UIApplication.shared.canOpenURL(reviewURL) {
                if #available(iOS 10.0, *) {
                    UIApplication.shared.open(reviewURL, options: [:], completionHandler: nil)
                } else {
                    UIApplication.shared.openURL(reviewURL)
                }
            }

        } else if indexPath.row == 0 && indexPath.section == 2 {
//            performSegue(withIdentifier: "AboutVC", sender: nil)
            print("tab logout")
            displayLogoutAlert()
        }
    }
    


    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {



        
        if indexPath.section == 1 && indexPath.row == 2 {
            
            if let version = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String {
                print("VERSION \(version)")
                let cell = tableView.dequeueReusableCell(withIdentifier: "UserNameCell") as? UserNameCell
                cell?.configureCell(detail: version, user: self.user!)
                return cell!
            } else {
                return UITableViewCell()
            }

        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "MoreCell") as? MoreCell
            
            let option = options[indexPath.section][indexPath.row]
            cell?.configureCell(labelName: option)
            return cell!
        }
        

        

    }

    func numberOfSections(in tableView: UITableView) -> Int {
        print("total sec \(options.count)")
        return options.count
    }

    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        headerView = Bundle.main.loadNibNamed("MoreHeader", owner: self, options: nil)?.first as! MoreHeader
        var lbl = headerView.viewWithTag(1) as! UILabel
        
        
        
        print("sec\(section)")
        if section == 0 {
            lbl.text = headerName[section]
        } else  if section == 1 {
            lbl.text = headerName[section]
        } else {
            lbl.text = ""
        }
        
        return headerView
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        
        return "hi"
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return options[section].count
        
    }

    func showCustomDialog(animated: Bool = true) {
        
        let user = Auth.auth().currentUser
        
        // Create a custom view controller
        let pwdVC = ChangePwdViewController(nibName: "ChangePwdViewController", bundle: nil)

        
        let popup = PopupDialog(viewController: pwdVC, buttonAlignment: .horizontal, transitionStyle: .bounceDown, gestureDismissal: false)
     
        // Create first button
        let buttonOne = CancelButton(title: "CANCEL", height: 60) {
            
        }
        
        // Create second button
        let buttonTwo = DefaultButton(title: "Ok", height: 60) {
            
            let currentPassword = pwdVC.pwdTxtField.text
//            let currentPassword = field.text
            let credential = EmailAuthProvider.credential(withEmail: (user?.email)!, password: currentPassword!)
            
            user?.reauthenticate(with: credential, completion: { (error) in
                if error != nil{
                    ErrorAlert().createAlert(title: "Incorrect Password", msg: "Your current password is incorrect", object: self)
                }else{
                    self.showNewPwdDialog()
//
//                    let confirmChange = UIAlertAction(title: "Change", style: .default) { (_) in
//                        if let field = alertController2.textFields?[0] {
//                            let newPassword = field.text
//
//                            if (newPassword?.characters.count)! >= 6 {
//                                Auth.auth().currentUser?.updatePassword(to: newPassword!) { (error) in
//
//                                    if error != nil {
//                                        ErrorAlert().createAlert(title: "Error in changing", msg: "Could not perfrom action", object: self)
//                                    } else {
//                                        print("done changing")
//                                        SVProgressHUD.showSuccess(withStatus: "Done!")
//
//                                    }
//                                }
//                            } else {
//                                ErrorAlert().createAlert(title: "Invalid Format", msg: "Password must be at least 6 characters", object: self)
//                            }
//
//
//                        }
//                    }
//
//                    alertController2.addAction(confirmChange)
//                    self.present(alertController2, animated: true, completion: nil)
                }
            })
            
          
        }
        
        // Add buttons to dialog
        popup.addButtons([buttonOne, buttonTwo])
        
        // Present dialog
        present(popup, animated: animated, completion: nil)
    }
    
    
    func showNewPwdDialog(animated: Bool = true) {
        let user = Auth.auth().currentUser
        
        // Create a custom view controller
        let pwdVC = ChangePwdAgainViewController(nibName: "ChangePwdAgainViewController", bundle: nil)
        
        
        let popup = PopupDialog(viewController: pwdVC, buttonAlignment: .horizontal, transitionStyle: .bounceDown, gestureDismissal: false)
        
        // Create first button
        let buttonOne = CancelButton(title: "CANCEL", height: 60) {
            
        }
        
        // Create second button
        let buttonTwo = DefaultButton(title: "Ok", height: 60) {


            let newPassword = pwdVC.pwdTxtField.text

            if (newPassword?.characters.count)! >= 6 {
                Auth.auth().currentUser?.updatePassword(to: newPassword!) { (error) in

                    if error != nil {
                        ErrorAlert().createAlert(title: "Error in changing", msg: "Could not perfrom action", object: self)
                    } else {
                        print("done changing")
                        SVProgressHUD.showSuccess(withStatus: "Done!")

                    }
                }
            } else {
                ErrorAlert().createAlert(title: "Invalid Format", msg: "Password must be at least 6 characters", object: self)
            }


        }
        
        // Add buttons to dialog
        popup.addButtons([buttonOne, buttonTwo])
        
        // Present dialog
        present(popup, animated: animated, completion: nil)
    }
    
    func presentChangePwdAlert() {
        
        
        
        let user = Auth.auth().currentUser
        
        
        
        let alertController = UIAlertController(title: "Password", message: "Please enter your current password:", preferredStyle: .alert)
        let alertController2 = UIAlertController(title: "Password", message: "Please enter your new password:", preferredStyle: .alert)
        
        
        let confirmAction = UIAlertAction(title: "Confirm", style: .default) { (_) in
            if let field = alertController.textFields?[0] {
                let currentPassword = field.text
                let credential = EmailAuthProvider.credential(withEmail: (user?.email)!, password: currentPassword!)
                
                user?.reauthenticate(with: credential, completion: { (error) in
                    if error != nil{
                        ErrorAlert().createAlert(title: "Incorrect Password", msg: "Your current password is incorrect", object: self)
                    }else{
                        
                        let confirmChange = UIAlertAction(title: "Change", style: .default) { (_) in
                            if let field = alertController2.textFields?[0] {
                                let newPassword = field.text
                                
                                if (newPassword?.characters.count)! >= 6 {
                                    Auth.auth().currentUser?.updatePassword(to: newPassword!) { (error) in
                                        
                                        if error != nil {
                                            ErrorAlert().createAlert(title: "Error in changing", msg: "Could not perfrom action", object: self)
                                        } else {
                                            print("done changing")
                                            SVProgressHUD.showSuccess(withStatus: "Done!")
                                            
                                        }
                                    }
                                } else {
                                    ErrorAlert().createAlert(title: "Invalid Format", msg: "Password must be at least 6 characters", object: self)
                                }
                                
                                
                            }
                        }
                        
                        alertController2.addAction(confirmChange)
                        self.present(alertController2, animated: true, completion: nil)
                    }
                })
                
                
            } else {
                // user did not fill field
            }
        }
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { (_) in }
        
        alertController.addTextField { (textField) in
            textField.placeholder = "Current password"
            textField.isSecureTextEntry = true
        }
        
        alertController2.addTextField { (textField) in
            textField.placeholder = "New password"
            textField.isSecureTextEntry = true
        }
        
        
        
        alertController.addAction(confirmAction)
        alertController.addAction(cancelAction)
        alertController2.addAction(cancelAction)
        
        self.present(alertController, animated: true, completion: nil)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let destinationVC = segue.destination as? ProfileVC {
            destinationVC.user = self.user

        }
        
        if let destinationVC = segue.destination as? SelfConfessionVC {
//            destinationVC.user = self.user
            
        }
    }

}
