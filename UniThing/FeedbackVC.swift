//
//  FeedbackVC.swift
//  UniThing
//
//  Created by Xavier TanXY on 1/8/17.
//  Copyright © 2017 Xavier TanXY. All rights reserved.
//

import UIKit
import Firebase
import SVProgressHUD
import PopupDialog

class FeedbackVC: UIViewController,UITextViewDelegate {

    @IBOutlet weak var textView: UITextView!
    @IBOutlet weak var sendBtn: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        textView.delegate = self
        textView.text = "Send us your feedback here..."
        textView.textColor = UIColor.lightGray
        
        sendBtn.isEnabled = false
        sendBtn.tintColor = UIColor(red: 158/255, green: 158/255, blue: 158/255, alpha: 0.7)
        
        
    }

    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.textColor == UIColor.lightGray {
            textView.text = nil
            textView.textColor = UIColor.black

        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty {
            textView.text = "Send us your feedback here..."
            textView.textColor = UIColor.lightGray
        }
    }
    
    func textViewDidChange(_ textView: UITextView) {
        
        if textView.text.characters.count > 0 {
            self.sendBtn.tintColor = UIColor(red: 33/255, green: 150/255, blue: 243/255, alpha: 1)
            self.sendBtn.isEnabled = true
            
        
        } else {
            self.sendBtn.tintColor = UIColor(red: 158/255, green: 158/255, blue: 158/255, alpha: 0.7)
            self.sendBtn.isEnabled = false
        }
    }
    
    @IBAction func sendFeedback(_ sender: Any) {
        
        if textView.isFirstResponder {
            textView.resignFirstResponder()
        }
        
        SVProgressHUD.show()
        
        postToFirebase {
            SVProgressHUD.dismiss()
            
            let alertController = UIAlertController(title: "Thank you for your feedback!", message: "Your feedback is important to us", preferredStyle: .alert)
            
            alertController.addAction(UIAlertAction(title: "Done", style: .default) { (action:UIAlertAction!) in
                self.dismiss(animated: true, completion: nil)
            })
            
            self.present(alertController, animated: true, completion: nil)
            
        }
        
        

    }
    
    func postToFirebase(completion: @escaping () -> ()) {
        
        let feedbackPost = DataService.ds.REF_FEEDBACK.child((Auth.auth().currentUser?.uid)!)
        let feedPost = feedbackPost.childByAutoId()
        var feedbackValue = [ "description" : textView.text ]
        //feedbackPost.setValue(feedbackValue)
        feedPost.updateChildValues(feedbackValue)
        completion()
    }
    
    @IBAction func cancelTapped(_ sender: Any) {
        
        if textView.isFirstResponder {
            textView.resignFirstResponder()
        }
        
        
        let title = "Warning"
        let message = "Are you sure you want to discard?"
        
        let popup = PopupDialog(title: title, message: message, image: nil)
        
        let buttonOne = DestructiveButton(title: "Discard") {
            self.dismiss(animated: true, completion: nil)
        }
        
        let buttonTwo = DefaultButton(title: "Continue", dismissOnTap: true) {
            
        }
        
        popup.addButtons([buttonOne,buttonTwo])
        popup.buttonAlignment = .horizontal
        
        // Present dialog
        
//
//        let alert = UIAlertController()
//
//        alert.title = "Continue?"
//        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel) { (action:UIAlertAction!) in
//        })
//
//        alert.addAction(UIAlertAction(title: "Discard", style: .destructive , handler:{ (UIAlertAction)in
//            self.dismiss(animated: true, completion: nil)
//        }))
        
        
        if textView.text.characters.count > 0 {
//           self.present(alert, animated: true, completion: nil)
            self.present(popup, animated: true, completion: nil)
        } else {
            self.dismiss(animated: true, completion: nil)
            
        }
        
        
        
        //self.dismiss(animated: true, completion: nil)
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }

}
