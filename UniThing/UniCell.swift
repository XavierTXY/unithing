//
//  UniCell.swift
//  UniThing
//
//  Created by Xavier TanXY on 30/11/16.
//  Copyright © 2016 Xavier TanXY. All rights reserved.
//

import UIKit

class UniCell: UITableViewCell {

    
    @IBOutlet weak var uniLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    func configureCell(uni: University) {
        uniLabel.text = uni.name
    }

}
