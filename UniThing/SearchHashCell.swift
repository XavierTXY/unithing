//
//  SearchHashCell.swift
//  UniThing
//
//  Created by XavierTanXY on 5/5/18.
//  Copyright © 2018 Xavier TanXY. All rights reserved.
//

import UIKit

class SearchHashCell: UITableViewCell {

    @IBOutlet weak var hashNameLbl: UILabel!
    @IBOutlet weak var hashCountLbl: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    func configureCell(hash: Hash) {
        hashNameLbl.text = hash.hashName
        hashCountLbl.text = "\(hash.hashCount) confessions"
    }
    
}
