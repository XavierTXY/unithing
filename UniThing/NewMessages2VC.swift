//
//  NewMessages2VC.swift
//  UniThing
//
//  Created by XavierTanXY on 24/5/18.
//  Copyright © 2018 Xavier TanXY. All rights reserved.
//

import UIKit
import Firebase

class NewMessages2VC: UIViewController, UITableViewDataSource, UITableViewDelegate {


    @IBOutlet weak var tableView: UITableView!
    
    let cellId = "NewCell"
    var users = [User]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.tableView.delegate = self
        self.tableView.dataSource = self
        navigationController?.title = "New Message"
        navigationItem.leftBarButtonItem = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(handleCancel))
        
        fetchFriends()
        tableView.tableFooterView = UIView()
    }

    
    func handleCancel() {
        dismiss(animated: true, completion: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
    }
    
    @IBAction func dismiss(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    func fetchFriends() {
        
        DataService.ds.REF_USERS.child((Auth.auth().currentUser?.uid)!).child("following").observeSingleEvent(of: .value, with: { (snapshot) in
            
            
            if let snapshots = snapshot.children.allObjects as? [DataSnapshot] {
                for snap in snapshots {
                    
                    if snap.key != "key" {
                        DataService.ds.REF_USERS.child(snap.key).observeSingleEvent(of: .value, with: { (snapshot2) in
                            if let dict = snapshot2.value as? Dictionary<String, AnyObject> {
                                let user = User(userKey: snap.key, userData: dict)
                                self.users.append(user)
                                self.tableView.reloadData()
                            }
                            //
                        })
                    }
                    //                    //print("SNAP: \(snap)")
                    //                    if let userDict = snap.value as? Dictionary<String, AnyObject> {
                    //                        let key = snap.key
                    //                        print(key)
                    
                    
                    
                    
                    
                    //                    }
                }
                //
                //
                //
            }
        })
    

    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return users.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
//        let cell = tableView.dequeueReusableCell(withIdentifier: "NewMsgCell") as? NewMsgCell
//        var user = users[indexPath.row]
//        cell?.configureCell(user: user)
//        print("cell \(user.userName)")
        return UITableViewCell()
    }
    
    var messagesVC: MessagesVC?
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
       
        dismiss(animated: true) {
            let user = self.users[indexPath.row]
            self.messagesVC?.showChatControllerForUser(user: user)
            
            
        }
    }

}
