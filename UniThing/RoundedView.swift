//
//  RoundView.swift
//  UniThing
//
//  Created by Xavier TanXY on 7/12/16.
//  Copyright © 2016 Xavier TanXY. All rights reserved.
//

import Foundation
import UIKit

class RoundedView: UIView {
    
    override func awakeFromNib() {
        super.awakeFromNib()
        

        layer.cornerRadius = 20.0
        layer.masksToBounds = false
//        clipsToBounds = true
        layer.shadowColor = UIColor.black.withAlphaComponent(0.2).cgColor
        layer.shadowOffset = CGSize(width:0, height: 0)
        layer.shadowOpacity = 0.4
        
        
        //E6ECF0 table view and content view
        
        //        layer.shadowPath = UIBezierPath(roundedRect: self.bounds, cornerRadius: 30.0).cgPath
        //        layer.shouldRasterize = true
        
        
    }
}
