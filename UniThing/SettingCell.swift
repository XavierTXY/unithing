//
//  SettingCell.swift
//  UniThing
//
//  Created by Xavier TanXY on 1/7/17.
//  Copyright © 2017 Xavier TanXY. All rights reserved.
//

import UIKit

class SettingCell: UITableViewCell {

    @IBOutlet weak var label: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
