//
//  RoundViewComment.swift
//  UniThing
//
//  Created by XavierTanXY on 29/3/18.
//  Copyright © 2018 Xavier TanXY. All rights reserved.
//

import Foundation
import UIKit

class RoundViewComment: UIView {
    
    override func awakeFromNib() {
        super.awakeFromNib()

        
        self.backgroundColor = UIColor.white
//        layer.cornerRadius = 5.0
        layer.masksToBounds = false
        layer.shadowColor = UIColor.black.withAlphaComponent(0.2).cgColor
        layer.shadowOffset = CGSize(width:-1, height: -1)
        layer.shadowOpacity = 0.4
        
        
        
    }
}
