//
//  ChatMessageCell.swift
//  UniThing
//
//  Created by Xavier TanXY on 20/12/16.
//  Copyright © 2016 Xavier TanXY. All rights reserved.
//

import UIKit

class ChatMessageCell: UICollectionViewCell {
    
    
    var newChatLogVC: NewChatLogVC?
    var blueImg: UIImageView!
    var grayImg: UIImageView!
    
    let textView: UITextView = {
        let tv = UITextView()
        
        //tv.textAlignment = .right
        tv.backgroundColor = UIColor.clear
        tv.textColor = UIColor.white
        tv.font = UIFont(name: "HelveticaNeue", size: 16)
        tv.isEditable = false
        tv.translatesAutoresizingMaskIntoConstraints = false
        
        return tv
    }()
    
    let dateLbl: UILabel = {
        let lbl = UILabel()
        
        lbl.textAlignment = .center
        lbl.backgroundColor = UIColor.clear
        lbl.textColor = UIColor.lightGray
        lbl.font = UIFont(name: "HelveticaNeue", size: 10)
        lbl.text = "01/01/2018"
        lbl.translatesAutoresizingMaskIntoConstraints = false
        
        return lbl
    }()
    
    let profileImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFill
        imageView.layer.cornerRadius = 15
        imageView.layer.masksToBounds = true
        imageView.backgroundColor = UIColor.red
        imageView.clipsToBounds = true
        imageView.translatesAutoresizingMaskIntoConstraints = false
        return imageView
    }()
    
    let initialLbl: UILabel = {
        let lbl = UILabel()
        lbl.textAlignment = .center
        lbl.backgroundColor = UIColor.clear
        lbl.textColor = UIColor.white
        lbl.font = UIFont(name: "HelveticaNeue-Bold", size: 16)
        //        tv.isEditable = false
        lbl.text = "A"
        lbl.translatesAutoresizingMaskIntoConstraints = false
        return lbl
    }()
    
//    static let grayBubbleImage = UIImage(named: "bubble_gray")!.resizableImage(withCapInsets: UIEdgeInsetsMake(22, 26, 22, 26)).withRenderingMode(.alwaysTemplate)
//    static let blueBubbleImage = UIImage(named: "bubble_blue")!.resizableImage(withCapInsets: UIEdgeInsetsMake(22, 26, 22, 26)).withRenderingMode(.alwaysTemplate)
//    
//    let bubbleImageView: UIImageView = {
//        let imageView = UIImageView()
//        imageView.image = ChatMessageCell.grayBubbleImage
//        imageView.tintColor = UIColor(white: 0.90, alpha: 1)
//        return imageView
//    }()
    
    let bubbleView: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.white
        view.translatesAutoresizingMaskIntoConstraints = false
        

        

        
     //   view.layer.cornerRadius = 10

        view.layer.cornerRadius = 20
        

        view.layer.masksToBounds = false
        view.layer.shadowColor = UIColor.black.withAlphaComponent(0.4).cgColor
        view.layer.shadowOffset = CGSize(width:0, height: 0)
        view.layer.shadowOpacity = 0.4
        
//       view.layer.shadowColor = UIColor(red: SHADOW_GRAY, green: SHADOW_GRAY, blue: SHADOW_GRAY, alpha: 0.6).cgColor
//        view.layer.shadowOpacity = 1
        view.layer.shadowRadius = 3.0
        //view.layer.masksToBounds = true
        return view
    }()
    
    lazy var  messageImageView: UIImageView = {
        let imageView = UIImageView()
       // imageView.backgroundColor = UIColor.blue
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.layer.cornerRadius = 20
        imageView.layer.masksToBounds = true
        
        imageView.contentMode = .scaleAspectFill
        imageView.isUserInteractionEnabled = true
        imageView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(handleZoomTapped)))
        
        return imageView
    }()
    

    func handleZoomTapped(sender: UITapGestureRecognizer) {
        if let imageView = sender.view as? UIImageView {
            self.newChatLogVC?.performZoomInForStartingImageView(startingImageView: imageView)
        }
    }
    var bubbleWidthAnchor: NSLayoutConstraint?
    var bubbleLeftAnchor: NSLayoutConstraint?
    var bubbleRightAnchor: NSLayoutConstraint?
    
    var ppWidthAnchor: NSLayoutConstraint?
    var ppLeftAnchor: NSLayoutConstraint?
    var ppRightAnchor: NSLayoutConstraint?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        

        
        addSubview(bubbleView)
        
        blueImg = UIImageView(image:#imageLiteral(resourceName: "SlideMenuBG"))
        blueImg.contentMode = .scaleAspectFill
        blueImg.clipsToBounds = true
        bubbleView.addSubview(blueImg)
        blueImg.translatesAutoresizingMaskIntoConstraints = false
        blueImg.topAnchor.constraint(equalTo: bubbleView.topAnchor).isActive = true
        blueImg.trailingAnchor.constraint(equalTo: bubbleView.trailingAnchor).isActive = true
        blueImg.bottomAnchor.constraint(equalTo: bubbleView.bottomAnchor).isActive = true
        blueImg.leadingAnchor.constraint(equalTo: bubbleView.leadingAnchor).isActive = true
        blueImg.layer.cornerRadius = 20
        
//        grayImg = UIImageView(image:#imageLiteral(resourceName: "logoIcon"))
//        grayImg.contentMode = .scaleAspectFill
//        grayImg.clipsToBounds = true
//        bubbleView.addSubview(grayImg)
//        grayImg.translatesAutoresizingMaskIntoConstraints = false
//        grayImg.topAnchor.constraint(equalTo: bubbleView.topAnchor).isActive = true
//        grayImg.trailingAnchor.constraint(equalTo: bubbleView.trailingAnchor).isActive = true
//        grayImg.bottomAnchor.constraint(equalTo: bubbleView.bottomAnchor).isActive = true
//        grayImg.leadingAnchor.constraint(equalTo: bubbleView.leadingAnchor).isActive = true
//        grayImg.layer.cornerRadius = 10
        
        addSubview(textView)
        
        bubbleView.addSubview(messageImageView)
        messageImageView.leftAnchor.constraint(equalTo: bubbleView.leftAnchor).isActive = true
        messageImageView.topAnchor.constraint(equalTo: bubbleView.topAnchor).isActive = true
        messageImageView.widthAnchor.constraint(equalTo: bubbleView.widthAnchor).isActive = true
        messageImageView.heightAnchor.constraint(equalTo: bubbleView.heightAnchor).isActive = true
        
        

        
        
        bubbleRightAnchor = bubbleView.rightAnchor.constraint(equalTo: self.rightAnchor, constant: -8)
        bubbleRightAnchor?.isActive = true
        
        bubbleView.topAnchor.constraint(equalTo: self.topAnchor).isActive = true
        bubbleWidthAnchor = bubbleView.widthAnchor.constraint(equalToConstant: 200)
        bubbleWidthAnchor?.isActive = true
        
        bubbleView.heightAnchor.constraint(equalTo: self.heightAnchor).isActive = true
        

        
        
        //textView.rightAnchor.constraint(equalTo: self.rightAnchor).isActive = true
        textView.leftAnchor.constraint(equalTo: bubbleView.leftAnchor, constant: 8).isActive = true
        textView.rightAnchor.constraint(equalTo: bubbleView.rightAnchor).isActive = true
        textView.topAnchor.constraint(equalTo: self.topAnchor).isActive = true
//        textView.widthAnchor.constraint(equalToConstant: 200).isActive = true
        textView.heightAnchor.constraint(equalTo: self.heightAnchor).isActive = true
        
        addSubview(dateLbl)
        
        dateLbl.centerXAnchor.constraint(equalTo: self.centerXAnchor).isActive = true
        dateLbl.widthAnchor.constraint(equalToConstant: 100).isActive = true
        dateLbl.heightAnchor.constraint(equalToConstant: 30).isActive = true
        dateLbl.bottomAnchor.constraint(equalTo: bubbleView.topAnchor).isActive = true
//        dateLbl.topAnchor.constraint(equalTo: self.topAnchor).isActive = true
        
        
        addSubview(profileImageView)
        profileImageView.leftAnchor.constraint(equalTo: self.leftAnchor, constant: 8).isActive = true
        profileImageView.widthAnchor.constraint(equalToConstant: 30).isActive = true
        profileImageView.heightAnchor.constraint(equalToConstant: 30).isActive = true
        profileImageView.bottomAnchor.constraint(equalTo: self.bottomAnchor).isActive = true

        
        bubbleLeftAnchor = bubbleView.leftAnchor.constraint(equalTo: profileImageView.rightAnchor, constant: 8)
        bubbleLeftAnchor?.isActive = false
        
        profileImageView.addSubview(initialLbl)
        initialLbl.centerXAnchor.constraint(equalTo: profileImageView.centerXAnchor).isActive = true
        initialLbl.centerYAnchor.constraint(equalTo: profileImageView.centerYAnchor).isActive = true
        initialLbl.widthAnchor.constraint(equalToConstant: 20).isActive = true
        initialLbl.heightAnchor.constraint(equalToConstant: 20).isActive = true



        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
}
