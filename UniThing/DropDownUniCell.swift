//
//  DropDownUniCell.swift
//  UniThing
//
//  Created by XavierTanXY on 6/5/18.
//  Copyright © 2018 Xavier TanXY. All rights reserved.
//

import UIKit

class DropDownUniCell: UITableViewCell {

    @IBOutlet weak var iconImg: UIImageView!
    @IBOutlet weak var uniLbl: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    func configureCell(name: String) {
        uniLbl.text = name
        
        if name == "Trending" {
//            iconImg.isHidden = false
            iconImg.image = #imageLiteral(resourceName: "trending")
        } else if name == "Latest" {
//            iconImg.isHidden = false
            iconImg.image = #imageLiteral(resourceName: "latest")
        } else {
//            iconImg.isHidden = true
            iconImg.image = nil
        }
    }
    
}
