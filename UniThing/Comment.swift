//
//  Comment.swift
//  UniThing
//
//  Created by Xavier TanXY on 19/12/16.
//  Copyright © 2016 Xavier TanXY. All rights reserved.
//

import Foundation
import UIKit
import Firebase
import FirebaseDatabase

class Comment {
    private var _commentKey: String!
    private var _comment: String!
    private var _likes: Int!
    private var _confessionkey: String!
    private var _time: NSNumber!
    private var _author: String!
    private var _authorGender: String!
    private var _authorFaculty: String!
    private var _userName: String!
    private var _usersLike: [String: Bool]!
    private var _commentRef: DatabaseReference!
    private var _profilePicUrl: String!
    private var _replyTo: String!
    private var _replyCommentContent: String!
    private var _anonymous: Bool!
    private var _anonymousID: Int?
    private var _deleted: Bool!
    private var _university: String!
    private var _authorUniversity: String!
    
    var anonymousID: Int? {
        return _anonymousID
    }
    
    var commentKey: String {
        return _commentKey
    }
    
    var comment: String {
        return _comment
    }
    
    var likes: Int {
        return _likes
    }
    
    var confessionKey: String {
        return _confessionkey
    }
    
    var time: NSNumber {
        return _time
    }
    
    var author: String {
        return _author
    }
    
    var authorGender: String {
        return _authorGender
    }
    
    var authorFaculty: String {
        return _authorFaculty
    }
    
    var commentRef: DatabaseReference {
        return _commentRef
    }
    
    var usersLike: [String: Bool] {
        return _usersLike
    }
    
    var userName: String {
        return _userName
    }
    
    var profilePicUrl: String {
        return _profilePicUrl
    }
    
    var replyTo: String {
        return _replyTo
    }
    
    var replyCommentContent: String {
        return _replyCommentContent
    }
    
    var university: String {
        return _university
    }
    
    var authorUniversity: String {
        return _authorUniversity
    }
    
    var anonymous: Bool {
        return _anonymous
    }
    
    var deleted: Bool {
        get {
            return _deleted
        }
        
        set(inBool) {
            self._deleted = inBool
        }
        
    }
    
    
    
    init(commentKey: String, postData: Dictionary<String, AnyObject>) {
        
        
        self._commentKey = commentKey
        
        if let comment = postData["comment"] as? String{
            self._comment = comment
        }
        
        
        if let likes = postData["likes"] as? Int {
            self._likes = likes
        }
        
        if let time = postData["time"] as? NSNumber {
            self._time = time
        }
        
        if let confessionKey = postData["confessionKey"] as? String {
            self._confessionkey = confessionKey
        }
        
        if let author = postData["author"] as? String {
            self._author = author
        }
        
        if let authorGender = postData["authorGender"] as? String {
            self._authorGender = authorGender
        }
        
        if let authorFaculty = postData["authorFaculty"] as? String {
            self._authorFaculty = authorFaculty
        }
        
        if let userName = postData["userName"] as? String {
            self._userName = userName
        }
        
        if let userLike = postData["userLike"] as? [String: Bool] {
            self._usersLike = userLike
        }
        
        if let profilePicUrl = postData["profilePicUrl"] as? String {
            self._profilePicUrl = profilePicUrl
        }
        
        if let replyTo = postData["replyTo"] as? String {
            self._replyTo = replyTo
        }
        
        if let replyCommentContent = postData["replyCommentContent"] as? String {
            self._replyCommentContent = replyCommentContent
        }
        
        if let uni = postData["university"] as? String {
            self._university = uni
        }
        
        if let aUni = postData["authorUniversity"] as? String {
            self._authorUniversity = aUni
        }
        
        if let anony = postData["anonymous"] as? Bool {
            self._anonymous = anony
        }
        
        if let deleted = postData["deleted"] as? Bool {
            self._deleted = deleted
        }
        

        _commentRef = DataService.ds.REF_COMMENTS.child(_commentKey)
    }
    
    func addUser(uid: String) {
        _usersLike[uid] =  true
    }
    
    func removeUser(uid: String) {
        _usersLike.removeValue(forKey: uid)
    
    }
    
    func removeNotification(completion: @escaping () -> ()) {
        var updateObj = [String:AnyObject]()
        let ref = DB_BASE
        if self.author != Auth.auth().currentUser?.uid {
            decreaseUserNotiCount(authorID: self.author)

            DataService.ds.REF_LIKER_COMMENT_NOTIFICATION.child((Auth.auth().currentUser?.uid)!).child(self.commentKey).observeSingleEvent(of: .value, with: { (snapshot) in
                if let dict = snapshot.value as? Dictionary<String, AnyObject> {
                    var notificationID = dict["notificationID"] as! String
                    updateObj["/notification/\(self.author)/\(notificationID)"] = NSNull()
                    updateObj["/liker-comment-notification/\((Auth.auth().currentUser?.uid)!)/\(self.commentKey)"] = NSNull()
                    print("here")
                    
                    updateObj["/comment-commentAuthor-notification/\(self.commentKey)/\(self.author)/notifications/\(notificationID)"] = NSNull()
                    //updateObj["/comment-notification/\(self.commentKey)/\(notificationID)"] = NSNull()
                    updateObj["/comment-liker/\(self.commentKey)/\((Auth.auth().currentUser?.uid)!)"] = NSNull()
                    ref.updateChildValues(updateObj)
                    completion()
                    
                    
                }
            })
            
        } else {
            
            ref.updateChildValues(updateObj)
            completion()
        }
    }
    
    func addNotification(completion: @escaping () -> ()) {
        var updateObj = [String:AnyObject]()
        let ref = DB_BASE
        
        if self.author != Auth.auth().currentUser?.uid {
            
            incrementUserNotiCount(authorID: self.author)

            DataService.ds.REF_USERS.child((Auth.auth().currentUser?.uid)!).observeSingleEvent(of: .value, with: { (snapshot) in
                if let snapshots = snapshot.children.allObjects as? [DataSnapshot] {
                    if let userDict = snapshot.value as? Dictionary<String, AnyObject> {
                        let key = snapshot.key
                        var user = User(userKey: key, userData: userDict)
                        
                        let timestamp = NSNumber(integerLiteral: Int(NSDate().timeIntervalSince1970))
                        var notifcationID = DataService.ds.REF_NOTIFICATION.child(self.author).childByAutoId()
                        var notificationValue: Dictionary<String, AnyObject> = ["from" : "\((Auth.auth().currentUser?.uid)!) " as AnyObject, "type" : "commentLike" as AnyObject, "userName" : user.userName as AnyObject, "commentKey" : self.commentKey as AnyObject, "confessionKey" : self.confessionKey as AnyObject, "time" : timestamp as AnyObject, "profilePicUrl" : user.profilePicUrl as AnyObject, "seen" : false as AnyObject, "likedCommentContent": "\(self.comment)" as AnyObject]
                        
                        updateObj["/liker-comment-notification/\((Auth.auth().currentUser?.uid)!)/\(self.commentKey)"] = ["notificationID" : notifcationID.key ] as AnyObject
                        notifcationID.updateChildValues(notificationValue)
                        
                        
                        //updateObj["/comment-commentAuthor-notification/\(self.commentKey)/commentAuthor"] = self.author  as AnyObject
                        updateObj["/comment-commentAuthor-notification/\(self.commentKey)/\(self.author)/notifications/\(notifcationID.key)"] = true  as AnyObject
                        //updateObj["/comment-notification/\(self.commentKey)/\(notifcationID.key)"] = true as AnyObject
                        updateObj["/comment-liker/\(self.commentKey)/\((Auth.auth().currentUser?.uid)!)"] = true as AnyObject
                        ref.updateChildValues(updateObj)
                        completion()
                        
                        
                        
                    }
                    
                }
                
            })
        } else {
            
            ref.updateChildValues(updateObj)
            completion()
        }
        
    }
    
    func adjustLikes(addLike: Bool, uid: String) {
        
//        let ref = DataService.ds.REF_COMMENTS.child(_commentKey).child("likes")
        let ref = DataService.ds.REF_COMMENTS.child(_commentKey)
        ref.runTransactionBlock({ (currentData) -> TransactionResult in
            
            if var commentDict = currentData.value as? Dictionary<String, AnyObject> {
                
                //var confessionDict = currentData.value as? Dictionary<String, AnyObject>
                
                
                
                var userLike = commentDict["userLike"] as? [String: Bool]
                if addLike {
                    userLike?[uid] = true
                    self.addUser(uid: uid)
                    
                } else {
                    // userLike.removeValue(forKey: uid)
                    userLike?.removeValue(forKey: uid)
                    self.removeUser(uid: uid)
                }
                
                commentDict["userLike"] = userLike as AnyObject
                
                var likes = commentDict["likes"] as? Int
                
                if likes != nil {
                    if addLike {
                        likes = likes! + 1
                        //                        self.addNotification()
                    } else {
                        if likes != 0 {
                            likes = likes! - 1
                            //self.removeNotification()
                        }
                    }
                }
                
                
                commentDict["likes"] = likes as AnyObject
                
                
                currentData.value = commentDict
                print("scueess")
                return TransactionResult.success(withValue: currentData)
            }
            
            return TransactionResult.success(withValue: currentData)

//        ref.runTransactionBlock({ (currentData) -> FIRTransactionResult in
//
//            //value of the counter before an update
//            var value = currentData.value as? Int
//
//            //checking for nil data is very important when using
//            //transactional writes
//            if value == nil {
//
//                value = 0
//            }
//            
//            if addLike {
//                currentData.value = value! + 1
//                print("add")
//            } else {
//                print("minus")
//                let like = currentData.value as! Int
//                if like != 0 {
//                    currentData.value = value! - 1
//                }
//
//            }
//            
//            //actual update
//            
//            return FIRTransactionResult.success(withValue: currentData)
        
        }) { (error, commited, snap) in
            
            if commited {
                
                var commentDict = snap?.value as? Dictionary<String, AnyObject>
                
                if let likeToBeUpdated = commentDict?["likes"] {
                    self._likes = likeToBeUpdated as! Int
                    
                    
                    var updateObj = [String:AnyObject]()
                    let ref = DB_BASE
                    
                    self.commentRef.observeSingleEvent(of: .value, with: { (snapshot) in
                        if snapshot.exists() {
                            if addLike {
                                
                                
                                self.addNotification {
                                    self.addUser(uid: uid)
                                    // self._commentRef.child("likes").setValue(self._likes)
                                    //self._commentRef.child("userLike").updateChildValues([uid: true])
                                    
                                    
                                    updateObj["/comments/\(self.commentKey)/likes"] = likeToBeUpdated as AnyObject
                                    updateObj["/comments/\(self.commentKey)/userLike/\(uid)"] = true as AnyObject
                                    updateObj["/confession-comments/\(self.confessionKey)/\(self.commentKey)/likes"] = likeToBeUpdated as AnyObject
                                    updateObj["/confession-comments/\(self.confessionKey)/\(self.commentKey)/userLike/\(uid)"] = true as AnyObject
                                    ref.updateChildValues(updateObj)
                                }
                                
                            } else {
                                
                                self.removeNotification {
                                    self.removeUser(uid: uid)
                                    //self._commentRef.child("likes").setValue(self._likes)
                                    //self._commentRef.child("userLike").child(uid).removeValue()
                                    
                                    
                                    updateObj["/comments/\(self.commentKey)/likes"] = likeToBeUpdated as AnyObject
                                    updateObj["/comments/\(self.commentKey)/userLike/\(uid)"] = NSNull()
                                    updateObj["/confession-comments/\(self.confessionKey)/\(self.commentKey)/likes"] = likeToBeUpdated as AnyObject
                                    updateObj["/confession-comments/\(self.confessionKey)/\(self.commentKey)/userLike/\(uid)"] = NSNull()
                                    ref.updateChildValues(updateObj)
                                }

                            }
                        } else {
                            print("NOT EXI")
                        }
                    })

                    
                }
                
                
                
            } else {
                //call error callback function if you want
                
                print("fail")
            }
//            if commited {
//
//                let likeToBeUpdated = snap!.value
//                self._likes = likeToBeUpdated as! Int
//                var updateObj = [String:AnyObject]()
//                let ref = DB_BASE
//
//                if addLike {
//
//                    self.addUser(uid: uid)
//                    // self._commentRef.child("likes").setValue(self._likes)
//                    //self._commentRef.child("userLike").updateChildValues([uid: true])
//
//
//                    updateObj["/comments/\(self.commentKey)/likes"] = likeToBeUpdated as AnyObject
//                    updateObj["/comments/\(self.commentKey)/userLike/\(uid)"] = true as AnyObject
//                    updateObj["/confession-comments/\(self.confessionKey)/\(self.commentKey)/likes"] = likeToBeUpdated as AnyObject
//                    updateObj["/confession-comments/\(self.confessionKey)/\(self.commentKey)/userLike/\(uid)"] = true as AnyObject
////                    ref.updateChildValues(updateObj)
//
//                    if self.author != FIRAuth.auth()?.currentUser?.uid {
//                        DataService.ds.REF_USERS.child((FIRAuth.auth()?.currentUser?.uid)!).observeSingleEvent(of: .value, with: { (snapshot) in
//                            if let snapshots = snapshot.children.allObjects as? [FIRDataSnapshot] {
//                                if let userDict = snapshot.value as? Dictionary<String, AnyObject> {
//                                    let key = snapshot.key
//                                    var user = User(userKey: key, userData: userDict)
//
//                                    let timestamp = NSNumber(integerLiteral: Int(NSDate().timeIntervalSince1970))
//                                    var notifcationID = DataService.ds.REF_NOTIFICATION.child(self.author).childByAutoId()
//                                    var notificationValue: Dictionary<String, AnyObject> = ["from" : "\((FIRAuth.auth()?.currentUser?.uid)!) " as AnyObject, "type" : "commentLike" as AnyObject, "userName" : user.userName as AnyObject, "commentKey" : self.commentKey as AnyObject, "time" : timestamp as AnyObject, "profilePicUrl" : user.profilePicUrl as AnyObject, "seen" : false as AnyObject]
//
//                                    updateObj["/liker-comment-notification/\((FIRAuth.auth()?.currentUser?.uid)!)/\(self.commentKey)"] = ["notificationID" : notifcationID.key ] as AnyObject
//                                    notifcationID.updateChildValues(notificationValue)
//
//
//                                    //updateObj["/comment-commentAuthor-notification/\(self.commentKey)/commentAuthor"] = self.author  as AnyObject
//                                    updateObj["/comment-commentAuthor-notification/\(self.commentKey)/\(self.author)/notifications/\(notifcationID.key)"] = true  as AnyObject
//                                    //updateObj["/comment-notification/\(self.commentKey)/\(notifcationID.key)"] = true as AnyObject
//                                    updateObj["/comment-liker/\(self.commentKey)/\((FIRAuth.auth()?.currentUser?.uid)!)"] = true as AnyObject
//                                    ref.updateChildValues(updateObj)
//
//
//
//                                }
//
//                            }
//
//                        })
//                    } else {
//
//                        ref.updateChildValues(updateObj)
//                    }
//
//
//                } else {
//
//
//                    self.removeUser(uid: uid)
//                    //self._commentRef.child("likes").setValue(self._likes)
//                    //self._commentRef.child("userLike").child(uid).removeValue()
//
//
//                    updateObj["/comments/\(self.commentKey)/likes"] = likeToBeUpdated as AnyObject
//                    updateObj["/comments/\(self.commentKey)/userLike/\(uid)"] = NSNull()
//                    updateObj["/confession-comments/\(self.confessionKey)/\(self.commentKey)/likes"] = likeToBeUpdated as AnyObject
//                    updateObj["/confession-comments/\(self.confessionKey)/\(self.commentKey)/userLike/\(uid)"] = NSNull()
//                    //ref.updateChildValues(updateObj)
//
//                    if self.author != FIRAuth.auth()?.currentUser?.uid {
//                        DataService.ds.REF_LIKER_COMMENT_NOTIFICATION.child((FIRAuth.auth()?.currentUser?.uid)!).child(self.commentKey).observeSingleEvent(of: .value, with: { (snapshot) in
//                            if let dict = snapshot.value as? Dictionary<String, AnyObject> {
//                                var notificationID = dict["notificationID"] as! String
//                                updateObj["/notification/\(self.author)/\(notificationID)"] = NSNull()
//                                updateObj["/liker-comment-notification/\((FIRAuth.auth()?.currentUser?.uid)!)/\(self.commentKey)"] = NSNull()
//                                print("here")
//
//                                updateObj["/comment-commentAuthor-notification/\(self.commentKey)/\(self.author)/notifications/\(notificationID)"] = NSNull()
//                                //updateObj["/comment-notification/\(self.commentKey)/\(notificationID)"] = NSNull()
//                                updateObj["/comment-liker/\(self.commentKey)/\((FIRAuth.auth()?.currentUser?.uid)!)"] = NSNull()
//                                ref.updateChildValues(updateObj)
//
//
//                            }
//                        })
//
//                    } else {
//
//                        ref.updateChildValues(updateObj)
//                    }
//
//
//
//
//                }
//
//            } else {
//                //call error callback function if you want
//
//                print("fail")
//            }
//        }

        
    }
    
    }
    
    func incrementUserNotiCount(authorID: String) {
        let ref2 = DataService.ds.REF_USERS.child(authorID)
        
        ref2.runTransactionBlock({ (currentData) -> TransactionResult in
            
            if var userDict = currentData.value as? Dictionary<String, AnyObject> {
                
                
                var notiCount = userDict["notiCount"] as? Int
                
                if notiCount != nil {
                    notiCount = notiCount! + 1
                    
                }
                
                
                userDict["notiCount"] = notiCount as AnyObject
                
                
                currentData.value = userDict
                print("scueess")
                return TransactionResult.success(withValue: currentData)
            }
            
            return TransactionResult.success(withValue: currentData)
        }) { (error, commited, snap) in
            
            
        }
    }
    
    func decreaseUserNotiCount(authorID: String) {
        let ref2 = DataService.ds.REF_USERS.child(authorID)
        
        ref2.runTransactionBlock({ (currentData) -> TransactionResult in
            
            if var userDict = currentData.value as? Dictionary<String, AnyObject> {
                
                
                var notiCount = userDict["notiCount"] as? Int
                
                if notiCount != nil {
                    
                    if notiCount != 0 {
                        notiCount = notiCount! - 1
                    }
                    
                    
                }
                
                
                userDict["notiCount"] = notiCount as AnyObject
                
                
                currentData.value = userDict
                print("scueess")
                return TransactionResult.success(withValue: currentData)
            }
            
            return TransactionResult.success(withValue: currentData)
        }) { (error, commited, snap) in
            
            
        }
    }


}
