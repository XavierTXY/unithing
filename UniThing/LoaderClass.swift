//
//  LoaderClass.swift
//  UniThing
//
//  Created by Xavier TanXY on 30/11/16.
//  Copyright © 2016 Xavier TanXY. All rights reserved.
//

import Foundation
import UIKit

class LoaderClass {
    
    var container: UIView = UIView()
    var loadingView: UIView = UIView()
    var actInd: UIActivityIndicatorView = UIActivityIndicatorView()
    
    func showActivityIndicatory(uiView: UIView) {
       // var container: UIView = UIView()
        container.frame = uiView.frame
        container.center = uiView.center
        
        container.backgroundColor = UIColor(colorLiteralRed: 1, green: 1, blue: 1, alpha: 0.3)
        //UIColor(white: 0xffffff, alpha: 0.3)
        
     //   var loadingView: UIView = UIView()
        
        loadingView.frame = CGRect(x: 0, y: 0, width: 80, height: 80)
        loadingView.center = uiView.center
        loadingView.backgroundColor = UIColor(colorLiteralRed: 0, green: 0, blue: 0, alpha: 0.7)
        //UIColor(white: 0x444444, alpha: 0.7)
        loadingView.clipsToBounds = true
        loadingView.layer.cornerRadius = 10
        
      //  var actInd: UIActivityIndicatorView = UIActivityIndicatorView()
        
        actInd.frame = CGRect(x: 0, y: 0, width: 40, height: 40)
        actInd.activityIndicatorViewStyle = UIActivityIndicatorViewStyle.whiteLarge
        actInd.center = CGPoint(x: loadingView.frame.size.width / 2, y: loadingView.frame.size.height / 2)
        
       // loadingView.addSubview(actInd)
      //  container.addSubview(loadingView)
        //uiView.addSubview(container)
        uiView.addSubview(actInd)
        actInd.startAnimating()
    }
    
    func hide()  {
        print("ssss2s")
        actInd.stopAnimating()
        actInd.removeFromSuperview()
       // loadingView.removeFromSuperview()
        //container.removeFromSuperview()
    }
    func hideActivityIndicator(uiView: UIView) {
        print("sssss")
        actInd.stopAnimating()
        //container.removeFromSuperview()
       // loadingView.removeFromSuperview()
        actInd.removeFromSuperview()
        
    }
    
}
