//
//  LoadingTableViewEmptyView.swift
//  UniThing
//
//  Created by Xavier TanXY on 5/7/17.
//  Copyright © 2017 Xavier TanXY. All rights reserved.
//

import UIKit

class LoadingTableViewEmptyView: UIView {

    @IBOutlet weak var label: UILabel!

    @IBOutlet weak var view: UIView!


    
//    required init?(coder aDecoder: NSCoder) {
//        super.init(coder: aDecoder)
//        setupView()
//    }
//    
//    override init(frame: CGRect) {
//        super.init(frame: frame)
//        setupView()
//    }
//    
//    func setupView() {
//        Bundle.main.loadNibNamed("LoadingTableViewEmptyView", owner: self, options: nil)
//        
//        
//        view.frame = self.frame
//        self.addSubview(view)
//    }
    
    class func instanceFromNib() -> UIView {
        return UINib(nibName: "LoadingTableViewEmptyView", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! UIView
    }

}
