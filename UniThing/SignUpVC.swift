//
//  SignUpVC.swift
//  UniThing
//
//  Created by Xavier TanXY on 30/11/16.
//  Copyright © 2016 Xavier TanXY. All rights reserved.
//

import UIKit
import Firebase
import FirebaseInstanceID
import FirebaseMessaging
import SVProgressHUD

class SignUpVC: UIViewController, UITextFieldDelegate {
    
    var uni: University!
    var email: String!
    var name: String!
    var password: String!
    var course: String!
    var year: String!
    var gender: String!
    var provider: String!
    var uid: String!
    var userName: String!
    
    @IBOutlet weak var userNameTxtField: UITextField!
    @IBOutlet weak var mainView: UIView!
//    @IBOutlet weak var coverImage: UIImageView!
    @IBOutlet weak var signUpBtn: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        print("1")
        print(email)
        print(name)
        print(course)
        print(year)
        print(gender)
        print(provider)
        
        let leftBarItem = UIBarButtonItem(image: #imageLiteral(resourceName: "Back Chevron"), style: .plain, target: self, action: "backTapped")
        //let rightBarItem = UIBarButtonItem(title: "Sign Up", style: .done, target: self, action: "checkUserName")
        //let rightBarItem = UIBarButtonItem(image: #imageLiteral(resourceName: "signUp"), style: .plain, target: self, action: "checkUserName")
        let rightBarItem = UIBarButtonItem(title: "Sign Up", style: .done, target: self, action: "checkUserName")
        leftBarItem.tintColor = UIColor.white
        rightBarItem.tintColor = UIColor.white
//        self.navigationItem.setLeftBarButton(leftBarItem, animated: true)
//        self.navigationItem.setRightBarButton(rightBarItem, animated: true)
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title:"", style:.plain, target:nil, action:nil)
        
       // self.navigationItem.rightBarButtonItem?.isEnabled = false
        self.navigationItem.leftBarButtonItem?.tintColor = UIColor.white
        
        userNameTxtField.delegate = self
        let userName = String(name.characters.filter { !" \n\t\r".characters.contains($0) })
        userNameTxtField.text = userName.trimmingCharacters(in: .whitespaces).lowercased()
        
        userNameTxtField.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
//        coverImage.isHidden = true
        
        
        self.navigationItem.title = "Sign Up with Email"
        self.navigationController?.navigationBar.titleTextAttributes = [ NSFontAttributeName: UIFont(name: "HelveticaNeue-Bold", size: 16)!]
        self.navigationController?.navigationBar.titleTextAttributes = [ NSForegroundColorAttributeName: UIColor.white]
        if #available(iOS 11.0, *) {
            
            self.navigationController?.navigationBar.prefersLargeTitles = true
            self.navigationController?.navigationBar.largeTitleTextAttributes = [ NSForegroundColorAttributeName: UIColor.white]
        } else {
            // Fallback on earlier versions
        }


        
    }
    


    
    override func viewDidAppear(_ animated: Bool) {
        UIView.animate(withDuration: 0.3, delay: 0.1, options: [], animations: { self.mainView.alpha = 1.0 }, completion: nil)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.mainView.alpha = 0.0
    }
    
    func textFieldDidChange(_ textField: UITextField) {
        print(userNameTxtField.text)
        if (userNameTxtField.text?.characters.count)! > 0 {
            print("what?")
            self.signUpBtn.setBackgroundImage(#imageLiteral(resourceName: "LoginBtnGreen"), for: .normal)
            self.signUpBtn.isEnabled = true
            
        } else {
            print("111")
            self.signUpBtn.setBackgroundImage(#imageLiteral(resourceName: "LoginBtnGray"), for: .normal)
            self.signUpBtn.isEnabled = false
        }
    }
    
    func backTapped() {
        self.navigationController?.popViewController(animated: true)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        print("lol")
        // textField.resignFirstResponder()
        if textField == userNameTxtField {
            textField.resignFirstResponder()
            //self.checkUserName()
            
        }
        return true
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
//    func createUser(uid: String, userProvider: Dictionary<String,String>, userEmail: Dictionary<String,String>, name: Dictionary<String,String>, userName: Dictionary<String,String>, uni: Dictionary<String,String>, uniShort: Dictionary<String,String>, year: Dictionary<String,String>, course: Dictionary<String,String>, gender: Dictionary<String,String>) {
//        
//        let userid = ["\(uid)": true]
//        let uniName = uni["university"]
//        //DataService.ds.createFirebaseDBUserWithFull(uid: uid, userProvider: userProvider, userEmail: userEmail, name: name, userName: userName, uni: uni, uniShort: uniShort, year: year, course: course, gender: gender)
//       // DataService.ds.updateUniversity(uid: userid, uni: self.uni)
//        
//        DataService.ds.createFirebaseDBUserWithFB(uid: uid, userProvider: userProvider, userEmail: userEmail, name: name, userName: userName, uni: uni, uniShort: uniShort, year: year, course: course, gender: gender) { 
//            VCSegue().segueVC(bundleName: "HomeSB", controllerName: "TabBarVC", vc: self)
//            Interaction().enableInteraction()
//        }
//        
//    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        let currentCharacterCount = textField.text?.characters.count ?? 0
        if (range.length + range.location > currentCharacterCount){
            
            return false
        }
        let newLength = currentCharacterCount + string.characters.count - range.length
        return newLength <= LIMIT_CHAR_NAME
    }
    
    func removeSpecialCharsFromString(str: String) -> String {
        let chars = Set("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLKMNOPQRSTUVWXYZ1234567890_".characters)
        
        return String(str.characters.filter { chars.contains($0) })
    }
    
    func checkValidFullName(str: String) -> Bool {
        let name = str
        let filterdName = removeSpecialCharsFromString(str: name)
        
        if filterdName == name {
            return true
        } else {
            return false
        }
    }
    
    @IBAction func signupTapped(_ sender: Any) {
        checkUserName()
    }
    
    func checkUserName() {
        
       
        
        if self.userNameTxtField.isFirstResponder {
            self.userNameTxtField.resignFirstResponder()
        }
        
        Interaction().disableInteraction(msg: "Checking user name...")
//        SVProgressHUD.show()
//        UIApplication.shared.beginIgnoringInteractionEvents()
        
        let trimmedName = userNameTxtField.text?.trimmingCharacters(in: .whitespaces).lowercased()
        
        if (trimmedName?.containsEmoji)! {
            Interaction().enableInteraction()
            ErrorAlert().createAlert(title: "Invalid Name", msg: "Name can't contain emoji", object: self)
        } else if trimmedName?.lowercased() == "student" || trimmedName?.lowercased() == "anonymousstudent" || trimmedName?.lowercased() == "admin"{
            Interaction().enableInteraction()
            ErrorAlert().createAlert(title: "Invalid Name", msg: "Please choose another name", object: self)
        } else if !checkValidFullName(str: trimmedName!) {
            Interaction().enableInteraction()
            ErrorAlert().createAlert(title: "Invalid Name", msg: "Name can't contain symbol or spaces", object: self)
        } else {
        
            signUpTapped(userName: trimmedName!)
        }
        
    }
    
    func signUpTapped(userName: String) {
//        coverImage.isHidden = false
       // Interaction().disableInteraction(msg: "Checking UserName Availability")
        
        if (userName.characters.count > 0 ) {
            
            self.userName = userName

            
            DataService.ds.REF_USERS.queryOrdered(byChild: "userName").queryEqual(toValue: self.userName).observeSingleEvent(of: .value, with: { (snapshot) in

                if ( snapshot.value is NSNull ) {
                    print("Xavier not found)")

                    if ( self.provider == "facebook.com" ) {
                        var uid = Auth.auth().currentUser?.uid

                        DataService.ds.createFirebaseDBUserWithFB(uid: uid!, userProvider: self.provider, userEmail: self.email!, name: self.name, userName: self.userName, uni: self.uni.name, uniShort: self.uni.shortName, year: self.year, course: self.course, gender: self.gender) {
                            
                            UserDefaults.standard.set(self.uni.shortName, forKey: "uniShort")
                            VCSegue().segueVC(bundleName: "HomeSB", controllerName: "TabBarVC", vc: self)
                            Interaction().enableInteraction()
                        }

                    } else {

                        var uid = (Auth.auth().currentUser?.uid)!
                        var providerID = (Auth.auth().currentUser?.providerID)!
                        var email = (Auth.auth().currentUser?.email)!
                        DataService.ds.createFirebaseUser(uid: uid, userProvider: providerID, userEmail: email, name: self.name, userName: self.userName, uni: self.uni.name, uniShort: self.uni.shortName, year: self.year, course: self.course, gender: self.gender) {
                            UserDefaults.standard.set(self.uni.shortName, forKey: "uniShort")
                            Interaction().showSuccess()
                            VCSegue().segueVC(bundleName: "HomeSB", controllerName: "TabBarVC", vc: self)
                        }

                    }


                } else {

                    Interaction().enableInteraction()
                    ErrorAlert().show(title: "Username is already in use", msg: "Please enter another username", object: self)
                }

            })
        } else {
            Interaction().enableInteraction()
            ErrorAlert().show(title: "Username can't be empty", msg: "Please enter another username", object: self)
        }
        
        
        
        
    }
    
    
    
    func completeSignIn(id: String, userProvider: Dictionary<String, String>, userEmail: Dictionary<String, String>) {
        //self.loaderStopAnimate()
        //DataService.ds.createFirebaseDBUser(uid: id, userProvider: userProvider, userEmail: userEmail)
        // performSegue(withIdentifier: "FacultyVC", sender: nil)
        //self.enableInteraction()
    }
    
    
    
}
