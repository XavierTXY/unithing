//
//  UniVC.swift
//  UniThing
//
//  Created by Xavier TanXY on 30/11/16.
//  Copyright © 2016 Xavier TanXY. All rights reserved.
//

import UIKit
import PopupDialog
import Firebase
import ViewAnimator
import SVProgressHUD

class UniVC: UIViewController, UITableViewDelegate, UITableViewDataSource  {

    var email: String?
    var name: String?
    var password: String?
    var selectedUni: University?
    var gender: String?
    var provider: String?
    var uid: String?
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var mainView: UIView!
    
    var inSearchMode = false
    var filteredUni = [University]()
    var universities = [University]()
    
    var userUni: String?
    
    var ACT = [University]()
    var NATIONAL = [University]()
    var NSW = [University]()
    var NT = [University]()
    var QLD = [University]()
    var WA = [University]()
    var TAS = [University]()
    var VIC = [University]()
    var SA = [University]()
    
    var states = [[University]]()
    
    var courses = [String]()
    
    var alertController: UIAlertController!
    
    private let animations = [AnimationType.from(direction: .bottom, offset: 30.0)]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        let barItem = UIBarButtonItem(image: #imageLiteral(resourceName: "Back Chevron"), style: .plain, target: self, action: "backTapped")
        let rightBarItem = UIBarButtonItem(title: "Help", style: .plain, target: self, action: "helpTapped")
        rightBarItem.tintColor = UIColor.white
        
        barItem.tintColor = UIColor.white
        self.navigationItem.setLeftBarButton(barItem, animated: true)
        self.navigationItem.setRightBarButton(rightBarItem, animated: true)
        
        tableView.delegate = self
        tableView.dataSource = self
        tableView.tableFooterView = UIView()
        
        
        
        
        self.navigationItem.title = "University";

        self.navigationController?.navigationBar.barTintColor = UIColor.white
        self.navigationController?.navigationBar.titleTextAttributes = [ NSFontAttributeName: UIFont(name: "HelveticaNeue-Bold", size: 16)!]
        self.navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName: UIColor.white]
        self.navigationController?.navigationBar.layer.borderColor = BORDER_COLOR_CGCOLOR
        
  
        //self.navigationItem.backBarButtonItem = UIBarButtonItem(title:"", style:.plain, target:nil, action:nil)
        
        
        parseUniversityCSV()
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
       // UIView.animate(withDuration: 0.3, delay: 0.1, options: [], animations: { self.mainView.alpha = 1.0 }, completion: nil)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
      // self.mainView.alpha = 0.0
    }
    
    func backTapped() {
        self.navigationController?.popViewController(animated: true)
    }
    
    func helpTapped() {
        let alertController = UIAlertController(title: "Can't find your university?", message: "Please enter your university", preferredStyle: .alert)
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { (_) in }
        
        let confirmAction = UIAlertAction(title: "Confirm", style: .default) { (_) in
            if let field = alertController.textFields?[0] {
                DataService.ds.REF_MISSING_UNI.childByAutoId().updateChildValues(["uni": field.text])
            }
            
            let lastAlertController = UIAlertController(title: "Thank you for your feedback!", message: "We will add your university as soon as possible", preferredStyle: .alert)
            
            lastAlertController.addAction(UIAlertAction(title: "Done", style: .default) { (action:UIAlertAction!) in
                //self.dismiss(animated: true, completion: nil)
            })
            
            self.present(lastAlertController, animated: true, completion: nil)
        }
        
        alertController.addTextField { (textField) in
            textField.placeholder = "Current University"
        }
        
        alertController.addAction(confirmAction)
        alertController.addAction(cancelAction)
        
        self.present(alertController, animated: true, completion: nil)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return states.count
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 43;
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        var retVal = ""
        
        switch section {
        case 0:
            retVal = "ACT"
            break
        case 1:
            retVal = "NATIONAL"
            break
        case 2:
            retVal = "NSW"
            break
        case 3:
            retVal = "NT"
            break
        case 4:
            retVal = "QLD"
            break
        case 5:
            retVal = "SA"
            break
        case 6:
            retVal = "TAS"
            break
        case 7:
            retVal = "VIC"
            break
        case 8:
            retVal = "WA"
            break
        default:
            break
        }
        
        
        return retVal
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return states[section].count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if let cell = tableView.dequeueReusableCell(withIdentifier: "UniCell") as? UniCell {
            
            let uni = states[indexPath.section][indexPath.row]
            
            cell.configureCell(uni: uni)
            
            return cell
        } else {
            return UniCell()
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        selectedUni = states[indexPath.section][indexPath.row]
    
//        self.showAlert()
        self.performSegue(withIdentifier: "FacultyVC", sender: nil)
        
        
    }
    
    
    
    func parseUniversityCSV() {
        SVProgressHUD.show()
        
//        let filepath = Bundle.main.path(forResource: "university", ofType: "csv")
//
//        do {
//
//            let contents = try String(contentsOfFile: filepath!)
//
//            let columns:[String] = contents.components(separatedBy: "\n")
//
//            let count = columns.count - 1
//
//            for index in 0...count {
//                var datas:[String] = columns[index].components(separatedBy: ",")
//
//                if index == count {
//                    break
//                } else {
//
//
//                    var uni = University(id: Int(datas[0])!, name: datas[1], shortName: datas[2], state: datas[3])
//
//                    let count = datas.count - 1
//
//
//                    for index2 in 4...count {
//                        courses.append(datas[index2])
//                    }
//
//                    uni.courses = courses
//
//                    universities.append(uni)
//
//                    switch uni.state {
//                    case "ACT":
//                        ACT.append(uni)
//                        break
//                    case "NATIONAL":
//                        NATIONAL.append(uni)
//                        break
//                    case "NSW":
//                        NSW.append(uni)
//                        break
//                    case "NT":
//                        NT.append(uni)
//                        break
//                    case "QLD":
//                        QLD.append(uni)
//                        break
//                    case "WA":
//                        WA.append(uni)
//                        break
//                    case "TAS":
//                        TAS.append(uni)
//                        break
//                    case "VIC":
//                        VIC.append(uni)
//                        break
//                    case "SA":
//                        SA.append(uni)
//                        break
//                    default:
//                        break
//                    }
//                    courses.removeAll()
//                }
//
//            }
//
//
//            states.append(ACT)
//            states.append(NATIONAL)
//            states.append(NSW)
//            states.append(NT)
//            states.append(QLD)
//            states.append(SA)
//            states.append(TAS)
//            states.append(VIC)
//            states.append(WA)
//
//        } catch let err as NSError {
//            print(err.debugDescription)
//
//        }
//
//        DataService.ds.uniArray = universities
        
        DataService.ds.REF_University_Info.observeSingleEvent(of: .value, with: { (snapshot) in

            if let snapshots = snapshot.children.allObjects as? [DataSnapshot] {
                for snap in snapshots {

                    if let uniDict = snap.value as? Dictionary<String, AnyObject> {


                        let uniName = snap.key
                        let uni = University(keyName: uniName, uni: uniDict)

                        self.universities.append(uni)

                        switch uni.state {
                            case "ACT":
                                self.ACT.append(uni)
                                break
                            case "NATIONAL":
                                self.NATIONAL.append(uni)
                                break
                            case "NSW":
                                self.NSW.append(uni)
                                break
                            case "NT":
                                self.NT.append(uni)
                                break
                            case "QLD":
                                self.QLD.append(uni)
                                break
                            case "WA":
                                self.WA.append(uni)
                                break
                            case "TAS":
                                self.TAS.append(uni)
                                break
                            case "VIC":
                                self.VIC.append(uni)
                                break
                            case "SA":
                                self.SA.append(uni)
                                break
                            default:
                                break
                        }

                    }
                }

                self.states.append(self.ACT)
                self.states.append(self.NATIONAL)
                self.states.append(self.NSW)
                self.states.append(self.NT)
                self.states.append(self.QLD)
                self.states.append(self.SA)
                self.states.append(self.TAS)
                self.states.append(self.VIC)
                self.states.append(self.WA)

                
                self.tableView.reloadData()
                UIView.animate(views: self.tableView.visibleCells, animations: self.animations, completion: {
                })
                SVProgressHUD.dismiss()
                DataService.ds.uniArray = self.universities


            }

        })
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let destinationVC = segue.destination as? FacultyVC {
            destinationVC.uni = self.selectedUni
            destinationVC.name = self.name
            destinationVC.email = self.email
            destinationVC.password = self.password
            destinationVC.gender = self.gender
            destinationVC.provider = self.provider
            destinationVC.uid = self.uid
            
        }
    }
    
    func showAlert() {
        
        // Prepare the popup assets
        let title = "University Selection"
        let message = "You can only see the posts from your university"
        
        let popup = PopupDialog(title: title, message: message, image: nil)
        
        let buttonOne = CancelButton(title: "Cancel") {
//            self.dismiss(animated: true, completion: nil)
        }
        
        let buttonTwo = DefaultButton(title: "Continue", dismissOnTap: true) {
            self.performSegue(withIdentifier: "FacultyVC", sender: nil)
        }
        
        popup.addButtons([buttonOne,buttonTwo])
        popup.buttonAlignment = .horizontal
        
        // Present dialog
        self.present(popup, animated: true, completion: nil)
        
//
//        alertController = UIAlertController(title: "Your University", message: "You can only see the posts from your university", preferredStyle: .alert)
//
//        alertController.addAction(UIAlertAction(title: "Cancel", style: .cancel) { (action:UIAlertAction!) in
//
//        })
//
//
//        alertController.addAction(UIAlertAction(title: "Continue", style: .default) { (action:UIAlertAction!) in
//
//            self.performSegue(withIdentifier: "FacultyVC", sender: nil)
//
//        })
//
//        self.present(alertController, animated: true, completion: nil)
    }

  

}
