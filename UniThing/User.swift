//
//  User.swift
//  UniThing
//
//  Created by Xavier TanXY on 19/12/16.
//  Copyright © 2016 Xavier TanXY. All rights reserved.
//

import Foundation
import Firebase
import FirebaseDatabase

class User: NSObject {

//    private var _userName: String?
//    private var userKey: String?
//   
//    var course: String?
//    var email: String?
//    var gender: String?
//    var name: String?
//    var provider: String?
//    var university: String?
//    var year: String?

    private var _userName: String?
    var userKey: String?
    private var _course: String?
    private var _email: String?
    private var _gender: String?
    private var _name: String?
    private var _provider: String?
    private var _university: String?
    private var _universityShort: String?
    private var _year: String?
    
    private var _genderCount: Int?
    private var _uniCount: Int?
    private var _courseCount: Int?
    private var _profilePicUrl: String?
    private var _notiCount: Int?
    private var _creationDate: NSNumber?
    private var _msgCount: [String: Bool]!
    
    private var _following: [String: Bool]!
    private var _follower: [String: Bool]!

    private var _userRef: DatabaseReference!
    
    var userName: String {
        return _userName!
    }
//    
//    var userKey: String {
//        return _userKey!
//    }
    
    var course: String {
        
        get {
           return _course!
        }
        
        set(newCourse) {
            _course = newCourse
        }
    }
    
    var email: String {
        return _email!
    }
    
    var gender: String {
        get {
            return _gender!
        }
        
        set(newGender) {
            _gender = newGender
        }
        
    }
    
    var name: String {
        get {
            return _name!
        }
        
        set(newName) {
            _name = newName
        }
        
    }
    
    var provider: String {
        return _provider!
    }
    
    var university: String {
        get {
            return _university!
        }
        
        set(newUni) {
            _university = newUni
        }
        
    }
    
    var universityShort: String {
        get {
           return _universityShort!
        }
        
        set(newUniShort) {
            _universityShort = newUniShort
        }
        
    }
    
    var year: String {
        get {
            return _year!
        }
        
        set(newYear) {
            _year = newYear
        }
        
    }
    
    var genderCount: Int {
        get {
            return _genderCount!
        }
        
        set(newCount) {
            _genderCount = newCount
        }
        
    }
    
    var uniCount: Int {
        get {
            return _uniCount!
        }
        
        set(newCount) {
            _uniCount = newCount
        }
        
    }
    
    var courseCount: Int {
        get {
            return _courseCount!
        }
        
        set(newCount) {
            _courseCount = newCount
        }
        
    }
    
    var profilePicUrl: String {
        get {
            return _profilePicUrl!
        }
        
        set(newUrl) {
            _profilePicUrl = newUrl
        }
    }
    
    
    
    
    var userRef: DatabaseReference {
        return _userRef
    }
    
    var creationDate: NSNumber {
        return _creationDate!
    }
    
    var notiCount: Int {
        get {
            return _notiCount!
        }
        
        set(newCount) {
            _notiCount = newCount
        }
        
    }
    
    var msgCount: [String: Bool] {
        return _msgCount
    }
    
    var following: [String: Bool] {
        return _following
    }
    
    var follower: [String: Bool] {
        return _follower
    }
    
    override init() {
        
    }
    
    
    init(userKey: String, userData: Dictionary<String, AnyObject>) {
        

        self.userKey = userKey
        
        if let userName = userData["userName"] as? String {
            self._userName = userName
        }
        
        if let name = userData["name"] as? String {
            self._name = name
        }
        
        if let course = userData["course"] as? String {
            self._course = course
        }
        
        if let email = userData["email"] as? String {
            self._email = email
        }
        
        if let gender = userData["gender"] as? String {
            self._gender = gender
        }
        
        if let provider = userData["provider"] as? String {
            self._provider = provider
        }
        
        if let university = userData["university"] as? String {
            self._university = university
        }
        
        if let universityS = userData["universityShort"] as? String {
            self._universityShort = universityS
        }
        
        if let year = userData["year"] as? String {
            self._year = year
        }
        
        if let genderCount = userData["genderCount"] as? String {
            self._genderCount = Int(genderCount)
        }
        
        if let uniCount = userData["uniCount"] as? String {
            self._uniCount = Int(uniCount)
        }
        
        if let courseCount = userData["courseCount"] as? Int {
            self._courseCount = courseCount
        }
        
        if let profilePicUrl = userData["profilePicUrl"] as? String {
            self._profilePicUrl = profilePicUrl
        }
        
        if let notiCount = userData["notiCount"] as? Int {
            self._notiCount = notiCount
        }
        
        if let date = userData["creationDate"] as? NSNumber {
            self._creationDate = date
        }
        
        if let msgCount = userData["msgCount"] as? [String: Bool] {
            self._msgCount = msgCount
        }
        
        if let follower = userData["follower"] as? [String: Bool] {
            self._follower = follower
        }
        
        if let following = userData["following"] as? [String: Bool] {
            self._following = following
        }

        
        
      //  _userRef = DataService.ds.REF_USERS.child(_userKey!)
    }
    
    func adjustMsgCount(addCount: Bool,fromId: String) {
        
        let ref2 = DataService.ds.REF_USERS.child(self.userKey!)
        
        ref2.runTransactionBlock({ (currentData) -> TransactionResult in
            
            if var userDict = currentData.value as? Dictionary<String, AnyObject> {
                
                var msgCount = userDict["msgCount"] as? [String: Bool]
                
                if addCount {
                    msgCount?[fromId] = true
//                    self.addUser(uid: uid)
                    
                } else {
                    // userLike.removeValue(forKey: uid)
                    msgCount?.removeValue(forKey: fromId)
//                    self.removeUser(uid: uid)
                }
                
                userDict["msgCount"] = msgCount as AnyObject
                

                
                currentData.value = userDict
                print("scueess")
                return TransactionResult.success(withValue: currentData)
            }
            
            return TransactionResult.success(withValue: currentData)
        }) { (error, commited, snap) in
            
        }
    }
    
    func adjustFollower(userID: String, add: Bool) {
        
        var updateObj = [String:AnyObject]()
        let ref = DB_BASE
        
        if add {
           self._follower[userID] = true
            updateObj["/users/\(self.userKey!)/follower/\(userID)"] = true as AnyObject
        } else {
            self._follower.removeValue(forKey: userID)
            updateObj["/users/\(self.userKey!)/follower/\(userID)"] = NSNull() as AnyObject
        }
        
        ref.updateChildValues(updateObj)

        
    }
    
    func adjustFollowing(userID: String, add: Bool) {
        var updateObj = [String:AnyObject]()
        let ref = DB_BASE
        
        if add {
            self._following[userID] = true
            updateObj["/users/\(self.userKey!)/following/\(userID)"] = true as AnyObject
        } else {
            self._following.removeValue(forKey: userID)
            updateObj["/users/\(self.userKey!)/following/\(userID)"] = NSNull() as AnyObject
        }
        
        ref.updateChildValues(updateObj)
    }
    
    func hasFollower(userID:String) -> Bool {
        if self._follower[userID] != nil {
            return true
        } else {
            return false
        }
    }
    
    func isFollowing(userID: String) -> Bool {
        if self._following[userID] != nil {
            return true
        } else {
            return false
        }
    }
}
