//
//  EmptyNotificationView.swift
//  UniThing
//
//  Created by Xavier TanXY on 6/8/17.
//  Copyright © 2017 Xavier TanXY. All rights reserved.
//

import UIKit

class EmptyNotificationView: UIView {

    class func instanceFromNib() -> UIView {
        return UINib(nibName: "EmptyNotificationView", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! UIView
    }

}
