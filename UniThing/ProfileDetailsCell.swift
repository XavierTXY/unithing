//
//  ProfileDetailsCell.swift
//  UniThing
//
//  Created by Xavier TanXY on 28/6/17.
//  Copyright © 2017 Xavier TanXY. All rights reserved.
//

import UIKit

class ProfileDetailsCell: UITableViewCell {

    @IBOutlet weak var label: UILabel!
    @IBOutlet weak var detailLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func configureCell(detail: String, user: User) {
        
        label.text = detail
        var userD = String()
        
        switch detail {
        case "Name":
            userD = user.name
            break
        case "Gender":
            userD = user.gender
            break
        case "Entry Year":
            userD = user.year
            break
        case "University":
            userD = user.universityShort
            break
        case "Course":
            userD = user.course
            break
            
        default: break
            
        }
        
        detailLabel.text = userD
    }

}
