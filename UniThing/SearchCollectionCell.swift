//
//  SearchCollectionCell.swift
//  UniThing
//
//  Created by XavierTanXY on 1/4/18.
//  Copyright © 2018 Xavier TanXY. All rights reserved.
//

import UIKit
import Foundation

class SearchCollectionCell: UICollectionViewCell {
    
    @IBOutlet weak var uniName: UILabel!
    @IBOutlet weak var numPosts: UILabel!
    @IBOutlet weak var roundView: RoundView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func configureCell(hash: Hash, color: UIColor) {
        uniName.text = "#\(hash.hashName)"
        numPosts.text = "\(hash.hashCount) posts"
        roundView.backgroundColor = color
    }
    

    
}
