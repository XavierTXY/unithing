//
//  PostButton.swift
//  UniThing
//
//  Created by XavierTanXY on 31/3/18.
//  Copyright © 2018 Xavier TanXY. All rights reserved.
//

import Foundation
import UIKit

class PostButton: UIView {
    
    var addVC: AddConfessionVC!
    
    @IBOutlet weak var sendBtn: UIButton!
    @IBAction func clickSend(_ sender: UIButton) {
        
        addVC.anonymousTapped()
    }
}
