//
//  Constant.swift
//  UniThing
//
//  Created by Xavier TanXY on 24/11/16.
//  Copyright © 2016 Xavier TanXY. All rights reserved.
//

import Foundation
import UIKit

let SHADOW_COLOR: CGFloat = 157.0 / 255.0

let SHADOW_GRAY: CGFloat = 120.0 / 255.0

let INVALID_EMAIL = 17999

let INVALID_PASSWORD = 17026

let WRONG_PASSWORD = 17009

let USER_NOT_FOUND = 17011

let USER_EXISTED = 17007

let EMAIL_BADLY_FORMATTED = 17008

let ADMIN_EMAIL = "xavier-889@hotmail.com"

let DISCUSSION = "DISCUSSION"

let FOLLOWING = "following"

let FOLLOWER = "follower"

let LIKER = "userLike"

let KEY_UID = "uid"

let NO_PIC = "NoPicture"

let FIREBASE = "Firebase"

let FB = "facebook.com"

let MALE = "Male"

let FEMALE = "Female"

let DELETE_MODE_AFTER_POSTED = 0

let DELETE_MODE_BEFORE_POSTED = 1

let MAIN_COLOR = UIColor(red: 33/255, green: 150/255, blue: 243/255, alpha: 1)

let BORDER_COLOR_CGCOLOR = UIColor(red: 226/255, green: 228/255, blue: 232/255, alpha: 1).cgColor

let BORDER_COLOR = UIColor(red: 226/255, green: 228/255, blue: 232/255, alpha: 1)

let LIMIT_CHAR_NAME = 18

let DEFAULT_COUNT = "3"

let PURPLE = UIColor(red: 18/255, green: 102/255, blue: 215/255, alpha: 1)

let PURPLE2 = UIColor(red: 0/255, green: 105/255, blue: 192/255, alpha: 1)

let NAV_BAR_TITLE_COLOR = [NSForegroundColorAttributeName: UIColor(red: 73/255, green: 73/255, blue: 81/255, alpha: 1)]

