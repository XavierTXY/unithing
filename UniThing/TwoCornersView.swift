//
//  TwoCornersView.swift
//  UniThing
//
//  Created by XavierTanXY on 14/5/18.
//  Copyright © 2018 Xavier TanXY. All rights reserved.
//

import Foundation
import UIKit

class TwoCornersView: UIView {
    
    override func awakeFromNib() {
        super.awakeFromNib()

        
        
        if #available(iOS 11.0, *) {
            self.clipsToBounds = true
            self.layer.cornerRadius = 20
            self.layer.maskedCorners = [.layerMaxXMaxYCorner, .layerMinXMaxYCorner]
        } else {
            self.clipsToBounds = true
            let path = UIBezierPath(roundedRect: self.bounds,
                                    byRoundingCorners: [.bottomRight, .bottomLeft],
                                    cornerRadii: CGSize(width: 20, height: 20))

            let maskLayer = CAShapeLayer()

            maskLayer.path = path.cgPath
            self.layer.mask = maskLayer

        }

        
        
    }
}
