//
//  AppDelegate.swift
//  UniThing
//
//  Created by Xavier TanXY on 23/11/16.
//  Copyright © 2016 Xavier TanXY. All rights reserved.
//

import UIKit
import Firebase
import FirebaseInstanceID
import FirebaseMessaging
import SVProgressHUD
import FBSDKLoginKit
import FBSDKCoreKit
import UserNotifications
import PopupDialog
import SlideMenuControllerSwift

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, UNUserNotificationCenterDelegate, MessagingDelegate {

    var window: UIWindow?
    var app: UIApplication!
//    var reachability:Reachability!
    
    override init() {
//        FIRApp.configure()

       // FIRDatabase.database().persistenceEnabled = true
        
    }
    
    func createMenuView() {
        
        // create viewController code...
        var storyboard = UIStoryboard(name: "HomeSB", bundle: nil)
        let mainViewController = storyboard.instantiateViewController(withIdentifier: "TabBarVC") as! UITabBarController
        
//        let mainViewController = storyboard.instantiateViewController(withIdentifier: "MainViewController") as! MainViewController
        let leftViewController = storyboard.instantiateViewController(withIdentifier: "LeftMenuVC") as! LeftMenuVC
//        let rightViewController = storyboard.instantiateViewController(withIdentifier: "RightViewController") as! RightViewController
        
//        let nvc: UINavigationController = UINavigationController(rootViewController: mainViewController)
        
//        UINavigationBar.appearance().tintColor = UIColor(hex: "689F38")
        
        leftViewController.mainViewController = mainViewController
        
        
        
        let slideMenuController = SlideMenuController(mainViewController: mainViewController, leftMenuViewController: leftViewController)
//        slideMenuController.automaticallyAdjustsScrollViewInsets = true
//        slideMenuController.delegate = mainViewController as! SlideMenuControllerDelegate
//        self.window?.backgroundColor = UIColor(red: 236.0, green: 238.0, blue: 241.0, alpha: 1.0)
        self.window?.rootViewController = slideMenuController
        self.window?.makeKeyAndVisible()
    }

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        FirebaseApp.configure()
        SVProgressHUD.setBackgroundColor(UIColor(red: 255/255, green: 255/255, blue: 255/255, alpha: 1.0))
        app = application
 
        if let currentVersion = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String {
            DataService.ds.OS = currentVersion
            
        }
        UITabBarItem.appearance().badgeColor = UIColor(red: 18/255, green: 102/255, blue: 215/255, alpha: 1.0)
    

        UINavigationBar.appearance().shadowImage = UIImage()
//
//        if #available(iOS 10.0, *) {
//            // For iOS 10 display notification (sent via APNS)
//            UNUserNotificationCenter.current().delegate = self
//
//            let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
//            UNUserNotificationCenter.current().requestAuthorization(
//                options: authOptions,
//                completionHandler: {_, _ in })
//
//        } else {
//            let settings: UIUserNotificationSettings =
//                UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
//            application.registerUserNotificationSettings(settings)
//        }
////        let settings: UIUserNotificationSettings =
////            UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
////        application.registerUserNotificationSettings(settings)
//
//
        application.registerForRemoteNotifications()
        
        FBSDKApplicationDelegate.sharedInstance().application(application, didFinishLaunchingWithOptions: launchOptions)
        
        let defaults = UserDefaults.standard
        var uid = defaults.object(forKey: "uid") as? String
        
        if uid == nil {
            var storyboard = UIStoryboard(name: "Main", bundle: nil)
            let controller = storyboard.instantiateViewController(withIdentifier: "NavVC") as! UINavigationController
            self.window?.rootViewController = controller
            print("yes")
            
            
        } else {
            
            
//            var storyboard = UIStoryboard(name: "HomeSB", bundle: nil)
//            let controller = storyboard.instantiateViewController(withIdentifier: "TabBarVC") as! UITabBarController
//            self.window?.rootViewController = controller
//
            createMenuView()
        }

        

        
        
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.tokenRefreshNotification(notification:)), name: NSNotification.Name.InstanceIDTokenRefresh, object: nil)
  
        return true
    }
    
    func checkUpdate(completion: @escaping (Bool)->Void) {
        
        DataService.ds.REF_Version.observeSingleEvent(of: .value, with: { (snapshot) in
            print("lala \(snapshot)")
            if let latestVersion = snapshot.value as? String {
                print("VERSOION \(latestVersion)")
                
                if let currentVersion = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String {
                    DataService.ds.OS = currentVersion
                    if currentVersion != latestVersion {
                        
                        DataService.ds.REF_Update.observeSingleEvent(of: .value, with: { (snapshot) in
                           
                            if let update = snapshot.value as? Bool{
                                
                                if update {
                                    completion(true)
                                } else {
                                    completion(false)
                                }
                                
                            }
                            
                        })
                    } else {
                        completion(false)
                    }

                }
                
            }
            
        })
        
    }
    
//
//    func enableNotification() {
//
//        if #available(iOS 10.0, *) {
//            // For iOS 10 display notification (sent via APNS)
//            UNUserNotificationCenter.current().delegate = self
//
//            let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
//            UNUserNotificationCenter.current().requestAuthorization(
//                options: authOptions,
//                completionHandler: {_, _ in })
//
//        } else {
//            let settings: UIUserNotificationSettings =
//                UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
//            app.registerUserNotificationSettings(settings)
//        }
//    }

    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        //print("Handle push from background or closed" );
        //print("%@", response.notification.request.content.userInfo);
        
        print("tapped on notification to vc")
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "SomeNotification"), object: nil)
        
        if self.window!.rootViewController as? UITabBarController != nil {
            var tababarController = self.window!.rootViewController as! UITabBarController
            tababarController.selectedIndex = 2
        }


//        var notiVC = self.window?.rootViewController?.childViewControllers[2]
//        self.window?.rootViewController?.present(notiVC!, animated: true, completion: nil)
        completionHandler()
    }
    
    func application(received remoteMessage: MessagingRemoteMessage) {
        print(remoteMessage.appData)
    }
    
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.

        Messaging.messaging().disconnect()
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
        checkUpdate { (update) -> Void in
            
            if update {
                
                let popup = PopupDialog(title: "New Version", message: "We have made the app even better! Please go to App Store and update.", image: nil)
                
                
                let buttonTwo = DefaultButton(title: "Update", dismissOnTap: true) {
                    if let reviewURL = URL(string: "https://itunes.apple.com/us/app/unithing/id1302177232?ls=1&mt=8"), UIApplication.shared.canOpenURL(reviewURL) {
                        if #available(iOS 10.0, *) {
                            UIApplication.shared.open(reviewURL, options: [:], completionHandler: nil)
                        } else {
                            UIApplication.shared.openURL(reviewURL)
                        }
                    }
                }
                
                popup.addButtons([buttonTwo])
                popup.buttonAlignment = .horizontal
                
                // Present dialog
                self.window?.rootViewController?.present(popup, animated: true, completion: nil)
            }
        }
        connectToFCM()
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.

    }
    
//
//    func application(_ application: UIApplication, open url: URL, sourceApplication: String?, annotation: Any) -> Bool {
//
//        return FBSDKApplicationDelegate.sharedInstance().application(application, open: url, sourceApplication: sourceApplication, annotation: annotation)
//
//
//    }
    
    func application(_ app: UIApplication, open url: URL, options: [UIApplicationOpenURLOptionsKey : Any] = [:]) -> Bool {
        return FBSDKApplicationDelegate.sharedInstance().application(app, open: url, options: options)
    }
    
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any], fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {

        if let messageID = userInfo["gcmMessageIDKey"] {
            print("Message ID: \(messageID)")
        }
        
        // Print full message.
        print(userInfo)
        
        completionHandler(UIBackgroundFetchResult.newData)
        
    }
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        print("getting instance id token")
        if let refreshedToken = InstanceID.instanceID().token() {
            print("InstanceID token: \(refreshedToken)")
            DataService.ds.PUSH_TOKEN = refreshedToken
            
            let defaults = UserDefaults.standard
            var uid = defaults.object(forKey: "uid") as? String
            if uid != nil {
                DataService.ds.addInstanceID()
            }
        }
        
        //f2ZbojVi_0Y:APA91bHR5e2p_h5TQGM89TAxwstiKEVraDeiaVXxyE15gii5kfif-220C2tCTIFGzVK-TzqWB7A11DXramAuknRQGP5DMCA-_Z-ai3XnVU0BSGcmJYaSoJbSWTBJbgrqvSwiSZWv84pM

    }
    
    func tokenRefreshNotification(notification: NSNotification) {
//        let refreshedToken = FIRInstanceID.instanceID().token()
//        print("instance id token \(refreshedToken)")
        
        if let refreshedToken = InstanceID.instanceID().token() {
            print("InstanceID token2: \(refreshedToken)")
            DataService.ds.PUSH_TOKEN = refreshedToken
            

            
        }

        connectToFCM()
      
    }
    
    func connectToFCM() {
        
        Messaging.messaging().connect { (error) in
            if error != nil {
                print("unable ro connect \(error?.localizedDescription)")
            } else {
                print("connected")
            }
        }
    }


}
extension UIApplication {
    var statusBarView: UIView? {
        return value(forKey: "statusBar") as? UIView
    }
}
extension AppDelegate {
    static var shared: AppDelegate {
        return UIApplication.shared.delegate as! AppDelegate
    }
    var rootViewController: UIViewController {
        
        set(vc) {
            window!.rootViewController = vc
        }
        
        get {
            return window!.rootViewController! 
        }
        
    }
    
    var win: UIWindow {
        get {
            return self.window!
        }
    }
}


