//
//  TopConfessionPicCollectionCell.swift
//  UniThing
//
//  Created by XavierTanXY on 2/4/18.
//  Copyright © 2018 Xavier TanXY. All rights reserved.
//

import UIKit

class TopConfessionPicCollectionCell: UICollectionViewCell {
    @IBOutlet weak var confessionPic: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        confessionPic.layer.cornerRadius = 5.0
    }
    
    func configureCell(confession: Confession) {
        
    }
}
