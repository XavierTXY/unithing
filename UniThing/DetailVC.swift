//
//  DetailVC.swift
//  UniThing
//
//  Created by Xavier TanXY on 24/11/16.
//  Copyright © 2016 Xavier TanXY. All rights reserved.
//

import UIKit

class DetailVC: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource, UITextFieldDelegate  {

    
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var nameTxtField: UITextField!
    
    var genderPicker: UIPickerView!
    var pickerData: [String] = [String]()
    
    @IBOutlet weak var genderTextField: UITextField!
    @IBOutlet weak var nextBtn: UIButton!
    var toolBar: UIToolbar!
    
    var email: String?
    var fullName: String?
    var password: String?
    //var gender: String?
    var provider: String?
    
    var selectedData: String?

    override func viewDidLoad() {
        super.viewDidLoad()

        let leftBarItem = UIBarButtonItem(image: #imageLiteral(resourceName: "Back Chevron"), style: .plain, target: self, action: "backTapped")
       // let rightBarItem = UIBarButtonItem(image: #imageLiteral(resourceName: "next"), style: .plain, target: self, action: "nextTapped")

        leftBarItem.tintColor = UIColor.white
        //rightBarItem.tintColor = UIColor.white
        self.navigationItem.setLeftBarButton(leftBarItem, animated: true)
        self.navigationItem.setRightBarButton(nil, animated: true)
        
        self.nextBtn.isEnabled = false
        
        genderPicker = UIPickerView()
        genderPicker.delegate = self
        genderPicker.dataSource = self
        //gender = "None"
        selectedData = "Male"
        
        genderTextField.delegate = self
        //genderTextField.addTarget(self, action: #selector(respondsToTf), for: .touchDown)
        nameTxtField.delegate = self
        
        
        nameTxtField.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        
       
        self.loadPicker()
        self.loadPickerData()
        nameTxtField.becomeFirstResponder()
        
        self.navigationItem.title = "More About You"
        self.navigationController?.navigationBar.titleTextAttributes = [ NSFontAttributeName: UIFont(name: "HelveticaNeue-Bold", size: 16)!]
        self.navigationController?.navigationBar.titleTextAttributes = [ NSForegroundColorAttributeName: UIColor.white]
        if #available(iOS 11.0, *) {
            
            self.navigationController?.navigationBar.prefersLargeTitles = true
            self.navigationController?.navigationBar.largeTitleTextAttributes = [ NSForegroundColorAttributeName: UIColor.white]
        } else {
            // Fallback on earlier versions
        }
    }
    

    override func viewDidAppear(_ animated: Bool) {
       UIView.animate(withDuration: 0.5, delay: 0.2, options: [], animations: { self.mainView.alpha = 1.0 }, completion: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
       mainView.alpha = 0.0
    }
    
    func textFieldDidChange(_ textField: UITextField) {
        
        if (nameTxtField.text?.characters.count)! > 0 {
            self.nextBtn.isEnabled = true
            self.nextBtn.setBackgroundImage(#imageLiteral(resourceName: "Log In Btn"), for: .normal)
            
        } else {
            self.nextBtn.isEnabled = false
            self.nextBtn.setBackgroundImage(#imageLiteral(resourceName: "LoginBtnGray"), for: .normal)
        }
    }

    func backTapped() {
        self.navigationController?.popViewController(animated: false)
    }
    
//    func respondsToTf()  {
//        self.genderTextField.becomeFirstResponder()
//        self.genderPicker.becomeFirstResponder()
//        
//        print("touch")
//    }
    
//    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
//        var retVal = true
//        if textField == genderTextField {
//            self.genderTextField.becomeFirstResponder()
//            self.genderPicker.becomeFirstResponder()
//            retVal = false
//        }
//        
//        return retVal
//    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        // textField.resignFirstResponder()
        if textField == nameTxtField {
            textField.resignFirstResponder()
            self.genderTextField.becomeFirstResponder()
        }
        
        return true
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }

    func resignAllResponder() {
        nameTxtField.resignFirstResponder()
        genderTextField.resignFirstResponder()
    }
    
    func loadPicker() {
        toolBar = UIToolbar()
        toolBar.barStyle = UIBarStyle.default
        toolBar.sizeToFit()
        
        let doneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.plain, target: self, action: Selector(("cancelPicker")))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: UIBarButtonItemStyle.plain, target: self, action: Selector(("cancelPicker")))
        
        toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        
        genderPicker.backgroundColor = UIColor.white
        genderTextField.inputView = self.genderPicker
        genderTextField.inputAccessoryView = toolBar
    }
    
    func cancelPicker() {
        genderTextField.resignFirstResponder()
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        if genderTextField.isFirstResponder {
            genderPicker?.selectRow(0, inComponent: 0, animated: true)
            selectedData = pickerData[0]
            genderTextField.placeholder = selectedData
        }

    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return pickerData.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return pickerData[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        nameTxtField.resignFirstResponder()
        selectedData = pickerData[row]
        genderTextField.placeholder = selectedData
    }
    
    func loadPickerData() {
        pickerData = ["Male", "Female"]
    }
    
    func removeSpecialCharsFromString(str: String) -> String {
        let chars = Set("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLKMNOPQRSTUVWXYZ1234567890 ".characters)
        
        return String(str.characters.filter { chars.contains($0) })
    }
    
    func checkValidFullName() -> Bool {
        let name = nameTxtField.text
        let filterdName = removeSpecialCharsFromString(str: name!)
        
        if filterdName == name {
            return true
        } else {
            return false
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let currentCharacterCount = textField.text?.characters.count ?? 0
        if (range.length + range.location > currentCharacterCount){
            
            return false
        }
        let newLength = currentCharacterCount + string.characters.count - range.length
        return newLength <= LIMIT_CHAR_NAME
    }
    
    @IBAction func nextTapped(_ sender: Any) {
        Interaction().disableInteraction(msg: "Loading")
        self.resignAllResponder()
        
        if !checkValidFullName() || (nameTxtField.text?.containsEmoji)! {
            Interaction().enableInteraction()
            ErrorAlert().show(title: "Invalid Name", msg: "Name can't contain emoji or symbol", object: self)
        } else if nameTxtField.text?.lowercased() == "student" {
            Interaction().enableInteraction()
            ErrorAlert().show(title: "Invalid Name", msg: "Please choose another name", object: self)
        } else if let name = nameTxtField.text, (name.characters.count > 0 && (selectedData == "Male" || selectedData == "Female") ) {
            
            
            self.fullName = name
            performSegue(withIdentifier: "UserNameVC", sender: nil)
            Interaction().enableInteraction()
            
        } else if (nameTxtField.text?.characters.count)! <= 0  && selectedData == "None" {
            Interaction().enableInteraction()
            ErrorAlert().show(title: "Invalid Detail", msg: "Please enter your details", object: self)
        } else if (nameTxtField.text?.characters.count)! <= 0 {
            Interaction().enableInteraction()
            ErrorAlert().show(title: "Invalid Name", msg: "Please enter a valid name", object: self)
        } else if selectedData != "Male" || selectedData != "Female" {
            Interaction().enableInteraction()
            ErrorAlert().show(title: "Invalid Gender", msg: "Please choose your gender", object: self)
        }
 
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if let destinationVC = segue.destination as? UniVC {
            destinationVC.email = self.email
            destinationVC.name = self.fullName
            destinationVC.password = password
            destinationVC.gender = self.selectedData
            destinationVC.provider = self.provider
            
        }
    }
   



}
