//
//  University.swift
//  UniThing
//
//  Created by Xavier TanXY on 30/11/16.
//  Copyright © 2016 Xavier TanXY. All rights reserved.
//

import Foundation

class University {
    private var _name: String!
    private var _shortName: String!
    private var _state: String!
    private var _id: Int!
    private var _courses: [String]!
    
    var name: String {
        
        set(iname) {
            if iname != "" {
                _name = iname
            }
        }
        
        get {
           return _name
        }

        
    }
    
    var shortName: String {
        
        return _shortName
    }
    
    var id: Int {
        return _id
    }
    
    var state: String {
        return _state
    }
    
    var courses: [String] {
        
        get {
            return _courses
        }
        
        set {
            _courses = newValue
        }
        
        
    }
    
    init() {
        
    }
    
    init(id: Int, name: String, shortName: String, state: String) {
        self._name = name
        self._id = id
        self._shortName = shortName
        self._state = state
        
    }
    
    init(keyName: String, uni: Dictionary<String, AnyObject>) {
        
        self._name = keyName
        
        if let sN = uni["shortName"] as? String{
            self._shortName = sN
        }
        
        if let s = uni["state"] as? String{
            self._state = s
        }
//        self._name = name
//        self._id = id
//        self._shortName = shortName
//        self._state = state
        
    }
    
}
