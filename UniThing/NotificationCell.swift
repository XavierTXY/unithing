//
//  NotificationCell.swift
//  UniThing
//
//  Created by Xavier TanXY on 5/8/17.
//  Copyright © 2017 Xavier TanXY. All rights reserved.
//

import UIKit

class NotificationCell: UITableViewCell {

    
    @IBOutlet weak var profilePic: UIImageView!
    @IBOutlet weak var notificationLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var userNameLabel: UILabel!
    @IBOutlet weak var outerView: UIView!
    @IBOutlet weak var bgView: RoundView!
    
    var initialLbl: UILabel!
    @IBOutlet weak var notificationImage: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        profilePic.contentMode = .scaleAspectFill
        profilePic.layer.cornerRadius = profilePic.frame.width / 2
        profilePic.clipsToBounds = true
        
        outerView.clipsToBounds = false
        outerView.layer.shadowColor = UIColor.black.cgColor
        outerView.layer.shadowOpacity = 0.2
        outerView.layer.shadowOffset = CGSize(width: 0, height: 2)
        
        outerView.layer.cornerRadius = outerView.frame.height / 2
        outerView.layer.shadowRadius = 3
//        outerView.layer.shadowPath = UIBezierPath(roundedRect: outerView.bounds, cornerRadius: outerView.frame.height / 2).cgPath
        
        
        
        initialLbl = UILabel()
        initialLbl.frame.size = CGSize(width: 50, height: 50)
        initialLbl.font = UIFont(name: "HelveticaNeue-Medium", size: 20.0)
        initialLbl.textColor = UIColor.white
        initialLbl.textAlignment = NSTextAlignment.center
        initialLbl.center = profilePic.center
        profilePic.addSubview(initialLbl)
    }


    func configureCell(notification: Notification) {

        let seconds = notification.time.doubleValue
        let timesStampDate = NSDate(timeIntervalSince1970: seconds)
        var timeAgo:String =  DateHelper().timeAgoSinceDate(timesStampDate as Date, currentDate: NSDate() as Date, numericDates: true)
        var dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd, MMM"
        
        self.userNameLabel.text = notification.userName
        
        if notification.type == "like" {
            self.notificationLabel.text = "likes your confession."
            self.notificationImage.image = #imageLiteral(resourceName: "smallerLove")
        } else if notification.type == "comment" {
            self.notificationImage.image = #imageLiteral(resourceName: "smallerComment")
            self.notificationLabel.text = "commented on your confession."
        } else if notification.type == "reply" {
            self.notificationImage.image = #imageLiteral(resourceName: "smallerComment")
            self.notificationLabel.text = "replied to your comment: \"\(notification.replyToCommentContent)\""
        } else if notification.type == "commentLike" {
            self.notificationImage.image = #imageLiteral(resourceName: "smallerLove")
            self.notificationLabel.text = "likes your comment: \"\(notification.likedCommentContent)\""
        } else if notification.type == "follow" {
            self.notificationImage.image = #imageLiteral(resourceName: "smallerAddFriend")
            self.notificationLabel.text = "started following you."
        }
        
        if !notification.seen {
            self.bgView.backgroundColor = UIColor(red: 235/255, green: 237/255, blue: 244/255, alpha: 1.0)
        } else {
            self.bgView.backgroundColor = UIColor.white
        }
       
        self.timeLabel.text = timeAgo
    }

}
