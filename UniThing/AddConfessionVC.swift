//
//  AddConfessionVC.swift
//  UniThing
//
//  Created by Xavier TanXY on 6/12/16.
//  Copyright © 2016 Xavier TanXY. All rights reserved.
//

import UIKit
//import Lottie
import Firebase
import FirebaseDatabase
import SVProgressHUD
import PopupDialog
import Whisper
import RSSelectionMenu

protocol PassConfessionProtocol
{
    func sendConfessionToPreviousVC(confession: Confession)
}

class AddConfessionVC: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate, UITextViewDelegate {
    
//    lazy var inputContainerView: ChatInputContainerViewImage = {
//        
//        let chatInputContainerView = ChatInputContainerViewImage(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: 50))
//       // chatInputContainerView.comment3VC = self
//        return chatInputContainerView
//        
//        
//    }()
    
//    lazy var inputContainerView: InputContainerViewImage = {
//        
//        let chatInputContainerView = InputContainerViewImage(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: 50))
//        chatInputContainerView.addConfessionVC = self
//        return chatInputContainerView
//        
//        
//    }()
    @IBOutlet weak var catBtn: UIButton!
    
    var mainViewController: UITabBarController!
    
    var imagePick: UIImagePickerController!
    
   // @IBOutlet weak var textField: UITextField!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var inputTextView: UITextView!
    @IBOutlet weak var wordCounterLbl: UILabel!

    @IBOutlet weak var scrollView: UIScrollView!
    
    var uniName: String!
//    var faculty: String!
//    var gender: String?
    
    var user: User!
    
    var sendBtn: PostButton!
    var anonymousMode: Bool!
    var isAdmin: Bool!

    var delegate: PassConfessionProtocol?
    var categories = [String]()
    var selectedcategories = [String]()
    var selectedcategory: String!
    
    var hasImg = false
   // var aniView: LOTAnimationView?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        UIApplication.shared.statusBarStyle = .default
        
        
        categories = DataService.ds.categoryArray
        self.selectedcategory = "None"
        
//        DataService.ds.REF_USERS.child((Auth.auth().currentUser?.uid)!).observe(.value, with: { (snapshot) in
//
//            if let snapshots = snapshot.children.allObjects as? [DataSnapshot] {
//               // print(snapshot.childSnapshot(forPath: "university").value )
//                self.uniName = snapshot.childSnapshot(forPath: "university").value as! String!
//                self.faculty = snapshot.childSnapshot(forPath: "course").value as? String
//                self.gender = snapshot.childSnapshot(forPath: "gender").value as! String!
//
//            }
//        })
        
        imagePick = UIImagePickerController()
        imagePick.allowsEditing = false
        imagePick.delegate = self
        
        inputTextView.delegate = self
        inputTextView.text = "What's happening in your university?"
        inputTextView.textColor = UIColor.lightGray
        
    
//        if let animationView = LOTAnimationView(name: "data") {
//            animationView.frame = CGRect(x: 0, y: 0, width: 50, height: 50)
//            animationView.center = self.view.center
//            animationView.contentMode = .scaleAspectFill
//            animationView.loopAnimation = true
//            animationView.animationSpeed = 0.8
//            aniView = animationView
//            
//        }
        let leftBarItem = UIBarButtonItem(image: #imageLiteral(resourceName: "cancel"), style: .plain, target: self, action: "cancelTapped")
        self.navigationItem.setLeftBarButton(leftBarItem, animated: true)
        
//        let rightBarItem = UIBarButtonItem(image: #imageLiteral(resourceName: "send"), style: .plain, target: self, action: "addTapped")
        let rightBarItem = UIBarButtonItem(title: "Post", style: .done, target: self, action: "addTapped")
        self.navigationItem.setRightBarButton(rightBarItem, animated: true)
        //self.navigationItem.rightBarButtonItem?.tintColor = UIColor(red: 158/255, green: 158/255, blue: 158/255, alpha: 0.7)
        self.navigationItem.rightBarButtonItem?.isEnabled = false
        self.navigationItem.title = "New Post";
        

//        self.navigationController?.navigationBar.barTintColor = UIColor.white
        self.navigationController?.navigationBar.titleTextAttributes = [ NSFontAttributeName: UIFont(name: "HelveticaNeue-Bold", size: 16)!]
        self.navigationController?.navigationBar.layer.borderColor = UIColor(red: 226/255, green: 228/255, blue: 232/255, alpha: 1).cgColor
        self.navigationController?.navigationBar.tintColor = PURPLE
        self.navigationController?.navigationBar.titleTextAttributes = [ NSForegroundColorAttributeName: PURPLE]
        
        
        view.backgroundColor = UIColor.white
        
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(addPicTapped))
        
        imageView.isUserInteractionEnabled = true
        imageView.addGestureRecognizer(tapGestureRecognizer)
        
        anonymousMode = false
        addAccessoryView()
        self.inputTextView.becomeFirstResponder()
        

        showAsFormSheetWithSearch()
//        
        
     

    }
    
    @IBAction func catBtnTapped(_ sender: Any) {
        showAsFormSheetWithSearch()
    }
    
    func showAsFormSheetWithSearch() {
        
        // Show menu with datasource array - PresentationStyle = Formsheet & SearchBar
        
        let selectionMenu = RSSelectionMenu(dataSource: categories) { (cell, object, indexPath) in
            cell.textLabel?.text = object
        }
        
    
        
        // show selected items
        selectionMenu.setSelectedItems(items: selectedcategories) { (text, selected, selectedItems) in
            self.selectedcategories = selectedItems
            self.selectedcategory = text
            self.catBtn.setTitle("Category: \(text!)", for: .normal)
        }
        
        // show searchbar with placeholder text and barTintColor
        // Here you'll get search text - when user types in seachbar
        
//        selectionMenu.showSearchBar(withPlaceHolder: "Search Player", tintColor: UIColor.white.withAlphaComponent(0.3)) { (searchText) -> ([String]) in
//
//            // return filtered array based on any condition
//            // here let's return array where firstname starts with specified search text
//
//            return self.dataArray.filter({ $0.lowercased().hasPrefix(searchText.lowercased()) })
//        }
        
        // show as formsheet
        selectionMenu.show(style: .Formsheet, from: self)
    }
    
    public func addAccessoryView() {
        
        sendBtn = Bundle.main.loadNibNamed("PostButton", owner: self, options: nil)?.first as! PostButton
        sendBtn.addVC = self
        sendBtn.sendBtn.isUserInteractionEnabled = true
        inputTextView.inputAccessoryView = sendBtn
        
    }
    

    func textViewDidChange(_ textView: UITextView) {
        

        if textView.text.characters.count > 0 && !textView.text.trimmingCharacters(in: .whitespaces).isEmpty {
            self.navigationItem.rightBarButtonItem?.tintColor = UIColor(red: 33/255, green: 150/255, blue: 243/255, alpha: 1)
            self.navigationItem.rightBarButtonItem?.isEnabled = true
//            sendBtn.sendBtn.setBackgroundImage(#imageLiteral(resourceName: "postEnable"), for: .normal)
//            sendBtn.sendBtn.isUserInteractionEnabled = true
        } else {
            self.navigationItem.rightBarButtonItem?.tintColor = UIColor(red: 158/255, green: 158/255, blue: 158/255, alpha: 0.7)
            self.navigationItem.rightBarButtonItem?.isEnabled = false
//            sendBtn.sendBtn.setBackgroundImage(#imageLiteral(resourceName: "postDisable"), for: .normal)
//            sendBtn.sendBtn.isUserInteractionEnabled = false
        }
        
        
        wordCounterLbl.text = "\( 420 - textView.text.characters.count)"
        
    }
 
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.textColor == UIColor.lightGray {
            textView.text = nil
            textView.textColor = UIColor.black
            
            
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty {
            textView.text = "What's happening in your university?"
            textView.textColor = UIColor.lightGray
        }
    }

    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        print("touches2")
        textField.resignFirstResponder()
        
        return true
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
  
        
//
//        let currentCharacterCount = textView.text?.characters.count ?? 0
//
//
//        if (range.length + range.location > currentCharacterCount){
//            return false
//        }
//        print(currentCharacterCount)
//        print(text.characters.count)
//        print(range.length)
//        let newLength = currentCharacterCount + text.characters.count - range.length
//        print(newLength)
//
//        return newLength <= 220
        
        let currentText = textView.text as NSString
        let updatedText = currentText.replacingCharacters(in: range, with: text)
        
        return updatedText.characters.count <= 420
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        print("touches")
        self.view.endEditing(true)
    }
    
//    override var inputAccessoryView: UIView? {
//        get {
//            
//            return inputContainerView
//        }
//    }
//    
//    override var canBecomeFirstResponder: Bool {
//        return true
//    }
//    

    func dismissView() {
        
        inputTextView.isEditable = true;
        self.view.isUserInteractionEnabled = true
        self.navigationItem.leftBarButtonItem?.isEnabled = true
        self.navigationItem.rightBarButtonItem?.isEnabled = true
        self.view.endEditing(true)
        self.dismiss(animated: true, completion: nil)
    }
    
    func cancelTapped() {
        
        self.view.endEditing(true)
        
        // Prepare the popup assets
        let title = "Warning"
        let message = "Are you sure you want to discard?"
   
        let popup = PopupDialog(title: title, message: message, image: nil)
        
        let buttonOne = DestructiveButton(title: "Discard") {
            self.dismiss(animated: true, completion: nil)
        }
        
        let buttonTwo = DefaultButton(title: "Continue", dismissOnTap: true) {
       
        }
        
        popup.addButtons([buttonOne,buttonTwo])
        popup.buttonAlignment = .horizontal
        
        // Present dialog
        self.present(popup, animated: true, completion: nil)
        
//        let alert = UIAlertController()
//
//        alert.title = "Continue?"
//        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel) { (action:UIAlertAction!) in
//        })
//
//        alert.addAction(UIAlertAction(title: "Discard", style: .destructive , handler:{ (UIAlertAction)in
//            self.dismiss(animated: true, completion: nil)
//        }))
//
//
//
//        self.present(alert, animated: true, completion: nil)
        
        
    }
    
    
    func disableView() {
        inputTextView.isEditable = false;
        self.view.isUserInteractionEnabled = false
        self.navigationItem.leftBarButtonItem?.isEnabled = false
        self.navigationItem.rightBarButtonItem?.isEnabled = false
    }
    
    func uploadImageToFb() {
        if !hasImg {
            self.postToFirebase(imgUrl: NO_PIC)
        } else {
            if let img = imageView.image {
                if let imgData = UIImageJPEGRepresentation(img, 0.1) {
                    
                    let imgUid = NSUUID().uuidString
                    let metaData = StorageMetadata()
                    metaData.contentType = "image/jpeg"
                    
                    DataService.ds.REF_CONFESSION_PIC.child(imgUid).putData(imgData, metadata: metaData) { (metaData, error) in
                        if error != nil {
                            print("Xavier: upload image to firebase failed")
                            print(error.debugDescription)
                        } else {
                            print("Xavier: successfuly upload img to firebase")
                            let downloadUrl = metaData?.downloadURL()?.absoluteString
                            if let url = downloadUrl {
                                self.postToFirebase(imgUrl: url)
                            }
                            
                        }
                        
                    }
                }
                
            }
        }
        
    }
    
    
    func anonymousTapped() {
        sendBtn.sendBtn.pulsate()
        
        if anonymousMode == false {
            anonymousMode = true
            sendBtn.sendBtn.setBackgroundImage(#imageLiteral(resourceName: "anonyModeOn"), for: .normal)
            
            var murmur = Murmur(title: "Posting Anonymously")
            murmur.backgroundColor = UIColor(red: 18/255, green: 102/255, blue: 215/255, alpha: 1)
            murmur.titleColor = UIColor.white
            
            // Show and hide a message after delay
            Whisper.show(whistle: murmur, action: .show(1.5))
            
        } else {
            anonymousMode = false
            sendBtn.sendBtn.setBackgroundImage(#imageLiteral(resourceName: "anonyModeOff"), for: .normal)
        }

        
        
    }
    
    func addTapped() {
       
        if self.selectedcategory != "None" {
            SVProgressHUD.show(withStatus: "Posting")
            
            disableView()
            uploadImageToFb()
        } else {
            ErrorAlert().show(title: "Category Needed", msg: "Please select one of the categories", object: self)
        }


        

    }
    
    func postToFirebase(imgUrl: String) {
        var dict = [String: Bool]()
        dict["key"] = true
        let timestamp = NSNumber(integerLiteral: Int(NSDate().timeIntervalSince1970))
//        ServerValue.timestamp()


        let confessionValueForHash: Dictionary<String, AnyObject> = ["caption": inputTextView.text! as AnyObject, "likes": 0 as AnyObject, "comments": 0 as AnyObject, "author": Auth.auth().currentUser?.uid as AnyObject, "faculty": self.user.course as AnyObject, "time": timestamp, "university": self.uniName as AnyObject, "authorUniversity": self.user.university as AnyObject, "imageUrl": imgUrl as AnyObject, "authorGender": self.user.gender as AnyObject, "userLike": dict as AnyObject, "userComment": dict as AnyObject, "anonymous" : anonymousMode as AnyObject, "authorProfilePicUrl": self.user.profilePicUrl as AnyObject, "authorUserName": self.user.userName as AnyObject, "category": self.selectedcategory as AnyObject]
        
        var firebasePost: DatabaseReference!
        if isAdmin {
            firebasePost = DataService.ds.REF_Confessions_Discussion
        } else {
            firebasePost = DataService.ds.REF_CONFESSIONS.childByAutoId()
        }
        
//        firebasePost.setValue(confessionValue)
//        let confession = Confession(confessionKey: firebasePost.key, postData: confessionValue)
//        
//        
        var words = inputTextView.text.components(separatedBy: " ")
        
     
        
        var hashDict = [String: Bool]()
        
        if !isAdmin {
            for var w in words {
                if w.hasPrefix("#") {
                    print(w)
                    var wordWithoutHash = String(w.characters.dropFirst())
                    wordWithoutHash = wordWithoutHash.lowercased()
                    print(wordWithoutHash)
                    hashDict[wordWithoutHash] = true
                    let hashtagPost = DataService.ds.REF_HASHTAG.child(wordWithoutHash).child(firebasePost.key)
                    // let confessionWithHash: Dictionary<String, AnyObject> =  dict as Dictionary<String, AnyObject>
                    hashtagPost.setValue(confessionValueForHash)
                    
                    //DataService.ds.REF_Confssion_Hash_LookUp.child(firebasePost.key).updateChildValues([wordWithoutHash: true])
                }
                
            }
        }

        
        let confessionValue: Dictionary<String, AnyObject> = ["caption": inputTextView.text! as AnyObject, "likes": 0 as AnyObject, "comments": 0 as AnyObject, "author": Auth.auth().currentUser?.uid as AnyObject, "faculty": self.user.course as AnyObject, "time": timestamp, "university": self.uniName as AnyObject, "authorUniversity": self.user.university as AnyObject, "imageUrl": imgUrl as AnyObject, "authorGender": self.user.gender as AnyObject, "userLike": dict as AnyObject, "hashes": hashDict as AnyObject, "userComment": dict as AnyObject,"anonymous" : anonymousMode as AnyObject, "authorProfilePicUrl": self.user.profilePicUrl as AnyObject, "authorUserName": self.user.userName as AnyObject,"category": self.selectedcategory as AnyObject]
      //  let firebasePost = DataService.ds.REF_CONFESSIONS.childByAutoId()
        
        firebasePost.setValue(confessionValue)
        DataService.ds.REF_UniConfession.child(self.uniName).child(firebasePost.key).setValue(confessionValue)
        DataService.ds.REF_User_Confession.child((Auth.auth().currentUser?.uid)!).child(firebasePost.key).setValue(confessionValue)
        DataService.ds.REF_Category.child(self.selectedcategory).child(firebasePost.key).setValue(confessionValue)
        
        //let notificationValue
        //DataService.ds.REF_NOTIFICATION.child((FIRAuth.auth()?.currentUser?.uid)!).setValue(<#T##value: Any?##Any?#>)
        let confession = Confession(confessionKey: firebasePost.key, postData: confessionValue)
        
        
        delegate?.sendConfessionToPreviousVC(confession: confession)
        
        
       // NotificationCenter.default.post(name: NSNotification.Name(rawValue: "reloadData"),object: self)
        dismissView()
        
    }
    
    
    func addPicTapped() {
        let alert = UIAlertController()
        if imageView.image == #imageLiteral(resourceName: "camera") {
            
            alert.addAction(UIAlertAction(title: "Camera", style: UIAlertActionStyle.default, handler:{ (UIAlertAction)in
                self.imagePick.sourceType = UIImagePickerControllerSourceType.camera;
                self.present(self.imagePick, animated: true, completion: nil)
            }))
            
            alert.addAction(UIAlertAction(title: "Photo library", style: UIAlertActionStyle.default, handler:{ (UIAlertAction)in
                self.imagePick.sourceType = UIImagePickerControllerSourceType.photoLibrary;
                self.present(self.imagePick, animated: true, completion: nil)
            }))
            
            alert.addAction(UIAlertAction(title: "Cancel", style: .cancel) { (action:UIAlertAction!) in
                
            })
            
            self.present(alert, animated: true, completion: nil)
            
        } else {
            
            
            alert.addAction(UIAlertAction(title: "Choose another picture", style: UIAlertActionStyle.default, handler:{ (UIAlertAction)in
                self.present(self.imagePick, animated: true, completion: nil)
            }))
            
            alert.addAction(UIAlertAction(title: "Remove current picture", style: .destructive , handler:{ (UIAlertAction)in
                self.imageView.image = #imageLiteral(resourceName: "camera")
                self.hasImg = false
            }))
            
            alert.addAction(UIAlertAction(title: "Cancel", style: .cancel) { (action:UIAlertAction!) in

            })
            
            self.present(alert, animated: true, completion: nil)
        }
       
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : AnyObject]) {
        
        if let image = info[UIImagePickerControllerOriginalImage] as? UIImage {
            imageView.image = image
            hasImg = true
        }

        
       /* if let image = info[UIImagePickerControllerEditedImage] as? UIImage {
            imageView.image = image
        }*/
        
        imagePick.dismiss(animated: true, completion: nil)
        
    }
    

}
