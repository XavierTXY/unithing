//
//  AnonymousButton.swift
//  UniThing
//
//  Created by XavierTanXY on 16/3/18.
//  Copyright © 2018 Xavier TanXY. All rights reserved.
//

import Foundation
import UIKit

class AnonymousButton: UIView {
    
    var replyVC: ReplyVC!
    
    @IBOutlet weak var anonBtn: UIButton!
    @IBAction func clickAnonymous(_ sender: UIButton) {
        
        replyVC.anonymousModeTapped()
    }
}
