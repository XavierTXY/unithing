//
//  UserCell.swift
//  UniThing
//
//  Created by XavierTanXY on 24/5/18.
//  Copyright © 2018 Xavier TanXY. All rights reserved.
//

import UIKit
import Firebase

class UserCell: UITableViewCell {



    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        textLabel?.frame = CGRect(x: 64, y: textLabel!.frame.origin.y - 2, width: textLabel!.frame.width+30, height: textLabel!.frame.height+5)
        textLabel?.font = UIFont(name: "HelveticaNeue-Bold", size: 16)
        
        detailTextLabel?.frame = CGRect(x: 64, y: detailTextLabel!.frame.origin.y + 2, width: detailTextLabel!.frame.width+30, height: detailTextLabel!.frame.height)
        detailTextLabel?.font = UIFont(name: "HelveticaNeue", size: 12)
        detailTextLabel?.textColor = UIColor.darkGray
    }
    
    let profileImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.layer.cornerRadius = 24
        imageView.layer.masksToBounds = true
        imageView.contentMode = .scaleAspectFill
        return imageView
    }()
    
    let timeLabel: UILabel = {
        let label = UILabel()
        //        label.text = "HH:MM:SS"
        label.font = UIFont.systemFont(ofSize: 13)
        label.textColor = UIColor.darkGray
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    let initialLbl: UILabel = {
        let lbl = UILabel()
        lbl.textAlignment = .center
        lbl.backgroundColor = UIColor.clear
        lbl.textColor = UIColor.white
        lbl.font = UIFont(name: "HelveticaNeue-Bold", size: 24)
        lbl.text = "A"
        lbl.translatesAutoresizingMaskIntoConstraints = false
        return lbl
    }()

    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: .subtitle, reuseIdentifier: reuseIdentifier)
        
        addSubview(profileImageView)
        addSubview(timeLabel)
        
        //ios 9 constraint anchors
        //need x,y,width,height anchors
        profileImageView.leftAnchor.constraint(equalTo: self.leftAnchor, constant: 8).isActive = true
        profileImageView.centerYAnchor.constraint(equalTo: self.centerYAnchor).isActive = true
        profileImageView.widthAnchor.constraint(equalToConstant: 48).isActive = true
        profileImageView.heightAnchor.constraint(equalToConstant: 48).isActive = true
        
        //need x,y,width,height anchors
        timeLabel.rightAnchor.constraint(equalTo: self.rightAnchor).isActive = true
        timeLabel.topAnchor.constraint(equalTo: self.topAnchor, constant: 18).isActive = true
        timeLabel.widthAnchor.constraint(equalToConstant: 100).isActive = true
        timeLabel.heightAnchor.constraint(equalTo: textLabel!.heightAnchor).isActive = true
        
        profileImageView.addSubview(initialLbl)
        initialLbl.centerXAnchor.constraint(equalTo: profileImageView.centerXAnchor).isActive = true
        initialLbl.centerYAnchor.constraint(equalTo: profileImageView.centerYAnchor).isActive = true
        initialLbl.widthAnchor.constraint(equalToConstant: 20).isActive = true
        initialLbl.heightAnchor.constraint(equalToConstant: 20).isActive = true
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

}
