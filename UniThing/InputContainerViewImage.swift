//
//  InputContainerViewImage.swift
//  UniThing
//
//  Created by Xavier TanXY on 8/1/17.
//  Copyright © 2017 Xavier TanXY. All rights reserved.
//

import UIKit
import Foundation

class InputContainerViewImage: UIView, UITextFieldDelegate {
    
    var addConfessionVC: AddConfessionVC? {
        didSet {

            
            let tapGestureRecognizer = UITapGestureRecognizer(target: addConfessionVC, action: #selector(addConfessionVC?.addPicTapped))

            uploadImageView.addGestureRecognizer(tapGestureRecognizer)
        }
    }
//
//    var comment3VC: Comment3VC? {
//        didSet {
//            sendButton.addTarget(comment3VC, action: #selector(comment3VC?.commentSend), for: .touchUpInside)
//            
//            //            let tapGestureRecognizer = UITapGestureRecognizer(target: chatLogVC, action: #selector(commentVC?.))
//            //            uploadImageView.addGestureRecognizer(tapGestureRecognizer)
//        }
//    }
    
//    lazy var inputTextField: UITextField = {
//        let textField = UITextField()
//        textField.placeholder = "Enter comment..."
//        textField.translatesAutoresizingMaskIntoConstraints = false
//        textField.delegate = self
//        return textField
//    }()
    
    let uploadImageView: UIImageView = {
        let uploadImageView = UIImageView()
        uploadImageView.translatesAutoresizingMaskIntoConstraints = false
        uploadImageView.image = UIImage(named: "love")
        uploadImageView.contentMode = .scaleAspectFit
        uploadImageView.isUserInteractionEnabled = true
        
        return uploadImageView
    }()
    
   // let sendButton = UIButton(type: .system)
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        translatesAutoresizingMaskIntoConstraints = false
        backgroundColor = UIColor.clear
        
        
        addSubview(uploadImageView)
        
        uploadImageView.leftAnchor.constraint(equalTo: leftAnchor).isActive = true
        uploadImageView.centerYAnchor.constraint(equalTo: centerYAnchor).isActive = true
        uploadImageView.widthAnchor.constraint(equalToConstant: 44).isActive = true
        uploadImageView.heightAnchor.constraint(equalToConstant: 44).isActive = true
        
        
//        sendButton.setTitle("Send", for: .normal)
//        sendButton.translatesAutoresizingMaskIntoConstraints = false
//        addSubview(sendButton)
//        
//        sendButton.rightAnchor.constraint(equalTo: rightAnchor).isActive = true
//        sendButton.centerYAnchor.constraint(equalTo: centerYAnchor).isActive = true
//        sendButton.widthAnchor.constraint(equalToConstant: 80).isActive = true
//        sendButton.heightAnchor.constraint(equalTo: heightAnchor).isActive = true
//        
//        
//        addSubview(self.inputTextField)
//        
//        self.inputTextField.leftAnchor.constraint(equalTo: uploadImageView.rightAnchor, constant: 8).isActive = true
//        self.inputTextField.centerYAnchor.constraint(equalTo: centerYAnchor).isActive = true
//        self.inputTextField.rightAnchor.constraint(equalTo: sendButton.leftAnchor).isActive = true
//        self.inputTextField.heightAnchor.constraint(equalTo: heightAnchor).isActive = true
//        
//        let seperatorLineView = UIView()
//        seperatorLineView.backgroundColor = UIColor.lightGray
//        seperatorLineView.translatesAutoresizingMaskIntoConstraints = false
//        addSubview(seperatorLineView)
//        
//        seperatorLineView.leftAnchor.constraint(equalTo: leftAnchor).isActive = true
//        seperatorLineView.topAnchor.constraint(equalTo: topAnchor).isActive = true
//        seperatorLineView.widthAnchor.constraint(equalTo: widthAnchor).isActive = true
//        seperatorLineView.heightAnchor.constraint(equalToConstant: 1).isActive = true
        
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    func textFieldShouldClear(_ textField: UITextField) -> Bool {
       // chatLogVC?.messageSend()
        return true
    }
    
    
}
