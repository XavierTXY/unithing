//
//  ChangeDetailVC.swift
//  UniThing
//
//  Created by Xavier TanXY on 29/6/17.
//  Copyright © 2017 Xavier TanXY. All rights reserved.
//

import UIKit
import Firebase
import SVProgressHUD

protocol ReloadName {
    func reloadNameFromChangeDetailVC(name: String)
}

class ChangeDetailVC: UIViewController, UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate {

    @IBOutlet weak var tableView: UITableView!
    var name: String?
    var nameToBeChanged: String?
    var delegate: ReloadName?
    
    var textField: UITextField?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        SVProgressHUD.setBackgroundColor(UIColor(red: 244/255, green: 244/255, blue: 244/255, alpha: 0.95))
        
        tableView.delegate = self
        tableView.dataSource = self
        
        self.navigationItem.title = "Name"
        
        let rightBarItem = UIBarButtonItem(title: "Save", style: .plain, target: self, action: "saveTapped")
        
        self.navigationItem.setRightBarButton(rightBarItem, animated: true)
        self.navigationItem.rightBarButtonItem?.tintColor = UIColor(red: 158/255, green: 158/255, blue: 158/255, alpha: 0.7)
        self.navigationItem.rightBarButtonItem?.isEnabled = false
        self.navigationController?.navigationBar.titleTextAttributes = [ NSForegroundColorAttributeName: PURPLE]
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        
        if (textField?.isFirstResponder)! {
            textField?.resignFirstResponder()
        }
        
    }
    var timer: Timer?
    
    func saveTapped() {
        let cell = tableView.cellForRow(at: IndexPath(row: 0, section: 0)) as? ChangeDetailCell
        cell?.textField.resignFirstResponder()
        SVProgressHUD.show()
        
        self.timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(postToFirebase), userInfo: nil, repeats: false)
    }
    
    func postToFirebase() {
        let trimmedName = nameToBeChanged?.trimmingCharacters(in: .whitespaces)
        
        if (trimmedName?.containsEmoji)! {
            ErrorAlert().createAlert(title: "Invalid Name", msg: "Name can't contain emoji", object: self)
            SVProgressHUD.dismiss()
        } else if trimmedName?.lowercased() == "student" {
            ErrorAlert().createAlert(title: "Invalid Name", msg: "Please choose another name", object: self)
            SVProgressHUD.dismiss()
        } else if !checkValidFullName(str: trimmedName!) {
            ErrorAlert().createAlert(title: "Invalid Name", msg: "Name can't contain symbol", object: self)
            SVProgressHUD.dismiss()
        } else {
            DataService.ds.REF_USERS.child((Auth.auth().currentUser?.uid)!).updateChildValues(["name": trimmedName]) { (error, ref) in
                
                if error != nil {
                    SVProgressHUD.showError(withStatus: "Can't Save")
                } else {
                    self.delegate?.reloadNameFromChangeDetailVC(name: trimmedName!)
                    SVProgressHUD.dismiss()
                    self.navigationController?.popViewController(animated: true)

                    
                }
            }
            
            
        }
        
    
        
    }


    func removeSpecialCharsFromString(str: String) -> String {
        let chars = Set("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLKMNOPQRSTUVWXYZ1234567890".characters)
        
        return String(str.characters.filter { chars.contains($0) })
    }

    func checkValidFullName(str: String) -> Bool {
        let name = str
        let filterdName = removeSpecialCharsFromString(str: name)
        
        if filterdName == name {
            return true
        } else {
            return false
        }
    }

    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    
        let cell = tableView.dequeueReusableCell(withIdentifier: "ChangeDetailCell") as? ChangeDetailCell
        cell?.textField.placeholder = self.name
        cell?.textField.delegate = self
        self.textField = cell?.textField
        cell?.textField.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        return cell!
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 15
    }
    
    func textFieldDidChange(_ textField: UITextField) {
        nameToBeChanged = textField.text
        if (textField.text?.characters.count)! > 0 && !(textField.text?.trimmingCharacters(in: .whitespaces).isEmpty)! {
            self.navigationItem.rightBarButtonItem?.tintColor = UIColor(red: 33/255, green: 150/255, blue: 243/255, alpha: 1)
            self.navigationItem.rightBarButtonItem?.isEnabled = true
        } else {
            self.navigationItem.rightBarButtonItem?.tintColor = UIColor(red: 158/255, green: 158/255, blue: 158/255, alpha: 0.7)
            self.navigationItem.rightBarButtonItem?.isEnabled = false
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let currentCharacterCount = textField.text?.characters.count ?? 0
        if (range.length + range.location > currentCharacterCount){

            return false
        }
        let newLength = currentCharacterCount + string.characters.count - range.length
        return newLength <= LIMIT_CHAR_NAME
    }


    


}
