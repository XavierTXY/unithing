//
//  CircleImageView.swift
//  UniThing
//
//  Created by Xavier TanXY on 13/8/17.
//  Copyright © 2017 Xavier TanXY. All rights reserved.
//

import UIKit

class CircleImageView: UIImageView {
    
    
    override func layoutSubviews() {
        contentMode = .scaleAspectFill
        layer.cornerRadius = self.frame.width / 2
        clipsToBounds = true
        
        
    }
    
}
