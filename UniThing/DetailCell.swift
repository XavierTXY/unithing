//
//  DetailCell.swift
//  UniThing
//
//  Created by XavierTanXY on 14/5/18.
//  Copyright © 2018 Xavier TanXY. All rights reserved.
//

import UIKit

class DetailCell: UITableViewCell {

    @IBOutlet weak var detailPic: UIImageView!
    @IBOutlet weak var outerView: UIView!
    
    @IBOutlet weak var cornerView: TwoCornersView!
    @IBOutlet weak var dateLbl: UILabel!
    @IBOutlet weak var captionLbl: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
//        outerView.clipsToBounds = false
//        outerView.layer.shadowColor = UIColor.black.cgColor
//        outerView.layer.shadowOpacity = 0.2
//        outerView.layer.shadowOffset = CGSize.zero
//        outerView.layer.cornerRadius = outerView.frame.height / 2
//        outerView.layer.shadowRadius = 3
//        outerView.layer.shadowPath = UIBezierPath(roundedRect: outerView.bounds, cornerRadius: outerView.frame.height / 2).cgPath


    }

    func configureCell(confession: Confession, time: NSNumber, last: Bool) {
        

        
        if !last {
            captionLbl.text = confession.caption
            
            let seconds = confession.time.doubleValue
            let timesStampDate = NSDate(timeIntervalSince1970: seconds)
            var dateFormatter = DateFormatter()
            
            dateFormatter.dateFormat = "EEEE, MMM d"
            dateLbl.text = dateFormatter.string(from: timesStampDate as Date)
            detailPic.image = #imageLiteral(resourceName: "post")
        } else {
            captionLbl.text = "Hurray! You joined UniThing!"
            
            let seconds = time.doubleValue
            let timesStampDate = NSDate(timeIntervalSince1970: seconds)
            var dateFormatter = DateFormatter()
            
            dateFormatter.dateFormat = "EEEE, MMM d"
            dateLbl.text = dateFormatter.string(from: timesStampDate as Date)
            
            detailPic.image = #imageLiteral(resourceName: "system")
            

        }
        
    }

}
