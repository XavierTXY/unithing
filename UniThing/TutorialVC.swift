//
//  TutorialVC.swift
//  UniThing
//
//  Created by XavierTanXY on 27/1/18.
//  Copyright © 2018 Xavier TanXY. All rights reserved.
//

import UIKit
import UserNotifications
import paper_onboarding

class TutorialVC: UIViewController,UIApplicationDelegate, PaperOnboardingDataSource,PaperOnboardingDelegate {

//    var swiftyOnboard: SwiftyOnboard!
    let colors:[UIColor] = [#colorLiteral(red: 0.9607843161, green: 0.7058823705, blue: 0.200000003, alpha: 1),#colorLiteral(red: 0.1764705926, green: 0.01176470611, blue: 0.5607843399, alpha: 1),#colorLiteral(red: 0.8078431487, green: 0.02745098062, blue: 0.3333333433, alpha: 1),#colorLiteral(red: 0.3254901961, green: 0.6549019608, blue: 0.5921568627, alpha: 1)]
    var titleArray: [String] = ["UniThing", "Say it out loud", "Universities" , "Notification Alert"]
    var subTitleArray: [String] = ["UniThing lets u discover what's is happening around your university.", "It’s completely up to you\n Either public or anonymous\n No one will know who you are", "You can view confessions across all universities\n Have fun!" , "Don't forget to enable notification so that you know what is going on around your campus!"]
    
    @IBOutlet weak var hideBtn: UIButton!
    var gradiant: CAGradientLayer = {
        //Gradiant for the background view
        let blue = UIColor(red: 69/255, green: 127/255, blue: 202/255, alpha: 1.0).cgColor
        let purple = UIColor(red: 166/255, green: 172/255, blue: 236/255, alpha: 1.0).cgColor
        let gradiant = CAGradientLayer()
        gradiant.colors = [purple, blue]
        gradiant.startPoint = CGPoint(x: 0.5, y: 0.18)
        return gradiant
    }()
    
    let titleFont = UIFont(name: "Nunito-Bold", size: 36.0) ?? UIFont.boldSystemFont(ofSize: 36.0)
    let descriptionFont = UIFont(name: "Helvetica-Regular", size: 14.0) ?? UIFont.systemFont(ofSize: 14.0)
    
    override func viewDidLoad() {
        super.viewDidLoad()


        
        gradient()
        
//        swiftyOnboard = SwiftyOnboard(frame: view.frame, style: .light)
////        view.addSubview(swiftyOnboard)
//        swiftyOnboard.dataSource = self
//        swiftyOnboard.delegate = self
//
        hideBtn.isHidden = true
        setupPaperOnboardingView()
        view.bringSubview(toFront: hideBtn)
        
    }
    
    @IBAction func dismissTapped(_ sender: Any) {
        //Enable notification
        if #available(iOS 10.0, *) {
            let center  = UNUserNotificationCenter.current()
            
            center.requestAuthorization(options: [.sound, .alert, .badge]) { (granted, error) in
                if error == nil{
                    DispatchQueue.main.async {
                        UIApplication.shared.registerForRemoteNotifications()
                    }
                    
                }
            }
        } else {
            UIApplication.shared.registerUserNotificationSettings(UIUserNotificationSettings(types: [.sound, .alert, .badge], categories: nil))
            DispatchQueue.main.async {
                UIApplication.shared.registerForRemoteNotifications()
            }
            
        }
        
        self.dismiss(animated: true, completion: nil)
    }
    
    private func setupPaperOnboardingView() {
        let onboarding = PaperOnboarding()
        onboarding.delegate = self
        onboarding.dataSource = self
        onboarding.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(onboarding)
        
        // Add constraints
        for attribute: NSLayoutAttribute in [.left, .right, .top, .bottom] {
            let constraint = NSLayoutConstraint(item: onboarding,
                                                attribute: attribute,
                                                relatedBy: .equal,
                                                toItem: view,
                                                attribute: attribute,
                                                multiplier: 1,
                                                constant: 0)
            view.addConstraint(constraint)
        }
    }
    
    func onboardingItem(at index: Int) -> OnboardingItemInfo {
        
        return
            [
                OnboardingItemInfo(informationImage: #imageLiteral(resourceName: "onboard0"),
                                   title: titleArray[0],
                                   description: subTitleArray[0],
                                   pageIcon: #imageLiteral(resourceName: "bigDot"),
                                   color: colors[0],
                                   titleColor: UIColor.white, descriptionColor: UIColor.white, titleFont: titleFont, descriptionFont: descriptionFont),
                
                OnboardingItemInfo(informationImage: #imageLiteral(resourceName: "onboard1"),
                                   title: titleArray[1],
                                   description: subTitleArray[1],
                                   pageIcon: #imageLiteral(resourceName: "bigDot"),
                                   color: colors[1],
                                   titleColor: UIColor.white, descriptionColor: UIColor.white, titleFont: titleFont, descriptionFont: descriptionFont),
                
                OnboardingItemInfo(informationImage: #imageLiteral(resourceName: "onboard2"),
                                   title: titleArray[2],
                                   description: subTitleArray[2],
                                   pageIcon: #imageLiteral(resourceName: "bigDot"),
                                   color: colors[2],
                                   titleColor: UIColor.white, descriptionColor: UIColor.white, titleFont: titleFont, descriptionFont: descriptionFont),
                
                OnboardingItemInfo(informationImage: #imageLiteral(resourceName: "onboard3"),
                                   title: titleArray[3],
                                   description: subTitleArray[3],
                                   pageIcon: #imageLiteral(resourceName: "bigDot"),
                                   color: colors[3],
                                   titleColor: UIColor.white, descriptionColor: UIColor.white, titleFont: titleFont, descriptionFont: descriptionFont)
                
        ][index]
    }
    
    func onboardingItemsCount() -> Int {
        return 4
    }
    
    func onboardingWillTransitonToIndex(_ index: Int) {
        hideBtn.isHidden = index == 3 ? false : true
    }

    
    func gradient() {
        //Add the gradiant to the view:
        self.gradiant.frame = view.bounds
        view.layer.addSublayer(gradiant)
    }
    
    func handleSkip() {
//        swiftyOnboard?.goToPage(index: 3, animated: true)
    }

//    func handleContinue(sender: UIButton) {
//        let index = sender.tag
//        if( index + 1 == 4 ) {
//
////            var ad = AppDelegate()
////            ad.enableNotification()
//
//            //Enable notification
//            if #available(iOS 10.0, *) {
//                let center  = UNUserNotificationCenter.current()
//
//                center.requestAuthorization(options: [.sound, .alert, .badge]) { (granted, error) in
//                    if error == nil{
//                        UIApplication.shared.registerForRemoteNotifications()
//                    }
//                }
//            }
//            else {
//                UIApplication.shared.registerUserNotificationSettings(UIUserNotificationSettings(types: [.sound, .alert, .badge], categories: nil))
//                UIApplication.shared.registerForRemoteNotifications()
//            }
//
//            self.dismiss(animated: true, completion: nil)
//        } else {
//            swiftyOnboard?.goToPage(index: index + 1, animated: true)
//        }
//
//    }
}

//extension TutorialVC: SwiftyOnboardDelegate, SwiftyOnboardDataSource {
//    
//    func swiftyOnboardNumberOfPages(_ swiftyOnboard: SwiftyOnboard) -> Int {
//        //Number of pages in the onboarding:
//        return 4
//    }
//    
//    func swiftyOnboardBackgroundColorFor(_ swiftyOnboard: SwiftyOnboard, atIndex index: Int) -> UIColor? {
//        //Return the background color for the page at index:
//        return colors[index]
//    }
//    
//    func swiftyOnboardPageForIndex(_ swiftyOnboard: SwiftyOnboard, index: Int) -> SwiftyOnboardPage? {
//        let view = SwiftyOnboardPage()
//        
//        //Set the image on the page:
//        view.imageView.image = UIImage(named: "onboard\(index)")
//        
//        //Set the font and color for the labels:
//        view.title.font = UIFont(name: "Lato-Heavy", size: 22)
//        view.subTitle.font = UIFont(name: "Lato-Regular", size: 16)
//        
//        //Set the text in the page:
//        view.title.text = titleArray[index]
//        view.subTitle.text = subTitleArray[index]
//        
//        //Return the page for the given index:
//        return view
//    }
//    
//    func swiftyOnboardViewForOverlay(_ swiftyOnboard: SwiftyOnboard) -> SwiftyOnboardOverlay? {
//        let overlay = SwiftyOnboardOverlay()
//        
//        //Setup targets for the buttons on the overlay view:
//        overlay.skipButton.addTarget(self, action: #selector(handleSkip), for: .touchUpInside)
//        overlay.continueButton.addTarget(self, action: #selector(handleContinue), for: .touchUpInside)
//        
//        //Setup for the overlay buttons:
//        overlay.continueButton.titleLabel?.font = UIFont(name: "Lato-Bold", size: 16)
//        overlay.continueButton.setTitleColor(UIColor.white, for: .normal)
//        overlay.skipButton.setTitleColor(UIColor.white, for: .normal)
//        overlay.skipButton.titleLabel?.font = UIFont(name: "Lato-Heavy", size: 16)
//        
//        //Return the overlay view:
//        return overlay
//    }
//    
//    func swiftyOnboardOverlayForPosition(_ swiftyOnboard: SwiftyOnboard, overlay: SwiftyOnboardOverlay, for position: Double) {
//        let currentPage = round(position)
//        overlay.pageControl.currentPage = Int(currentPage)
//        overlay.continueButton.tag = Int(position)
//        
//        if currentPage == 0.0 || currentPage == 1.0  || currentPage == 2.0 {
//            overlay.continueButton.setTitle("Continue", for: .normal)
//            overlay.skipButton.setTitle("Skip", for: .normal)
//            overlay.skipButton.isHidden = false
//        } else {
//            overlay.continueButton.setTitle("Get Started!", for: .normal)
//            overlay.skipButton.isHidden = true
//        }
//    }
//}
