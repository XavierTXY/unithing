//
//  tryViewController.swift
//  UniThing
//
//  Created by Xavier TanXY on 3/12/16.
//  Copyright © 2016 Xavier TanXY. All rights reserved.
//

import UIKit

class tryViewController: UIViewController {

    @IBOutlet weak var textField: UITextField!
    @IBOutlet weak var scrollView: UIScrollView!
   
    @IBOutlet weak var image: UIImageView!
    override func viewDidLoad() {
        super.viewDidLoad()
       // textField.delegate = self
       // image.isHidden = true
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        // Do any additional setup after loading the view.
    }

    /*
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        image.isHidden = false
        if textField == self.textField {
            scrollView.setContentOffset(CGPoint(x: 0, y: 250), animated: true)
            
        }
        
        
    }
    
    func textFieldDidEndEditing(_ textField: UITextField, reason: UITextFieldDidEndEditingReason) {
        image.isHidden = true
        scrollView.setContentOffset(CGPoint(x: 0, y: 0), animated: true)
    }*/
    
    func keyboardWillShow(notification: NSNotification) {
        print("hi1")
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            if self.view.frame.origin.y == 0{
                self.view.frame.origin.y -= keyboardSize.height
            }
        }
        
    }
    
    func keyboardWillHide(notification: NSNotification) {
        print("hi2")
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            print("hi22")
            //self.view.frame.origin.y += keyboardSize.height
            if self.view.frame.origin.y != 0{
                print("hi222")
                self.view.frame.origin.y += keyboardSize.height
            }
            
            
        }
    }
    
   

}
