//
//  Comment3VC.swift
//  UniThing
//
//  Created by Xavier TanXY on 25/12/16.
//  Copyright © 2016 Xavier TanXY. All rights reserved.
//

import UIKit
import Firebase
import FirebaseDatabase
import SVProgressHUD
//import Lottie
import SDWebImage
import BRYXBanner
import Whisper
import PopupDialog
import ViewAnimator

protocol ReloadLikeProtocol
{
    func reloadLikeFromCommentVC(confession: Confession, reloadLike: Bool)
}

protocol ReloadCommentFromPreviousVCProtocal
{
    func reloadCommentFromPreviousVC(confession: Confession, addComment: Bool, isDiscussion: Bool)
}

//Protocol: When deleting confession in comment vc, reload deleted confession in previous vc (Confession)
//protocol PassConfessionFromPreviousVCDeletionProtocol
//{
//    func reloadDeletedConfessionFromPreviousVC(confession: Confession)
//}

class Comment3VC: UIViewController, UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate, UITextViewDelegate, ReloadCommentFromReplyVC  {
    
    private let animations = [AnimationType.from(direction: .bottom, offset: 30.0)]
    
    lazy var inputContainerView: ChatInputContainerComment = {
        
        let chatInputContainerView = ChatInputContainerComment(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: 55))
        
        chatInputContainerView.comment3VC = self
        //        chatInputContainerView.inputTextField.addTarget(self, action: #selector(self.textFieldDidChange(_:)), for: UIControlEvents.editingChanged)
        
        //                let tapRec = UITapGestureRecognizer()
        //
        //
        //                tapRec.addTarget(self, action: #selector(self.tappedView))
        //
        //
        //        let vv = UIView()
        //        vv.isUserInteractionEnabled = true
        //        vv.backgroundColor = UIColor.blue
        //        vv.translatesAutoresizingMaskIntoConstraints = false
        //        vv.addGestureRecognizer(tapRec)
        //        //        anonBtn.isEnabled = true
        //        chatInputContainerView.addSubview(vv)
        //
        //        vv.bottomAnchor.constraint(equalTo: chatInputContainerView.topAnchor, constant: -10).isActive = true
        //        vv.trailingAnchor.constraint(equalTo:chatInputContainerView.trailingAnchor, constant: -10).isActive = true
        //        vv.widthAnchor.constraint(equalToConstant: 50).isActive = true
        //        vv.heightAnchor.constraint(equalToConstant: 50).isActive = true
        
        
        return chatInputContainerView
        
        
    }()
    
    //    lazy var anonBtn: UIButton = {
    //
    //        let btn = UIButton()
    //        btn.backgroundColor = UIColor.blue
    //
    //        btn.bottomAnchor.constraint(equalTo: self.inputContainerView.topAnchor, constant: 15).isActive = true
    //        btn.rightAnchor.constraint(equalTo: self.inputContainerView.rightAnchor, constant: 15).isActive = true
    //        btn.widthAnchor.constraint(equalToConstant: 40).isActive = true
    //        btn.heightAnchor.constraint(equalToConstant: 40).isActive = true
    //
    //        return btn
    //    }()
    
    @IBOutlet weak var tableView: UITableView!
    var confessionId: String?
    var discussion: Discussion?
    var confession: Confession?
    var commments = [Comment]()
    var commentIDs = [String]()
    var currentComments = [Comment]()
    var commentDict = [String: Comment]()
    var uniName: String?
    var user: User!
    
    var timer: Timer?
    var originalFrame: CGRect?
    
    var actInd: UIActivityIndicatorView!
    var totalComments: Int!
    var numberOfObjectsinArray: Int = 12
    
    var check: Int?
    var notificationEnable: Bool?
    
    var replyAnonymously = false
    
    var delegate: ReloadLikeProtocol?
    // var passDeletedConfessionDelegate: PassConfessionFromPreviousVCDeletionProtocol?
    var passReloadCommentConfessionDelegate: ReloadCommentFromPreviousVCProtocal?
    
    
    var dimView: UIView!
    
    var confessionVC: ConfessionVC?
    //var aniView: LOTAnimationView?
    
    var deleteMode: Int?
    
    var refreshControl: UIRefreshControl!
    
    var animateTableView = true
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        dimView = UIView(frame: (UIApplication.shared.keyWindow?.frame)!)
        dimView.backgroundColor = UIColor.black
        dimView.alpha = 0.5
        
        tableView.delegate = self
        tableView.dataSource = self
        

        
        let rightBarItem = UIBarButtonItem(image: #imageLiteral(resourceName: "option"), style: .plain, target: self, action: "optionTapped")
        rightBarItem.tintColor = UIColor(red: 0/255, green: 105/255, blue: 192/255, alpha: 1)
        self.navigationItem.setRightBarButton(rightBarItem, animated: true)
        self.navigationItem.title = "Comments";
        
//        self.navigationController?.navigationBar.barTintColor = UIColor.white
//        self.navigationItem.leftBarButtonItem?.tintColor = PURPLE
        self.navigationController?.navigationBar.tintColor = PURPLE
        self.navigationController?.navigationBar.titleTextAttributes = [ NSFontAttributeName: UIFont(name: "HelveticaNeue-Bold", size: 16)!]
        self.navigationController?.navigationBar.layer.borderColor = UIColor(red: 226/255, green: 228/255, blue: 232/255, alpha: 1).cgColor
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title:"", style:.plain, target:nil, action: "")
        
        self.navigationController?.navigationBar.titleTextAttributes = [ NSForegroundColorAttributeName: PURPLE]
        
        
        tableView.keyboardDismissMode = .interactive
        //        tableView.alwaysBounceVertical = true
        tableView.estimatedRowHeight = 200
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedSectionHeaderHeight = 400
        tableView.sectionHeaderHeight = UITableViewAutomaticDimension
        //        tableView.allowsMultipleSelectionDuringEditing = true
        
        refreshControl = UIRefreshControl()
        refreshControl?.backgroundColor = UIColor.clear
        refreshControl?.tintColor = UIColor.lightGray
        refreshControl?.addTarget(self, action: "refresh", for: UIControlEvents.valueChanged)
        //        refreshControl?.beginRefreshing()
        tableView.addSubview(refreshControl!)
        
        
        let longPressGesture:UILongPressGestureRecognizer = UILongPressGestureRecognizer(target: self, action: #selector(self.handleLongPress(_:)))
        longPressGesture.minimumPressDuration = 0.5
        self.tableView.addGestureRecognizer(longPressGesture)
        
        
        //        if let animationView = LOTAnimationView(name: "data") {
        //            animationView.frame = CGRect(x: 0, y: 0, width: 50, height: 50)
        //            animationView.center = self.view.center
        //            animationView.contentMode = .scaleAspectFill
        //            animationView.loopAnimation = true
        //            animationView.animationSpeed = 0.8
        //            self.aniView = animationView
        //
        //        }
        
        
        
        getUserDetails()
        initFooter()
        //        setupKeyboardObserver()
        
        
        
        fetchConfession()
        fetchComments()
        
        

        
        
        
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
       UIApplication.shared.statusBarStyle = .default
        self.navigationController?.navigationBar.tintColor = PURPLE
        self.navigationItem.title = "Comments";
        self.navigationController?.navigationBar.titleTextAttributes = [ NSForegroundColorAttributeName: PURPLE]

        setupKeyboardObserver()
        //scrollToBottom()
        check = 0
        deleteMode = DELETE_MODE_BEFORE_POSTED
        
        animateTVFromLike = false
        reloadTable()
        displayNoInternetConnection()
//        self.tableView.reloadData()
        
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationController?.view.backgroundColor = .clear
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        removeKeyboardObserver()
        
        if inputContainerView.inputTextField.isFirstResponder {
            inputContainerView.inputTextField.resignFirstResponder()
        }
        check = 0

        // self.tabBarController?.tabBar.frame = originalFrame!
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        
    }
    
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidAppear(true)
        DataService.ds.REF_CONFESSIONS.child((self.confessionId)!).removeAllObservers()
    }
    
    func handleLongPress(_ longPressGestureRecognizer: UILongPressGestureRecognizer) {
        
        if longPressGestureRecognizer.state == UIGestureRecognizerState.began {
            
            let touchPoint = longPressGestureRecognizer.location(in: self.tableView)
            if let indexPath = tableView.indexPathForRow(at: touchPoint) {
                
                // your code here, get the row for the indexPath or do whatever you want
                var comment = commments[indexPath.row]
             
                if !comment.deleted {
                    
                    selectedComment = comment
                    displayOption(comment: comment)
                }
                
            }
        }
    }
    
    func getUserDetails() {
        DataService.ds.REF_USERS.child((Auth.auth().currentUser?.uid)!).observe(.value, with: { (snapshot) in
            if let snapshots = snapshot.children.allObjects as? [DataSnapshot] {
                //print("\(snapshot.value)")
                
                if let userDict = snapshot.value as? Dictionary<String, AnyObject> {
                    let key = snapshot.key
                    self.user = User(userKey: key, userData: userDict)
                }
            }
            
        })
    }
    
    func displayNoInternetConnection() {
        
        if !DataService.ds.connectedToNetwork() {
            if (self.refreshControl?.isRefreshing)! {
                self.refreshControl?.endRefreshing()
            }
            
            if self.actInd.isAnimating {
                self.actInd.stopAnimating()
            }
            
            self.navigationController?.popToRootViewController(animated: true)
            
        }
    }
    
    //    func textFieldDidChange(_ textField: UITextField) {
    //
    //        if (textField.text?.characters.count)! > 0 && !(textField.text?.trimmingCharacters(in: .whitespaces).isEmpty)! {
    ////            enableSendButton(tf: textField)
    //            print("enable")
    ////            inputContainerView.inputTextField.enablesReturnKeyAutomatically = true
    //        } else {
    //            print("disable")
    ////            inputContainerView.inputTextField.enablesReturnKeyAutomatically = false
    ////            disableSendButton(tf: textField)
    //        }
    //    }
    
    func displayEmptyTxtFieldAlert() {
        
        
        ErrorAlert().createAlert(title: "Reminder", msg: "Comment can't be blank", object: self)
        
        
        
        //        let alertController = UIAlertController(title: "Reminder", message: "Comment can't be blank", preferredStyle: .alert)
        //
        //        alertController.addAction(UIAlertAction(title: "Ok", style: .cancel) { (action:UIAlertAction!) in
        //
        //            // println("you have pressed the Cancel button");
        //        })
        //
        //        self.present(alertController, animated: true, completion: nil)
        
        
        
    }
    
    
    func enableSendButton(tf: UITextField) {
        //        self.inputContainerView.sendButton.tintColor = UIColor(red: 33/255, green: 150/255, blue: 243/255, alpha: 1)
        //        self.inputContainerView.sendButton.isEnabled = true
        inputContainerView.inputTextField.enablesReturnKeyAutomatically = true
    }
    
    func disableSendButton(tf: UITextField) {
        //        self.inputContainerView.sendButton.tintColor = UIColor(red: 158/255, green: 158/255, blue: 158/255, alpha: 0.7)
        //        self.inputContainerView.sendButton.isEnabled = false
        inputContainerView.inputTextField.enablesReturnKeyAutomatically = false
    }
    
    func setupKeyboardObserver() {
        self.notificationEnable = true
        NotificationCenter.default.addObserver(self, selector: #selector(handleKeyBoardDidShow), name: NSNotification.Name.UIKeyboardDidShow, object: nil)
        
    }
    
    func removeKeyboardObserver() {
        self.notificationEnable = false
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardDidShow, object: nil)
    }
    
    
    func handleKeyBoardDidShow() {
        
        if commments.count > 0 {
            
            let indexPath = NSIndexPath(item: commments.count - 1, section: 0)
            // self.tableView?.scrollToRow(at: indexPath as IndexPath, at: .top, animated: true)
            
        }
        
        
        
        
    }
    
    
    
    func optionTapped() {
        
        if isKeyboardShowing() {
            inputContainerView.inputTextField.resignFirstResponder()
        }
        
        let alert = UIAlertController()
        
        alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel, handler:{ (UIAlertAction)in
            print("User click Dismiss button")
            
        }))
        
        
        if self.confession?.author == (Auth.auth().currentUser!.uid) {
            
            if self.confession?.confessionKey != DISCUSSION {
                alert.addAction(UIAlertAction(title: "Delete", style: .destructive , handler:{ (UIAlertAction)in
                    self.displayAlert()
                }))
            } else {
                
                if self.user.email == ADMIN_EMAIL {
                    alert.addAction(UIAlertAction(title: "Delete", style: .destructive , handler:{ (UIAlertAction)in
                        self.displayAlert()
                    }))
                }
            }
            
            
        } else {
            
            alert.addAction(UIAlertAction(title: "Hide this post", style: .default , handler:{ (UIAlertAction)in
                self.hideThisPost()
            }))
            
            alert.addAction(UIAlertAction(title: "Report", style: .destructive , handler:{ (UIAlertAction)in
                self.displayReportAlertConfession(confession: self.confession!)
            }))
            
            
        }
        
        self.present(alert, animated: true, completion: nil)
    }
    
    
    
    func displayAlert() {
        
        
        
        let popup = PopupDialog(title: "Warning", message: "Are you sure?", image: nil)
        
        let buttonOne = DestructiveButton(title: "Delete") {
            
            
            self.inputContainerView.isHidden = true
            //            UIApplication.shared.keyWindow?.addSubview(self.dimView)
            //            UIApplication.shared.keyWindow?.addSubview(self.aniView!)
            //            self.aniView?.play()
            //            SVProgressHUD.show(withStatus: "Deleting")
            
            //            DataService.ds.REF_CONFESSIONS.child((self.confession?.confessionKey)!).removeValue()
            //            DataService.ds.REF_USERS.child((FIRAuth.auth()?.currentUser!.uid)! ).child("likes").child((self.confession?.confessionKey)!).removeValue()
            
            //self.deleteConfession(confessionKey: (self.confession?.confessionKey)!, completion: () -> ())
            
            
            self.deleteConfession(confessionKey: (self.confession?.confessionKey)!, completion: {
                SVProgressHUD.showSuccess(withStatus: "Deleted!")
                self.deleteCompletion()
            })
            // self.navigationController?.popViewController(animated: true)
            
            
            
            let stack = self.navigationController?.viewControllers
            
            if self.navigationController?.viewControllers.first is ConfessionVC {
                print("reload in Confession vc from comment vc")
                let rootVC = self.navigationController?.viewControllers.first as? ConfessionVC
                rootVC?.reloadDeletedConfessionFromPreviousVC(confession: self.confession!)
            }
            
            if self.navigationController?.viewControllers.first is SelfConfessionVC {
                print("reload in self Confession vc from comment vc")
                let rootVC = self.navigationController?.viewControllers.first as? SelfConfessionVC
                rootVC?.reloadDeletedConfessionFromPreviousVC(confession: self.confession!)
            }
            
            
            SVProgressHUD.show(withStatus: "Deleting")
            // let rootVC = self.navigationController?.viewControllers.first as! ConfessionVC
            //rootVC.reloadDeletedConfessionFromPreviousVC(confession: self.confession!)
            
            
            //self.passDeletedConfessionDelegate?.reloadDeletedConfessionFromPreviousVC(confession: self.confession!)
            //            let stack = self.navigationController?.viewControllers
            //            let rootVC = self.navigationController?.viewControllers.first as! ConfessionVC
            //            rootVC.reloadCommentFromPreviousVC(confession: self.confession!)
            //            print(stack?.count)
            //            if ((stack?.count)! > 1) {
            //                var count = stack?.count
            //                while count! > 1  && count != 2 {
            //                    let previousController = stack?[count!-2] as! HashtagVC
            //                    //previousController.delegateRecurse = self
            //                    previousController.reloadDeletedConfessionFromPreviousVC(confession: self.confession!)
            //                    count = count! - 1
            //                }
            //
            //            }
            //            self.navigationController?.popViewController(animated: true)
        }
        
        let buttonTwo = CancelButton(title: "Cancel") {
            
        }
        
        popup.addButtons([buttonTwo,buttonOne])
        popup.buttonAlignment = .horizontal
        
        // Present dialog
        self.present(popup, animated: true, completion: nil)
        
        
        
        //        let alertController = UIAlertController(title: "Are you sure?", message: "", preferredStyle: .alert)
        //
        //        alertController.addAction(UIAlertAction(title: "Cancel", style: .cancel) { (action:UIAlertAction!) in
        //
        //            // println("you have pressed the Cancel button");
        //        })
        //
        //        alertController.addAction(UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction!) in
        //
        //            print("deleted")
        //
        //
        //
        //
        //
        //        })
        //
        //
        //        self.present(alertController, animated: true, completion: nil)
        
        
    }
    
    func transaction() {
        //let ref2 = DataService.ds.REF_CONFESSIONS.child((self.confession?.confessionKey)!)
        let ref2 = DataService.ds.REF_NOTIFICATION.child((Auth.auth().currentUser?.uid)!)
        ref2.runTransactionBlock({ (currentData) -> TransactionResult in
            print("hehe")
            if var confessionDict = currentData.value as? Dictionary<String, AnyObject> {
                
                //                var likes = confessionDict["likes"] as? Int
                //
                //                if likes != nil {
                //                    print(likes)
                //                }
                
                
                currentData.value = confessionDict
                print("scueess")
                return TransactionResult.success(withValue: currentData)
            }
            
            return TransactionResult.success(withValue: currentData)
        }) { (error, commited, snap) in
            
        }
    }
    
    
    func deleteConfession(confessionKey: String, completion: @escaping () -> ()) {
        //let ref2 = DataService.ds.REF_CONFESSIONS.child(confessionKey)
        let ref2 = DataService.ds.REF_NOTIFICATION.child((Auth.auth().currentUser?.uid)!)
        ref2.runTransactionBlock({ (currentData) -> TransactionResult in
            print("hehe")
            if var confessionDict = currentData.value as? Dictionary<String, AnyObject> {
                
                currentData.value = confessionDict
                print("scueess")
                return TransactionResult.success(withValue: currentData)
            }
            
            return TransactionResult.success(withValue: currentData)
        }) { (error, commited, snap) in
            
            if commited {
                DataService.ds.REF_CONFESSIONS.child((self.confession?.confessionKey)!).removeValue()
                
                var updateObj = [String:AnyObject]()
                let ref = DB_BASE
                if !(self.confession?.hashDict.isEmpty)! {
                    
                    for hash in (self.confession?.hashDict)! {
                        updateObj["/hashtag/\(hash.key)/\(confessionKey)"] = NSNull()
                        
                    }
                    
                }
                
                //updateObj["/notification2/\((FIRAuth.auth()?.currentUser?.uid)!)/\(confessionKey)"] = NSNull()
                
                //REF_CONFESSION_NOTIFICATION can be deleted
                //                DataService.ds.REF_CONFESSION_NOTIFICATION.child(self.confession!.confessionKey).observeSingleEvent(of: .value, with: { (snapshot) in
                //                    print("out")
                //                    //if snapshot.hasChildren() {
                //                        if let snapshots = snapshot.children.allObjects as? [FIRDataSnapshot] {
                //                            for snap in snapshots {
                //                                //current uid means this confession author
                //                                print("noti")
                //                                //updateObj["/notification/\((FIRAuth.auth()?.currentUser?.uid)!)/\(snap.key)"] = NSNull()
                //                            }
                //                        }
                //                    //}
                //
                //                })
                
                updateObj["/confession-liker/\((self.confession?.confessionKey)!)"] = NSNull()
                //updateObj["/confession-notification/\(self.confession!.confessionKey)"] = NSNull()
                updateObj["/confessions/\(confessionKey)"] = NSNull()
                updateObj["/confession-comments/\(confessionKey)"] = NSNull()
                updateObj["/uni-confession/\(self.confession!.university)/\(confessionKey)"] = NSNull()
                updateObj["/user-confession/\(self.confession!.author)/\(confessionKey)"] = NSNull()
                
                if let c = self.confession?.category {
                    updateObj["/category/\(c)/\(confessionKey)"] = NSNull()
                }
                updateObj["confession-commenter/\(confessionKey)"] = NSNull()
                updateObj["confession-commenter-commentID/\((self.confession?.confessionKey)!)"] = NSNull()
                updateObj["/FOR_REPLY_DELETION/\(self.confession!.confessionKey)/"] = NSNull()
                
                DB_BASE.child("FOR_REPLY_DELETION").child(self.confession!.confessionKey).observeSingleEvent(of: .value, with: { (snapshot) in
                    
                    if snapshot.hasChildren() {
                        if let snapshots = snapshot.children.allObjects as? [DataSnapshot] {
                            
                            for snap in snapshots {
                                //current uid means this confession author
                                
                                var userUid = snap.key
                                
                                if let grandChildren = snap.children.allObjects as? [DataSnapshot] {
                                    for snap in grandChildren {
                                        var notiId = snap.key
                                        print("even here")
                                        updateObj["/notification/\(userUid)/\(notiId)"] = NSNull()
                                    }
                                }
                                
                            }
                        }
                    }
                    
                })
                
                
                
                
                
                //                DataService.ds.REF_CONFESSION_LIKER.child((self.confession?.confessionKey)!).observeSingleEvent(of: .value, with: { (snapshot2) in
                //                    if snapshot2.hasChildren() {
                //                        if let snapshots2 = snapshot2.children.allObjects as? [FIRDataSnapshot] {
                //
                //                            //if snapshots.count != 0 {
                //                            for snap2 in snapshots2 {
                //                                print("even here 2")
                //                                updateObj["/liker-confession-notification/\(snap2.key)/\((self.confession?.confessionKey)!)"] = NSNull()
                //                            }
                //                        }
                //                    }
                //
                //                })
                
                
                
                updateObj["confession-liker-notification/\((self.confession?.confessionKey)!)"] = NSNull()
                DataService.ds.REF_CONFESSION_COMMENTS.child((self.confession?.confessionKey)!).observeSingleEvent(of: .value, with: { (snapshot3) in
                    print("first call")
                    if let snapshots3 = snapshot3.children.allObjects as? [DataSnapshot] {
                        
                        for snap3 in snapshots3 {
                            print("first for loop")
                            var commentID = snap3.key
                            updateObj["/comments/\(commentID)"] = NSNull()
                            updateObj["/comment-liker/\(commentID)"] = NSNull()
                            updateObj["user-comment/\(self.confession!.author)/\(commentID)"] = NSNull()
                            
                            DB_BASE.child("comment-liker").child(commentID).observeSingleEvent(of: .value, with: { (snapshot4) in
                                print("second call")
                                print(snapshot4)
                                if let snapshots4 = snapshot4.children.allObjects as? [DataSnapshot] {
                                    print(snapshots4)
                                    for snap in snapshots4 {
                                        print("second for loop")
                                        var liker = snap.key
                                        
                                        updateObj["/liker-comment-notification/\(liker)/\(commentID)"] = NSNull()
                                        
                                        
                                    }
                                }
                                
                            })
                            
                            if let commentDict = snap3.value as? Dictionary<String, AnyObject> {
                                var commentAuthor = commentDict["author"] as! String
                                DB_BASE.child("comment-commentAuthor-notification").child(commentID).child(commentAuthor).child("notifications").observeSingleEvent(of: .value, with: { (snap) in
                                    print("third call")
                                    if let snapshots = snap.children.allObjects as? [DataSnapshot] {
                                        
                                        for s in snapshots {
                                            print("third for loop")
                                            var notificationID = s.key
                                            
                                            updateObj["/notification/\(commentAuthor)/\(notificationID)"] = NSNull()
                                            updateObj["/comment-commentAuthor-notification/\(commentID)"] = NSNull()
                                        }
                                    }
                                })
                            }
                        }
                    }
                    
                    DB_BASE.child("confession-commenter-commentID").child((self.confession?.confessionKey)!).observeSingleEvent(of: .value, with: { (snapshots) in
                        if snapshots.hasChildren() {
                            if let snapshot = snapshots.children.allObjects as? [DataSnapshot] {
                                for snap in snapshot {
                                    
                                    
                                    if let children = snap.children.allObjects as? [DataSnapshot] {
                                        for s in children {
                                            
                                            updateObj["commenter-commentID-notiID/\(snap.key)/\(s.key)"] = NSNull()
                                            updateObj["user-comment/\(snap.key)/\(s.key)"] = NSNull()
                                        }
                                    }
                                    
                                }
                                
                                
                                ref.updateChildValues(updateObj)
                                
                                self.deleteImageInStorage()
                                
                                completion()
                            }
                        } else {
                            ref.updateChildValues(updateObj)
                            self.deleteImageInStorage()
                            completion()
                        }
                        
                        
                    })
                })
                
            }
        }
        
        
        //Timer.scheduledTimer(timeInterval: 2, target: self, selector: #selector(self.deleteCompletion), userInfo: nil, repeats: false)
        
        
        
    }
    
    func deleteImageInStorage() {
        
        var imgUrl = (self.confession?.imageUrl)!
        SDImageCache.shared().removeImage(forKey: (imgUrl as NSString) as String!)
        
        if self.confession?.imageUrl != "NoPicture" {
            let ref = Storage.storage().reference(forURL: imgUrl)
            ref.delete(completion: { (error) in
                if error != nil {
                    print("there is an error deleting picture in fb storage")
                } else {
                    print("delete picture successfully in fb storage")
                }
            })
        }
    }
    
    func deleteCompletion() {
        
        
        self.inputContainerView.isHidden = false
        
        self.navigationController?.popToRootViewController(animated: true)
        
    }
    
    func hideThisPost() {
        
        
        let popup = PopupDialog(title: "Warning", message: "Once you hide it you can't unhide", image: nil)
        
        let buttonOne = DestructiveButton(title: "Hide") {
            var updateObj = [String:AnyObject]()
            let ref = DB_BASE
            updateObj["/userBlock-uni-confession/\((Auth.auth().currentUser?.uid)!)/\(self.uniName!)/\(self.confession!.confessionKey)/blocked"] = true as AnyObject
            ref.updateChildValues(updateObj)
            
            let stack = self.navigationController?.viewControllers
            
            if self.navigationController?.viewControllers.first is ConfessionVC {
                
                let rootVC = self.navigationController?.viewControllers.first as? ConfessionVC
                rootVC?.reloadDeletedConfessionFromPreviousVC(confession: self.confession!)
            }
            
            self.navigationController?.popToRootViewController(animated: true)
        }
        
        let buttonTwo = CancelButton(title: "Cancel") {
            
        }
        
        popup.addButtons([buttonTwo,buttonOne])
        popup.buttonAlignment = .horizontal
        
        // Present dialog
        self.present(popup, animated: true, completion: nil)
        
        
        
        //        let alertController = UIAlertController(title: "Are you sure?", message: "Once you hide it you can't unhide", preferredStyle: .alert)
        //
        //        alertController.addAction(UIAlertAction(title: "Cancel", style: .cancel) { (action:UIAlertAction!) in
        //
        //            // println("you have pressed the Cancel button");
        //        })
        //
        //        alertController.addAction(UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction!) in
        //
        //        })
        //
        //        self.present(alertController, animated: true, completion: nil)
        
        
    }
    
    func displayReportAlertConfession(confession: Confession) {
        
        let alert = UIAlertController()
        
        
        alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel, handler:{ (UIAlertAction)in
            
        }))
        
        alert.addAction(UIAlertAction(title: "Report Spam", style: UIAlertActionStyle.default, handler:{ (UIAlertAction)in
            self.submitReport(confession: confession, reason: "Spam")
            
        }))
        
        alert.addAction(UIAlertAction(title: "Report Inappropriate", style: UIAlertActionStyle.default, handler:{ (UIAlertAction)in
            self.submitReport(confession: confession, reason: "Inappropriate")
            
        }))
        
        alert.addAction(UIAlertAction(title: "Report Bullying", style: UIAlertActionStyle.default, handler:{ (UIAlertAction)in
            self.submitReport(confession: confession, reason: "Bullying")
            
        }))
        
        self.present(alert, animated: true, completion: nil)
    }
    
    func submitReport(confession: Confession, reason: String) {
        
        //        let firebaseReport = DataService.ds.REF_REPORTS.childByAutoId()
        //
        //        let reportValues: Dictionary<String, AnyObject> = ["userId": FIRAuth.auth()?.currentUser?.uid as AnyObject, "Reason": reason as AnyObject]
        //
        //        DataService.ds.REF_CONFESSION_REPORTS.child(confession.confessionKey).updateChildValues([firebaseReport.key: true])
        //        DataService.ds.REF_REPORTS.child(firebaseReport.key).updateChildValues(reportValues)
        //
        //        SVProgressHUD.setMinimumDismissTimeInterval(0.5)
        //        SVProgressHUD.showSuccess(withStatus: "Thanks for reporting this comment")
        
        confession.postRef.observeSingleEvent(of: .value, with: { (snapshot) in
            if snapshot.exists() {
                let firebaseReport = DataService.ds.REF_CONFESSION_REPORTS.child(confession.confessionKey).childByAutoId()
                
                let reportValues: Dictionary<String, AnyObject> = ["reporter": Auth.auth().currentUser?.uid as AnyObject, "Reason": reason as AnyObject]
                
                firebaseReport.updateChildValues(reportValues)
                
                
                SVProgressHUD.setMinimumDismissTimeInterval(0.5)
                SVProgressHUD.showSuccess(withStatus: "Done!")
            } else {
                ErrorAlert().createAlert(title: "Can't Report", msg: "This confession has been deleted", object: self)
            }
        })
        
        
    }
    
    func getTotalDiscussionComments() {
        //        DataService.ds.REF_Discussion.child((self.discussion?.discussionKey)!).child("comments").observeSingleEvent(of: .value, with: { (snapshot) in
        //
        //
        //            //if let snapshots = snapshot.children.allObjects as? [FIRDataSnapshot] {
        //
        //            self.totalComments = snapshot.value as! Int
        //            if self.totalComments == 0 {
        //                self.refreshControl?.endRefreshing()
        //                self.commments.removeAll()
        //                self.commentDict.removeAll()
        //                self.attemptToReloadTable()
        //
        //            }
        //            //}
        //
        //        })
    }
    
    func getTotalComments() {
        DataService.ds.REF_CONFESSIONS.child(self.confessionId!).child("comments").observeSingleEvent(of: .value, with: { (snapshot) in
            
            
            //if let snapshots = snapshot.children.allObjects as? [FIRDataSnapshot] {
            
            self.totalComments = snapshot.value as! Int
            if self.totalComments == 0 {
                self.refreshControl?.endRefreshing()
                self.commments.removeAll()
                self.commentDict.removeAll()
                self.attemptToReloadTable()
                
            }
            //}
            
        })
    }
    
    
    
    func fetchConfession() {
        SVProgressHUD.show()
        
        DataService.ds.REF_CONFESSIONS.child(self.confessionId!).observeSingleEvent(of: .value, with: {(snapshot) in
            let confession = Confession(confessionKey: snapshot.key, postData: snapshot.value as! Dictionary<String, AnyObject>)
            self.confession = confession
            
            
            
            //self.attemptToReloadTable()
            
            //self.passReloadCommentConfessionDelegate?.reloadCommentFromPreviousVC(confession: self.confession!, addComment: true)
            let stack = self.navigationController?.viewControllers
            
            if self.navigationController?.viewControllers.first is ConfessionVC {
                let rootVC = self.navigationController?.viewControllers.first as? ConfessionVC
                
                if self.confession?.confessionKey != DISCUSSION {
                    rootVC?.reloadCommentFromPreviousVC(confession: self.confession!, addComment: true, isDiscussion: false)
                } else {
                    rootVC?.reloadCommentFromPreviousVC(confession: self.confession!, addComment: true, isDiscussion: true)
                }
                
            }
            // let rootVC = self.navigationController?.viewControllers.first as! ConfessionVC
            var count = stack?.count
            //rootVC.reloadCommentFromPreviousVC(confession: self.confession!, addComment: true)
            if (stack?.count)! > 2 {
                
                while count! > 1 && count != 2 {
                    // print("wtf \(count!)")
                    if stack?[count!-2] is HashtagVC {
                        let previousController = stack?[count!-2] as! HashtagVC
                        
                        previousController.reloadCommentFromPreviousVC(confession: confession, addComment: true)
                        
                    }
                    
                    if stack?[count!-2] is SelfConfessionVC {
                        let previousController = stack?[count!-2] as! SelfConfessionVC
                        
                        previousController.reloadCommentFromPreviousVC(confession: confession)
                        
                    }
                    count = count! - 1
                    
                }
            }
            
            
            
        })
    }
    
    
    //    var anonyCommentDict = [String: Comment]()
    //    var anonyComments = [Comment]()
    
    func fetchComments() {
        getTotalComments()
        
        DataService.ds.REF_CONFESSION_COMMENTS.child(self.confessionId!).queryLimited(toFirst: UInt(self.numberOfObjectsinArray)).observeSingleEvent(of: .value, with: { (snapshot) in
            // DataService.ds.REF_COMMENTS.queryOrdered(byChild: "confessionKey").queryEqual(toValue: self.confessionId!).queryLimited(toFirst: UInt(self.numberOfObjectsinArray)).observeSingleEvent(of: .value, with: { (snapshot) in
            
            self.commments = []
            self.commentIDs = []
            self.commentDict.removeAll()
            //self.currentComments = []
            
            
            if let snapshots = snapshot.children.allObjects as? [DataSnapshot] {
                
                for snap in snapshots {
                    
                    
                    if let commentDict = snap.value as? Dictionary<String, AnyObject> {
                        
                        
                        let key = snap.key
                        let comment = Comment(commentKey: key, postData: commentDict)
                        //
                        //                        if( comment.anonymous ) {
                        //                            self.anonyCommentDict[key] = comment
                        //                        } else {
                        self.commentDict[key] = comment
                        //                        }
                        
                        
                        
                        
                        
                        
                        
                    }
                }
                
                self.attemptToReloadTable()
                
            }
            
            
        })
        
        DataService.ds.REF_COMMENTS.observe(.childRemoved, with: { (snapshot) in
            
            self.commentDict.removeValue(forKey: snapshot.key)
            self.totalComments = self.totalComments - 1
            
        })
    }
    
    func attemptToReloadTable() {
        //        self.timer?.invalidate()
        //
//        Timer.scheduledTimer(timeInterval: 0.5, target: self, selector: #selector(self.reloadTable), userInfo: nil, repeats: false)
        
        reloadTable()
        
    }
    
    func reloadTable() {
        
        
        //
        //        self.anonyComments = Array(self.anonyCommentDict.values)
        //
        //        self.anonyComments.sort(by: { (c1, c2) -> Bool in
        //
        //            return (c1.time.intValue) < (c2.time.intValue)
        //        })
        //
        
        
        self.commments = Array(self.commentDict.values)
        
        self.commments.sort(by: { (c1, c2) -> Bool in
            
            return (c1.time.intValue) < (c2.time.intValue)
        })
        
        //Put all the keys into a separate arrays
        for com in commments {
            print("in order \(com.commentKey)")
            commentIDs.append(com.commentKey)
        }
        
        if actInd.isAnimating {
            self.actInd.stopAnimating()
        }
        
        
        
        
        if (refreshControl?.isRefreshing)! {
            self.refreshControl?.endRefreshing()
        }
        
        self.tableView.reloadData()
        self.tableView.isUserInteractionEnabled = true
        
        SVProgressHUD.dismiss()
        
        if animateTableView {
            if animateTVFromLike {
                UIView.animate(views: tableView.visibleCells, animations: animations, completion: {
                })
            }
            
        }
        
        animateTVFromLike = true
        animateTableView = true
        
        
        
    }
    
    func refresh() {
        print("refresh")
        self.tableView.isUserInteractionEnabled = false
        Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(reloadDataWhenPulled), userInfo: nil, repeats: false)
        
        
    }
    
    func reloadDataWhenPulled() {
        
        if (self.refreshControl?.isRefreshing)! {
            
            // reloadTable()
            //  self.tableView.reloadData()
            fetchDatas()
            // self.refreshControl.endRefreshing()
            
            self.tableView.isUserInteractionEnabled = true
            
        }
        
        //        if (self.refreshControl?.isRefreshing)! {
        //
        //            fetchDatas()
        //            if refreshControl?.isRefreshing {
        //                //self.refreshControl?.endRefreshing()
        //            }
        //
        ////            if self.commments.count == 0 {
        ////                self.refreshControl?.endRefreshing()
        ////
        ////            }
        //            self.tableView.isUserInteractionEnabled = true
        //        }
        
    }
    
    func fetchDatas() {
        
        //        if checkExisted() {
        //
        //            fetchConfession()
        //            fetchComments()
        //        } else {
        //            print("what")
        //        }
        //
        checkExisted()
        
        
        
    }
    
    var authorGender: String?
    var authorFaculty: String?
    var userName: String?
    var commentedBefore: Bool = false
    var anonymousId: String?
    var commentValues: Dictionary<String, AnyObject>?
    var dict = [String: Bool]()
    
    func checkExisted() -> Bool {
        var exist = false
        self.confession?.postRef.observeSingleEvent(of: .value, with: { (snapshot) in
            if snapshot.exists() {
                exist = true
                self.fetchConfession()
                self.fetchComments()
            } else {
                
                let popup = PopupDialog(title: "Unvailable Confession", message: "Could not perform action", image: nil)
                
                
                let buttonTwo = CancelButton(title: "OK") {
                    if (self.refreshControl?.isRefreshing)! {
                        self.refreshControl?.endRefreshing()
                    }
                    
                    self.dismiss(animated: true, completion: nil)
                    
                }
                
                popup.addButtons([buttonTwo])
                popup.buttonAlignment = .horizontal
                
                // Present dialog
                self.present(popup, animated: true, completion: nil)
                
                //                let alert = UIAlertController(title: "Unvailable Confession", message: "Could not perform action", preferredStyle: .alert)
                //                //alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
                //                alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: { (action) in
                //                    if (self.refreshControl?.isRefreshing)! {
                //                        self.refreshControl?.endRefreshing()
                //                    }
                //
                //                    self.dismiss(animated: true, completion: nil)
                //
                //                }))
                //
                //                self.present(alert, animated: true, completion: nil)
                
                //ErrorAlert().createAlert(title: "Something went wrong", msg: "Could not perform action", object: self)
                
                
                
            }
        })
        
        
        return exist
    }
    
    func anonymousModeTapped(){
        print("you become anon")
        
        
        if confession?.author == Auth.auth().currentUser?.uid {
            inputContainerView.anonBtn.shake()
            var murmur = Murmur(title: "Can't reply anonymously because you wrote this post.")
            murmur.backgroundColor = UIColor.red
            murmur.titleColor = UIColor.white
            
            // Show and hide a message after delay
            Whisper.show(whistle: murmur, action: .show(2.5))
        } else {
            inputContainerView.anonBtn.pulsate()
            if replyAnonymously {
                
                inputContainerView.anonBtn.setImage(#imageLiteral(resourceName: "anonyModeOff"), for: .normal)
                replyAnonymously = false
            } else {
                inputContainerView.anonBtn.setImage(#imageLiteral(resourceName: "anonyModeOn"), for: .normal)
                var murmur = Murmur(title: "Replying Anonymously")
                murmur.backgroundColor = UIColor(red: 18/255, green: 102/255, blue: 215/255, alpha: 1)
                murmur.titleColor = UIColor.white
                
                // Show and hide a message after delay
                Whisper.show(whistle: murmur, action: .show(1.5))
                replyAnonymously = true
            }
        }
        
        
        
        
    }
    
    func commentSend()  {
        //        disableSendButton()
        self.confession?.postRef.observeSingleEvent(of: .value, with: { (snapshot) in
            if snapshot.exists() {
                
                self.dict["key"] = true
                self.createComment()
                
                //                DataService.ds.REF_USERS.child((FIRAuth.auth()?.currentUser?.uid)!).observeSingleEvent(of: .value, with: {(snapshot) in
                //                    if let snapshots = snapshot.children.allObjects as? [FIRDataSnapshot] {
                //
                //                        self.authorGender = snapshot.childSnapshot(forPath: "gender").value as! String!
                //                        self.authorFaculty = snapshot.childSnapshot(forPath: "course").value as! String!
                //                        self.userName = snapshot.childSnapshot(forPath: "userName").value as! String!
                //
                //                        //self.getTotalComments()
                //                       // self.a()
                //
                //                        //                    self.cc()
                //                                            //self.newMethod()
                //                        self.createComment()
                //
                //                        //Timer.scheduledTimer(timeInterval: 0.5, target: self, selector: #selector(self.b), userInfo: nil, repeats: false)
                //
                //
                //
                //                    }
                //
                //                })
                
                
                //       delayScrolling()
                
            } else {
                ErrorAlert().createAlert(title: "Something went wrong", msg: "Could not perform action", object: self)
                //                self.enableSendButton()
            }
        })
        
        
        
        
        
    }
    
    
    var tempTotalComments: Int?
    var tempComments = [Comment]()
    
    var handle1: UInt = 0
    var handle2: UInt = 0
    
    func cc() {
        DataService.ds.REF_COMMENTS.queryOrdered(byChild: "confessionKey").queryEqual(toValue: self.confessionId!).observeSingleEvent(of: .value, with: { (snapshot) in
            //observe(.value, with: { (snapshot) in
            
            
            if let snapshots = snapshot.children.allObjects as? [DataSnapshot] {
                self.tempTotalComments = snapshots.count
                
            }
            
        })
        
        self.a()
    }
    
    
    
    func a() {
        DataService.ds.REF_COMMENTS.queryOrdered(byChild: "confessionKey").queryEqual(toValue: self.confessionId!).queryLimited(toFirst: UInt(self.numberOfObjectsinArray)).observeSingleEvent(of: .value, with: { (snapshot) in
            // .observe(.value, with: { (snapshot) in
            //
            
            //self.commments = []
            self.tempComments = []
            //self.currentComments = []
            
            
            if let snapshots = snapshot.children.allObjects as? [DataSnapshot] {
                
                for snap in snapshots {
                    
                    
                    if let commentDict = snap.value as? Dictionary<String, AnyObject> {
                        
                        
                        let key = snap.key
                        let comment = Comment(commentKey: key, postData: commentDict)
                        
                        
                        self.commentDict[key] = comment
                        
                        
                        self.c()
                        
                        
                        
                    }
                }
                
            }
            
            
        })
    }
    
    func c() {
        
        
        self.tempComments = Array(self.commentDict.values)
        
        self.tempComments.sort(by: { (c1, c2) -> Bool in
            
            return (c1.time.intValue) < (c2.time.intValue)
        })
        
        
    }
    
    var totalCom: Int?
    func newMethod() {
        let ref = DataService.ds.REF_CONFESSIONS.child(self.confessionId!).child("comments")
        ref.runTransactionBlock({ (currentData) -> TransactionResult in
            print("aloha")
            self.totalCom = currentData.value as? Int
            
            if self.totalCom == nil {
                self.totalCom = 0
            }
            
            return TransactionResult.success(withValue: currentData)
        }) { (error, commited, snapshot) in
            
            if commited {
                print("syn \(self.totalCom)")
                self.b()
            } else {
                //fail
            }
        }
        
    }
    
    func createComment() {
        
        var uid = (Auth.auth().currentUser?.uid)!
        let timestamp = NSNumber(integerLiteral: Int(NSDate().timeIntervalSince1970))
        var anonymousID = 0
        var commentors: Int?
        
        
        self.commentValues = ["comment": self.inputContainerView.inputTextField.text as AnyObject, "likes": 0 as AnyObject, "author": Auth.auth().currentUser?.uid as AnyObject, "userName": self.user.userName as AnyObject, "time": timestamp, "confessionKey": self.confessionId as AnyObject, "authorGender": self.user.gender as AnyObject, "authorFaculty": self.user.course as AnyObject, "userLike": self.dict as AnyObject, "profilePicUrl" : self.user.profilePicUrl as AnyObject, "replyTo": "" as AnyObject , "replyCommentContent": "" as AnyObject, "deleted": false as AnyObject, "anonymous": replyAnonymously as AnyObject, "authorUniversity": self.user.university as AnyObject, "university": self.confession?.university as AnyObject]
        
        let firebaseComment = DataService.ds.REF_CONFESSION_COMMENTS.childByAutoId()
        
        if let values = self.commentValues {
            
            let comment = Comment(commentKey: firebaseComment.key, postData: values)
            
            DataService.ds.REF_CONFESSION_COMMENTS.child(self.confessionId!).child(firebaseComment.key).updateChildValues(values)
            DataService.ds.REF_User_Comment.child((Auth.auth().currentUser?.uid)!).child(firebaseComment.key).updateChildValues(values)
            DataService.ds.REF_COMMENTS.child(firebaseComment.key).updateChildValues(values)
            
            
            self.confession?.comments = (self.confession?.comments)! + 1
            self.confession?.adjustComments(addComment: true, uid: (Auth.auth().currentUser?.uid)!, commentKey: firebaseComment.key, comment: comment)
            //NotificationCenter.default.post(name: NSNotification.Name(rawValue: "reloadAddComment"), object: nil)
            
            
            self.commments.append(comment)
            self.commentDict[firebaseComment.key] = comment
            
            
            
            self.recursiveReloadPreviousVC(newComment: comment,addCom: true)
            
            
            self.inputContainerView.inputTextField.text = nil
            //            self.disableSendButton()
            
            
            self.inputContainerView.inputTextField.resignFirstResponder()
            
            self.tableView.reloadData()
            self.deleteMode = DELETE_MODE_AFTER_POSTED
        }
        
        //        DataService.ds.REF_USERS.child(uid).observeSingleEvent(of: .value, with: { (snapshot) in
        //            //if let snap = snapshot.children.allObjects as? [FIRDataSnapshot] {
        //                    if let userDict = snapshot.value as? Dictionary<String, AnyObject> {
        //                        var user = User(userKey: snapshot.key, userData: userDict)
        //
        //                        self.commentValues = ["comment": self.inputContainerView.inputTextField.text as AnyObject, "likes": 0 as AnyObject, "author": FIRAuth.auth()?.currentUser?.uid as AnyObject, "userName": self.userName as AnyObject, "time": timestamp, "confessionKey": self.confessionId as AnyObject, "authorGender": self.authorGender as AnyObject, "authorFaculty": self.authorFaculty as AnyObject, "userLike": self.dict as AnyObject, "profilePicUrl" : user.profilePicUrl as AnyObject, "replyTo": "" as AnyObject]
        //
        //                        let firebaseComment = DataService.ds.REF_CONFESSION_COMMENTS.childByAutoId()
        //
        //                        if let values = self.commentValues {
        //
        //                            let comment = Comment(commentKey: firebaseComment.key, postData: values)
        //
        //                            DataService.ds.REF_CONFESSION_COMMENTS.child(self.confessionId!).child(firebaseComment.key).updateChildValues(values)
        //                            DataService.ds.REF_COMMENTS.child(firebaseComment.key).updateChildValues(values)
        //
        //
        //                            self.confession?.comments = (self.confession?.comments)! + 1
        //                            self.confession?.adjustComments(addComment: true, uid: (FIRAuth.auth()?.currentUser?.uid)!, commentKey: firebaseComment.key, comment: comment)
        //                            //NotificationCenter.default.post(name: NSNotification.Name(rawValue: "reloadAddComment"), object: nil)
        //
        //
        //                            self.commments.append(comment)
        //                            self.commentDict[firebaseComment.key] = comment
        //
        //
        //
        //                            self.recursiveReloadPreviousVC(addCom: true)
        //
        //
        //                            self.inputContainerView.inputTextField.text = nil
        //                            self.disableSendButton()
        //
        //
        //                            self.inputContainerView.inputTextField.resignFirstResponder()
        //
        //                            self.tableView.reloadData()
        //                            self.deleteMode = DELETE_MODE_AFTER_POSTED
        //
        //
        //                        }
        //                    }
        //
        //            //}
        //        })
    }
    
    func reloadComentFromReplyVC(confession: Confession, addComment: Bool, comment: Comment) {
        self.commments.append(comment)
        self.commentDict[comment.commentKey] = comment
        self.recursiveReloadPreviousVC(newComment: comment, addCom: addComment)
        self.reloadTable()
    }
    
    func b() {
        print("how many comments in this confess \(self.totalComments)")
        
        let timestamp = NSNumber(integerLiteral: Int(NSDate().timeIntervalSince1970))
        var anonymousID = 0
        var commentors: Int?
        
        if self.totalCom == 0 && self.confession?.author == Auth.auth().currentUser?.uid {
            anonymousID = 0
            commentValues = ["comment": self.inputContainerView.inputTextField.text as AnyObject, "likes": 0 as AnyObject, "author": Auth.auth().currentUser?.uid as AnyObject, "time": timestamp, "confessionKey": self.confessionId as AnyObject, "authorGender": authorGender as AnyObject, "authorFaculty": authorFaculty as AnyObject, "userLike": dict as AnyObject, "anonymousId": String(anonymousID) as AnyObject]
        } else if self.totalCom == 0 && self.confession?.author != Auth.auth().currentUser?.uid {
            anonymousID = 1
            
            commentValues = ["comment": self.inputContainerView.inputTextField.text as AnyObject, "likes": 0 as AnyObject, "author": Auth.auth().currentUser?.uid as AnyObject, "time": timestamp, "confessionKey": self.confessionId as AnyObject, "authorGender": authorGender as AnyObject, "authorFaculty": authorFaculty as AnyObject, "userLike": dict as AnyObject, "anonymousId": String(anonymousID) as AnyObject]
            
            // createComment(anonymousID: anonymousID)
            
        } else if self.totalCom! > 0 {
            
            let ref = DataService.ds.REF_CONFESSIONS.child(self.confessionId!).child("userComment").child((Auth.auth().currentUser?.uid)!)
            ref.observeSingleEvent(of: .value, with: { (snap) in
                if snap.exists() {
                    self.commentedBefore = true
                    anonymousID = snap.value as! Int
                    print("commented before \(snap.value )")
                    
                    print("before \(anonymousID)")
                    self.commentValues = ["comment": self.inputContainerView.inputTextField.text as AnyObject, "likes": 0 as AnyObject, "author": Auth.auth().currentUser?.uid as AnyObject, "time": timestamp, "confessionKey": self.confessionId as AnyObject, "authorGender": self.authorGender as AnyObject, "authorFaculty": self.authorFaculty as AnyObject, "userLike": self.dict as AnyObject,  "anonymousId": String(anonymousID) as AnyObject]
                    
                    //self.createComment(anonymousID: anonymousID)
                    
                    
                }  else {
                    self.commentedBefore = false
                    
                    let ref2 = DataService.ds.REF_CONFESSIONS.child(self.confessionId!).child("userComment")
                    ref2.observeSingleEvent(of: .value, with: { (snap) in
                        if let snapshots = snap.children.allObjects as? [DataSnapshot] {
                            commentors = snapshots.count
                            print("didnt comment before, count is \(commentors)")
                            
                            print("after \(commentors!)")
                            self.commentValues = ["comment": self.inputContainerView.inputTextField.text as AnyObject, "likes": 0 as AnyObject, "author": Auth.auth().currentUser?.uid as AnyObject, "time": timestamp, "confessionKey": self.confessionId as AnyObject, "authorGender": self.authorGender as AnyObject, "authorFaculty": self.authorFaculty as AnyObject, "userLike": self.dict as AnyObject,  "anonymousId": String(commentors!) as AnyObject]
                            anonymousID = commentors!
                            // self.createComment(anonymousID: anonymousID)
                        }
                    })
                    
                    
                }
                
                
            })
            
            
            //            for comment in self.tempComments {
            //                if comment.author == (FIRAuth.auth()?.currentUser?.uid)! {
            //                    anonymousId = comment.anonymousId
            //                    print("you commented before")
            //                    commentedBefore = true
            //                    break
            //
            //                }
            //            }
            
            //            if commentedBefore {
            //                print("before \(anonymousID)")
            //               // anonymousID = Int(anonymousId!)!
            //                //commentValues = ["comment": self.inputContainerView.inputTextField.text as AnyObject, "likes": 0 as AnyObject, "author": FIRAuth.auth()?.currentUser?.uid as AnyObject, "time": timestamp, "confessionKey": self.confessionId as AnyObject, "authorGender": authorGender as AnyObject, "authorFaculty": authorFaculty as AnyObject, "userLike": dict as AnyObject,  "anonymousId": anonymousId as AnyObject]
            //                commentValues = ["comment": self.inputContainerView.inputTextField.text as AnyObject, "likes": 0 as AnyObject, "author": FIRAuth.auth()?.currentUser?.uid as AnyObject, "time": timestamp, "confessionKey": self.confessionId as AnyObject, "authorGender": authorGender as AnyObject, "authorFaculty": authorFaculty as AnyObject, "userLike": dict as AnyObject,  "anonymousId": String(anonymousID) as AnyObject]
            //
            //            } else {
            //                print("after \(anonymousID)")
            //                var myMax = 0
            //
            //                for comment in self.tempComments {
            //                    if ( Int(comment.anonymousId)! > myMax){myMax = Int(comment.anonymousId)!}
            //                }
            //
            //                print("max \(myMax)")
            //
            //               // commentValues = ["comment": self.inputContainerView.inputTextField.text as AnyObject, "likes": 0 as AnyObject, "author": FIRAuth.auth()?.currentUser?.uid as AnyObject, "time": timestamp, "confessionKey": self.confessionId as AnyObject, "authorGender": authorGender as AnyObject, "authorFaculty": authorFaculty as AnyObject, "userLike": dict as AnyObject,  "anonymousId": String(myMax + 1) as AnyObject]
            //                commentValues = ["comment": self.inputContainerView.inputTextField.text as AnyObject, "likes": 0 as AnyObject, "author": FIRAuth.auth()?.currentUser?.uid as AnyObject, "time": timestamp, "confessionKey": self.confessionId as AnyObject, "authorGender": authorGender as AnyObject, "authorFaculty": authorFaculty as AnyObject, "userLike": dict as AnyObject,  "anonymousId": String(myMax + 1) as AnyObject]
            //
            //                anonymousID = myMax + 1
            //
            //            }
            
            
        }
        
        //
        //
        //
        //        let firebaseComment = DataService.ds.REF_CONFESSION_COMMENTS.childByAutoId()
        //
        //        if let values = commentValues {
        //
        //            let comment = Comment(commentKey: firebaseComment.key, postData: values)
        //
        //            DataService.ds.REF_CONFESSION_COMMENTS.child(self.confessionId!).child(firebaseComment.key).updateChildValues(values)
        //            DataService.ds.REF_COMMENTS.child(firebaseComment.key).updateChildValues(values)
        //
        //
        //            self.confession?.comments = (self.confession?.comments)! + 1
        //            self.confession?.adjustComments(addComment: true, uid: (FIRAuth.auth()?.currentUser?.uid)!, anonymousId: anonymousID)
        //            //NotificationCenter.default.post(name: NSNotification.Name(rawValue: "reloadAddComment"), object: nil)
        //
        //
        //            self.commments.append(comment)
        //            self.commentDict[firebaseComment.key] = comment
        //
        //
        //
        //            recursiveReloadPreviousVC(addCom: true)
        //
        //
        //            self.inputContainerView.inputTextField.text = nil
        //            self.disableSendButton()
        //
        //
        //            self.inputContainerView.inputTextField.resignFirstResponder()
        //
        //            self.tableView.reloadData()
        //            deleteMode = DELETE_MODE_AFTER_POSTED
        //
        //
        //        }
        
        
    }
    
    func reloadAllComments( vc: UINavigationController, confession: Confession, newComment: Comment, reloadComment: Bool) {
        for vc2 in vc.viewControllers {
            if vc2 is ConfessionVC {
                let confessionVC = vc2 as! ConfessionVC
                
                if confessionVC.confessionDict[confession.confessionKey] != nil {
                    print("YES ITS CONFESSION VC")
                    //                    confessionVC.confessions.append(confession)
                    //                    confessionVC.attemptToReloadTable()
                    confessionVC.reloadLikeFromHashtagVC(confession: confession, reloadLike: reloadComment)
                }
                
                
            }
            
            if vc2 is HashtagVC {
                let hashVC = vc2 as! HashtagVC
                if hashVC.confessionDict[confession.confessionKey] != nil {
                    hashVC.confessionDict[confession.confessionKey] = confession
                    hashVC.attemptToReloadTable()
                }
                
                
            }
            
            if vc2 is Comment3VC {
                let commentVC = vc2 as! Comment3VC
                if commentVC.confessionId == confession.confessionKey {
                    commentVC.confession = confession
                    commentVC.commentDict[newComment.commentKey] = newComment
//                    commentVC.attemptToReloadTable()
                }
                
                
            }
            
            if vc2 is OtherProfileVC {
                let profileVC = vc2 as! OtherProfileVC
                if profileVC.confessionDict[confession.confessionKey] != nil {
                    print("welala")
                    profileVC.confessionDict[confession.confessionKey] = confession
                    profileVC.attemptToReloadTable()
                    //                    self.tableView.reloadData()
                }
                
                
            }
            
        }
        
        //        /* Reload search vc's hash vc confession from comment vc */
        //        let navVC = self.tabBarController?.viewControllers?[1] as! UINavigationController
        //
        //        for vcs in navVC.viewControllers {
        //
        //            if vcs is HashtagVC {
        //                print("reload in hashtag22")
        //                let hashVC = vcs as! HashtagVC
        //                if self.user?.university == confession.university {
        //                    hashVC.confessionDict[confession.confessionKey] = confession
        //                    hashVC.attemptToReloadTable()
        //                }
        //
        //            }
        //
        //        }
    }

    
    func recursiveReloadPreviousVC(newComment: Comment, addCom: Bool) {
        
        animateTableView = false
        
        let vc = self.tabBarController?.viewControllers?[0] as! UINavigationController
        reloadAllComments(vc: vc, confession: self.confession!, newComment: newComment, reloadComment: addCom)
        
        let vc2 = self.tabBarController?.viewControllers?[1] as! UINavigationController
        reloadAllComments(vc: vc2, confession: self.confession!, newComment: newComment, reloadComment: addCom)
        
        let vc3 = self.tabBarController?.viewControllers?[3] as! UINavigationController
        reloadAllComments(vc: vc3, confession: self.confession!, newComment: newComment,reloadComment: addCom)
        
        let vc4 = self.tabBarController?.viewControllers?[4] as! UINavigationController
        reloadAllComments(vc: vc4, confession: self.confession!, newComment: newComment, reloadComment: addCom)
        
//        var isRoot = false
//        var rootVC: ConfessionVC?
//
//
//
//
//
//        if DataService.ds.currentUser.userKey == confession?.author {
//            let navSelfVC = self.tabBarController?.viewControllers?[3] as! UINavigationController
//            for vcs in navSelfVC.viewControllers {
//                if vcs is SelfConfessionVC {
//                    let selfVC = vcs as! SelfConfessionVC
//
//                    selfVC.confessionDict[self.confession!.confessionKey] = confession
//                    //                    selfVC.attemptToReloadTable()
//                }
//
//            }
//        }
//
//
//
//
//        /* Reload comment in SearchVC - HashVC from ConfessionVC - CommentVC */
//        let navVC = self.tabBarController?.viewControllers?[1] as! UINavigationController
//
//        for vcs in navVC.viewControllers {
//            if vcs is HashtagVC {
//                let hashVC = vcs as! HashtagVC
//
//                if uniName == self.confession?.university && self.confession?.confessionKey != DISCUSSION {
//                    print("reload cooment")
//                    hashVC.confessionDict[self.confession!.confessionKey] = self.confession!
//                    hashVC.attemptToReloadTable()
//                }
//
//            }
//
//        }
//
//        /* Reload comment in ConfessionVC - HasVC from SearchVC - HashVC - CommentVC */
//        let firstNavVC = self.tabBarController?.viewControllers?[0] as! UINavigationController
//
//        for vcs in firstNavVC.viewControllers {
//            if vcs is ConfessionVC {
//                let conVC = vcs as! ConfessionVC
//                if uniName == self.confession?.university && self.confession?.confessionKey != DISCUSSION {
//                    conVC.confessionDict[self.confession!.confessionKey] = self.confession!
//                    conVC.attemptToReloadTable()
//                }
//
//            }
//
//            if vcs is HashtagVC {
//                let hashVC = vcs as! HashtagVC
//                if uniName == self.confession?.university && self.confession?.confessionKey != DISCUSSION {
//                    print("reload cooment")
//                    hashVC.confessionDict[self.confession!.confessionKey] = self.confession!
//                    hashVC.attemptToReloadTable()
//                }
//
//            }
//
//        }
//
//        let stack = self.navigationController?.viewControllers
//        if self.navigationController?.viewControllers.first is ConfessionVC {
//            rootVC = self.navigationController?.viewControllers.first as? ConfessionVC
//            isRoot = true
//        }
//        // let rootVC = self.navigationController?.viewControllers.first as! ConfessionVC
//        var count = stack?.count
//        if addCom == false {
//            if stack?.count == 2 {
//                //     NotificationCenter.default.post(name: NSNotification.Name(rawValue: "reloadMinusComment"), object: nil)
//                if isRoot {
//                    rootVC?.reloadTable()
//                }
//
//            } else if (stack?.count)! > 2 {
//
//                //this mode can be deleted since it is doing the same thing, just in case
//                if deleteMode == DELETE_MODE_BEFORE_POSTED {
//
//                    if isRoot {
//                        rootVC?.reloadTable()
//                    }
//                    // rootVC.reloadTable()
//                    //                    rootVC.version2(confession: self.confession!)
//                    //
//                    while count! > 1 && count != 2 {
//
//                        if stack?[count!-2] is HashtagVC {
//                            let previousController = stack?[count!-2] as! HashtagVC
//
//                            //                        previousController.version2(confession: self.confession!)
//                            previousController.reload()
//                        }
//
//                        if stack?[count!-2] is SelfConfessionVC {
//                            let previousController = stack?[count!-2] as! SelfConfessionVC
//
//                            //                        previousController.version2(confession: self.confession!)
//
//
//                            //   previousController.reloadCommentFromPreviousVC(confession: self.confession!, addComment: addCom)
//                            //if self.uniName == self.confession?.university {
//                            previousController.reloadView(confession: self.confession!)
//                            //}
//
//                        }
//
//                        count = count! - 1
//
//
//                    }
//                } else if deleteMode == DELETE_MODE_AFTER_POSTED {
//
//                    if isRoot {
//                        if self.uniName == self.confession?.university && self.confession?.confessionKey != DISCUSSION {
//                            rootVC?.reloadCommentFromPreviousVC(confession: self.confession!, addComment: addCom, isDiscussion: false)
//                        }
//
//                    }
//                    //                    rootVC.reloadCommentFromPreviousVC(confession: self.confession!, addComment: addCom)
//                    while count! > 1 && count != 2 {
//
//                        if stack?[count!-2] is HashtagVC {
//                            let previousController = stack?[count!-2] as! HashtagVC
//                            if self.uniName == self.confession?.university && self.confession?.confessionKey != DISCUSSION {
//                                previousController.reloadCommentFromPreviousVC(confession: self.confession!, addComment: addCom)
//                            }
//
//                        }
//
//                        if stack?[count!-2] is SelfConfessionVC {
//                            let previousController = stack?[count!-2] as! SelfConfessionVC
//
//                            //                        previousController.version2(confession: self.confession!)
//
//
//                            // previousController.reloadCommentFromPreviousVC(confession: self.confession!, addComment: addCom)
//                            //if self.uniName == self.confession?.university {
//                            previousController.reloadView(confession: self.confession!)
//                            //}
//
//                        }
//
//                        count = count! - 1
//
//                    }
//
//                }
//
//
//                //                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "reloadMinusComment"), object: nil)
//            }
//        } else {
//            if stack?.count == 2 {
//                //NotificationCenter.default.post(name: NSNotification.Name(rawValue: "reloadMinusComment"), object: nil)
//                if isRoot {
//                    rootVC?.reloadTable()
//                }
//
//            } else if (stack?.count)! > 2 {
//
//                if isRoot {
//                    if self.uniName == self.confession?.university && self.confession?.confessionKey != DISCUSSION {
//                        rootVC?.reloadCommentFromPreviousVC(confession: self.confession!, addComment: addCom, isDiscussion: false)
//                    }
//
//                }
//
//
//                while count! > 1 && count != 2 {
//                    if stack?[count!-2] is HashtagVC {
//                        let previousController = stack?[count!-2] as! HashtagVC
//                        if self.uniName == self.confession?.university && self.confession?.confessionKey != DISCUSSION {
//                            previousController.reloadCommentFromPreviousVC(confession: self.confession!, addComment: addCom)
//                        }
//
//                    }
//
//                    if stack?[count!-2] is SelfConfessionVC {
//                        let previousController = stack?[count!-2] as! SelfConfessionVC
//
//                        //                        previousController.version2(confession: self.confession!)
//                        //  previousController.reloadCommentFromPreviousVC(confession: self.confession!, addComment: addCom)
//                        //if self.uniName == self.confession?.university {
//                        previousController.reloadView(confession: self.confession!)
//                        //}
//
//                    }
//
//                    count = count! - 1
//
//                }
//            }
//
//
//        }
        
        

    }
    
    func delayScrolling() {
        Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(self.scrollToBottom), userInfo: nil, repeats: false)
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return  commments.count
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        if let c = self.confession {
            
            if c.confessionKey == DISCUSSION {
                let cell = tableView.dequeueReusableCell(withIdentifier: "DiscussionCell") as? DiscussionCell
                cell?.configureCell(confession: c)
                cell?.vc = self
                return cell
            } else {
                if c.imageUrl == NO_PIC {
                    print("haha")
                    let cell = tableView.dequeueReusableCell(withIdentifier: "MainCommentCell") as? MainCommentCell
                    cell?.vc = self
                    cell?.selectionStyle = .none
                    
                    cell?.configureCell(confession: c)
                    
                    
                    return cell
                } else if c.imageUrl != NO_PIC {
                    print("why u here")
                    let cell = tableView.dequeueReusableCell(withIdentifier: "MainCommentPicCell") as? MainCommentPicCell
                    cell?.commentVC = self
                    cell?.selectionStyle = .none
                    
                    print(c.caption)
                    print(c.imageUrl)
                    
                    //                if c.authorGender == MALE {
                    //                    cell?.profilePic.image = UIImage(named: "male")
                    //                } else if c.authorGender == FEMALE {
                    //                    cell?.profilePic.image = UIImage(named: "female")
                    //                }
                    
                    cell?.confessionPic.loadImageUsingCacheWithUrlStringWithoutIndex(imageUrl: c.imageUrl!)
                    
                    
                    
                    cell?.configureCell(confession: c)
                    
                    return cell
                    
                }
            }
            
            
        }
        
        
        
        
        
        
        return UITableViewCell()
        
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var state: String?
        
        
        
        
        // if let cell = tableView.dequeueReusableCell(withIdentifier: "CommentCell") as? CommentCell{
        if let cell = tableView.dequeueReusableCell(withIdentifier: "CommentCell", for: indexPath) as? CommentCell{
            let comment = self.commments[indexPath.row]
            
            cell.selectionStyle = .none
            cell.commentVC = self
            
            if comment.author != confession?.author {
                
                if comment.anonymous {
                    //default picture
                    if comment.authorGender == MALE {
                        cell.profilePic.image = #imageLiteral(resourceName: "ProfilePicMale")
                    } else {
                        cell.profilePic.image = #imageLiteral(resourceName: "ProfilePicFemale")
                    }
                    
                    cell.genderPic.isHidden = true
                } else {
                    
                    if comment.deleted {
                        cell.initialLbl.isHidden = true
                        cell.profilePic.image = UIImage(named: "ProfilePic")
                        
                    } else {
                        
                        if comment.profilePicUrl != "NoPicture" {
                            cell.profilePic.loadImageUsingCacheWithUrlString(imageUrl: comment.profilePicUrl, tv: self.tableView, index: indexPath)
                        } else {
                            //default picture
                            //                                cell.profilePic.image = UIImage(named: "ProfilePic")
                            var iniName = String((comment.userName[(comment.userName.startIndex)])).capitalized
                            cell.initialLbl.text = iniName
                            cell.profilePic.backgroundColor = ColorHelper().pickColor(alphabet: Character(iniName))
                            cell.initialLbl.isHidden = false
                            cell.profilePic.image = nil
                            
                        }
                    }
                    
                    
                    if comment.authorGender == MALE {
                        cell.genderPic.image = UIImage(named: "male")
                    } else if comment.authorGender == FEMALE {
                        cell.genderPic.image = UIImage(named: "female")
                    }
                    
                    cell.genderPic.isHidden = false
                    
                    
                }
                
                
                
            } else {
                
                if comment.deleted {
                    cell.initialLbl.isHidden = true
                    cell.profilePic.image = UIImage(named: "ProfilePic")
                } else {
                    if comment.authorGender == MALE {
                        cell.profilePic.image = UIImage(named: "male")
                    } else if comment.authorGender == FEMALE {
                        cell.profilePic.image = UIImage(named: "female")
                    }
                    
                    //                        if comment.profilePicUrl != NO_PIC {
                    //                            cell.profilePic.loadImageUsingCacheWithUrlString(imageUrl: comment.profilePicUrl, tv: self.tableView, index: indexPath)
                    //                        } else {
                    ////                            cell.profilePic.image = UIImage(named: "ProfilePic")
                    //                            var iniName = String((comment.userName[(comment.userName.startIndex)])).capitalized
                    //                            cell.initialLbl.text = iniName
                    //                            cell.profilePic.backgroundColor = ColorHelper().pickColor(alphabet: Character(iniName))
                    //                            cell.initialLbl.isHidden = false
                    //                            cell.profilePic.image = nil
                    //                        }
                    
                    if (confession?.anonymous) != nil {
                        
                        if (confession?.anonymous)! {
                            cell.genderPic.isHidden = true
                        } else {
                            cell.genderPic.isHidden = false
                        }
                    } else {
                        cell.genderPic.isHidden = false
                    }
                    
                }
                
                
                
            }
            
            
            if comment.comment.contains(find: "COMMENTID:") {
                print("yes")
                
                if let first = comment.comment.components(separatedBy: " ").first {
                    // Do something with the first component.
                    var firstPart = first.components(separatedBy: ":")
                    var id = firstPart[1]
                    print("yes \(id)")
                    
                    if( commentIDs.contains(id) ) {
                        
                        //                            let lvlIndex = find(commentIDs, id)
                        var lvlIndex = commentIDs.index(of: id)!
                        print("LVL INDEX \(lvlIndex)")
                        
                        
                        let newString = comment.comment.replace(target: first, withString: "@L\(lvlIndex+1)")
                        
                        cell.commentLabel.text = newString
                        
                    }
                }
                
            } else {
                cell.commentLabel.text = "\(comment.comment)"
            }
            
            if( comment.deleted ) {
                cell.commentLabel.text = "This comment has been deleted."
            }
            
            
            cell.configureCell(comment: comment, confession: confession!, levelID: indexPath.row)
            
            
            //                if comment.authorUniversity != comment.university {
            var shortUni = DataService.ds.convertUniNameToShort(uniName: comment.authorUniversity)
            //            print("short uni \(shortUni)")
            cell.facultyLabel.text = "\(shortUni) - \(comment.authorFaculty)"
            //                } else {
            //                    cell.facultyLabel.text = "\(comment.authorFaculty)"
            //                }
            //
            
            
            
            
            return cell
            
            //   }
        }
        
        
        
        return UITableViewCell()
        
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if indexPath.row == commments.count - 2 {
            //  self.tableView.tableFooterView = actInd
            // self.tableView.isUserInteractionEnabled = false
            //self.actInd.startAnimating()
            if totalComments > commments.count {
                self.tableView.tableFooterView = actInd
                self.actInd.startAnimating()
                
                self.timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(self.addMoreObjects), userInfo: nil, repeats: false)
                
            } else {
                
                self.tableView.isUserInteractionEnabled = true
                // self.actInd.stopAnimating()
                self.actInd.stopAnimating()
//                self.tableView.tableFooterView = UIView()
                
                self.tableView.tableFooterView =  UIView(frame: CGRect(x: 0, y: 0, width: 320, height: 60))
                
            }
            
        }
        
    }
    
    
    func displayOption(comment: Comment) {
        if isKeyboardShowing() {
            inputContainerView.inputTextField.resignFirstResponder()
        }
        
        let alert = UIAlertController()
        
        alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel, handler:{ (UIAlertAction)in
            
        }))
        
        if comment.author != (Auth.auth().currentUser!.uid) {
            
            alert.addAction(UIAlertAction(title: "Reply", style: UIAlertActionStyle.default, handler:{ (UIAlertAction)in
                self.replyTapped(comment: comment)
                
                
            }))
            
            
            alert.addAction(UIAlertAction(title: "Report", style: UIAlertActionStyle.destructive, handler:{ (UIAlertAction)in
                self.displayReportAlert(comment: comment)
                
                
            }))
            
            
        }
        
        
        
        if comment.author == (Auth.auth().currentUser!.uid) || self.confession?.author == (Auth.auth().currentUser!.uid) {
            alert.addAction(UIAlertAction(title: "Delete", style: .destructive , handler:{ (UIAlertAction)in
                self.displayDeleteAlert(comment: comment)
            }))
            
        }
        
        self.present(alert, animated: true, completion: nil)
        
        
    }
    
    var selectedComment: Comment?
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("click \(indexPath.row)")
        
        //        var comment = commments[indexPath.row]
        //        selectedComment = comment
        //        displayOption(comment: comment)
        
    }
    
    var commentLike: Bool!
    var selectedCommentID: String!
    
    func likersTapped(isComment: Bool, commentID: String) {
        self.commentLike = isComment
        self.selectedCommentID = commentID
        self.performSegue(withIdentifier: "FollowerVC", sender: nil)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let destinationVC = segue.destination as? ReplyVC {
            
            destinationVC.replyingComment = self.selectedComment
            destinationVC.confession = self.confession
            destinationVC.delegate = self
            
            
        }
        
        if let destinationVC = segue.destination as? OtherProfileVC {

            destinationVC.commentVC = self
            destinationVC.userID = self.selectedUserID
            
            
            
        }
        
        if let destinationVC = segue.destination as? FollowerVC {
            if commentLike {
                destinationVC.mode = "COMMENT"
            } else {
                destinationVC.mode = "CONFESSION"
            }
            
            destinationVC.confessionID = self.confessionId!
            destinationVC.commentID = self.selectedCommentID!
            destinationVC.userVisiting = DataService.ds.currentUser
            
            
        }
    }
    
    func setNavBarColor() {
        self.navigationController?.navigationBar.titleTextAttributes = [ NSForegroundColorAttributeName: PURPLE]
    }
    
    //    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
    //
    //        return true
    //
    //    }
    //
    //    override func tableView(_ tableView: UITableView, editActionsForRowAt: IndexPath) -> [UITableViewRowAction]? {
    //
    //        let comment = commments[editActionsForRowAt.row]
    //        var actionArray = [UITableViewRowAction]()
    //
    //        if comment.author == (FIRAuth.auth()?.currentUser!.uid)! || self.confession?.author == (FIRAuth.auth()?.currentUser!.uid)! {
    //
    //            let delete = UITableViewRowAction(style: .normal, title: "Delete") { action, index in
    //                print("delete button tapped \(index)")
    //                self.displayDeleteAlert(comment: comment, index: index.row)
    //
    //            }
    //
    //            actionArray.append(delete)
    //
    //        }
    //
    //        if comment.author != (FIRAuth.auth()?.currentUser!.uid)! {
    //
    //            let reply = UITableViewRowAction(style: .normal, title: "Reply") { action, index in
    //                print("reply button tapped \(index)")
    //                self.replyTapped(comment: comment)
    //            }
    //
    //            let report = UITableViewRowAction(style: .normal, title: "Report") { action, index in
    //                print("report button tapped \(index)")
    //                self.displayReportAlert(comment: comment)
    //
    //            }
    //
    //            actionArray.append(report)
    //            actionArray.append(reply)
    //
    //
    //
    //
    //        }
    //
    //
    //
    //
    //        return actionArray
    //
    //
    //    }
    
    
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        
        if isKeyboardShowing() {
            if self.notificationEnable! {
                removeKeyboardObserver()
                inputContainerView.inputTextField.resignFirstResponder()
            }
            
            
        } else {
            if self.notificationEnable! == false {
                setupKeyboardObserver()
            }
            
            
        }
        
        // self.inputAccessoryView?.resignFirstResponder()
        
        
    }
    
    @objc func deleteComment() {
        print("HAHA")
        self.inputContainerView.isHidden = true
        
        
        var updateObj = [String:AnyObject]()
        let ref = DB_BASE
        
        print(toBeDeletedComment.author)
        print(toBeDeletedComment.commentKey)
        
        updateObj["/user-comment/\(toBeDeletedComment.author)/\(toBeDeletedComment.commentKey)"] = NSNull()
        
        updateObj["/confession-commenter-commentID/\(self.confession?.confessionKey)/\(toBeDeletedComment.author)\(toBeDeletedComment.commentKey)"] = NSNull()
        
        
        
        //updateObj["/comments/\(comment.commentKey)/"] = NSNull()
        updateObj["/confession-comments/\((self.confession?.confessionKey)!)/\(toBeDeletedComment.commentKey)/deleted"] = true as AnyObject
        
        
        //        self.commentDict.removeValue(forKey: toBeDeletedComment.commentKey)
        
        
        
        var index = 0
        for c in self.commments {
            if c.commentKey == toBeDeletedComment.commentKey {
                
                toBeDeletedComment.deleted = true
                //                self.commments.remove(at: index)
                
                
                
            }
            index += 1
        }
        
        
        SVProgressHUD.dismiss()
        self.inputContainerView.isHidden = false
        
        self.confession?.adjustComments(addComment: false, uid: (Auth.auth().currentUser?.uid)!, commentKey: toBeDeletedComment.commentKey, comment: toBeDeletedComment)
        
        self.recursiveReloadPreviousVC(newComment: toBeDeletedComment, addCom: false)
        
        //        self.confession?.comments = (self.confession?.comments)! - 1
        
        self.tableView.reloadData()
        ref.updateChildValues(updateObj)
    }
    
    var toBeDeletedComment: Comment!
    
    func displayDeleteAlert(comment: Comment) {
        //        let alertController = UIAlertController(title: "Are you sure?", message: "", preferredStyle: .alert)
        //
        //        alertController.addAction(UIAlertAction(title: "Cancel", style: .cancel) { (action:UIAlertAction!) in
        //
        //        })
        
        var timer: Timer?
        toBeDeletedComment = comment
        
        let popup = PopupDialog(title: "Warning", message: "Are you sure you want to delete?", image: nil)
        popup.buttonAlignment = .horizontal
        let buttonOne = CancelButton(title: "Cancel") {
            
        }
        
        let buttonTwo = DestructiveButton(title: "Delete") {
            SVProgressHUD.show(withStatus: "Deleting")
            
            DataService.ds.REF_COMMENTS.child((comment.commentKey)).removeValue()
            self.timer = Timer.scheduledTimer(timeInterval: 2.0, target: self, selector: #selector(self.deleteComment), userInfo: nil, repeats: false)
            
            
            
            
            
            
            
            
            //from here old codes
            //            comment.commentRef.observeSingleEvent(of: .value, with: { (snapshot) in
            //                if snapshot.exists() {
            //                    SVProgressHUD.show(withStatus: "Deleting")
            //
            //                    self.inputContainerView.isHidden = true
            //
            //
            //                    var updateObj = [String:AnyObject]()
            //                    let ref = DB_BASE
            //
            //                    updateObj["/confession-commenter-commentID/\(self.confession?.confessionKey)/\(comment.author)\(comment.commentKey)"] = NSNull()
            //
            //                    DataService.ds.REF_COMMENTER_COMMENT_NOTIFICATION.child(comment.author).child(comment.commentKey).observeSingleEvent(of: .value, with: { (snapshot) in
            //                        if let snapshots = snapshot.children.allObjects as? [FIRDataSnapshot] {
            //                            for snap in snapshots {
            //                                print(snap.key)
            //                                print(snap.value)
            //                            }
            //                        }
            //                    })
            //
            //                    updateObj["/comments/\(comment.commentKey)/"] = NSNull()
            //                    updateObj["/confession-comments/\((self.confession?.confessionKey)!)/\(comment.commentKey)/"] = NSNull()
            //
            //
            //                    self.commentDict.removeValue(forKey: comment.commentKey)
            //
            //                    var index = 0
            //                    for c in self.commments {
            //                        if c.commentKey == comment.commentKey {
            //
            //                            self.commments.remove(at: index)
            //
            //
            //                        }
            //                        index += 1
            //                    }
            //
            //
            //                    SVProgressHUD.dismiss()
            //                    self.inputContainerView.isHidden = false
            //
            //                    self.confession?.adjustComments(addComment: false, uid: (FIRAuth.auth()?.currentUser?.uid)!, commentKey: comment.commentKey, comment: comment)
            //
            //                    self.recursiveReloadPreviousVC(addCom: false)
            //
            //                    self.confession?.comments = (self.confession?.comments)! - 1
            //
            //                    self.tableView.reloadData()
            //                    ref.updateChildValues(updateObj)
            
            //                    DataService.ds.REF_COMMENTS.child(comment.commentKey).removeValue(completionBlock: { (error, ref) in
            //
            //                        if error != nil {
            //                            print("failed to delete comment", error)
            //                            return
            //                        }
            //
            //
            //                        self.commentDict.removeValue(forKey: comment.commentKey)
            //
            //                        var index = 0
            //                        for c in self.commments {
            //                            if c.commentKey == comment.commentKey {
            //
            //                                self.commments.remove(at: index)
            //
            //
            //                            }
            //                            index += 1
            //                        }
            //
            //
            //
            //
            //
            //                        SVProgressHUD.dismiss()
            //                        self.inputContainerView.isHidden = false
            //
            //                        self.confession?.adjustComments(addComment: false, uid: (FIRAuth.auth()?.currentUser?.uid)!, commentKey: comment.commentKey, comment: comment)
            //
            //                        self.recursiveReloadPreviousVC(addCom: false)
            //
            //                        self.confession?.comments = (self.confession?.comments)! - 1
            //                        self.tableView.reloadData()
            //
            //                    })
            //
            //                    DataService.ds.REF_CONFESSION_COMMENTS.child((self.confession?.confessionKey)!).child(comment.commentKey).removeValue()
            
            
            //                } else {
            //                    ErrorAlert().createAlert(title: "Can't Delete", msg: "This comment has been deleted", object: self)
            //
            //                }
            //            })
            
        }
        
        
        popup.addButtons([buttonOne,buttonTwo])
        
        // Present dialog
        self.present(popup, animated: true, completion: nil)
        
        
        
        //        alertController.addAction(UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction!) in
        //
        //
        //
        //
        //
        //        })
        //
        //        self.present(alertController, animated: true, completion: nil)
        
        
    }
    
    func displayReportAlert(comment: Comment) {
        
        let alert = UIAlertController()
        
        
        alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel, handler:{ (UIAlertAction)in
            
        }))
        
        alert.addAction(UIAlertAction(title: "Report Spam", style: UIAlertActionStyle.default, handler:{ (UIAlertAction)in
            self.submitReport(comment: comment, reason: "Spam")
            
        }))
        
        alert.addAction(UIAlertAction(title: "Report Inappropriate", style: UIAlertActionStyle.default, handler:{ (UIAlertAction)in
            self.submitReport(comment: comment, reason: "Inappropriate")
            
        }))
        
        alert.addAction(UIAlertAction(title: "Report Bullying", style: UIAlertActionStyle.default, handler:{ (UIAlertAction)in
            self.submitReport(comment: comment, reason: "Bullying")
            
        }))
        
        self.present(alert, animated: true, completion: nil)
    }
    
    func submitReport(comment: Comment, reason: String) {
        
        comment.commentRef.observeSingleEvent(of: .value, with: { (snapshot) in
            if snapshot.exists() {
                let firebaseReport = DataService.ds.REF_COMMENT_REPORTS.child(comment.commentKey).childByAutoId()
                
                let reportValues: Dictionary<String, AnyObject> = ["reporter": Auth.auth().currentUser?.uid as AnyObject, "Reason": reason as AnyObject]
                
                firebaseReport.updateChildValues(reportValues)
                
                
                SVProgressHUD.setMinimumDismissTimeInterval(0.5)
                SVProgressHUD.showSuccess(withStatus: "Done!")
            } else {
                ErrorAlert().createAlert(title: "Can't Report", msg: "This comment has been deleted", object: self)
            }
        })
        
        
    }
    
    
    func replyTapped(comment: Comment) {
        
        comment.commentRef.observeSingleEvent(of: .value, with: { (snapshot) in
            if snapshot.exists() {
                self.performSegue(withIdentifier: "ReplyVC", sender: nil)
                
            } else {
                ErrorAlert().createAlert(title: "Can't Reply", msg: "This comment has been deleted", object: self)
            }
        })
        
        //performSegue(withIdentifier: "ReplyVC", sender: nil)
        //        inputContainerView.inputTextField.becomeFirstResponder()
        //
        //        if comment.author == confession?.author {
        //            inputContainerView.inputTextField.text = "@Student "
        //        } else {
        //            inputContainerView.inputTextField.text = "@\(comment.userName) "
        //        }
        //
        //        enableSendButton()
        //
        
    }
    
    
    override var inputAccessoryView: UIView? {
        get {
            
            return inputContainerView
        }
    }
    
    override var canBecomeFirstResponder: Bool {
        return true
    }
    
    func initFooter() {
        
        actInd = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.gray)
        actInd.frame = CGRect(x: 0, y: 0, width: 320, height: 35)
        actInd.hidesWhenStopped = true
        self.tableView.tableFooterView = actInd
        
        
    }
    
    func addMoreObjects() {
        animateTableView = false
        numberOfObjectsinArray += 6
        
        DataService.ds.REF_CONFESSION_COMMENTS.child(self.confessionId!).queryLimited(toFirst: UInt(self.numberOfObjectsinArray)).observeSingleEvent(of: .value, with: { (snapshot) in
            //  DataService.ds.REF_COMMENTS.queryOrdered(byChild: "confessionKey").queryEqual(toValue: self.confessionId!).queryLimited(toFirst: UInt(self.numberOfObjectsinArray)).observeSingleEvent(of: .value, with: { (snapshot) in
            
            
            //self.commments = []
            //self.currentComments = []
            
            
            if let snapshots = snapshot.children.allObjects as? [DataSnapshot] {
                
                for snap in snapshots {
                    
                    
                    if let commentDict = snap.value as? Dictionary<String, AnyObject> {
                        let key = snap.key
                        let comment = Comment(commentKey: key, postData: commentDict)
                        
                        
                        self.commentDict[key] = comment
                        
                        
                        
                        
                        
                        
                    }
                }
                self.attemptToReloadTable()
                
            }
            
            
            
            //   self.tableView.tableFooterView = UIView()
        })
    }
    

    var animateTVFromLike = true
    func reloadAllLikes( vc: UINavigationController, confession: Confession, reloadLike: Bool) {
        for vc2 in vc.viewControllers {
            if vc2 is ConfessionVC {
                let confessionVC = vc2 as! ConfessionVC
                
                if confessionVC.confessionDict[confession.confessionKey] != nil {
                    print("YES ITS CONFESSION VC")
                    //                    confessionVC.confessions.append(confession)
                    //                    confessionVC.attemptToReloadTable()
                    confessionVC.reloadLikeFromHashtagVC(confession: confession, reloadLike: reloadLike)
                }
                
                
            }
            
            if vc2 is HashtagVC {
                let hashVC = vc2 as! HashtagVC
                if hashVC.confessionDict[confession.confessionKey] != nil {
                    hashVC.confessionDict[confession.confessionKey] = confession
                    hashVC.attemptToReloadTable()
                }
                
                
            }
            
            if vc2 is Comment3VC {
                let commentVC = vc2 as! Comment3VC
                if commentVC.confessionId == confession.confessionKey {
                    commentVC.confession = confession
//                    commentVC.attemptToReloadTable()
                }
                
                
            }
            
            if vc2 is OtherProfileVC {
                let profileVC = vc2 as! OtherProfileVC
                if profileVC.confessionDict[confession.confessionKey] != nil {
                    print("welala")
                    profileVC.confessionDict[confession.confessionKey] = confession
                    profileVC.attemptToReloadTable()
                    //                    self.tableView.reloadData()
                }
                
                
            }
            
        }
        
        //        /* Reload search vc's hash vc confession from comment vc */
        //        let navVC = self.tabBarController?.viewControllers?[1] as! UINavigationController
        //
        //        for vcs in navVC.viewControllers {
        //
        //            if vcs is HashtagVC {
        //                print("reload in hashtag22")
        //                let hashVC = vcs as! HashtagVC
        //                if self.user?.university == confession.university {
        //                    hashVC.confessionDict[confession.confessionKey] = confession
        //                    hashVC.attemptToReloadTable()
        //                }
        //
        //            }
        //
        //        }
    }
  
    func reloadLike(confession: Confession, reloadLike: Bool) {
        animateTVFromLike = false
        
        let vc = self.tabBarController?.viewControllers?[0] as! UINavigationController
        reloadAllLikes(vc: vc, confession: confession, reloadLike: reloadLike)
        
        let vc2 = self.tabBarController?.viewControllers?[1] as! UINavigationController
        reloadAllLikes(vc: vc2, confession: confession, reloadLike: reloadLike)
        
        let vc3 = self.tabBarController?.viewControllers?[3] as! UINavigationController
        reloadAllLikes(vc: vc3, confession: confession, reloadLike: reloadLike)
        
        let vc4 = self.tabBarController?.viewControllers?[4] as! UINavigationController
        reloadAllLikes(vc: vc4, confession: confession, reloadLike: reloadLike)
        
        
        
    }
//    func reloadLike(confession: Confession, reloadLike: Bool) {
//        animateTVFromLike = false
//
//        //        delegate?.reloadLikeFromCommentVC(confession: confession, reloadLike: reloadLike)
//
//
//        // delegate?.reloadLikeFromHashtagVC(confession: confession, reloadLike: reloadLike)
//
//        /* reload confession vc and hash vc from search vc - comment vc*/
//        let vc = self.tabBarController?.viewControllers?[0] as! UINavigationController
//
//        for vc2 in vc.viewControllers {
//            if vc2 is ConfessionVC {
//                let confessionVC = vc2 as! ConfessionVC
//
//                if DataService.ds.currentVisitingUni == confession.university {
//                    print("reload here")
//                    confessionVC.reloadLikeFromHashtagVC(confession: confession, reloadLike: reloadLike)
//                } else {
//                    if confessionVC.confessionDict[confession.confessionKey] != nil {
//                        confessionVC.reloadLikeFromHashtagVC(confession: confession, reloadLike: reloadLike)
//                    }
//                }
//
//
//                if confession.confessionKey == DISCUSSION {
//                    confessionVC.mainConfession = confession
//                    confessionVC.attemptToReloadTable()
//                }
//
//
//
//            }
//
//            if vc2 is HashtagVC {
//                let hashVC = vc2 as! HashtagVC
//                if self.uniName == confession.university {
//                    hashVC.confessionDict[confession.confessionKey] = confession
//                    hashVC.attemptToReloadTable()
//                }
//
//
//            }
//
//
//            if vc2 is Comment3VC {
//                let commentVC = vc2 as! Comment3VC
//                if commentVC.confessionId == confession.confessionKey {
//                    commentVC.confession = confession
////                    commentVC.attemptToReloadTable()
//                }
//
//
//            }
//
//            if vc2 is OtherProfileVC {
//                let profileVC = vc2 as! OtherProfileVC
//                if profileVC.confessionDict[confession.confessionKey] != nil {
//
//                    profileVC.confessionDict[confession.confessionKey] = confession
//                    profileVC.attemptToReloadTable()
//
//                }
//
//
//            }
//        }
//
//
//        if DataService.ds.currentUser.userKey == confession.author {
//            let navSelfVC = self.tabBarController?.viewControllers?[3] as! UINavigationController
//
//            for vcs in navSelfVC.viewControllers {
//
//                if vcs is SelfConfessionVC {
//                    let selfVC = vcs as! SelfConfessionVC
//                    selfVC.confessionDict[confession.confessionKey] = confession
//                    //                selfVC.attemptToReloadTable()
//                }
//
//                if vcs is OtherProfileVC {
//                    let profileVC = vcs as! OtherProfileVC
//                    if profileVC.confessionDict[confession.confessionKey] != nil {
//
//                        profileVC.confessionDict[confession.confessionKey] = confession
//                        profileVC.attemptToReloadTable()
//
//                    }
//
//
//                }
//
//                if vcs is Comment3VC {
//                    let commentVC = vcs as! Comment3VC
//                    if commentVC.confessionId == confession.confessionKey {
//                        commentVC.confession = confession
////                        commentVC.attemptToReloadTable()
//                    }
//
//
//                }
//
//            }
//        }
//
//
//
//
//        /* Reload search vc's hash vc confession from comment vc */
//        let navVC = self.tabBarController?.viewControllers?[1] as! UINavigationController
//
//        for vcs in navVC.viewControllers {
//
//            if vcs is HashtagVC {
//                print("reload in hashtag22")
//                let hashVC = vcs as! HashtagVC
//                if self.uniName == confession.university {
//                    hashVC.confessionDict[confession.confessionKey] = confession
//                    hashVC.attemptToReloadTable()
//                }
//
//            }
//
//            if vcs is OtherProfileVC {
//                let profileVC = vcs as! OtherProfileVC
//                if profileVC.confessionDict[confession.confessionKey] != nil {
//
//                    profileVC.confessionDict[confession.confessionKey] = confession
//                    profileVC.attemptToReloadTable()
//
//                }
//
//
//            }
//
//            if vcs is Comment3VC {
//                let commentVC = vcs as! Comment3VC
//                if commentVC.confessionId == confession.confessionKey {
//                    commentVC.confession = confession
////                    commentVC.attemptToReloadTable()
//                }
//
//
//            }
//
//        }
//
//
//
//        self.confession = confession
//
//        /* Reload confession if it is root from Comment VC */
//        let stack = self.navigationController?.viewControllers
//        if self.navigationController?.viewControllers.first is ConfessionVC {
//            let rootVC = self.navigationController?.viewControllers.first as? ConfessionVC
//            if self.uniName == confession.university {
//                rootVC?.reloadLikeFromHashtagVC(confession: confession, reloadLike: reloadLike)
//            }
//
//        }
//
//
//        // let rootVC = self.navigationController?.viewControllers.first as! ConfessionVC
//        //rootVC.reloadLikeFromHashtagVC(confession: confession, reloadLike: reloadLike)
//        //  print("likes here \(confession.likes)")
//
//        if ((stack?.count)! > 1) {
//            var count = stack?.count
//            while count! > 1  && count != 2 {
//
//                /* Reload previous hashtag vc from Comment VC */
//                if stack?[count!-2] is HashtagVC {
//                    let previousController = stack?[count!-2] as! HashtagVC
//                    //previousController.delegateRecurse = self
//                    if self.uniName == confession.university {
//                        previousController.reloadLikeFromHashtagVC(confession: confession, reloadLike: reloadLike)
//                    }
//
//                }
//
//                /* Reload SelfConfessionVC from Comment VC */
//                if stack?[count!-2] is SelfConfessionVC {
//                    let previousController = stack?[count!-2] as! SelfConfessionVC
//                    //previousController.delegateRecurse = self
//                    previousController.reloadView(confession: confession)
//                }
//
//
//                count = count! - 1
//            }
//
//        }
//
//
//    }
    
    
    
    
    func scrollToBottom() {
        if self.commments.count > 0 {
            let indexPath = NSIndexPath(item: self.commments.count - 1 , section: 0)
            self.tableView.scrollToRow(at: indexPath as IndexPath, at: .bottom, animated: false)
            
        }
    }
    
    func showKeyboard() {
        if inputContainerView.inputTextField.isFirstResponder {
            inputContainerView.inputTextField.resignFirstResponder()
        } else {
            inputContainerView.inputTextField.becomeFirstResponder()
        }
        
        
    }
    
    func isKeyboardShowing() -> Bool {
        
        if inputContainerView.inputTextField.isFirstResponder {
            return true
        } else {
            return false
        }
    }
    
    var startingImgView: UIImageView?
    var startingFrame: CGRect?
    var blackBlackgroundView: UIView?
    
    func performZoomInForStartingImageView(startingImageView: UIImageView) {
        print("zoom in")
        
        startingFrame = startingImageView.superview?.convert(startingImageView.frame, to: nil)
        startingImgView = startingImageView
        startingImgView?.isHidden = true
        
        let zoomingImageView = UIImageView(frame: startingFrame!)
        zoomingImageView.contentMode = .scaleAspectFit
        zoomingImageView.clipsToBounds = true
        zoomingImageView.image = startingImageView.image
        zoomingImageView.isUserInteractionEnabled = true
        zoomingImageView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(handleZoomOut)))
        
        
        
        if let keyWindow = UIApplication.shared.keyWindow {
            blackBlackgroundView = UIView(frame: keyWindow.frame)
            blackBlackgroundView?.backgroundColor = UIColor.black
            blackBlackgroundView?.alpha = 0
            keyWindow.addSubview(blackBlackgroundView!)
            keyWindow.addSubview(zoomingImageView)
            
            
            UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
                self.blackBlackgroundView?.alpha = 1
                //self.inputContainerView.alpha = 0
                let height = (self.startingFrame?.height)! / (self.startingFrame?.width)! * keyWindow.frame.width
                
                //zoomingImageView.frame = CGRect(x: 0, y: 0, width: keyWindow.frame.width, height: height)
                zoomingImageView.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: UIScreen.main.bounds.size.height)
                
                zoomingImageView.center = keyWindow.center
                self.navigationController?.isNavigationBarHidden = true
                self.inputContainerView.isHidden = true
                
                
            }, completion: { (comepleted: Bool) in
                
            })
        }
        
    }
    
    override var prefersStatusBarHidden: Bool {
        return navigationController?.isNavigationBarHidden == true
    }
    
    override var preferredStatusBarUpdateAnimation: UIStatusBarAnimation {
        return UIStatusBarAnimation.none
    }
    
    func handleZoomOut(sender: UITapGestureRecognizer) {
        print("sup2")
        navigationController?.isNavigationBarHidden = false
        inputContainerView.isHidden = false
        
        //        if let zoomOutImageView = sender.view {
        //            zoomOutImageView.layer.cornerRadius = 2
        //            zoomOutImageView.contentMode = .scaleAspectFill
        //            zoomOutImageView.clipsToBounds = true
        //
        ////            UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
        ////                zoomOutImageView.frame = self.startingFrame!
        ////                self.blackBlackgroundView?.alpha = 0
        ////            }, completion: { (comepleted: Bool) in
        ////                self.startingImgView?.isHidden = false
        ////                zoomOutImageView.removeFromSuperview()
        ////            })
        //
        //
        //            UIView.animate(withDuration: 0.5, animations: {
        //                zoomOutImageView.frame = self.startingFrame!
        //                self.blackBlackgroundView?.alpha = 0
        //            }, completion: { (comepleted: Bool) in
        //                self.startingImgView?.isHidden = false
        //                zoomOutImageView.removeFromSuperview()
        //            })
        //
        //
        //        }
        
        if let zoomOutImageView = sender.view {
            zoomOutImageView.layer.cornerRadius = 2
            zoomOutImageView.contentMode = .scaleAspectFill
            zoomOutImageView.clipsToBounds = true
            
            UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: [], animations: {
                zoomOutImageView.frame = self.startingFrame!
                self.blackBlackgroundView?.alpha = 0
                //  self.inputContainerView.alpha = 1
                
            }, completion: { (comepleted: Bool) in
                self.startingImgView?.isHidden = false
                zoomOutImageView.removeFromSuperview()
            })
            
            
            
        }
        
        
    }
    
    var selectedUserID: String!
    func showProfile(authorID: String) {
        selectedUserID = authorID
        self.performSegue(withIdentifier: "OtherProfileVC", sender: nil)
//        let profileVC = PreviewProfileVC(nibName: "PreviewProfileVC", bundle: nil)
//
//        profileVC.configurePreview(authorID: authorID) {
//
//            let popup = PopupDialog(viewController: profileVC, buttonAlignment: .horizontal, transitionStyle: .zoomIn, gestureDismissal: false)
//
//            let buttonOne = CancelButton(title: "OK", height: 40) {
//
//            }
//
//            popup.addButtons([buttonOne])
//
//            self.present(popup, animated: true, completion: nil)
//        }
        
        
    }
}
