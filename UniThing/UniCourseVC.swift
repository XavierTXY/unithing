//
//  UniYearVC.swift
//  UniThing
//
//  Created by Xavier TanXY on 30/6/17.
//  Copyright © 2017 Xavier TanXY. All rights reserved.
//

import UIKit
import Firebase
import SVProgressHUD
import PopupDialog

protocol ReloadCourse {
    func reloadCourseFromUniCourseVC(courseName: String)
}

class UniCourseVC: UIViewController, UITableViewDelegate, UITableViewDataSource {

    var option: Int?
    
    var user: User?
    var universities = [University]()
    var courses = [String]()
    
    var uniFromUniversities: University?
    var delegate: ReloadCourse?

    
    var userUni: String?
    
    var ACT = [University]()
    var NATIONAL = [University]()
    var NSW = [University]()
    var NT = [University]()
    var QLD = [University]()
    var WA = [University]()
    var TAS = [University]()
    var VIC = [University]()
    var SA = [University]()
    
    var states = [[University]]()
    
    
    @IBOutlet weak var tableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.delegate = self
        tableView.dataSource = self
        tableView.tableFooterView = UIView()
        
        SVProgressHUD.setDefaultStyle(SVProgressHUDStyle.custom)
        SVProgressHUD.setMinimumDismissTimeInterval(1.0)
        
        SVProgressHUD.setBackgroundColor(UIColor(red: 255/255, green: 255/255, blue: 255/255, alpha: 1.0))
        SVProgressHUD.setRingThickness(3)
        self.navigationController?.navigationBar.titleTextAttributes = [ NSForegroundColorAttributeName: PURPLE]
        

       // tableView.rowHeight = UITableViewAutomaticDimension

        self.navigationItem.title = "University"
        setupDetails()
        
        if option == 1 {
            self.navigationItem.title = "Course"
            userUni = user?.university
            
            for u in universities {
                if userUni! == u.name {
                    uniFromUniversities = u
                    break
                }
            }
            
            setupCourse()
        } 
        
    }
    

    
    func setupDetails() {
        
        self.universities = DataService.ds.uniArray
        
        for uni in universities {
            switch uni.state {
            case "ACT":
                ACT.append(uni)
                break
            case "NATIONAL":
                NATIONAL.append(uni)
                break
            case "NSW":
                NSW.append(uni)
                break
            case "NT":
                NT.append(uni)
                break
            case "QLD":
                QLD.append(uni)
                break
            case "WA":
                WA.append(uni)
                break
            case "TAS":
                TAS.append(uni)
                break
            case "VIC":
                VIC.append(uni)
                break
            case "SA":
                SA.append(uni)
                break
            default:
                break
            }

        }
        
        states.append(ACT)
        states.append(NATIONAL)
        states.append(NSW)
        states.append(NT)
        states.append(QLD)
        states.append(SA)
        states.append(TAS)
        states.append(VIC)
        states.append(WA)
//
//
//        let filepath = Bundle.main.path(forResource: "university", ofType: "csv")
//
//        do {
//
//            let contents = try String(contentsOfFile: filepath!)
//
//            let columns:[String] = contents.components(separatedBy: "\n")
//
//            let count = columns.count - 1
//
//            for index in 0...count {
//                var datas:[String] = columns[index].components(separatedBy: ",")
//
//                if index == count {
//                    break
//                } else {
//
//
//
//                    var uni = University(id: Int(datas[0])!, name: datas[1], shortName: datas[2], state: datas[3])
//
//                    let count = datas.count - 1
//
//
//                    for index2 in 4...count {
//                        courses.append(datas[index2])
//                    }
//
//                    uni.courses = courses
//
//                    universities.append(uni)
//
//                    switch uni.state {
//                        case "ACT":
//                            ACT.append(uni)
//                            break
//                        case "NATIONAL":
//                            NATIONAL.append(uni)
//                            break
//                        case "NSW":
//                            NSW.append(uni)
//                            break
//                        case "NT":
//                            NT.append(uni)
//                            break
//                        case "QLD":
//                            QLD.append(uni)
//                            break
//                        case "WA":
//                            WA.append(uni)
//                            break
//                        case "TAS":
//                            TAS.append(uni)
//                            break
//                        case "VIC":
//                            VIC.append(uni)
//                            break
//                        case "SA":
//                            SA.append(uni)
//                            break
//                        default:
//                            break
//                    }
//                    courses.removeAll()
//                }
//
//            }
//
//
//            states.append(ACT)
//            states.append(NATIONAL)
//            states.append(NSW)
//            states.append(NT)
//            states.append(QLD)
//            states.append(SA)
//            states.append(TAS)
//            states.append(VIC)
//            states.append(WA)
//
//
//
//        } catch let err as NSError {
//            print(err.debugDescription)
//
//        }
    }
    
    func setupCourse() {
       // courses.removeAll()
//        courses = (uniFromUniversities?.courses)!
        self.courses = DataService.ds.courseArray
        
    }


    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        
        var retVal = ""
        
        if option == 0 {
            switch section {
            case 0:
                retVal = "ACT"
                break
            case 1:
                retVal = "NATIONAL"
                break
            case 2:
                retVal = "NSW"
                break
            case 3:
                retVal = "NT"
                break
            case 4:
                retVal = "QLD"
                break
            case 5:
                retVal = "SA"
                break
            case 6:
                retVal = "TAS"
                break
            case 7:
                retVal = "VIC"
                break
            case 8:
                retVal = "WA"
                break
            default:
                break
            }
        }

        return retVal
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        var retVal = 1
        if option == 0 {
            retVal = states.count
        } else if option == 1 {
            retVal = 1
        }
        return retVal
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        var retVal = 1
        if option == 0 {
            retVal = states[section].count
        } else if option == 1 {
            retVal = courses.count
        }
        return retVal
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        if option == 0 {
            
           // var uni: University?
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "UniCourseCell") as? UniCourseCell
            
            let uni = states[indexPath.section][indexPath.row]
            
            cell?.configureCell(detail: uni.name)
            cell?.selectionStyle = .none
            
            return cell!
        } else if option == 1{
            let cell = tableView.dequeueReusableCell(withIdentifier: "UniCourseCell") as? UniCourseCell
            
            let course = courses[indexPath.row]
            
            cell?.configureCell(detail: course)
            cell?.selectionStyle = .none
            return cell!
        }
       
        
        return UITableViewCell()
    }
    
    var uni: University?
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if option == 0 {
            
            uni = states[indexPath.section][indexPath.row]
            
            if uni!.name != self.user?.university {
                if self.user!.uniCount <= 0 {
                    ErrorAlert().createAlert(title: "Could not perform action", msg: "You can't change your university anymore", object: self)
                } else {
                    
                    self.performSegue(withIdentifier: "CourseVC", sender: nil)
                    //displayAlert(uni: uni!)

                }
            } else {
                self.navigationController?.popViewController(animated: true)
            }

           
        } else if option == 1 {
            
            SVProgressHUD.show()
            let course = courses[indexPath.row]
            
            DataService.ds.REF_USERS.child((Auth.auth().currentUser?.uid)!).updateChildValues(["course": course])
            self.updateCourseInDB(newCourse: course)


        }
    }
    
    
    func updateCourseInDB(newCourse: String) {
        let ref = DB_BASE
        
        var uid = (Auth.auth().currentUser?.uid)!
        var updateObj = [String:AnyObject]()
        DataService.ds.REF_User_Comment.child(uid).observeSingleEvent(of: .value, with: { (snapshot) in
            
            if let snapshots = snapshot.children.allObjects as? [DataSnapshot] {
                
                for snap in snapshots {
                    
                    
                    if let commentDict = snap.value as? Dictionary<String, AnyObject> {
                        
                        
                        let key = snap.key
                        let comment = Comment(commentKey: key, postData: commentDict)
                        
                        
                        
                        updateObj["confession-comments/\(comment.confessionKey)/\(comment.commentKey)/authorFaculty"] = newCourse as AnyObject
                        
                        
                        
                    }
                }
                
                //update confession
                DataService.ds.REF_User_Confession.child(uid).observeSingleEvent(of: .value, with: { (snapshot) in
                    
                    if let snapshots = snapshot.children.allObjects as? [DataSnapshot] {
                        
                        for snap in snapshots {
                            
                            
                            if let confessionDict = snap.value as? Dictionary<String, AnyObject> {
                                
                                
                                let key = snap.key
                                let confession = Confession(confessionKey: key, postData: confessionDict)
                                
                                
                                updateObj["user-confession/\(uid)/\(confession.confessionKey)/faculty"] = newCourse as AnyObject
                                updateObj["uni-confession/\((confession.university))/\(confession.confessionKey)/faculty"] = newCourse as AnyObject
                                updateObj["confessions/\(confession.confessionKey)/faculty"] = newCourse as AnyObject
                                
                                if let c = confession.category {
                                    updateObj["category/\(c)/\(confession.confessionKey)/faculty"] = newCourse as AnyObject
                                }
                                
                                if !confession.hashDict.isEmpty {
                                    
                                    for hash in confession.hashDict {
                                        updateObj["hashtag/\(hash.key)/\(confession.confessionKey)/faculty"] = newCourse as AnyObject
                                        
                                    }
                                    
                                }
                                
                                
                                
                                
                                
                            }
                        }
                        
                        //update here
                        ref.updateChildValues(updateObj)
                        self.delegate?.reloadCourseFromUniCourseVC(courseName: newCourse)
                        SVProgressHUD.dismiss()
                        self.navigationController?.popViewController(animated: true)
                        
                        
                    }
                    
                    
                })

                
            }
            
            
        })
    }

    func displayAlert(uni: University) {

        let popup = PopupDialog(title: "You are only allowed to change your university 3 times", message: "\(self.user!.uniCount) remainder left", image: nil)
        
        let buttonOne = CancelButton(title: "Cancel") {
            
        }
        
        let buttonTwo = DefaultButton(title: "Change", dismissOnTap: true) {
            SVProgressHUD.show()
            
            DataService.ds.REF_USERS.child((Auth.auth().currentUser?.uid)!).updateChildValues(["university": uni.name])
            DataService.ds.REF_USERS.child((Auth.auth().currentUser?.uid)!).updateChildValues(["universityShort": uni.shortName])
            DataService.ds.REF_USERS.child((Auth.auth().currentUser?.uid)!).updateChildValues(["uniCount": "\(self.user!.uniCount - 1)"])
            
            self.user!.uniCount = self.user!.uniCount - 1
            
            self.navigationController?.popViewController(animated: true)
        }
        
        popup.addButtons([buttonOne,buttonTwo])
        popup.buttonAlignment = .horizontal
        
        // Present dialog
        self.present(popup, animated: true, completion: nil)
//        
//        let alertController = UIAlertController(title: "You are only allowed to change your university 3 times", message: "\(self.user!.uniCount) remainder left", preferredStyle: .alert)
//        
//        alertController.addAction(UIAlertAction(title: "Cancel", style: .cancel) { (action:UIAlertAction!) in
//            
//        })
//        
//        
//        alertController.addAction(UIAlertAction(title: "Change", style: .default) { (action:UIAlertAction!) in
//
//            
//        })
//        
//
//        self.present(alertController, animated: true, completion: nil)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let destinationVC = segue.destination as? CourseVC {
            destinationVC.uni = self.uni
            destinationVC.user = self.user
        }
    }

}
