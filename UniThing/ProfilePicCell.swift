//
//  ProfilePicCell.swift
//  UniThing
//
//  Created by Xavier TanXY on 28/6/17.
//  Copyright © 2017 Xavier TanXY. All rights reserved.
//

import UIKit

class ProfilePicCell: UITableViewCell {

    @IBOutlet weak var profilePic: UIImageView!
    var profileVC: ProfileVC?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        let likeTap = UITapGestureRecognizer(target: self, action: #selector(profilePicTapped))
        likeTap.numberOfTapsRequired = 1
        profilePic.addGestureRecognizer(likeTap)
        profilePic.isUserInteractionEnabled = true
        
        profilePic.contentMode = .scaleAspectFill
        profilePic.layer.cornerRadius = profilePic.frame.width / 2
        profilePic.clipsToBounds = true
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func configureCell(user: User) {
        
    }
    
    func profilePicTapped(_ sender: UITapGestureRecognizer) {
        
        profileVC?.addPicTapped()
    }

}
