//
//  ReplyVC.swift
//  UniThing
//
//  Created by Xavier TanXY on 14/8/17.
//  Copyright © 2017 Xavier TanXY. All rights reserved.
//

import UIKit
import Firebase
import Whisper

protocol ReloadCommentFromReplyVC
{
    func reloadComentFromReplyVC(confession: Confession, addComment: Bool, comment: Comment)
}

class ReplyVC: UIViewController,UITextViewDelegate {

    @IBOutlet weak var sendBtn: UIButton!
    @IBOutlet weak var replyLabel: UILabel!
    @IBOutlet weak var textView: UITextView!
    
    @IBOutlet weak var scrollView: UIScrollView!
    var delegate: ReloadCommentFromReplyVC?
    
    var replyingComment: Comment?
    var confession: Confession?
    var commentAuthor: String?
    var replyAnonymously = false
    var anonBtnView: AnonymousButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        

        addAccessoryView()
        if confession?.author == replyingComment?.author {
            replyLabel.text = "Replying Student: "
            commentAuthor = "Student"
        } else {
            if( replyingComment?.anonymous )! {
                replyLabel.text = "Replying Anonymous Student: "
                commentAuthor = "\((replyingComment?.userName)!)"
            } else {
                replyLabel.text = "Replying \((replyingComment?.userName)!): "
                commentAuthor = "\((replyingComment?.userName)!)"
            }

        }
        

        
        textView.delegate = self
        textView.text = "Enter your comment..."
        textView.textColor = UIColor.lightGray
        
        disableSendBtn()
        
    }

    func enableSendBtn() {
        self.sendBtn.setTitleColor(UIColor(red: 18/255, green: 102/255, blue: 215/255, alpha: 1), for: .normal)
//            = UIColor(red: 18/255, green: 102/255, blue: 215/255, alpha: 1)
        self.sendBtn.isEnabled = true
    }
    
    func disableSendBtn() {
        self.sendBtn.setTitleColor(UIColor.lightGray, for: .normal)
//        self.sendBtn.titleLabel?.textColor = UIColor.lightGray
//        self.sendBtn.tintColor = UIColor.lightGray
        self.sendBtn.isEnabled = false
    }
    
    public func addAccessoryView() {
        
        anonBtnView = Bundle.main.loadNibNamed("AnonymousButton", owner: self, options: nil)?.first as! AnonymousButton
        anonBtnView.replyVC = self
        
        textView.inputAccessoryView = anonBtnView
        
    }
    
    @IBAction func dismissTapped(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }

    func textViewDidChange(_ textView: UITextView) {
        
        if textView.text.characters.count > 0 && !textView.text.trimmingCharacters(in: .whitespaces).isEmpty {
            enableSendBtn()
        } else {
            disableSendBtn()
        }
        
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.textColor == UIColor.lightGray {
            textView.text = nil
            textView.textColor = UIColor.black
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty {
            textView.text = "Enter your comment..."
            textView.textColor = UIColor.lightGray
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        print("touches2")
        textField.resignFirstResponder()
        
        return true
    }
    
    func anonymousModeTapped() {
        print("tap")
        
        if confession?.author == Auth.auth().currentUser?.uid {
            anonBtnView.anonBtn.shake()
            var murmur = Murmur(title: "Can't reply anonymously because you wrote this post.")
            murmur.backgroundColor = UIColor.red
            murmur.titleColor = UIColor.white
            
            // Show and hide a message after delay
            Whisper.show(whistle: murmur, action: .show(2.5))
            
        } else {
            anonBtnView.anonBtn.pulsate()
            if replyAnonymously {
                
                anonBtnView.anonBtn.setBackgroundImage(#imageLiteral(resourceName: "anonyModeOff"), for: .normal)
                
                replyAnonymously = false
            } else {
                
                
                anonBtnView.anonBtn.setBackgroundImage(#imageLiteral(resourceName: "anonyModeOn"), for: .normal)
                
                var murmur = Murmur(title: "Replying Anonymously")
                murmur.backgroundColor = UIColor(red: 18/255, green: 102/255, blue: 215/255, alpha: 1)
                murmur.titleColor = UIColor.white
                
                // Show and hide a message after delay
                Whisper.show(whistle: murmur, action: .show(1.5))
                replyAnonymously = true
            }
        }

        
    }
    
    var dict = [String: Bool]()
    var authorGender: String!
    var authorFaculty: String!
    var userName: String!
    var commentValues: Dictionary<String, AnyObject>?
    
    
    @IBAction func sendReply(_ sender: Any) {
        disableSendBtn()
        self.confession?.postRef.observeSingleEvent(of: .value, with: { (snapshot) in
            if snapshot.exists() {
                
                DataService.ds.REF_COMMENTS.child((self.replyingComment?.commentKey)!).observeSingleEvent(of: .value, with: { (snapshot2) in
                    if snapshot2.exists() {
                        self.dict["key"] = true
                        
                        DataService.ds.REF_USERS.child((Auth.auth().currentUser?.uid)!).observeSingleEvent(of: .value, with: {(snapshot) in
                            if let snapshots = snapshot.children.allObjects as? [DataSnapshot] {
                                
                                self.authorGender = snapshot.childSnapshot(forPath: "gender").value as! String!
                                self.authorFaculty = snapshot.childSnapshot(forPath: "course").value as! String!
                                self.userName = snapshot.childSnapshot(forPath: "userName").value as! String!
                                
                                self.createComment()
                                
                            }
                            
                        })
                    } else {
                        ErrorAlert().createAlert(title: "Something went wrong", msg: "This comment no longer exists", object: self)
                        self.enableSendBtn()
                    }
                })

                
            } else {
                ErrorAlert().createAlert(title: "Something went wrong", msg: "This confession no longer exists", object: self)
                self.enableSendBtn()
            }
        })
        
    }
    

    func createComment() {
        
        var uid = (Auth.auth().currentUser?.uid)!
        let timestamp = NSNumber(integerLiteral: Int(NSDate().timeIntervalSince1970))
        var anonymousID = 0
        var commentors: Int?
        
        DataService.ds.REF_USERS.child(uid).observeSingleEvent(of: .value, with: { (snapshot) in
            if let userDict = snapshot.value as? Dictionary<String, AnyObject> {
                var user = User(userKey: snapshot.key, userData: userDict)
                
                if uid == self.confession?.author{
                    
                    if (self.confession?.anonymous) != nil {
                        if (self.confession?.anonymous)! {
                            self.replyAnonymously = true
                        } else {
                            self.replyAnonymously = false
                        }
                    }
                } 
                
                if( self.replyingComment?.anonymous )! {
                    self.commentValues = ["comment": "COMMENTID:\((self.replyingComment?.commentKey)!) \((self.textView.text)!)" as AnyObject, "likes": 0 as AnyObject, "author": Auth.auth().currentUser?.uid as AnyObject, "userName": self.userName as AnyObject, "time": timestamp, "confessionKey": self.confession?.confessionKey as AnyObject, "authorGender": self.authorGender as AnyObject, "authorFaculty": self.authorFaculty as AnyObject, "userLike": self.dict as AnyObject, "profilePicUrl" : user.profilePicUrl as AnyObject, "replyTo": "\((self.replyingComment?.author)!)" as AnyObject, "replyCommentContent": "\((self.replyingComment?.comment)!)" as AnyObject,"deleted": false as AnyObject, "anonymous": self.replyAnonymously as AnyObject, "authorUniversity": user.university as AnyObject, "university": self.confession?.university as AnyObject]
                } else {
                    self.commentValues = ["comment": "@\((self.commentAuthor)!) \((self.textView.text)!)" as AnyObject, "likes": 0 as AnyObject, "author": Auth.auth().currentUser?.uid as AnyObject, "userName": self.userName as AnyObject, "time": timestamp, "confessionKey": self.confession?.confessionKey as AnyObject, "authorGender": self.authorGender as AnyObject, "authorFaculty": self.authorFaculty as AnyObject, "userLike": self.dict as AnyObject, "profilePicUrl" : user.profilePicUrl as AnyObject, "replyTo": "\((self.replyingComment?.author)!)" as AnyObject, "replyCommentContent": "\((self.replyingComment?.comment)!)" as AnyObject,"deleted": false as AnyObject,"anonymous": self.replyAnonymously as AnyObject, "authorUniversity": user.university as AnyObject, "university": self.confession?.university as AnyObject]
                }

                
                let firebaseComment = DataService.ds.REF_CONFESSION_COMMENTS.childByAutoId()
                
                if let values = self.commentValues {
                    
                    let newComment = Comment(commentKey: firebaseComment.key, postData: values)
                    
                    DataService.ds.REF_CONFESSION_COMMENTS.child((self.confession?.confessionKey)!).child(firebaseComment.key).updateChildValues(values)
                    DataService.ds.REF_COMMENTS.child(firebaseComment.key).updateChildValues(values)
                    
                    
                    self.confession?.comments = (self.confession?.comments)! + 1
                    self.confession?.adjustComments(addComment: true, uid: (Auth.auth().currentUser?.uid)!, commentKey: firebaseComment.key, comment: newComment)
                    //NotificationCenter.default.post(name: NSNotification.Name(rawValue: "reloadAddComment"), object: nil)
                    
                    self.delegate?.reloadComentFromReplyVC(confession: self.confession!, addComment: true, comment: newComment)
                    self.dismiss(animated: true, completion: nil)

                    
                }
            }
        })
    }

}
