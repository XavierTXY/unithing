//
//  VCSegue.swift
//  UniThing
//
//  Created by Xavier TanXY on 6/12/16.
//  Copyright © 2016 Xavier TanXY. All rights reserved.
//

import Foundation
import UIKit
import SlideMenuControllerSwift

class VCSegue {
    
    func segueVC(bundleName: String, controllerName: String, vc: UIViewController) {
        var storyboard = UIStoryboard(name: bundleName, bundle: nil)

        if controllerName == "TabBarVC" {
            
//            let controller = storyboard.instantiateViewController(withIdentifier: controllerName) as! UITabBarController
//            print(vc.presentedViewController?.nibName)
//            vc.present(controller, animated: true, completion: nil)
        
            
//            let slideMenuController = createMenuView()
//            vc.present(slideMenuController, animated: true, completion: nil)
            
            createMenuView(vc:vc)
           
            
        } else if controllerName == "NavVC" {
            

            let controller = storyboard.instantiateViewController(withIdentifier: controllerName) as! UINavigationController
            //UIApplication.shared.keyWindow?.rootViewController? = controller
            vc.present(controller, animated: true, completion: nil)
           
            
        }
        
        
    }
    
    
    func createMenuView(vc: UIViewController) {
        
        // create viewController code...
        var storyboard = UIStoryboard(name: "HomeSB", bundle: nil)
        let mainViewController = storyboard.instantiateViewController(withIdentifier: "TabBarVC") as! UITabBarController
        
        //        let mainViewController = storyboard.instantiateViewController(withIdentifier: "MainViewController") as! MainViewController
        let leftViewController = storyboard.instantiateViewController(withIdentifier: "LeftMenuVC") as! LeftMenuVC
        //        let rightViewController = storyboard.instantiateViewController(withIdentifier: "RightViewController") as! RightViewController
        
        //        let nvc: UINavigationController = UINavigationController(rootViewController: mainViewController)
        
        //        UINavigationBar.appearance().tintColor = UIColor(hex: "689F38")
        
        leftViewController.mainViewController = mainViewController
        
        
        
        let slideMenuController = SlideMenuController(mainViewController: mainViewController, leftMenuViewController: leftViewController)
        //        slideMenuController.automaticallyAdjustsScrollViewInsets = true
        //        slideMenuController.delegate = mainViewController as! SlideMenuControllerDelegate
        //        self.window?.backgroundColor = UIColor(red: 236.0, green: 238.0, blue: 241.0, alpha: 1.0)
        AppDelegate.shared.rootViewController = slideMenuController
        AppDelegate.shared.win.makeKeyAndVisible()
//        rootViewController = slideMenuController

//        vc.present(AppDelegate.shared.rootViewController, animated: true, completion: nil)
//        rootViewController.makeKeyAndVisible()
        
    }
    
//    func createMenuView() -> SlideMenuController{
//
//        // create viewController code...
//        var storyboard = UIStoryboard(name: "HomeSB", bundle: nil)
//        let mainViewController = storyboard.instantiateViewController(withIdentifier: "TabBarVC") as! UITabBarController
//
//        //        let mainViewController = storyboard.instantiateViewController(withIdentifier: "MainViewController") as! MainViewController
//        let leftViewController = storyboard.instantiateViewController(withIdentifier: "LeftMenuVC") as! LeftMenuVC
//        //        let rightViewController = storyboard.instantiateViewController(withIdentifier: "RightViewController") as! RightViewController
//
//        //        let nvc: UINavigationController = UINavigationController(rootViewController: mainViewController)
//
//        //        UINavigationBar.appearance().tintColor = UIColor(hex: "689F38")
//
//        leftViewController.mainViewController = mainViewController
//
//
//
//        let slideMenuController = SlideMenuController(mainViewController: mainViewController, leftMenuViewController: leftViewController)
//        //        slideMenuController.automaticallyAdjustsScrollViewInsets = true
//        //        slideMenuController.delegate = mainViewController as! SlideMenuControllerDelegate
//        //        self.window?.backgroundColor = UIColor(red: 236.0, green: 238.0, blue: 241.0, alpha: 1.0)
////        self.window?.rootViewController = slideMenuController
////        self.window?.makeKeyAndVisible()
//
//        return slideMenuController
//    }
}
