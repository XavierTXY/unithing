//
//  TopConfessionCollectionCell.swift
//  UniThing
//
//  Created by XavierTanXY on 2/4/18.
//  Copyright © 2018 Xavier TanXY. All rights reserved.
//

import UIKit

class TopConfessionCollectionCell: UICollectionViewCell {
 
    @IBOutlet weak var confessionText: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func configureCell(confession: Confession) {
        confessionText.text = confession.caption
        print("in cell \(confessionText.text)")
//        uniName.text = "#\(hash.hashName)"
//        numPosts.text = "\(hash.hashCount) posts"
//        roundView.backgroundColor = color
    }
}
