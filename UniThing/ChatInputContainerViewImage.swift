//
//  ChatInputContainerView.swift
//  UniThing
//
//  Created by Xavier TanXY on 22/12/16.
//  Copyright © 2016 Xavier TanXY. All rights reserved.
//

import Foundation
import UIKit

class ChatInputContainerViewImage: UIView, UITextFieldDelegate {
    
    var newChatLogVC: NewChatLogVC? {
        didSet {
//            anonBtn.addTarget(chatLogVC, action: #selector(chatLogVC?.messageSend), for: .touchUpInside)
            addBtn.addTarget(newChatLogVC, action: #selector(newChatLogVC?.uploadImageTapped), for: .touchUpInside)
//            let tapGestureRecognizer = UITapGestureRecognizer(target: chatLogVC, action: #selector(chatLogVC?.uploadImageTapped))
//            tapGestureRecognizer.numberOfTapsRequired = 1
//            uploadImageView.addGestureRecognizer(tapGestureRecognizer)
        }
    }
    

 //From here
//    lazy var inputTextField: UITextField = {
//        let textField = UITextField()
//        textField.placeholder = "Enter comment..."
//        textField.translatesAutoresizingMaskIntoConstraints = false
//        textField.delegate = self
//        return textField
//    }()
//
//    lazy var rectangleView: UIView = {
//        let view = UIView()
//        view.translatesAutoresizingMaskIntoConstraints = false
//        view.layer.borderColor = UIColor.black.cgColor
//       // view.backgroundColor = UIColor.blue
//        view.layer.borderWidth = 1.0
//        view.layer.cornerRadius = 10
//
//        return view
//
//    }()//Stop here
//
//    let uploadImageView: UIImageView = {
//        let uploadImageView = UIImageView()
//        uploadImageView.translatesAutoresizingMaskIntoConstraints = false
//        uploadImageView.image = #imageLiteral(resourceName: "addBtn")
//        uploadImageView.contentMode = .scaleAspectFit
//        uploadImageView.isUserInteractionEnabled = true
//
//        return uploadImageView
//    }()
    
    lazy var inputTextField: UITextField = {
        let textField = UITextField()
        textField.placeholder = "Write a message"
        textField.translatesAutoresizingMaskIntoConstraints = false
        textField.textColor = UIColor.lightGray
        textField.returnKeyType = UIReturnKeyType.send
        textField.backgroundColor = UIColor.white
        textField.font = UIFont(name: "HelveticaNeue", size: 16)
        // textField.backgroundColor = UIColor.blue
        textField.enablesReturnKeyAutomatically = true
        textField.delegate = self
        
        return textField
    }()
    
    
    lazy var rectangleView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        // view.backgroundColor = UIColor.red
        // view.layer.borderColor = UIColor(red: 0/255, green: 0/255, blue: 0/255, alpha: 0.1).cgColor
        
        view.layer.borderColor = UIColor.clear.cgColor
        //view.backgroundColor = UIColor.white
        view.layer.borderWidth = 1.0
        view.layer.cornerRadius = 16
        
        return view
        
    }()
    
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        if (textField.text?.characters.count)! > 0 && !(textField.text?.trimmingCharacters(in: .whitespaces).isEmpty)! {
            self.newChatLogVC?.messageSend()
        } else {
            textField.resignFirstResponder()
            self.newChatLogVC?.displayEmptyTxtFieldAlert()
        }
        
        return true
    }
    
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if inputTextField.textColor! == UIColor.lightGray {
            inputTextField.text = nil
            inputTextField.textColor = UIColor.black
            print("a1")
            
            //            if (textField.text?.characters.count)! > 0 && !(textField.text?.trimmingCharacters(in: .whitespaces).isEmpty)! {
            //                textField.enablesReturnKeyAutomatically = false
            //            } else {
            //                textField.enablesReturnKeyAutomatically = true
            //            }
            
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if (inputTextField.text?.isEmpty)! {
            inputTextField.text = "Write a message"
            inputTextField.textColor = UIColor.lightGray
            print("A2")
            //            textField.enablesReturnKeyAutomatically = false
        }
    }
    
//
//    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
//
//        if (textField.text?.characters.count)! > 0 && !(textField.text?.trimmingCharacters(in: .whitespaces).isEmpty)! {
//            self.comment3VC?.commentSend()
//        } else {
//            textField.resignFirstResponder()
//            self.comment3VC?.displayEmptyTxtFieldAlert()
//        }
//
//        return true
//    }
    
    
//    func textFieldDidBeginEditing(_ textField: UITextField) {
//        if inputTextField.textColor! == UIColor.lightGray {
//            inputTextField.text = nil
//            inputTextField.textColor = UIColor.black
//            print("a1")
//
//            //            if (textField.text?.characters.count)! > 0 && !(textField.text?.trimmingCharacters(in: .whitespaces).isEmpty)! {
//            //                textField.enablesReturnKeyAutomatically = false
//            //            } else {
//            //                textField.enablesReturnKeyAutomatically = true
//            //            }
//
//        }
//    }
//
//    func textFieldDidEndEditing(_ textField: UITextField) {
//        if (inputTextField.text?.isEmpty)! {
//            inputTextField.text = "Enter a comment..."
//            inputTextField.textColor = UIColor.lightGray
//            print("A2")
//            //            textField.enablesReturnKeyAutomatically = false
//        }
//    }
    
    
    //    let sendButton = UIButton(type: .custom)
//    let anonBtn = UIButton(type: .custom)
    let addBtn = UIButton(type: .custom)
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        
        translatesAutoresizingMaskIntoConstraints = false
        //        backgroundColor = UIColor(red: 250/255, green: 250/255, blue: 250/255, alpha: 1)
        
        backgroundColor = UIColor.white
        
        
        
        //        addSubview(sendButton)
        
        
        
        
        
        addBtn.setImage(#imageLiteral(resourceName: "Add"), for: .normal)
        //sendButton.setTitle("Post", for: .normal)
        addBtn.translatesAutoresizingMaskIntoConstraints = false
        //        sendButton.tintColor = UIColor(red: 158/255, green: 158/255, blue: 158/255, alpha: 0.7)
        addBtn.isEnabled = true
        addSubview(addBtn)
        
        addBtn.rightAnchor.constraint(equalTo: rightAnchor).isActive = true
        addBtn.centerYAnchor.constraint(equalTo: centerYAnchor).isActive = true
        addBtn.widthAnchor.constraint(equalToConstant: 50).isActive = true
        addBtn.heightAnchor.constraint(equalTo: heightAnchor).isActive = true
        
        
        let seperatorLineView = UIView()
        seperatorLineView.backgroundColor = UIColor(displayP3Red: 230/255, green: 236/255, blue: 240/255, alpha: 1)
        seperatorLineView.translatesAutoresizingMaskIntoConstraints = false
        addSubview(seperatorLineView)
        
        seperatorLineView.leftAnchor.constraint(equalTo: leftAnchor).isActive = true
        seperatorLineView.topAnchor.constraint(equalTo: topAnchor).isActive = true
        seperatorLineView.widthAnchor.constraint(equalTo: widthAnchor).isActive = true
        seperatorLineView.heightAnchor.constraint(equalToConstant: 1).isActive = true
        
        addSubview(rectangleView)
        
        //        let view = UIView()
        //        view.backgroundColor = UIColor.blue
        //        addSubview(view)
        //        view.leftAnchor.constraint(equalTo: leftAnchor).isActive = true
        //        view.bottomAnchor.constraint(equalTo: bottomAnchor).isActive = true
        //        view.rightAnchor.constraint(equalTo: rightAnchor).isActive = true
        ////        view.widthAnchor.constraint(equalTo: widthAnchor).isActive = true
        //        view.heightAnchor.constraint(equalToConstant: 50).isActive = true
        
        rectangleView.addSubview(inputTextField)
        //         rectangleView.addSubview(anonBtn)
        
        
        inputTextField.rightAnchor.constraint(equalTo: rectangleView.rightAnchor, constant: -4).isActive = true
        inputTextField.leftAnchor.constraint(equalTo: rectangleView.leftAnchor, constant: 8).isActive = true
        
        inputTextField.topAnchor.constraint(equalTo: rectangleView.topAnchor).isActive = true
        inputTextField.bottomAnchor.constraint(equalTo: rectangleView.bottomAnchor).isActive = true
        // inputTextView.heightAnchor.constraint(equalTo: rectangleView.heightAnchor).isActive = true
        
        
        
        
        self.rectangleView.leftAnchor.constraint(equalTo: leftAnchor, constant: 4).isActive = true
        self.rectangleView.centerYAnchor.constraint(equalTo: centerYAnchor).isActive = true
        self.rectangleView.rightAnchor.constraint(equalTo: addBtn.leftAnchor, constant: 8).isActive = true
        self.rectangleView.topAnchor.constraint(equalTo: seperatorLineView.bottomAnchor, constant: 4).isActive = true
        
        //        self.anonBtn.bottomAnchor.constraint(equalTo: rectangleView.topAnchor, constant: 15).isActive = true
        //        self.anonBtn.rightAnchor.constraint(equalTo: rectangleView.rightAnchor, constant: 15).isActive = true
        //        anonBtn.widthAnchor.constraint(equalToConstant: 40).isActive = true
        //        anonBtn.heightAnchor.constraint(equalToConstant: 40).isActive = true
        
        
        
        let seperatorLineView2 = UIView()
        seperatorLineView2.backgroundColor = BORDER_COLOR
        seperatorLineView2.translatesAutoresizingMaskIntoConstraints = false
        addSubview(seperatorLineView2)
        
        seperatorLineView2.leftAnchor.constraint(equalTo: leftAnchor).isActive = true
        seperatorLineView2.topAnchor.constraint(equalTo: rectangleView.bottomAnchor, constant: 4).isActive = true
        seperatorLineView2.bottomAnchor.constraint(equalTo: bottomAnchor).isActive = true
        seperatorLineView2.widthAnchor.constraint(equalTo: widthAnchor).isActive = true
        seperatorLineView2.heightAnchor.constraint(equalToConstant: 1).isActive = true
        
    }
    
    
    
    
    //From here
//    let sendButton = UIButton(type: .system)
//
//
//    override init(frame: CGRect) {
//        super.init(frame: frame)
//
//        translatesAutoresizingMaskIntoConstraints = false
//        backgroundColor = UIColor.white
//
//
//        addSubview(uploadImageView)
//
//        uploadImageView.leftAnchor.constraint(equalTo: leftAnchor).isActive = true
//        uploadImageView.centerYAnchor.constraint(equalTo: centerYAnchor).isActive = true
//        uploadImageView.widthAnchor.constraint(equalToConstant: 44).isActive = true
//        uploadImageView.heightAnchor.constraint(equalToConstant: 44).isActive = true
//
//
//
//
//        sendButton.setTitle("Send", for: .normal)
//        sendButton.translatesAutoresizingMaskIntoConstraints = false
//        addSubview(sendButton)
//
//        sendButton.rightAnchor.constraint(equalTo: rightAnchor).isActive = true
//        sendButton.centerYAnchor.constraint(equalTo: centerYAnchor).isActive = true
//        sendButton.widthAnchor.constraint(equalToConstant: 80).isActive = true
//        sendButton.heightAnchor.constraint(equalTo: heightAnchor).isActive = true
//
//
//        addSubview(self.inputTextField)
//
//        self.inputTextField.leftAnchor.constraint(equalTo: uploadImageView.rightAnchor, constant: 8).isActive = true
//        self.inputTextField.centerYAnchor.constraint(equalTo: centerYAnchor).isActive = true
//        self.inputTextField.rightAnchor.constraint(equalTo: sendButton.leftAnchor).isActive = true
//        self.inputTextField.heightAnchor.constraint(equalTo: heightAnchor).isActive = true
//
//        let seperatorLineView = UIView()
//        seperatorLineView.backgroundColor = UIColor.lightGray
//        seperatorLineView.translatesAutoresizingMaskIntoConstraints = false
//        addSubview(seperatorLineView)
//
//        seperatorLineView.leftAnchor.constraint(equalTo: leftAnchor).isActive = true
//        seperatorLineView.topAnchor.constraint(equalTo: topAnchor).isActive = true
//        seperatorLineView.widthAnchor.constraint(equalTo: widthAnchor).isActive = true
//        seperatorLineView.heightAnchor.constraint(equalToConstant: 1).isActive = true
//
//
//
//        addSubview(rectangleView)
//
//        self.rectangleView.leftAnchor.constraint(equalTo: leftAnchor).isActive = true
//        self.rectangleView.centerYAnchor.constraint(equalTo: centerYAnchor).isActive = true
//        self.rectangleView.rightAnchor.constraint(equalTo: rightAnchor).isActive = true
//        self.rectangleView.topAnchor.constraint(equalTo: seperatorLineView.bottomAnchor, constant: 2).isActive = true
//      //  self.rectangleView.bottomAnchor.constraint(equalTo: seperatorLineView2.topAnchor, constant: 4).isActive = true
//
////        let seperatorLineView2 = UIView()
////        seperatorLineView2.backgroundColor = UIColor.lightGray
////        seperatorLineView2.translatesAutoresizingMaskIntoConstraints = false
////        addSubview(seperatorLineView2)
////
////        seperatorLineView2.leftAnchor.constraint(equalTo: leftAnchor).isActive = true
////        seperatorLineView2.topAnchor.constraint(equalTo: rectangleView.bottomAnchor, constant: 2).isActive = true
////        seperatorLineView2.bottomAnchor.constraint(equalTo: bottomAnchor).isActive = true
////        seperatorLineView2.widthAnchor.constraint(equalTo: widthAnchor).isActive = true
////        seperatorLineView2.heightAnchor.constraint(equalToConstant: 20).isActive = true
//
//    }
//
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    

    func textFieldShouldClear(_ textField: UITextField) -> Bool {
        newChatLogVC?.messageSend()
        return true
    }

    
}
