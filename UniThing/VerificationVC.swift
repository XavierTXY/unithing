//
//  VerificationVC.swift
//  UniThing
//
//  Created by Xavier TanXY on 21/8/17.
//  Copyright © 2017 Xavier TanXY. All rights reserved.
//

import UIKit
import Firebase
import SVProgressHUD

class VerificationVC: UIViewController {

    var email: String!
    var password: String!
    var provider: String!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        let leftBarItem = UIBarButtonItem(image: #imageLiteral(resourceName: "Back Chevron"), style: .plain, target: self, action: "backTapped")
        leftBarItem.tintColor = UIColor.white
        self.navigationItem.setLeftBarButton(leftBarItem, animated: true)
       
        self.navigationItem.title = "Verification"
        self.navigationController?.navigationBar.titleTextAttributes = [ NSFontAttributeName: UIFont(name: "HelveticaNeue-Bold", size: 16)!]
        self.navigationController?.navigationBar.titleTextAttributes = [ NSForegroundColorAttributeName: UIColor.white]
        

        
        sendVerification()
    }
    
    func backTapped() {
        self.navigationController?.popViewController(animated: false)
    }
    
    func sendVerification() {
        Auth.auth().currentUser?.sendEmailVerification { (error) in
            if error != nil {
                print("send verfi failed \(error?.localizedDescription)")
            } else {
                print("send veri succes")
            }
        }
    }

    @IBAction func resendVerification(_ sender: Any) {
        SVProgressHUD.show(withStatus: "Sending...")
        Auth.auth().currentUser?.sendEmailVerification { (error) in
            SVProgressHUD.dismiss()
            if error != nil {
                //print("send verfi failed \(error?.localizedDescription)")
                ErrorAlert().show(title: "Error", msg: "\(error?.localizedDescription)", object: self)
            } else {
                //print("send veri succes")
                ErrorAlert().show(title: "Sent!", msg: "Please check your email for verification", object: self)
            }
        }
    }
    
    @IBAction func verifiedTapped(_ sender: Any) {
        SVProgressHUD.show(withStatus: "Checking...")
//        SVProgressHUD.dismiss()
//        self.performSegue(withIdentifier: "DetailVC", sender: nil)
        
        Auth.auth().signIn(withEmail: self.email!, password: self.password!) { (user, error) in
            if error != nil {
                //print("what \(error?.localizedDescription)")
                SVProgressHUD.dismiss()
                ErrorAlert().show(title: "Error", msg: "\(error?.localizedDescription)", object: self)
            } else {
                if let user = user {
                    if user.isEmailVerified {
                        print("you are verified")
                        SVProgressHUD.dismiss()
                        self.performSegue(withIdentifier: "DetailVC", sender: nil)
                    } else {
                        SVProgressHUD.dismiss()
                        print("you are not verified, pls check you email again")
                        ErrorAlert().show(title: "Not Verified", msg: "Please check your email for verification", object: self)
                    }
                    
                }
            }
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if let destinationVC = segue.destination as? DetailVC {
            destinationVC.email = self.email
            destinationVC.password = self.password
            destinationVC.provider = self.provider
            
        }
    }

}
