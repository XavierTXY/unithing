//
//  AboutVC.swift
//  UniThing
//
//  Created by XavierTanXY on 29/10/17.
//  Copyright © 2017 Xavier TanXY. All rights reserved.
//

import UIKit

class AboutVC: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var tableView: UITableView!
    var options = [[String]]()
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        tableView.delegate = self
        tableView.dataSource = self
        
        self.navigationItem.title = "About"
        setupOptions()
        tableView.tableFooterView = UIView()
    }
    
    func setupOptions() {
        options = [ ["Term of Service", "Privacy Policy"],  ["Feedback"] ]
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SettingCell") as? SettingCell
        cell?.selectionStyle = .none
        cell?.label.text = options[indexPath.section][indexPath.row]
        return cell!
    }

    func numberOfSections(in tableView: UITableView) -> Int {
        return options.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return options[section].count
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("touch")
        if indexPath.row == 0 && indexPath.section == 0{
//            performSegue(withIdentifier: "ProfileVC", sender: nil)
            print("term")
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let viewController = storyboard.instantiateViewController(withIdentifier :"TCVC") as! TCVC
            self.navigationController?.pushViewController(viewController, animated: true)
        } else if indexPath.row == 1 && indexPath.section == 0 {
//            displayLogoutAlert()
            print("policy")
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let viewController = storyboard.instantiateViewController(withIdentifier :"PrivacyVC") as! PrivacyVC
            self.navigationController?.pushViewController(viewController, animated: true)
        } else if indexPath.row == 0 && indexPath.section == 1 {
            print("feed")
            performSegue(withIdentifier: "FeedbackVC", sender: nil)
            
        } else if indexPath.row == 1 && indexPath.section == 1  {
            print("rate")
//            performSegue(withIdentifier: "SelfConfessionVC", sender: nil)
        }
    }


}
