//
//  LeftMenuVC.swift
//  UniThing
//
//  Created by XavierTanXY on 18/5/18.
//  Copyright © 2018 Xavier TanXY. All rights reserved.
//

import UIKit
import Firebase
import CollapsibleTableSectionViewController

enum LeftMenu: Int {
    case main = 0
    case swift
    case java
    case go
    case nonMenu
}

public struct Item {
    public var name: String
    public var detail: String
    
    public init(name: String, detail: String) {
        self.name = name
        self.detail = detail
    }
}

public struct Section {
    public var name: String
    public var items: [Item]
    
    public init(name: String, items: [Item]) {
        self.name = name
        self.items = items
    }
}

//public var sectionsData: [Section] = [
//    Section(name: "Mac", items: [
//        Item(name: "MacBook", detail: "Apple's ultraportable laptop, trading portability for speed and connectivity."),
//        Item(name: "MacBook Air", detail: "While the screen could be sharper, the updated 11-inch MacBook Air is a very light ultraportable that offers great performance and battery life for the price."),
//        Item(name: "MacBook Pro", detail: "Retina Display The brightest, most colorful Mac notebook display ever. The display in the MacBook Pro is the best ever in a Mac notebook."),
//        Item(name: "iMac", detail: "iMac combines enhanced performance with our best ever Retina display for the ultimate desktop experience in two sizes."),
//        Item(name: "Mac Pro", detail: "Mac Pro is equipped with pro-level graphics, storage, expansion, processing power, and memory. It's built for creativity on an epic scale."),
//        Item(name: "Mac mini", detail: "Mac mini is an affordable powerhouse that packs the entire Mac experience into a 7.7-inch-square frame."),
//        Item(name: "OS X El Capitan", detail: "The twelfth major release of OS X (now named macOS)."),
//        Item(name: "Accessories", detail: "")
//        ]),
//    Section(name: "iPad", items: [
//        Item(name: "iPad Pro", detail: "iPad Pro delivers epic power, in 12.9-inch and a new 10.5-inch size."),
//        Item(name: "iPad Air 2", detail: "The second-generation iPad Air tablet computer designed, developed, and marketed by Apple Inc."),
//        Item(name: "iPad mini 4", detail: "iPad mini 4 puts uncompromising performance and potential in your hand."),
//        Item(name: "Accessories", detail: "")
//        ]),
//    Section(name: "iPhone", items: [
//        Item(name: "iPhone 6s", detail: "The iPhone 6S has a similar design to the 6 but updated hardware, including a strengthened chassis and upgraded system-on-chip, a 12-megapixel camera, improved fingerprint recognition sensor, and LTE Advanced support."),
//        Item(name: "iPhone 6", detail: "The iPhone 6 and iPhone 6 Plus are smartphones designed and marketed by Apple Inc."),
//        Item(name: "iPhone SE", detail: "The iPhone SE was received positively by critics, who noted its familiar form factor and design, improved hardware over previous 4-inch iPhone models, as well as its overall performance and battery life."),
//        Item(name: "Accessories", detail: "")
//        ])
//]

class LeftMenuVC : CollapsibleTableSectionViewController {
    
//    @IBOutlet weak var tableView: UITableView!
    var menus = ["Main", "Swift", "Java", "Go", "NonMenu"]
    var mainViewController: UITabBarController!
//    var swiftViewController: UIViewController!
//    var javaViewController: UIViewController!
//    var goViewController: UIViewController!
//    var nonMenuViewController: UIViewController!
//    var imageHeaderView: ImageHeaderView!
    
    var universities = [University]()
    var ACT = [University]()
    var NATIONAL = [University]()
    var NSW = [University]()
    var NT = [University]()
    var QLD = [University]()
    var WA = [University]()
    var TAS = [University]()
    var VIC = [University]()
    var SA = [University]()
    var TOP = [University]()
    var states = [[University]]()
    
    var sections = [Section]()
    var items = [Item]()
    var categories = [String]()

    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
//        self.tableView.separatorColor = UIColor(red: 224/255, green: 224/255, blue: 224/255, alpha: 1.0)

////        self.tableView.delegate = self
////        self.tableView.dataSource = self
//
////        self.tableView.register(DropDownUniCell.self, forCellReuseIdentifier: "DropDownUniCell")
//        self.tableView.register(UINib(nibName: "DropDownUniCell", bundle: nil), forCellReuseIdentifier: "DropDownUniCell")
//        self.tableView.rowHeight = UITableViewAutomaticDimension
//        tableView.sectionHeaderHeight = UITableViewAutomaticDimension
        
        self.delegate = self

       
            parseUniversityCSV {
                self.fetchCategory()
            }
        
        

        
//        let storyboard = UIStoryboard(name: "Main", bundle: nil)
//        let swiftViewController = storyboard.instantiateViewController(withIdentifier: "SwiftViewController") as! SwiftViewController
//        self.swiftViewController = UINavigationController(rootViewController: swiftViewController)
//
//        let javaViewController = storyboard.instantiateViewController(withIdentifier: "JavaViewController") as! JavaViewController
//        self.javaViewController = UINavigationController(rootViewController: javaViewController)
//
//        let goViewController = storyboard.instantiateViewController(withIdentifier: "GoViewController") as! GoViewController
//        self.goViewController = UINavigationController(rootViewController: goViewController)
//
//        let nonMenuController = storyboard.instantiateViewController(withIdentifier: "NonMenuController") as! NonMenuController
//        nonMenuController.delegate = self
//        self.nonMenuViewController = UINavigationController(rootViewController: nonMenuController)
//
//        self.tableView.registerCellClass(BaseTableViewCell.self)
//
//        self.imageHeaderView = ImageHeaderView.loadNib()
//        self.view.addSubview(self.imageHeaderView)
    }
    
    func fetchCategory() {
        
        var itemCats = [Item]()
        DataService.ds.REF_Category_Info.observeSingleEvent(of: .value, with: { (snapshot) in
            
            if let snapshots = snapshot.children.allObjects as? [DataSnapshot] {
                for snap in snapshots {
                    
                    if let uniDict = snap.value as? Dictionary<String, AnyObject> {
                        
                        let cat = snap.key
                        print(cat)
                        itemCats.append(Item(name: cat, detail: ""))
                        self.categories.append(cat)
                    }
                }
                DataService.ds.categoryArray = self.categories
                let cats = Section(name: "Category", items: itemCats)
                self.sections.append(cats)
                self.reloadData()
                
            }
        })
    }

    func parseUniversityCSV(completion: @escaping () -> ()) {
        
        
        var trending = University(id: 00, name: "Trending", shortName: "Trending", state: "TOP")
        var latest = University(id: 01, name: "Latest", shortName: "Latest", state: "TOP")
        
        var all = Section(name: "All", items: [
            Item(name: "Trending", detail: ""),
            Item(name: "Latest", detail: ""),
            Item(name: "Followed", detail: "")
        ])
        sections.append(all)
        
        TOP.append(trending)
        TOP.append(latest)
        
        DataService.ds.REF_University_Info.observeSingleEvent(of: .value, with: { (snapshot) in
           
            if let snapshots = snapshot.children.allObjects as? [DataSnapshot] {
                for snap in snapshots {
                    
                    if let uniDict = snap.value as? Dictionary<String, AnyObject> {
                        
                        
                        let uniName = snap.key
                        let uni = University(keyName: uniName, uni: uniDict)
                        
                        self.universities.append(uni)
                        
                        if uni.name != "Trending" && uni.name != "Latest" {
                            var i = Item(name: uni.name, detail: uni.shortName)
                            self.items.append(i)
                        }

                        
                        switch uni.state {
                        case "ACT":
                            self.ACT.append(uni)
                            break
                        case "NATIONAL":
                            self.NATIONAL.append(uni)
                            break
                        case "NSW":
                            self.NSW.append(uni)
                            break
                        case "NT":
                            self.NT.append(uni)
                            break
                        case "QLD":
                            self.QLD.append(uni)
                            break
                        case "WA":
                            self.WA.append(uni)
                            break
                        case "TAS":
                            self.TAS.append(uni)
                            break
                        case "VIC":
                            self.VIC.append(uni)
                            break
                        case "SA":
                            self.SA.append(uni)
                            break
                        default:
                            break
                        }
                        
                    }
                }
                self.states.append(self.TOP)
                self.states.append(self.ACT)
                self.states.append(self.NATIONAL)
                self.states.append(self.NSW)
                self.states.append(self.NT)
                self.states.append(self.QLD)
                self.states.append(self.SA)
                self.states.append(self.TAS)
                self.states.append(self.VIC)
                self.states.append(self.WA)
                
                

                
                DataService.ds.uniArray = self.universities
                DataService.ds.states = self.states
                
                let unis = Section(name: "University", items: self.items)
                self.sections.append(unis)
                self.reloadData()
                completion()
//                self.tableView.reloadData()
                
            }
            
        })
    }
    

    
   
    
}

extension LeftMenuVC: CollapsibleTableSectionDelegate {

    func numberOfSections(_ tableView: UITableView) -> Int {
        return sections.count
    }
    
    func collapsibleTableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return sections[section].items.count
    }
    
    func collapsibleTableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "BasicCell") as UITableViewCell? ?? UITableViewCell(style: .subtitle, reuseIdentifier: "BasicCell")
        
//        cell.backgroundColor = UIColor(red: 18/255, green: 102/255, blue: 215/255, alpha: 1)
        cell.backgroundColor = UIColor.clear
        let item: Item = sections[indexPath.section].items[indexPath.row]
        
        cell.textLabel?.text = item.name
        cell.detailTextLabel?.text = item.detail
        
        return cell
    }
    
    func collapsibleTableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return sections[section].name
    }
    
    func collapsibleTableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let item: Item = sections[indexPath.section].items[indexPath.row]
        print(item.detail)
        print(item.name)
        for navVC in (self.mainViewController.viewControllers)! {

            let nav = navVC as! UINavigationController
            for vcs in nav.viewControllers {

                if vcs is ConfessionVC {

                    let cVC = vcs as! ConfessionVC
                    
                    if item.name == "Trending" || item.name == "Latest" || item.name == "Followed" {
                        cVC.didSelectUni(uniName: item.name, uniShort: item.name, isCategory: false, cat: "")
                    } else if indexPath.section == 2 {
                        cVC.didSelectUni(uniName: item.name, uniShort: item.name, isCategory: true, cat: item.name)
                    } else {
                        cVC.didSelectUni(uniName: item.name, uniShort: item.detail, isCategory: false, cat: "")
                    }
                    
                    self.slideMenuController()?.closeLeft()
                    

                }
            }
        }
    }
    
    func shouldCollapseByDefault(_ tableView: UITableView) -> Bool {
        return true
    }
    
}


