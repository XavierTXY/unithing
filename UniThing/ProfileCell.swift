//
//  ProfileCell.swift
//  UniThing
//
//  Created by Xavier TanXY on 28/6/17.
//  Copyright © 2017 Xavier TanXY. All rights reserved.
//

import UIKit

class ProfileCell: UITableViewCell {

    @IBOutlet weak var profilePic: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var userNameLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        profilePic.contentMode = .scaleAspectFill
        profilePic.layer.cornerRadius = profilePic.frame.height / 2
        profilePic.clipsToBounds = true
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func configureCell(name: String, userName: String) {
        
        nameLabel.text = name
        userNameLabel.text = "User Name: \(userName)"
    }

}
