//
//  HashtagVC.swift
//  UniThing
//
//  Created by Xavier TanXY on 4/6/17.
//  Copyright © 2017 Xavier TanXY. All rights reserved.
//

import UIKit
import Firebase
import FirebaseDatabase
import BRYXBanner
import SVProgressHUD
import ViewAnimator

protocol ReloadLikeFromHashtagVCProtocol
{
    func reloadLikeFromHashtagVC(confession: Confession, reloadLike: Bool)
}

class HashtagVC: UIViewController, UITableViewDelegate, UITableViewDataSource, ReloadLikeFromHashtagVCProtocol, ReloadCommentFromPreviousVCProtocal /*, PassConfessionFromPreviousVCDeletionProtocol*/ {
    func reloadCommentFromPreviousVC(confession: Confession, addComment: Bool, isDiscussion: Bool) {
        
    }
    


    @IBOutlet weak var tableView: UITableView!
    var refreshControl: UIRefreshControl!
    
    private let animations = [AnimationType.from(direction: .bottom, offset: 30.0)]
    
    var hashtag: String?
    var uniName: String?
    var confessions = [Confession]()
    var confessionDict = [String: Confession]()
    var numberOfObjectsinArray: Int = 7
    
    var delegate: ReloadLikeFromHashtagVCProtocol?
    var delegateRecurse: ReloadLikeFromHashtagVCProtocol?
    var actInd: UIActivityIndicatorView!
    var totalConfession: Int!

    var emptyView: LoadingHashVCView!
    var animateTVFromLike = true
    override func viewDidLoad() {
        super.viewDidLoad()

        
        SVProgressHUD.setDefaultStyle(SVProgressHUDStyle.custom)
        SVProgressHUD.setBackgroundColor(UIColor(red: 255/255, green: 255/255, blue: 255/255, alpha: 1.0))
        SVProgressHUD.setRingThickness(3)
        SVProgressHUD.show(withStatus: "Loading")
        
        tableView.delegate = self
        tableView.dataSource = self
        
        tableView.tableFooterView = UIView()
        tableView.estimatedRowHeight = 400
        tableView.rowHeight = UITableViewAutomaticDimension
        
        
        refreshControl = UIRefreshControl()
        refreshControl.backgroundColor = UIColor.clear
        refreshControl.tintColor = UIColor.lightGray
        refreshControl.addTarget(self, action: "refresh", for: UIControlEvents.valueChanged)
        //tableView.addSubview(refreshControl)
        refreshControl.beginRefreshing()
        
        self.navigationItem.title = hashtag;
        self.navigationController?.navigationBar.barTintColor = UIColor.white
        self.navigationController?.navigationBar.titleTextAttributes = [ NSFontAttributeName: UIFont(name: "HelveticaNeue-Bold", size: 16)!]
        self.navigationController?.navigationBar.layer.borderColor = UIColor(red: 226/255, green: 228/255, blue: 232/255, alpha: 1).cgColor
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title:"", style:.plain, target:nil, action: "")
        self.navigationController?.navigationBar.titleTextAttributes = [ NSForegroundColorAttributeName: PURPLE]

        emptyView = LoadingHashVCView.instanceFromNib() as! LoadingHashVCView
        emptyView.frame = self.tableView.bounds
        
        self.tableView.addSubview(emptyView)
        self.hideEmptyView()
        
        initFooter()
        fetchData()
        
    }
    
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        UIApplication.shared.statusBarStyle = .default
        displayNoInternetConnection()
        
        animateTVFromLike = false
        self.reloadTable()
        
    }
    

    
    func showEmptyView() {
        emptyView.isHidden = false
        self.tableView.bringSubview(toFront: emptyView)
    }
    
    func hideEmptyView() {
        emptyView.isHidden = true
        self.tableView.sendSubview(toBack: emptyView)
        
    }
    
    func displayNoInternetConnection() {
        if !DataService.ds.connectedToNetwork() {

            if self.refreshControl.isRefreshing {
                self.refreshControl.endRefreshing()
            }
            
            if self.actInd.isAnimating {
                self.actInd.stopAnimating()
            }
            
            self.navigationController?.popToRootViewController(animated: true)


            
        }
    }
    
    func initFooter() {
        
        actInd = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.gray)
        actInd.frame = CGRect(x: 0, y: 0, width: 320, height: 40)
        actInd.hidesWhenStopped = true
        //        self.tableView.tableFooterView = actInd
    }
    
        
        
    
    func refresh() {
        // self.tableView.isScrollEnabled = false
        self.tableView.isUserInteractionEnabled = false
        
        Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(reloadDataWhenPulled), userInfo: nil, repeats: false)
        
    }
    
    func reloadDataWhenPulled() {
        
        if self.refreshControl.isRefreshing {
            
            // reloadTable()
            //  self.tableView.reloadData()
            getConfessionWithHashtag()
            // self.refreshControl.endRefreshing()
            
            
            self.tableView.isUserInteractionEnabled = true
        }
        
    }
    
    func fetchData() {
        getTotalConfessionWithHashTag()
//        getConfessionWithHashtag()
    }
    
    var keys = [String]()
    
    func getTotalConfessionWithHashTag() {
        DataService.ds.REF_HASHTAG.child(self.hashtag!).observeSingleEvent(of: .value, with: { (snapshot) in
            
            if snapshot.hasChildren() {
                if let snapshots = snapshot.children.allObjects as? [DataSnapshot] {
                    self.totalConfession = snapshots.count
                     print("total confession with hash\(self.totalConfession)")
                    
                    if self.totalConfession == 0 {
                        print("emptyhere")
                        self.refreshControl.endRefreshing()
                        self.confessions.removeAll()
                        self.confessionDict.removeAll()
                        self.attemptToReloadTable()
                    } else {
                        self.getConfessionWithHashtag()
                    }
                }
            } else {
                print("lal")
               self.attemptToReloadTable()
            }
            

        })
    }
    
    func getConfessionWithHashtag() {

        DataService.ds.REF_HASHTAG.child(self.hashtag!).queryLimited(toLast: UInt(self.numberOfObjectsinArray)).observeSingleEvent(of: .value, with: { (snapshot) in
            
            if snapshot.hasChildren() == false {
                self.tableView.reloadData()
                self.refreshControl.endRefreshing()
                self.tableView.isUserInteractionEnabled = true
                SVProgressHUD.dismiss()
            }
            
           // self.confessions = []
            
            if let snapshots = snapshot.children.allObjects as? [DataSnapshot] {
                for snap in snapshots {
                    if let confessionDict = snap.value as? Dictionary<String, AnyObject> {
                        let key = snap.key
                     
                        
                        DataService.ds.REF_CONFESSIONS.queryOrderedByKey().queryEqual(toValue: key).observeSingleEvent(of: .value, with: { (snapshot2) in
                            
                            if let snapshots2 = snapshot2.children.allObjects as? [DataSnapshot] {
                                for snap2 in snapshots2 {
                                    if let confessionDict = snap2.value as? Dictionary<String, AnyObject> {
                                        let key2 = snap2.key
                                        
                                        let confession = Confession(confessionKey: key2, postData: confessionDict)
                                        
                                        //self.confessions.append(confession)
                                        self.keys.append(key2)
                                        
                                        self.confessionDict[key2] = confession
                                        
                                        self.reloadCommentsWhenLoaded(confession: confession)
                                        
                                        self.attemptToReloadTable()
                                 
                                        
                                    }
                                }
                            }
                        })
                        
                        
                    }
                }
                SVProgressHUD.dismiss()
            }
        })
        

    }
    
    //Reload comments for previous vcs when data is refreshed in order to make data consistent across views
    func reloadCommentsWhenLoaded(confession: Confession) {
        let stack = self.navigationController?.viewControllers
     
        
        if self.navigationController?.viewControllers.first is ConfessionVC {
            let rootVC = self.navigationController?.viewControllers.first as? ConfessionVC
            if confession.confessionKey == DISCUSSION {
                rootVC?.reloadCommentFromPreviousVC(confession: confession, addComment: true, isDiscussion: true)
            } else {
                rootVC?.reloadCommentFromPreviousVC(confession: confession, addComment: true, isDiscussion: false)
            }
            
        }
        var count = stack?.count
        
        if (stack?.count)! > 2 {
            
            while count! > 1 && count != 2 {
                if stack?[count!-2] is HashtagVC {
                    let previousController = stack?[count!-2] as! HashtagVC
                    
                    previousController.reloadCommentFromPreviousVC(confession: confession, addComment: true)
                    count = count! - 1
                }

                
            }
        }
    }
    
    var timer: Timer?
    func attemptToReloadTable() {
        
        self.timer = Timer.scheduledTimer(timeInterval: 0.5, target: self, selector: #selector(self.reloadTable), userInfo: nil, repeats: false)
//        reloadTable()
    }
    
    
    func reloadTable() {
        // self.refreshControl.endRefreshing()
        
        self.confessions = Array(self.confessionDict.values)
        
        self.confessions.sort(by: { (c1, c2) -> Bool in
            
            return (c1.time.intValue) > (c2.time.intValue)
        })

        
        if self.actInd.isAnimating {
           self.actInd.stopAnimating()
        }
        
        
        
        
        if self.refreshControl.isRefreshing {
            refreshControl.endRefreshing()
        }
        
        self.tableView.isUserInteractionEnabled = true
        
        if animateTVFromLike {
            if self.confessions.count == 0 {
                self.showEmptyView()
            } else {
                self.hideEmptyView()
            }
        }

        
        self.tableView.reloadData()
//        SVProgressHUD.dismiss()
        

        UIView.animate(views: tableView.visibleCells, animations: animations, completion: {
            })

    
        animateTVFromLike = true
        
        
        
    }
    
    func reload() {
        self.tableView.reloadData()
    }

    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return confessions.count
    }
    
    var currentConfessionId: String?
    var currentConfession: Confession?
    var currentIndex: Int?
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.currentConfessionId  = confessions[indexPath.row].confessionKey
        self.currentConfession = confessions[indexPath.row]
        currentIndex = indexPath.row
        print("you have selected \(currentConfession?.caption)")
        
        self.currentConfession?.postRef.observeSingleEvent(of: .value, with: { (snapshot) in
            if snapshot.exists() {
                self.performSegue(withIdentifier: "Comment3VC", sender: nil)
            } else {
                ErrorAlert().createAlert(title: "Something went wrong", msg: "Could not perform action", object: self)
            }
        })
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        if indexPath.row == self.confessions.count - 1 {
            print("in the end")
            self.tableView.isUserInteractionEnabled = false
            self.tableView.tableFooterView = actInd
            self.actInd.startAnimating()
            
            if totalConfession > confessions.count {
                self.timer = Timer.scheduledTimer(timeInterval: 0.5, target: self, selector: #selector(self.addMoreObjects), userInfo: nil, repeats: false)
                print("load more")
                
            } else {
                print("dont load more")
                self.tableView.isUserInteractionEnabled = true
                self.actInd.stopAnimating()
                self.tableView.tableFooterView = UIView()
            }
            
        }
    }
    
    func addMoreObjects() {
//        self.actInd.stopAnimating()
//        self.tableView.tableFooterView? = UIView()
        
        numberOfObjectsinArray += 6
        
        DataService.ds.REF_HASHTAG.child(self.uniName!).child(self.hashtag!).queryLimited(toLast: UInt(self.numberOfObjectsinArray)).observeSingleEvent(of: .value, with: { (snapshot) in
            
            if snapshot.hasChildren() == false {
                self.tableView.reloadData()
                self.refreshControl.endRefreshing()
                self.tableView.isUserInteractionEnabled = true
            }
            
            
            
            if let snapshots = snapshot.children.allObjects as? [DataSnapshot] {
                for snap in snapshots {
                    if let confessionDict = snap.value as? Dictionary<String, AnyObject> {
                        let key = snap.key
                        
                        
                        DataService.ds.REF_CONFESSIONS.queryOrderedByKey().queryEqual(toValue: key).observeSingleEvent(of: .value, with: { (snapshot2) in
                            
                            if let snapshots2 = snapshot2.children.allObjects as? [DataSnapshot] {
                                for snap2 in snapshots2 {
                                    if let confessionDict = snap2.value as? Dictionary<String, AnyObject> {
                                        let key2 = snap2.key
                                        
                                        let confession = Confession(confessionKey: key2, postData: confessionDict)
                                        
                                        //self.confessions.append(confession)
                                        self.keys.append(key2)
                                        
                                        self.confessionDict[key2] = confession
                                        
                                        self.reloadCommentsWhenLoaded(confession: confession)
                                        
                                        self.attemptToReloadTable()
                                        
                                        
                                    }
                                }
                            }
                            
                            
                            
                          //  self.tableView.tableFooterView? = UIView()
                        })
                        
                        
                    }
                }
            }
        })

    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let destinationVC = segue.destination as? Comment3VC {
            
            //destinationVC.confession = self.currentConfession
            destinationVC.confessionId = self.currentConfessionId
            destinationVC.uniName = self.uniName
            
            //            destinationVC.delegate = self
//            destinationVC.passDeletedConfessionDelegate = self
//            destinationVC.passReloadCommentConfessionDelegate = self
            
        }
        
        if let destinationVC = segue.destination as? HashtagVC {
            print("hi")
            destinationVC.hashtag = self.hashtag
            destinationVC.uniName = self.uniName
            //destinationVC.delegate = self
        }
    }


    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let confession = confessions[indexPath.row]
        //let gender = confession.authorGender
        
        let seconds = confession.time.doubleValue
        let timesStampDate = NSDate(timeIntervalSince1970: seconds)
        var timeAgo:String =  DateHelper().timeAgoSinceDate(timesStampDate as Date, currentDate: NSDate() as Date, numericDates: true)
        
        var dateFormatter = DateFormatter()
        
        dateFormatter.dateFormat = "dd, MMM"
        var shortUni = DataService.ds.convertUniNameToShort(uniName: confession.authorUni)
        //  if !self.refreshControl.isRefreshing {
        
        if confession.imageUrl == NO_PIC {
            let cell = tableView.dequeueReusableCell(withIdentifier: "ConfessionCell") as? ConfessionCell
            cell?.hashtagVC = self
            cell?.configureCell(confession: confession)
            
//            cell?.dateLabel.text = "\(shortUni) - \(confession.faculty)・\(timeAgo)"
            cell?.dateLabel.text = "\(timeAgo)"
            cell?.facultyLabel.text = "\(shortUni) - \(confession.faculty)"
            //cell?.commentButton.addTarget(self, action: "commentTapped", for: .touchUpInside)
            
//            if gender == MALE {
//                cell?.profilePic.image = UIImage(named: "male")
//            } else if gender == FEMALE {
//                cell?.profilePic.image = UIImage(named: "female")
//            }
            return cell!
        }
        else {
            
            
            let cell2 = tableView.dequeueReusableCell(withIdentifier: "ConfessionPicCell") as? ConfessionPicCell
            cell2?.hashtagVC = self
            cell2?.confessionPic.image = nil
            
            
            cell2?.confessionPic.loadImageUsingCacheWithUrlString(imageUrl: confession.imageUrl!, tv: self.tableView, index: indexPath)
            
            
            
            cell2?.configureCell(confession: confession)
//            cell2?.dateLabel.text = "\(shortUni) - \(confession.faculty)・\(timeAgo)"
            cell2?.dateLabel.text = "\(timeAgo)"
            cell2?.facultyLabel.text = "\(shortUni) - \(confession.faculty)"
            
//            if gender == MALE {
//                cell2?.profilePic.image = UIImage(named: "male")
//            } else if gender == FEMALE {
//                cell2?.profilePic.image = UIImage(named: "female")
//            }
            
            
            
            
            cell2?.hashtagVC = self
            return cell2!
        }
        
        //    }
        
        
        
//          return UITableViewCell()
    }
    
    func reloadAllLikes( vc: UINavigationController, confession: Confession, reloadLike: Bool) {
        for vc2 in vc.viewControllers {
            if vc2 is ConfessionVC {
                let confessionVC = vc2 as! ConfessionVC
                
                if confessionVC.confessionDict[confession.confessionKey] != nil {
                    print("YES ITS CONFESSION VC")
                    //                    confessionVC.confessions.append(confession)
                    //                    confessionVC.attemptToReloadTable()
                    confessionVC.reloadLikeFromHashtagVC(confession: confession, reloadLike: reloadLike)
                }
                
                
            }
            
            if vc2 is HashtagVC {
                let hashVC = vc2 as! HashtagVC
                if hashVC.confessionDict[confession.confessionKey] != nil {
                    hashVC.confessionDict[confession.confessionKey] = confession
//                    hashVC.attemptToReloadTable()
                }
                
                
            }
            
            if vc2 is Comment3VC {
                let commentVC = vc2 as! Comment3VC
                if commentVC.confessionId == confession.confessionKey {
                    commentVC.confession = confession
                    commentVC.attemptToReloadTable()
                }
                
                
            }
            
            if vc2 is OtherProfileVC {
                let profileVC = vc2 as! OtherProfileVC
                if profileVC.confessionDict[confession.confessionKey] != nil {
                    print("welala")
                    profileVC.confessionDict[confession.confessionKey] = confession
                    profileVC.attemptToReloadTable()
                    //                    self.tableView.reloadData()
                }
                
                
            }
            
        }
        
        //        /* Reload search vc's hash vc confession from comment vc */
        //        let navVC = self.tabBarController?.viewControllers?[1] as! UINavigationController
        //
        //        for vcs in navVC.viewControllers {
        //
        //            if vcs is HashtagVC {
        //                print("reload in hashtag22")
        //                let hashVC = vcs as! HashtagVC
        //                if self.user?.university == confession.university {
        //                    hashVC.confessionDict[confession.confessionKey] = confession
        //                    hashVC.attemptToReloadTable()
        //                }
        //
        //            }
        //
        //        }
    }
    
    func reloadLike(confession: Confession, reloadLike: Bool) {
        animateTVFromLike = false
        
        let vc = self.tabBarController?.viewControllers?[0] as! UINavigationController
        reloadAllLikes(vc: vc, confession: confession, reloadLike: reloadLike)
        
        let vc2 = self.tabBarController?.viewControllers?[1] as! UINavigationController
        reloadAllLikes(vc: vc2, confession: confession, reloadLike: reloadLike)
        
        let vc3 = self.tabBarController?.viewControllers?[3] as! UINavigationController
        reloadAllLikes(vc: vc3, confession: confession, reloadLike: reloadLike)
        
        let vc4 = self.tabBarController?.viewControllers?[4] as! UINavigationController
        reloadAllLikes(vc: vc4, confession: confession, reloadLike: reloadLike)
        
        
        
    }
    
//    func reloadLike(confession: Confession, reloadLike: Bool) {
//        print("liking")
//       // delegate?.reloadLikeFromHashtagVC(confession: confession, reloadLike: reloadLike)
//
//
//        if DataService.ds.currentUser.userKey == confession.author {
//            for navVC in (self.tabBarController?.viewControllers)! {
//
//                let nav = navVC as! UINavigationController
//                for vcs in nav.viewControllers {
//                    if vcs is SelfConfessionVC {
//                        let selfVC = vcs as! SelfConfessionVC
//
//                        selfVC.confessionDict[confession.confessionKey] = confession
////                        selfVC.attemptToReloadTable()
//                    }
//                }
//            }
//        }
//
//
//        /* Reload Confession in HashVC from ConfessionVC - HashVC */
//        let navVC = self.tabBarController?.viewControllers?[1] as! UINavigationController
//
//        for vcs in navVC.viewControllers {
//            print("1")
//            if vcs is HashtagVC {
//                print("reload in hashtag22")
//                let hashVC = vcs as! HashtagVC
//
//                hashVC.confessionDict[confession.confessionKey] = confession
//                hashVC.attemptToReloadTable()
//            }
//
//
//
//        }
//
//
//
//        print("index here \(self.tabBarController?.selectedIndex)")
//        /* reload confession vc view from search vc */
////        for vc in (self.tabBarController?.viewControllers)! {
////
////            if vc is UINavigationController {
////                let navVC = vc as! UINavigationController
////
////                for vc2 in navVC.viewControllers {
////                    if vc2 is ConfessionVC {
////                        let confessionVC = vc2 as! ConfessionVC
////                        confessionVC.reloadLikeFromHashtagVC(confession: confession, reloadLike: reloadLike)
////                    }
////                }
////
////            }
////        }
////
//        /* reload confession vc view from search vc */
//        let vc = self.tabBarController?.viewControllers?[0] as! UINavigationController
//
//            for vc2 in vc.viewControllers {
//                if vc2 is ConfessionVC {
//                    let confessionVC = vc2 as! ConfessionVC
//                    print("reload confession vc 1 ")
//                    if DataService.ds.currentVisitingUni == confession.university {
//                        confessionVC.reloadLikeFromHashtagVC(confession: confession, reloadLike: reloadLike)
//                    } else {
//                        if confessionVC.confessionDict[confession.confessionKey] != nil {
//                            confessionVC.reloadLikeFromHashtagVC(confession: confession, reloadLike: reloadLike)
//                        }
//                    }
//
//                }
//
//
//
//                if self.tabBarController?.selectedIndex == 1 {
//                    if vc2 is HashtagVC {
//                        let hashVC = vc2 as! HashtagVC
//                        hashVC.confessionDict[confession.confessionKey] = confession
//                        hashVC.attemptToReloadTable()
//
//                    }
//                }
//
//            }
//
//
//        /* reload confession vc view from hash tag vc */
//        let stack = self.navigationController?.viewControllers
////        if self.navigationController?.viewControllers.first is ConfessionVC {
////            let rootVC = self.navigationController?.viewControllers.first as? ConfessionVC
////            print("reload confession vc 2 ")
////            rootVC?.reloadLikeFromHashtagVC(confession: confession, reloadLike: reloadLike)
////        }
//       // let rootVC = self.navigationController?.viewControllers.first as! ConfessionVC
//        //rootVC.reloadLikeFromHashtagVC(confession: confession, reloadLike: reloadLike)
//
//        if ((stack?.count)! > 1) {
//            var count = stack?.count
//            while count! > 1  && count != 2 {
//                let previousController = stack?[count!-2] as! HashtagVC
//                //previousController.delegateRecurse = self
//                previousController.reloadLikeFromHashtagVC(confession: confession, reloadLike: reloadLike)
//                count = count! - 1
//            }
//
//        }
//
//
//
//
//    }
    
    func reloadLikeFromHashtagVC(confession: Confession, reloadLike: Bool) {
        print("2")
        confessionDict[confession.confessionKey] = confession
        attemptToReloadTable()
    }
    
    func reloadCommentFromPreviousVC(confession: Confession, addComment: Bool) {
        
        if addComment {
            print("Reload add comment in hash")
            confessionDict[confession.confessionKey] = confession
            attemptToReloadTable()
        } else {
            print("Reload minus comment in hash")
            self.reloadTable()
//            for c in confessions {
//                if c.confessionKey == confession.confessionKey {
//                    print("before number \(c.comments)")
//                    c.comments = c.comments - 1
//                    print("now number \(c.comments)")
//                    self.reloadTable()
//                }
//            }
        }

    }
    
    func version2(confession: Confession) {
        for c in confessions {
            if c.confessionKey == confession.confessionKey {
                c.comments = c.comments - 1
                self.reloadTable()
            }
        }
    }
    
    func reloadDeletedConfessionFromPreviousVC(confession: Confession) {
        confessionDict.removeValue(forKey: confession.confessionKey)
        attemptToReloadTable()
    }

    

    var selectedHashtag: String?
    var vc: HashtagVC?
    
    func hashTap(hashtag: String) {
        self.selectedHashtag = hashtag.lowercased()
        
        let storyboard = UIStoryboard(name: "HomeSB", bundle: nil)
        vc = storyboard.instantiateViewController(withIdentifier: "HashtagVC") as? HashtagVC
        self.navigationController?.pushViewController(vc!, animated: true)
        

        vc?.hashtag = self.selectedHashtag
        vc?.uniName = self.uniName
        vc?.delegateRecurse = self
        vc?.delegate = self

        
        
        
        //performSegue(withIdentifier: "HashtagVC", sender: nil)
    }
    
    var startingImgView: UIImageView?
    var startingFrame: CGRect?
    var blackBlackgroundView: UIView?
    
    func performZoomInForStartingImageView(startingImageView: UIImageView) {
        print("zoom in")
        
        startingFrame = startingImageView.superview?.convert(startingImageView.frame, to: nil)
        startingImgView = startingImageView
        startingImgView?.isHidden = true
        
        let zoomingImageView = UIImageView(frame: startingFrame!)
        zoomingImageView.contentMode = .scaleAspectFit
        zoomingImageView.clipsToBounds = true
        zoomingImageView.image = startingImageView.image
        zoomingImageView.isUserInteractionEnabled = true
        zoomingImageView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(handleZoomOut)))
        
        
        
        if let keyWindow = UIApplication.shared.keyWindow {
            blackBlackgroundView = UIView(frame: keyWindow.frame)
            blackBlackgroundView?.backgroundColor = UIColor.black
            blackBlackgroundView?.alpha = 0
            keyWindow.addSubview(blackBlackgroundView!)
            keyWindow.addSubview(zoomingImageView)
            
            
            UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
                self.blackBlackgroundView?.alpha = 1
                //self.inputContainerView.alpha = 0
                let height = (self.startingFrame?.height)! / (self.startingFrame?.width)! * keyWindow.frame.width
                
                //zoomingImageView.frame = CGRect(x: 0, y: 0, width: keyWindow.frame.width, height: height)
                zoomingImageView.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: UIScreen.main.bounds.size.height)
                
                zoomingImageView.center = keyWindow.center
                self.navigationController?.isNavigationBarHidden = true
                
                
            }, completion: { (comepleted: Bool) in
                
            })
        }
        
    }
    
    func handleZoomOut(sender: UITapGestureRecognizer) {
        print("sup2")
        navigationController?.isNavigationBarHidden = false
      
        
        //        if let zoomOutImageView = sender.view {
        //            zoomOutImageView.layer.cornerRadius = 2
        //            zoomOutImageView.contentMode = .scaleAspectFill
        //            zoomOutImageView.clipsToBounds = true
        //
        ////            UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
        ////                zoomOutImageView.frame = self.startingFrame!
        ////                self.blackBlackgroundView?.alpha = 0
        ////            }, completion: { (comepleted: Bool) in
        ////                self.startingImgView?.isHidden = false
        ////                zoomOutImageView.removeFromSuperview()
        ////            })
        //
        //
        //            UIView.animate(withDuration: 0.5, animations: {
        //                zoomOutImageView.frame = self.startingFrame!
        //                self.blackBlackgroundView?.alpha = 0
        //            }, completion: { (comepleted: Bool) in
        //                self.startingImgView?.isHidden = false
        //                zoomOutImageView.removeFromSuperview()
        //            })
        //
        //
        //        }
        
        if let zoomOutImageView = sender.view {
            zoomOutImageView.layer.cornerRadius = 2
            zoomOutImageView.contentMode = .scaleAspectFill
            zoomOutImageView.clipsToBounds = true
            
            UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: [], animations: {
                zoomOutImageView.frame = self.startingFrame!
                self.blackBlackgroundView?.alpha = 0
                //  self.inputContainerView.alpha = 1
                
            }, completion: { (comepleted: Bool) in
                self.startingImgView?.isHidden = false
                zoomOutImageView.removeFromSuperview()
            })
            
            
            
        }
        
        
    }
    
    override var prefersStatusBarHidden: Bool {
        return navigationController?.isNavigationBarHidden == true
    }
    
    override var preferredStatusBarUpdateAnimation: UIStatusBarAnimation {
        return UIStatusBarAnimation.none
    }
    

}
