//
//  FacultyCell.swift
//  UniThing
//
//  Created by Xavier TanXY on 30/11/16.
//  Copyright © 2016 Xavier TanXY. All rights reserved.
//

import UIKit

class FacultyCell: UITableViewCell {

   
    @IBOutlet weak var facultyLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    func configureCell(course: String) {
        facultyLabel.text = course
    }


}
