//
//  SearchCell.swift
//  UniThing
//
//  Created by Xavier TanXY on 1/7/17.
//  Copyright © 2017 Xavier TanXY. All rights reserved.
//

import UIKit

class SearchCell: UITableViewCell {

    @IBOutlet weak var label: UILabel!
    @IBOutlet weak var actLoader: UIActivityIndicatorView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
