//
//  SocialVC.swift
//  UniThing
//
//  Created by XavierTanXY on 3/6/18.
//  Copyright © 2018 Xavier TanXY. All rights reserved.
//

import UIKit

class SocialTableCell: UITableViewCell, UICollectionViewDataSource {

    @IBOutlet weak var collectionView: UICollectionView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        collectionView.dataSource = self
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 10
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if indexPath.row == 9 {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "AddSocialCollectionCell", for: indexPath) as! AddSocialCollectionCell
            return cell
        } else {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "SocialCollectionCell", for: indexPath) as! SocialCollectionCell
            return cell
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 170.0, height: 120.0)
        
    }
    
}

class AddSocialCollectionCell: UICollectionViewCell {
    override func awakeFromNib() {
        super.awakeFromNib()
    }
}

class SocialCollectionCell: UICollectionViewCell {
    override func awakeFromNib() {
        super.awakeFromNib()
    }
}


class SocialVC: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var collectionView: UICollectionView!
    
    @IBOutlet weak var tableView: UITableView!
    
    
    var headers = ["Meetup", "Study Group", "Event"]
    override func viewDidLoad() {
        super.viewDidLoad()

        tableView.delegate = self
        tableView.dataSource = self
        

        tableView.estimatedRowHeight = 450
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.tableFooterView = UIView()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 170.0
    }
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return headers[section]
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SocialTableCell") as! SocialTableCell
        return cell
    }
    



}
