//
//  CourseVC.swift
//  UniThing
//
//  Created by Xavier TanXY on 30/6/17.
//  Copyright © 2017 Xavier TanXY. All rights reserved.
//

import UIKit
import Firebase
import SVProgressHUD
import PopupDialog

protocol ReloadUniCourse {
    func reloadUniCourseFromCourseVC(courseName: String,uniName: String, uniShortName: String)
}

class CourseVC: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var tableView: UITableView!
    
    var user: User?
    var uni: University?
    var courses = [String]()
    
    var delegate: ReloadUniCourse?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        tableView.delegate = self
        tableView.dataSource = self
        tableView.tableFooterView = UIView()
        
        SVProgressHUD.setDefaultStyle(SVProgressHUDStyle.custom)
        SVProgressHUD.setMinimumDismissTimeInterval(1.0)
        self.navigationController?.navigationBar.titleTextAttributes = [ NSForegroundColorAttributeName: PURPLE]
        
        SVProgressHUD.setBackgroundColor(UIColor(red: 255/255, green: 255/255, blue: 255/255, alpha: 1.0))
        SVProgressHUD.setRingThickness(3)

        
        self.courses = DataService.ds.courseArray
        
        print(self.courses)
        self.navigationItem.title = "Course"
        
//        if let c = uni?.courses {
//            courses = c
//        }
        
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return courses.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "FacultyCell") as? FacultyCell
        let course = courses[indexPath.row]
        
        cell?.configureCell(course: course)
        cell?.selectionStyle = .none
        
        return cell!
    }
    
    var course: String?
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        course = courses[indexPath.row]
        displayAlert(uni: uni!)
    }
    
    var timer: Timer?
    
    func popVC() {
        SVProgressHUD.dismiss()
        if let navVC = self.navigationController?.viewControllers {
            for vc in navVC {
                if  vc is ProfileVC {
                    self.navigationController?.popToViewController(vc, animated: true)
                }
            }
            
        }
    }

    func displayAlert(uni: University) {
        //        self.dimView.removeFromSuperview()
        
        let popup = PopupDialog(title: "You are only allowed to change your university 3 times", message: "\(self.user!.uniCount) remainder left", image: nil)
        
        let buttonOne = CancelButton(title: "Cancel") {
        
        }
        
        let buttonTwo = DefaultButton(title: "Change", dismissOnTap: true) {
            SVProgressHUD.show()
            DataService.ds.REF_USERS.child((Auth.auth().currentUser?.uid)!).updateChildValues(["university": uni.name])
            DataService.ds.REF_USERS.child((Auth.auth().currentUser?.uid)!).updateChildValues(["universityShort": uni.shortName])
            DataService.ds.REF_USERS.child((Auth.auth().currentUser?.uid)!).updateChildValues(["uniCount": "\(self.user!.uniCount - 1)"])
            DataService.ds.REF_USERS.child((Auth.auth().currentUser?.uid)!).updateChildValues(["course": self.course])
            self.updateCourseInDB(newCourse: self.course!)
            self.user!.uniCount = self.user!.uniCount - 1
            
            UserDefaults.standard.setValue(uni.shortName, forKey: "uniShort")
        }
        
        popup.addButtons([buttonOne,buttonTwo])
        popup.buttonAlignment = .horizontal
        
        // Present dialog
        self.present(popup, animated: true, completion: nil)
        
//        let alertController = UIAlertController(title: "You are only allowed to change your university 3 times", message: "\(self.user!.uniCount) remainder left", preferredStyle: .alert)
//
//        alertController.addAction(UIAlertAction(title: "Cancel", style: .cancel) { (action:UIAlertAction!) in
//
//        })
//
        
//        alertController.addAction(UIAlertAction(title: "Change", style: .default) { (action:UIAlertAction!) in
//
//
//
//            })
        
        
//        self.present(alertController, animated: true, completion: nil)
    }
    
    func updateCourseInDB(newCourse: String) {
        let ref = DB_BASE
        
        var uid = (Auth.auth().currentUser?.uid)!
        var updateObj = [String:AnyObject]()
        DataService.ds.REF_User_Comment.child(uid).observeSingleEvent(of: .value, with: { (snapshot) in
            
            if let snapshots = snapshot.children.allObjects as? [DataSnapshot] {
                
                for snap in snapshots {
                    
                    
                    if let commentDict = snap.value as? Dictionary<String, AnyObject> {
                        
                        
                        let key = snap.key
                        let comment = Comment(commentKey: key, postData: commentDict)
                        
                        
                        
                        updateObj["confession-comments/\(comment.confessionKey)/\(comment.commentKey)/authorFaculty"] = newCourse as AnyObject
                        updateObj["confession-comments/\(comment.confessionKey)/\(comment.commentKey)/authorUniversity"] = self.uni?.name as AnyObject
                        
                        
                        
                    }
                }
                
                //update confession
                DataService.ds.REF_User_Confession.child(uid).observeSingleEvent(of: .value, with: { (snapshot) in
                    
                    if let snapshots = snapshot.children.allObjects as? [DataSnapshot] {
                        
                        for snap in snapshots {
                            
                            
                            if let confessionDict = snap.value as? Dictionary<String, AnyObject> {
                                
                                
                                let key = snap.key
                                let confession = Confession(confessionKey: key, postData: confessionDict)
                                
                                //Update course
                                updateObj["user-confession/\(uid)/\(confession.confessionKey)/faculty"] = newCourse as AnyObject
                                updateObj["uni-confession/\((confession.university))/\(confession.confessionKey)/faculty"] = newCourse as AnyObject
                                updateObj["confessions/\(confession.confessionKey)/faculty"] = newCourse as AnyObject
                                
                                if let c = confession.category {
                                    updateObj["category/\(c)/\(confession.confessionKey)/faculty"] = newCourse as AnyObject
                                }
                                
                                //Update uni
                                updateObj["user-confession/\(uid)/\(confession.confessionKey)/authorUniversity"] = self.uni!.name as AnyObject
                                updateObj["uni-confession/\((confession.university))/\(confession.confessionKey)/authorUniversity"] = self.uni!.name as AnyObject
                                updateObj["confessions/\(confession.confessionKey)/authorUniversity"] = self.uni!.name as AnyObject
                                
                                if let c = confession.category {
                                    updateObj["category/\(c)/\(confession.confessionKey)/authorUniversity"] = self.uni!.name as AnyObject
                                }
                                
                                if !confession.hashDict.isEmpty {
                                    
                                    for hash in confession.hashDict {
                                        updateObj["hashtag/\(hash.key)/\(confession.confessionKey)/faculty"] = newCourse as AnyObject
                                        updateObj["hashtag/\(hash.key)/\(confession.confessionKey)/authorUniversity"] = self.uni!.name as AnyObject
                                        
                                    }
                                    
                                }
                                
                                
                                
                                
                                
                            }
                        }
                        
                        //update here
                        ref.updateChildValues(updateObj)
                        
                        // self.delegate?.reloadUniFromUniCourseVC(uniName: uni.name, uniShortName: uni.shortName)
                        
                        self.delegate?.reloadUniCourseFromCourseVC(courseName: self.course!, uniName: (self.uni?.name)!, uniShortName: (self.uni?.shortName)!)
                        
                        let profileVC = self.navigationController?.viewControllers[2] as! ProfileVC
                        profileVC.reload_Uni_Course_FromCourseVC(courseName: self.course!, uniName: (self.uni?.name)!, uniShortName: (self.uni?.shortName)!)
                        
                        
                        
                        self.popVC()
                        
                    }
                    
                    
                })
                


                
            }
            
            
        })
    }



}
