//
//  Confession.swift
//  UniThing
//
//  Created by Xavier TanXY on 6/12/16.
//  Copyright © 2016 Xavier TanXY. All rights reserved.
//

import Foundation
import UIKit
import Firebase
import FirebaseDatabase

class Confession {
    private var _caption: String!
   // private var _imageUrl: String!
    var imageUrl: String?
    private var _likes: Int!
    private var _comments: Int!
    private var _confessionKey: String!
    private var _time: NSNumber!
    private var _faculty: String!
    private var _author: String!
    private var _authorGender: String!
    private var _authorUniversity: String!
    private var _usersLike: [String: Bool]!
    private var _usersComment: [String: Bool]!
    private var _postRef: DatabaseReference!

    private var _hashDict: [String: Bool]!
    private var _university: String!
   
    
    
    var anonymous: Bool?
    var authorUserName: String?
    var authorProfilePicUrl: String?
    var category: String?
    
//    var confessionKey: String {
//        return _confessionKey
//    }
    
    var caption: String {
        return _caption
    }
    
//    var imageUrl: String {
//        return _imageUrl
//    }
    
    var likes: Int {
        get {
            return _likes
        }
        
        set(newLikes) {
            if newLikes < 0 {
                _likes = 0
            } else {
                if _likes >= 0 {
                    _likes = newLikes
                }
            }

            
        }
        
    }
    
    var comments: Int {
        get {
            return _comments
        }
        
        set(newComments) {
            _comments = newComments
        }
        
    }
    
    var confessionKey: String {
        return _confessionKey
    }
    
    var time: NSNumber {
        return _time
    }
    
    var faculty: String {
        return _faculty
    }
    
    var author: String {
        return _author
    }
    
    var authorGender: String {
        return _authorGender
    }
    
    var postRef: DatabaseReference {
        return _postRef
    }
    
    var usersLike: [String: Bool] {
        return _usersLike
    }
    
    var usersComment: [String: Bool] {
        return _usersComment
    }
    
    var hashDict: [String: Bool] {
        return _hashDict
    }
    
    var university: String {
        return _university
    }
    
    var authorUni: String {
        return _authorUniversity
    }
    
//    var category: String {
//        return _category
//    }
    
//    var anonymous: Bool {
//        return _anonymous!
//    }
    
//    var authorProfilePicUrl: String {
//        return _authorProfilePicUrl
//    }
    
    
//    init(caption: String, imageUrl: String, likes: Int) {
//        self._caption = caption
//        self._imageUrl = imageUrl
//        self._likes = likes
//        
//    }
    
    init() {
        
    }
    
    init(confessionKey: String, postData: Dictionary<String, AnyObject>) {
        
        
        self._confessionKey = confessionKey
        
        if let caption = postData["caption"] as? String{
            self._caption = caption
        }
        
        self.imageUrl = postData["imageUrl"] as! String?
        
//        if let imageUrl = postData["imageUrl"] as? String{
//            self._imageUrl = imageUrl
//            print(imageUrl)
//        }
        
        if let likes = postData["likes"] as? Int {
            self._likes = likes
        }
        
        if let comments = postData["comments"] as? Int {
            self._comments = comments
        }
   
        if let time = postData["time"] as? NSNumber {
            self._time = time
        }
        
        if let faculty = postData["faculty"] as? String {
            self._faculty = faculty
        }
        
        if let author = postData["author"] as? String {
            self._author = author
        }

        if let authorGender = postData["authorGender"] as? String {
            self._authorGender = authorGender
        }
        
        if let userLike = postData["userLike"] as? [String: Bool] {
            self._usersLike = userLike
        }
        
        if let userComment = postData["userComment"] as? [String: Bool] {
            self._usersComment = userComment
        }
        
        if let uni = postData["university"] as? String {
            self._university = uni
        }
        
        
        if let authorUni = postData["authorUniversity"] as? String {
            self._authorUniversity = authorUni
        }
        
        if let hashDict = postData["hashes"] as? [String: Bool] {
            self._hashDict = hashDict
        } else {
            self._hashDict = [:]
        }
        
        if let anon = postData["anonymous"] as? Bool {
//            self._anonymous = anon
            self.anonymous = anon
        }
        
        if let userName = postData["authorUserName"] as? String {
            //            self._anonymous = anon
            self.authorUserName = userName
        }
        
        if let url = postData["authorProfilePicUrl"] as? String {
            self.authorProfilePicUrl = url
        }
        
        if let cat = postData["category"] as? String {
            self.category = cat
        }
        
        
        _postRef = DataService.ds.REF_CONFESSIONS.child(_confessionKey)
    }
    
    func addUser(uid: String) {
        _usersLike[uid] = true
        //true
    }
    
    func removeUser(uid: String) {
        _usersLike.removeValue(forKey: uid)
        
    }
    
    func addCommentUser(uid: String) {
        _usersComment[uid] = true
    }
    
    func removeCommentUser(uid: String) {
        _usersComment.removeValue(forKey: uid)
    }
    
//    func addUserComment(uid: String, anonymousId: AnyObject) {
//        _usersComment[uid] = anonymousId
//    }
    
//    func removeUserComment(uid: String, anonymousId: Int) {
//        _usersComment.removeValue(forKey: uid)
//    }
    
    func addNotification(completion: @escaping () -> ()) {
        var updateObj = [String:AnyObject]()
        let ref = DB_BASE
        
        if self.author != Auth.auth().currentUser?.uid {
            
            incrementUserNotiCount(authorID: self.author)

            DataService.ds.REF_USERS.child((Auth.auth().currentUser?.uid)!).observeSingleEvent(of: .value, with: { (snapshot) in
                if let snapshots = snapshot.children.allObjects as? [DataSnapshot] {
                    if let userDict = snapshot.value as? Dictionary<String, AnyObject> {
                        let key = snapshot.key
                        var user = User(userKey: key, userData: userDict)
                        
                        let timestamp = NSNumber(integerLiteral: Int(NSDate().timeIntervalSince1970))
                        var notifcationID = DataService.ds.REF_NOTIFICATION.child(self.author).childByAutoId()
                        var notificationValue: Dictionary<String, AnyObject> = ["from" : "\((Auth.auth().currentUser?.uid)!) " as AnyObject, "type" : "like" as AnyObject, "userName" : user.userName as AnyObject, "confessionKey" : self.confessionKey as AnyObject, "time" : timestamp as AnyObject, "profilePicUrl" : user.profilePicUrl as AnyObject, "seen" : false as AnyObject]
                        
                        
                        updateObj["/confession-liker-notification/\(self.confessionKey)/\((Auth.auth().currentUser?.uid)!)"] = ["notificationID" : notifcationID.key ] as AnyObject
                        //updateObj["/liker-confession-notification/\((FIRAuth.auth()?.currentUser?.uid)!)/\(self.confessionKey)"] = ["notificationID" : notifcationID.key ] as AnyObject
                        //updateObj["/notification2/\(self.confessionKey)/\(self.author)"] = ["notificationID" : notifcationID.key ] as AnyObject
                        notifcationID.updateChildValues(notificationValue)
                        
                        updateObj["/notification2/\(self.author)/\(self.confessionKey)/\(notifcationID.key)"] = notificationValue as AnyObject
                        
                        //updateObj["/confession-notification/\(self.confessionKey)/\(notifcationID.key)"] = true as AnyObject
                        updateObj["/confession-liker/\(self.confessionKey)/\((Auth.auth().currentUser?.uid)!)"] = true as AnyObject
                        ref.updateChildValues(updateObj)
                        completion()
                        
                        
                        
                    }
                    
                }
                
            })
        } else {
            completion()
        }
    }
    
    func removeNotification() {
        var updateObj = [String:AnyObject]()
        let ref = DB_BASE
        
        if self.author != Auth.auth().currentUser?.uid {
           
            decreaseUserNotiCount(authorID: self.author)

           DB_BASE.child("confession-liker-notification").child(self.confessionKey).child((Auth.auth().currentUser?.uid)!).observeSingleEvent(of: .value, with: { (snapshot) in
                if let dict = snapshot.value as? Dictionary<String, AnyObject> {
                    var notificationID = dict["notificationID"] as! String
                    updateObj["/notification/\(self.author)/\(notificationID)"] = NSNull()
                    //updateObj["/liker-confession-notification/\((FIRAuth.auth()?.currentUser?.uid)!)/\(self.confessionKey)"] = NSNull()
                    updateObj["/notification2/\(self.author)/\(self.confessionKey)/\(notificationID)"] = NSNull()
                    
                    updateObj["/confession-liker-notification/\(self.confessionKey)/\((Auth.auth().currentUser?.uid)!)"] = NSNull()
                    //updateObj["/confession-notification/\(self.confessionKey)/\(notificationID)"] = NSNull()
                    updateObj["/confession-liker/\(self.confessionKey)/\((Auth.auth().currentUser?.uid)!)"] = NSNull()
                    ref.updateChildValues(updateObj)
                    
                }
            })
            
        }
    }
    
    func adjustLikes(addLike: Bool, uid: String) {
        
        let ref2 = DataService.ds.REF_CONFESSIONS.child(_confessionKey)
        
        ref2.runTransactionBlock({ (currentData) -> TransactionResult in

            if var confessionDict = currentData.value as? Dictionary<String, AnyObject> {
             
                //var confessionDict = currentData.value as? Dictionary<String, AnyObject>
                
                
                
                var userLike = confessionDict["userLike"] as? [String: Bool]
                if addLike {
                    userLike?[uid] = true
                    self.addUser(uid: uid)
                    
                } else {
                    // userLike.removeValue(forKey: uid)
                    userLike?.removeValue(forKey: uid)
                    self.removeUser(uid: uid)
                }
                
                confessionDict["userLike"] = userLike as AnyObject
                
                var likes = confessionDict["likes"] as? Int
                
                if likes != nil {
                    if addLike {
                        likes = likes! + 1
//                        self.addNotification()
                    } else {
                        if likes != 0 {
                            likes = likes! - 1
                            //self.removeNotification()
                        }
                    }
                }
                
                
                confessionDict["likes"] = likes as AnyObject
                
                
                currentData.value = confessionDict
                print("scueess")
                return TransactionResult.success(withValue: currentData)
            }

            return TransactionResult.success(withValue: currentData)
        }) { (error, commited, snap) in

            if commited {
              
                var confessionDict = snap?.value as? Dictionary<String, AnyObject>
               
//                let likeToBeUpdated = snap!.value
                if let likeToBeUpdated = confessionDict?["likes"] {
                    self._likes = likeToBeUpdated as! Int
                    
                    
                    var updateObj = [String:AnyObject]()
                    let ref = DB_BASE
                    
                    if addLike {
                        
                        self.addNotification {
                            
                            self.addUser(uid: uid)
                            
                            if !self.hashDict.isEmpty {
                                
                                for hash in self.hashDict {
                                    updateObj["/hashtag/\(hash.key)/\(self.confessionKey)/likes"] = likeToBeUpdated as AnyObject
                                    updateObj["/hashtag/\(hash.key)/\(self.confessionKey)/userLike/\(uid)"] = true as AnyObject
                                    
                                    
                                }
                                
                            }

                            updateObj["/confessions/\(self.confessionKey)/likes"] = likeToBeUpdated as AnyObject
                            updateObj["/confessions/\(self.confessionKey)/userLike/\(uid)"] = true as AnyObject
                            updateObj["/uni-confession/\(self.university)/\(self.confessionKey)/likes"] = likeToBeUpdated as AnyObject
                            updateObj["/uni-confession/\(self.university)/\(self.confessionKey)/userLike/\(uid)"] = true as AnyObject
                            updateObj["/user-confession/\(self.author)/\(self.confessionKey)/likes"] = likeToBeUpdated as AnyObject
                            updateObj["/user-confession/\(self.author)/\(self.confessionKey)/userLike/\(uid)"] = true as AnyObject
                            
                            if let c = self.category {
                                updateObj["/category/\(c)/\(self.confessionKey)/likes"] = likeToBeUpdated as AnyObject
                                updateObj["/category/\(c)/\(self.confessionKey)/userLike/\(uid)"] = true as AnyObject
                            }
                           
                            
                            ref.updateChildValues(updateObj)
                          
                        }

                        
                        
                        
                        
                        
                    } else {
                        
                        self.removeNotification()
                        self.removeUser(uid: uid)
                        
                        if !self.hashDict.isEmpty {
                            
                            for hash in self.hashDict {
                                updateObj["/hashtag/\(hash.key)/\(self.confessionKey)/likes"] = likeToBeUpdated as AnyObject
                                updateObj["/hashtag/\(hash.key)/\(self.confessionKey)/userLike/\(uid)"] = NSNull()
                                
                            }
                            
                        }
                        
                        updateObj["/confessions/\(self.confessionKey)/likes"] = likeToBeUpdated as AnyObject
                        updateObj["/confessions/\(self.confessionKey)/userLike/\(uid)"] = NSNull()
                        updateObj["/uni-confession/\(self.university)/\(self.confessionKey)/likes"] = likeToBeUpdated as AnyObject
                        updateObj["/uni-confession/\(self.university)/\(self.confessionKey)/userLike/\(uid)"] = NSNull()
                        updateObj["/user-confession/\(self.author)/\(self.confessionKey)/likes"] = likeToBeUpdated as AnyObject
                        updateObj["/user-confession/\(self.author)/\(self.confessionKey)/userLike/\(uid)"] = NSNull()
                        
                        if let c = self.category {
                            updateObj["/category/\(c)/\(self.confessionKey)/likes"] = likeToBeUpdated as AnyObject
                            updateObj["/category/\(c)/\(self.confessionKey)/userLike/\(uid)"] = NSNull()
                        }
                        
                        ref.updateChildValues(updateObj)
                        
                        
                    }
                }

                
            } else {
                //call error callback function if you want
                
                print("fail")
            }

        }

     }
    
    func addCommentNotification(commentKey: String, comment: Comment,completion: @escaping () -> ()) {
        var updateObj = [String:AnyObject]()
        let ref = DB_BASE
        var notificationID: DatabaseReference!
        
        DataService.ds.REF_USERS.child((Auth.auth().currentUser?.uid)!).observeSingleEvent(of: .value, with: { (snapshot) in
            if let snapshots = snapshot.children.allObjects as? [DataSnapshot] {
                if let userDict = snapshot.value as? Dictionary<String, AnyObject> {
                    let key = snapshot.key
                    var user = User(userKey: key, userData: userDict)
                    
                    let timestamp = NSNumber(integerLiteral: Int(NSDate().timeIntervalSince1970))
                    
                    
                    var type: String!
                    var notificationValue: Dictionary<String, AnyObject>!
                    
                    if comment.replyTo != "" {
                        notificationID = DataService.ds.REF_NOTIFICATION.child(comment.replyTo).childByAutoId()
                        type = "reply"
                        
                        updateObj["/FOR_REPLY_DELETION/\(self.confessionKey)/\(comment.replyTo)/\(notificationID.key)"] = true as AnyObject
                        
                        self.incrementUserNotiCount(authorID: comment.replyTo)

                        
                        if comment.anonymous {
                            notificationValue = ["from" : "\((Auth.auth().currentUser?.uid)!) " as AnyObject, "type" : type as AnyObject, "userName" : "One Student" as AnyObject, "confessionKey" : self.confessionKey as AnyObject, "time" : timestamp as AnyObject, "profilePicUrl" : user.profilePicUrl as AnyObject, "seen" : false as AnyObject, "replyToCommentContent": "\((comment.replyCommentContent))" as AnyObject, "fromGender": comment.authorGender as AnyObject]
                        } else {
                            notificationValue = ["from" : "\((Auth.auth().currentUser?.uid)!) " as AnyObject, "type" : type as AnyObject, "userName" : user.userName as AnyObject, "confessionKey" : self.confessionKey as AnyObject, "time" : timestamp as AnyObject, "profilePicUrl" : user.profilePicUrl as AnyObject, "seen" : false as AnyObject, "replyToCommentContent": "\((comment.replyCommentContent))" as AnyObject,"fromGender": comment.authorGender as AnyObject]
                        }

                        
                        
                        notificationID.updateChildValues(notificationValue)
                        
                        
                        updateObj["/confession-notification/\(self.confessionKey)/\(notificationID.key)"] = true as AnyObject
                        updateObj["commenter-commentID-notiID/\((Auth.auth().currentUser?.uid)!)/\(commentKey)/\(notificationID.key)"] = true as AnyObject
                        updateObj["confession-commenter-commentID/\(self.confessionKey)/\((Auth.auth().currentUser?.uid)!)/\(commentKey)"] = true as AnyObject
                        ref.updateChildValues(updateObj)
                        completion()
                    } else {
                        if self.author != Auth.auth().currentUser?.uid {
                            notificationID = DataService.ds.REF_NOTIFICATION.child(self.author).childByAutoId()
                            type = "comment"
                            
                            self.incrementUserNotiCount(authorID: self.author)

                            
                            if comment.anonymous {
                                 notificationValue = ["from" : "\((Auth.auth().currentUser?.uid)!) " as AnyObject, "type" : type as AnyObject, "userName" : "One Student" as AnyObject, "confessionKey" : self.confessionKey as AnyObject, "time" : timestamp as AnyObject, "profilePicUrl" : user.profilePicUrl as AnyObject, "seen" : false as AnyObject, "fromGender": comment.authorGender as AnyObject]
                            } else {
                                 notificationValue = ["from" : "\((Auth.auth().currentUser?.uid)!) " as AnyObject, "type" : type as AnyObject, "userName" : user.userName as AnyObject, "confessionKey" : self.confessionKey as AnyObject, "time" : timestamp as AnyObject, "profilePicUrl" : user.profilePicUrl as AnyObject, "seen" : false as AnyObject,"fromGender": comment.authorGender as AnyObject]
                            }
                           
                            
                            
                            notificationID.updateChildValues(notificationValue)
                            
                            
                            updateObj["/confession-notification/\(self.confessionKey)/\(notificationID.key)"] = true as AnyObject
                            updateObj["commenter-commentID-notiID/\((Auth.auth().currentUser?.uid)!)/\(commentKey)/\(notificationID.key)"] = true as AnyObject
                            updateObj["confession-commenter-commentID/\(self.confessionKey)/\((Auth.auth().currentUser?.uid)!)/\(commentKey)"] = true as AnyObject
                            ref.updateChildValues(updateObj)
                            completion()
                        } else {
                            ref.updateChildValues(updateObj)
                            completion()
                        }
                        
                    }
                    
                }
                
            }
            
        })
    }
    
    func removeCommentNotification(commentKey: String, comment: Comment,completion: @escaping () -> ()) {
        
        var updateObj = [String:AnyObject]()
        let ref = DB_BASE
        var notificationID: DatabaseReference!
        
        var uid: String!
        if self.author != Auth.auth().currentUser?.uid {
            uid = Auth.auth().currentUser?.uid
        } else {
            uid = comment.author
        }
        
        decreaseUserNotiCount(authorID: self.author)

        //to be done
        DB_BASE.child("comment-liker").child(comment.commentKey).observeSingleEvent(of: .value, with: { (snapshot) in
            if let snapshots = snapshot.children.allObjects as? [DataSnapshot] {
                for snap in snapshots {
                    print("yiweri")
                    print(snap.key)
                    var commentLiker = snap.key
                    updateObj["/liker-comment-notification/\(commentLiker)/\(comment.commentKey)"] = NSNull()
                    
                }
                
            }
        })
        
        DB_BASE.child("comment-commentAuthor-notification").child(comment.commentKey).child(comment.author).child("notifications").observeSingleEvent(of: .value, with: { (snapshot) in
            if let snapshots = snapshot.children.allObjects as? [DataSnapshot] {
                for snap in snapshots {
                    print("yiweri")
                    print(snap.key)
                    var notiID = snap.key
                    
                    updateObj["/notification/\(comment.author)/\(notiID)"] = NSNull()
                    //var commentLiker = snap.key
                    
                }
                
            }
            
        })
        
        updateObj["/comment-commentAuthor-notification/\(comment.commentKey)"] = NSNull()
        updateObj["/comment-liker/\(comment.commentKey)"] = NSNull()
        
        
        // if self.author != FIRAuth.auth()?.currentUser?.uid {
        
        DataService.ds.REF_COMMENTER_COMMENT_NOTIFICATION.child(uid!).child(commentKey).observeSingleEvent(of: .value, with: { (snapshot) in
            print("wtf")
            //print(snap)
            //var notiID = snap.value
            if let snapshots = snapshot.children.allObjects as? [DataSnapshot] {
                
                for snap in snapshots {
                    updateObj["/notification/\(self.author)/\(snap.key)"] = NSNull()
                    updateObj["commenter-commentID-notiID/\(uid!)/\(commentKey)/\(snap.key)"] = NSNull()
                    updateObj["/confession-notification/\(self.confessionKey)/\(snap.key)"] = NSNull()
                    
                }
                
                
                
                DB_BASE.child("commenter-commentID-notiID").child(uid!).observeSingleEvent(of: .value, with: { (snap) in
                    if !snap.exists() {
                        updateObj["confession-commenter-commentID/\(self.confessionKey)/\(uid!)"] = NSNull()
                        ref.updateChildValues(updateObj)
                        completion()
                        
                    }
                    
                })
                
                ref.updateChildValues(updateObj)
                completion()
                
                
                
            }
            
        })
        
    }
    func adjustComments(addComment: Bool, uid: String, commentKey: String, comment: Comment) {
        let ref = DataService.ds.REF_CONFESSIONS.child(_confessionKey)
        
        ref.runTransactionBlock({ (currentData) -> TransactionResult in
            var confessionDict = currentData.value as? Dictionary<String,AnyObject>
            //value of the counter before an update
//            var value = currentData.value as? Int
            //var value = confessionDict?["comments"] as? Int
            
            var userComment = confessionDict?["userComment"] as? [String: Bool]
            
            if addComment {
                userComment?[uid] = true
                self.addCommentUser(uid: uid)
            } else {
                userComment?.removeValue(forKey: uid)
                self.removeCommentUser(uid: uid)
            }

            confessionDict?["userComment"] = userComment as AnyObject
            var comments = confessionDict?["comments"] as? Int
            //checking for nil data is very important when using
            //transactional writes
            if comments == nil {
                comments = 0
            }

            if addComment {
                comments = comments! + 1
            } else {

//                if comments != 0 {
//                    comments = comments! - 1
//
//                }

            }

            confessionDict?["comments"] = comments as AnyObject
            currentData.value = confessionDict
            
            //actual update
            
            return TransactionResult.success(withValue: currentData)
            
        }) { (error, commited, snap) in
            
            if commited {
                print("commite")
                var confessionDict = snap?.value as? Dictionary<String,AnyObject>
                //let commentsToBeUpdated = snap!.value
                let commentsToBeUpdated = confessionDict?["comments"]
                self._comments = commentsToBeUpdated as! Int
                
                var updateObj = [String:AnyObject]()
                let ref = DB_BASE
                
                
                if addComment {
                    self.addCommentNotification(commentKey: commentKey, comment: comment) {
                        self.addCommentUser(uid: uid)
                        if !self.hashDict.isEmpty {
                            
                            for hash in self.hashDict {
                                updateObj["/hashtag/\(hash.key)/\(self.confessionKey)/comments"] = commentsToBeUpdated as AnyObject
                                updateObj["/hashtag/\(hash.key)/\(self.confessionKey)/userComment/\(uid)"] = true as AnyObject
                                if self.author != uid {
                                    //updateObj["/hashtag/\(self.university)/\(hash.key)/\(self.confessionKey)/userComment/\(uid)"] = true as AnyObject
                                }
                                
                            }
                            
                        }
                        
                        
     
                        
                        updateObj["/confessions/\(self.confessionKey)/comments"] = commentsToBeUpdated as AnyObject
                        updateObj["/confessions/\(self.confessionKey)/userComment/\(uid)"] = true as AnyObject
                        // updateObj["/confessions/\(self.confessionKey)/userComment/\(uid)"] = anonymousId as AnyObject
                        //updateObj["/uni-confession/\(self.university)/\(self.confessionKey)/comments"] = commentsToBeUpdated as AnyObject
                        //updateObj["/uni-confession/\(self.university)/\(self.confessionKey)/userComment/\(uid)"] = anonymousId as AnyObject
                        //  updateObj["/user-confession/\(self.author)/\(self.confessionKey)/comments"] = commentsToBeUpdated as AnyObject
                        //updateObj["/user-confession/\(self.author)/\(self.confessionKey)/userComment/\(uid)"] = anonymousId as AnyObject
                        
                        
                        updateObj["/uni-confession/\(self.university)/\(self.confessionKey)/comments"] = commentsToBeUpdated as AnyObject
                        updateObj["/uni-confession/\(self.university)/\(self.confessionKey)/userComment/\(uid)"] = true as AnyObject
                        updateObj["/user-confession/\(self.author)/\(self.confessionKey)/comments"] = commentsToBeUpdated as AnyObject
                        updateObj["/user-confession/\(self.author)/\(self.confessionKey)/userComment/\(uid)"] = true as AnyObject
                        
                        if let c = self.category {
                            updateObj["/category/\(c)/\(self.confessionKey)/comments"] = commentsToBeUpdated as AnyObject
                            updateObj["/category/\(c)/\(self.confessionKey)/userComment/\(uid)"] = true as AnyObject
                        }
                        
                        ref.updateChildValues(updateObj)
                    }

                    
        

                    
                } else {
        
                    self.removeCommentNotification(commentKey: commentKey, comment: comment) {
                        self.removeCommentUser(uid: uid)
                        if !self.hashDict.isEmpty {
                            
                            for hash in self.hashDict {
                                updateObj["/hashtag/\(hash.key)/\(self.confessionKey)/comments"] = commentsToBeUpdated as AnyObject
                                updateObj["/hashtag/\(hash.key)/\(self.confessionKey)/userComment/\(uid)"] = NSNull()
                                
                            }
                            
                        }
                        
                        
                        
                        updateObj["FOR_REPLY_DELETION/\(self.confessionKey)/\(comment.replyTo)"] = NSNull()
                        updateObj["/confessions/\(self.confessionKey)/comments"] = commentsToBeUpdated as AnyObject
                        updateObj["/confessions/\(self.confessionKey)/userComment/\(uid)"] = NSNull()
                        updateObj["/uni-confession/\(self.university)/\(self.confessionKey)/comments"] = commentsToBeUpdated as AnyObject
                        updateObj["/uni-confession/\(self.university)/\(self.confessionKey)/userComment/\(uid)"] = NSNull()
                        updateObj["/user-confession/\(self.author)/\(self.confessionKey)/comments"] = commentsToBeUpdated as AnyObject
                        updateObj["/user-confession/\(self.author)/\(self.confessionKey)/userComment/\(uid)"] = NSNull()
                        
                        if let c = self.category {
                            updateObj["/category/\(c)/\(self.confessionKey)/comments"] = commentsToBeUpdated as AnyObject
                            updateObj["/category/\(c)/\(self.confessionKey)/userComment/\(uid)"] = NSNull()
                        }
                        
                        ref.updateChildValues(updateObj)
                    }




                    
                }
                
            } else {
                
                print("fail")
            }
        }

        
    }
    
    func incrementUserNotiCount(authorID: String) {
        let ref2 = DataService.ds.REF_USERS.child(authorID)
        
        ref2.runTransactionBlock({ (currentData) -> TransactionResult in
            
            if var userDict = currentData.value as? Dictionary<String, AnyObject> {
                
                
                var notiCount = userDict["notiCount"] as? Int
                
                if notiCount != nil {
                    notiCount = notiCount! + 1
                    
                }
                
                
                userDict["notiCount"] = notiCount as AnyObject
                
                
                currentData.value = userDict
                print("scueess")
                return TransactionResult.success(withValue: currentData)
            }
            
            return TransactionResult.success(withValue: currentData)
        }) { (error, commited, snap) in
            
            
        }
    }
    
    func decreaseUserNotiCount(authorID: String) {
        let ref2 = DataService.ds.REF_USERS.child(authorID)
        
        ref2.runTransactionBlock({ (currentData) -> TransactionResult in
            
            if var userDict = currentData.value as? Dictionary<String, AnyObject> {
                
                
                var notiCount = userDict["notiCount"] as? Int
                
                if notiCount != nil {
                    
                    if notiCount != 0 {
                        notiCount = notiCount! - 1
                    }
                    
                    
                }
                
                
                userDict["notiCount"] = notiCount as AnyObject
                
                
                currentData.value = userDict
                print("scueess")
                return TransactionResult.success(withValue: currentData)
            }
            
            return TransactionResult.success(withValue: currentData)
        }) { (error, commited, snap) in
            
            
        }
    }


}
