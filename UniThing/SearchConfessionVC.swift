//
//  SearchConfessionVC.swift
//  UniThing
//
//  Created by XavierTanXY on 4/5/18.
//  Copyright © 2018 Xavier TanXY. All rights reserved.
//

import UIKit

class SearchConfessionVC: UIViewController, UITableViewDataSource, UITableViewDelegate {

    @IBOutlet weak var actIndicator: UIActivityIndicatorView!
    @IBOutlet weak var tableView: UITableView!
    var parentNavigationController : UINavigationController?
    
    var confessions = [Confession]()
    var confessionDict = [String: Confession]()
    var searchVC: SearchVC!
    
    var emptyView: LoadingHashVCView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        tableView.delegate = self
        tableView.dataSource = self
        
        self.tableView.register(UINib(nibName: "SearchConfessionCell", bundle: nil), forCellReuseIdentifier: "SearchConfessionCell")
        // Do any additional setup after loading the view.
        
        self.tableView.alpha = 0.0
        self.actIndicator.hidesWhenStopped = true
        
        emptyView = LoadingHashVCView.instanceFromNib() as! LoadingHashVCView
        
        emptyView.frame = self.tableView.bounds
        self.tableView.addSubview(emptyView)
    }

    
    func show() {
        
        self.actIndicator.stopAnimating()
        
        if self.tableView.alpha == 0.0 {
            UIView.animate(withDuration: 0.2, delay: 0.0, options:[], animations: { self.tableView.alpha = 1.0 }, completion: nil)
            
            
            
        }
        
        
    }
    
    func hide() {
        
        self.actIndicator.startAnimating()
        
        if self.tableView.alpha != 0.0 {
            UIView.animate(withDuration: 0.2, delay: 0.0, options:[], animations: { self.tableView.alpha = 0.0 }, completion: nil)
        }
    }
    
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        if searchVC.searchController.searchBar.isFirstResponder {
            searchVC.searchController.searchBar.resignFirstResponder()
//            searchVC.searchController.isActive = false
            searchVC.inSearchMode = false
            
        }
        
        
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return confessions.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        if indexPath.row + 5 >= confessions.count {
            self.loadMore()
        }


        
        let confession = confessions[indexPath.row]
        let cell : SearchConfessionCell = tableView.dequeueReusableCell(withIdentifier: "SearchConfessionCell") as! SearchConfessionCell
        cell.selectionStyle = .none
        
        if confession.imageUrl == NO_PIC {
            cell.confessionImage.isHidden = true
        } else {
            cell.confessionImage.isHidden = false
            cell.confessionImage.loadImageUsingCacheWithUrlString(imageUrl: confession.imageUrl!, tv: self.tableView, index: indexPath)
        }
        
        cell.configureCell(confession: confession)
    
        
//        if indexPath.row == self.confessions.count - 1 {
//
//            self.timer = Timer.scheduledTimer(timeInterval: 0.5, target: self, selector: #selector(self.loadMore), userInfo: nil, repeats: false)
//
//
//        }
        
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("did select \(indexPath.row)")
        let c = confessions[indexPath.row]
        
        c.postRef.observeSingleEvent(of: .value, with: { (snapshot) in
            if snapshot.exists() {
                self.searchVC.moveToConfessionVC(confession: c)
            } else {
                ErrorAlert().createAlert(title: "Something went wrong", msg: "Could not perform action", object: self)
            }
        })
        
    }
//    var timer: Timer!
//    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
//
////        print("end edn")
//        print("nah \(indexPath.row) -  \(self.confessions.count - 1)")
//        if indexPath.row == self.confessions.count - 1 {
//            print("in the end")
//
//            self.timer = Timer.scheduledTimer(timeInterval: 0.5, target: self, selector: #selector(self.loadMore), userInfo: nil, repeats: false)
//
//
//        }
//    }
//    


    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let c = confessions[indexPath.row]
        
        if c.imageUrl == NO_PIC {
            return 100
        } else {
            return 116
        }
    }
    
    func loadMore() {
//        print("wuhoo")
        searchVC.loadMoreConfession()
//        self.tableView.reloadData()
    }
    
    var timer2: Timer!
    func attemptToReloadTable() {
        
        
        //      self.timer?.invalidate()
        
        self.timer2 = Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(reloadTable), userInfo: nil, repeats: false)
        //
//        reloadAndSortTable()
    }
    
    func reloadTable() {
        // self.refreshControl.endRefreshing()
        
        self.confessions = Array(self.confessionDict.values)
        self.confessions.sort(by: { (c1, c2) -> Bool in
            
            return (c1.time.intValue) > (c2.time.intValue)
        })
        
        if self.confessions.count == 0 {
            self.showEmptyView()
        } else {
            self.hideEmptyView()
        }
        
        self.tableView.reloadData()
        self.show()
    }

    func showEmptyView() {
        emptyView.isHidden = false
        self.tableView.bringSubview(toFront: emptyView)
    }
    
    func hideEmptyView() {
        emptyView.isHidden = true
        self.tableView.sendSubview(toBack: emptyView)
        
    }
    
}
