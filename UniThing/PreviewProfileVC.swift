//
//  PreviewProfileVC.swift
//  UniThing
//
//  Created by XavierTanXY on 6/5/18.
//  Copyright © 2018 Xavier TanXY. All rights reserved.
//

import UIKit
import Firebase

class PreviewProfileVC: UIViewController {

    @IBOutlet weak var profilePic: UIImageView!
    @IBOutlet weak var nameLbl: UILabel!
    @IBOutlet weak var userNameLbl: UILabel!
    @IBOutlet weak var detailLbl: UILabel!
    @IBOutlet weak var genderImg: UIImageView!
    
    var userPreview: User!
    
    @IBOutlet weak var initialLbl: UILabel!
    @IBOutlet weak var outerView: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        profilePic.layer.cornerRadius = profilePic.frame.height / 2
        outerView.layer.cornerRadius = outerView.frame.height / 2
        profilePic.clipsToBounds = true
        outerView.clipsToBounds = true
        
        
        
        

    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

 
    }

    func configurePreview(authorID: String, completion: @escaping () -> ()) {
        
        getUserDetails(authorID: authorID) {
            completion()
            self.initialLbl.isHidden = true
            
            self.nameLbl.text = self.userPreview.name
            self.userNameLbl.text = "@\(self.userPreview.userName)"
            self.detailLbl.text = "\(self.userPreview.universityShort) - \(self.userPreview.course)"
            
            if self.userPreview.profilePicUrl != NO_PIC {
                self.profilePic.loadImageUsingCacheWithUrlStringWithoutIndex(imageUrl: self.userPreview.profilePicUrl)
                
            } else {
                //            self.profilePic.image = #imageLiteral(resourceName: "ProfilePic")
                self.initialLbl.isHidden = false
                var iniName = String((self.userPreview.userName[(self.userPreview.userName.startIndex)])).capitalized
                self.initialLbl.text = iniName
                self.outerView.backgroundColor = ColorHelper().pickColor(alphabet: Character(iniName))
                self.profilePic.image = nil
            }
            
            if self.userPreview.gender == MALE {
                self.genderImg.image = #imageLiteral(resourceName: "male")
            } else {
                self.genderImg.image = #imageLiteral(resourceName: "female")
            }
        }

    }
    
    func getUserDetails(authorID: String, completion: @escaping () -> ()) {
        
        DataService.ds.REF_USERS.child(authorID).observeSingleEvent(of: .value, with: { (snapshot) in
            
            if let userDict = snapshot.value as? Dictionary<String, AnyObject> {
                let key = snapshot.key
                self.userPreview = User(userKey: key, userData: userDict)

                completion()
            }
            
            
        })
    }
}
