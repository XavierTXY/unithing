//
//  SearchHashVC.swift
//  UniThing
//
//  Created by XavierTanXY on 5/5/18.
//  Copyright © 2018 Xavier TanXY. All rights reserved.
//

import UIKit

class SearchHashVC: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var actIndicator: UIActivityIndicatorView!
    
    var parentNavigationController : UINavigationController?
    var searchVC: SearchVC!
    
    var hashArray = [Hash]()
    var hashDict = [String: Hash]()
    var emptyView: LoadingHashVCView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        tableView.delegate = self
        tableView.dataSource = self
        
        self.tableView.register(UINib(nibName: "SearchHashCell", bundle: nil), forCellReuseIdentifier: "SearchHashCell")
        
        self.tableView.alpha = 0.0
        self.actIndicator.hidesWhenStopped = true
        
        
        emptyView = LoadingHashVCView.instanceFromNib() as! LoadingHashVCView
        
        emptyView.frame = self.tableView.bounds
        self.tableView.addSubview(emptyView)
        
        
    }

    func show() {
        
        self.actIndicator.stopAnimating()
        
        if self.tableView.alpha == 0.0 {
            UIView.animate(withDuration: 0.2, delay: 0.0, options:[], animations: { self.tableView.alpha = 1.0 }, completion: nil)
            
            
            
        }
        
        
    }
    
    func hide() {
        
        self.actIndicator.startAnimating()
        
        if self.tableView.alpha != 0.0 {
            UIView.animate(withDuration: 0.2, delay: 0.0, options:[], animations: { self.tableView.alpha = 0.0 }, completion: nil)
        }
    }

    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        if searchVC.searchController.searchBar.isFirstResponder {
            searchVC.searchController.searchBar.resignFirstResponder()
        }
        
        
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return hashArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        let cell : SearchHashCell = tableView.dequeueReusableCell(withIdentifier: "SearchHashCell") as! SearchHashCell
        cell.selectionStyle = .none
        
        let h = hashArray[indexPath.row]
        cell.configureCell(hash: h)
        
        print("name of hash \(h.hashName)and number \(h.hashCount)")
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let h = hashArray[indexPath.row]
        print("touch")
        
        DataService.ds.REF_HASHTAG.child(h.hashName).observeSingleEvent(of: .value, with: { (snapshot) in
            if snapshot.exists() {
                self.searchVC.moveToHashtagVC(hash: h)
            } else {
                ErrorAlert().createAlert(title: "Something went wrong", msg: "Could not perform action", object: self)
            }
        })
        
    }
    
    func reloadTable() {

        self.hashArray = Array(self.hashDict.values)

        
        if self.hashArray.count == 0 {
            self.showEmptyView()
        } else {
            self.hideEmptyView()
        }
        
        
        self.tableView.reloadData()
        self.show()
    }
    
    func showEmptyView() {
        emptyView.isHidden = false
        self.tableView.bringSubview(toFront: emptyView)
    }
    
    func hideEmptyView() {
        emptyView.isHidden = true
        self.tableView.sendSubview(toBack: emptyView)
        
    }
}
