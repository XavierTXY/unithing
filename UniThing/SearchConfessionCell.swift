//
//  SearchConfessionCell.swift
//  UniThing
//
//  Created by XavierTanXY on 5/5/18.
//  Copyright © 2018 Xavier TanXY. All rights reserved.
//

import UIKit

class SearchConfessionCell: UITableViewCell {

    @IBOutlet weak var confessionImage: UIImageView!
    @IBOutlet weak var confessionCaption: UILabel!
    @IBOutlet weak var uniCourse: UILabel!
    @IBOutlet weak var likeNumLbl: UILabel!
    @IBOutlet weak var commentNumLbl: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }


    func configureCell(confession: Confession) {
        
        confessionCaption.text = confession.caption
        likeNumLbl.text = "\(confession.likes)"
        commentNumLbl.text = "\(confession.comments)"
        uniCourse.text = "\(confession.authorUni) - \(confession.faculty)"
    }
    
}
