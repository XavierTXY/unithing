let functions = require('firebase-functions');
let admin = require('firebase-admin');
admin.initializeApp(functions.config().firebase);

const algoliasearch = require('algoliasearch');
const dotenv = require('dotenv');
const firebase = require('firebase');

// load values from the .env file in this directory into process.env
dotenv.load();

// configure firebase
firebase.initializeApp({
  databaseURL: process.env.FIREBASE_DATABASE_URL,
});
const database = firebase.database();


const algolia = algoliasearch(
  process.env.ALGOLIA_APP_ID,
  process.env.ALGOLIA_API_KEY
);

const index = algolia.initIndex(process.env.ALGOLIA_INDEX_NAME);
const contactsRef = database.ref('/confessions');
// contactsRef.on('child_added', addOrUpdateIndexRecord);
// contactsRef.on('child_changed', addOrUpdateIndexRecord);
// contactsRef.on('child_removed', deleteIndexRecord);

// function addOrUpdateIndexRecord(confession) {
//   // Get Firebase object
//   const record = confession.val();
//   // Specify Algolia's objectID using the Firebase object key
//   record.objectID = confession.key;
//   // Add or update object
//   index
//     .saveObject(record)
//     .then(() => {
//       console.log('Firebase object indexed in Algolia', record.objectID);
//     })
//     .catch(error => {
//       console.error('Error when indexing confession into Algolia', error);
//       process.exit(1);
//     });
// }

function addOrUpdateIndexRecord(confession, confessionID) {

  console.log('this get called as well');
  // Get Firebase object
  const record = confession;
  // Specify Algolia's objectID using the Firebase object key
  record.objectID = confessionID;
  // Add or update object
  index
    .saveObject(record)
    .then(() => {
      console.log('Firebase object indexed in Algolia', record.objectID);
    })
    .catch(error => {
      console.error('Error when indexing confession into Algolia', error);
      process.exit(1);
    });
}

function deleteIndexRecord(confession, confessionID) {
  // Get Algolia's objectID from the Firebase object key
  const objectID = confessionID;
  // Remove the object from Algolia
  index
    .deleteObject(objectID)
    .then(() => {
      console.log('Firebase object deleted from Algolia', objectID);
    })
    .catch(error => {
      console.error('Error when deleting confession from Algolia', error);
      process.exit(1);
    });
}

// function deleteIndexRecord(confession) {
//   // Get Algolia's objectID from the Firebase object key
//   const objectID = confession.key;
//   // Remove the object from Algolia
//   index
//     .deleteObject(objectID)
//     .then(() => {
//       console.log('Firebase object deleted from Algolia', objectID);
//     })
//     .catch(error => {
//       console.error('Error when deleting confession from Algolia', error);
//       process.exit(1);
//     });
// }


exports.newConfessionCreated = functions.database.ref('/confessions/{confessionId}').onCreate(event => {
    console.log('newConfessionCreated get called');
    var uid = event.auth.variable ? event.auth.variable.uid : null;
    var confessionID = event.params.confessionId;

    return getConfessionDetails(confessionID, uid).then(confession => {
        addOrUpdateIndexRecord(confession, confessionID);
    });

});


exports.sendWelcomeEmail = functions.database.ref('/users/{userId}').onCreate(event => {


  return getAuthorDetails(event.params.userId).then(user => {
    // var email = user.email;
    // console.log('TRIGGERD email ', email);
    // console.log('TRIGGERD id ', event.params.userId);
    addToMailChimp(user.email)
    // notifyMe(event.params.userId)
    

  });
});

exports.notifyAdmin = functions.database.ref('/users/{userId}').onCreate(event => {


	return getAuthorDetails(event.params.userId).then(user => {
		// var email = user.email;
		// console.log('TRIGGERD email ', email);
		// console.log('TRIGGERD id ', event.params.userId);
    // addToMailChimp(user.email)
		notifyMe(event.params.userId)
    

	});
});


function addToMailChimp(email) {
  let fetch = require('isomorphic-fetch');
  let btoa = require('btoa');


      var MAILCHIMP_API_KEY = '0f50a4014958b9ca4bc600957af55db7-us18'
      // NOTE: mailchimp's API uri differs depending on your location. us6 is the east coast. 
      var url = 'https://us18.api.mailchimp.com/3.0/lists/cd0cd5fe4b/members'
      var method = 'POST'
      var headers = {
        'authorization': "Basic " + btoa('randomstring:' + MAILCHIMP_API_KEY),
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      }
      var body = JSON.stringify({email_address: email, status: 'subscribed'})

      console.log('Adding this email to mailchimp', email);
      return fetch(url, {
        method,
        headers,
        body
      }).then(resp => resp.json())
      .then(resp => {
        console.log(resp)
      })
}

function notifyMe(id){


		// var updates = {};
		// var userData = {
		//     email: email
		//  };
  // 		updates['newUser/' + id] = userData;
		// admin.database().update(updates)


		admin.database().ref('newUsers/' + id).set({
		    email: id
		  });

	    return getAuthorDetails('yiLeTV1o6uXnwr3bShxXGLxdysY2').then(me => {

	    	// return getAuthorDetails(id).then(newUser => {
		    	let payload = {
		            notification: {
		                //title: 'Firebase Notification',
		                body:  id + ' joined UniThing. Hurray another new user!',
		                sound: 'default',
		                badge: me.notiCount.toString()

		            }
		        };

		        //var registrationToken = "f2ZbojVi_0Y:APA91bHR5e2p_h5TQGM89TAxwstiKEVraDeiaVXxyE15gii5kfif-220C2tCTIFGzVK-TzqWB7A11DXramAuknRQGP5DMCA-_Z-ai3XnVU0BSGcmJYaSoJbSWTBJbgrqvSwiSZWv84pM";
		        return admin.messaging().sendToDevice(me.pushToken, payload);

	    	// });

	    });





		// var request = Request('https://<dc>.api.mailchimp.com/3.0/lists/[listid]/members', {
  //               method: 'POST',
  //               mode: 'no-cors',
  //               json: {
  //                   "email_address": "am_it@live.com",
  //                   "status": "subscribed",
  //               },
  //               redirect: 'follow',
  //               headers: {
  //                   'Content-Type': 'application/json',
  //                   'Authorization': 'Basic apikey'
  //               },
  //               auth: {
  //                   'user': 'yourUserName',
  //                   'pass': 'yourApiKey'
  //               }
  //           });

		//  fetch(request).then(function (data) {
  //                   console.log(data);
  //         });


}

//Notification for messageing
exports.sendMessageNotification = functions.database.ref('/messages/{msgId}').onCreate(event => {

  var uid = event.auth.variable ? event.auth.variable.uid : null;
  var msgID = event.params.msgId;
  var txt = '';
  return getMessageDetails(msgID).then(msg => {
    if( msg.toId != uid ) {
      return getAuthorDetails(msg.toId).then(user => {

      if( msg.text == null ) {
        txt = msg.fromUserName + ' sent you a picture.';
      } else {
        txt = msg.fromUserName + ': ' + msg.text;
      }

      let payload = {
            notification: {
                //title: 'Firebase Notification',
                body:  txt,
                sound: 'default',
                badge: '1'

            }
      };
        
      return admin.messaging().sendToDevice(user.pushToken, payload);

      });
    }
  });



});

exports.sendFollowingNotification = functions.database.ref('/users/{userID}/following/{followingID}').onCreate(event => {

  var userID = event.params.userID;
  var followingID = event.params.followingID;
  return getAuthorDetails(userID).then(user => {
    return getAuthorDetails(followingID).then(followingUser => {
      let payload = {
          notification: {
              //title: 'Firebase Notification',
              body:  user.userName + ' started following you.',
              //sound: 'default',
              badge: followingUser.notiCount.toString()

          }
      };
      return admin.messaging().sendToDevice(followingUser.pushToken, payload);
    });  
  });

});

//Notification for likes
exports.sendLikePush = functions.database.ref('/confessions/{confessionId}/likes').onUpdate(event => {
	var isAdmin = event.auth.admin;
    var uid = event.auth.variable ? event.auth.variable.uid : null;
    console.log('current online user uid', uid);

	var likerName = "";
	return getAuthorDetails(uid).then(liker => {
		likerName = liker.userName;
		console.log('liker name ', likerName);

		var confessionID = event.params.confessionId;
	
		//var author = projectData.author;
		console.log('conffesion id ', confessionID);
		return getConfessionDetails(confessionID, uid).then(confession => {

      addOrUpdateIndexRecord(confession, confessionID);

			console.log('author id ', confession.author);
			
            let userLike = confession.userLike;
            console.log('likes' , userLike[uid]);

            if( confessionID != 'DISCUSSION' ) {
              if ( userLike[uid] == true ) {
                if( uid != confession.author ) {
                    return getAuthorDetails(confession.author).then(user => {
                        //let tokens = [];
                        //for (let user of users) {
                            //tokens.push(user.pushToken);
                            //console.log('user from details', user.pushToken, tokens);
                        //}
                        let payload = {
                            notification: {
                                //title: 'Firebase Notification',
                                body:  likerName + ' likes your confession.',
                                //sound: 'default',
                                badge: user.notiCount.toString()

                            }
                        };

                        //var registrationToken = "f2ZbojVi_0Y:APA91bHR5e2p_h5TQGM89TAxwstiKEVraDeiaVXxyE15gii5kfif-220C2tCTIFGzVK-TzqWB7A11DXramAuknRQGP5DMCA-_Z-ai3XnVU0BSGcmJYaSoJbSWTBJbgrqvSwiSZWv84pM";
                        return admin.messaging().sendToDevice(user.pushToken, payload);
                    });
                }
              }
            }



	    });
	});


});

//Notification for liking comment
exports.sendLikeCommentPush = functions.database.ref('/comments/{commentId}/likes').onUpdate(event => {
    var isAdmin = event.auth.admin;
    var uid = event.auth.variable ? event.auth.variable.uid : null;
    console.log('current online user uid', uid);

    var likerName = "";
    return getAuthorDetails(uid).then(liker => {
        likerName = liker.userName;
        console.log('liker name ', likerName);

        var commentID = event.params.commentId;
    
        //var author = projectData.author;
        console.log('comment id ', commentID);
        return getCommentDetails(commentID, uid).then(comment => {
            
            let userLike = comment.userLike;
            console.log('likes' , userLike[uid]);
            let isAnonymous = comment.anonymous;

            if ( userLike[uid] == true ) {
                if( uid != comment.author ) {


	                return getAuthorDetails(comment.author).then(user => {
	                    //let tokens = [];
	                    //for (let user of users) {
	                        //tokens.push(user.pushToken);
	                        //console.log('user from details', user.pushToken, tokens);
	                    //}
	                    let payload = {
	                        notification: {
	                            //title: 'Firebase Notification',
	                            body:  likerName + ' likes your comment.',
	                            //sound: 'default',
	                            badge: user.notiCount.toString()

	                        }
	                    };

	                    //var registrationToken = "f2ZbojVi_0Y:APA91bHR5e2p_h5TQGM89TAxwstiKEVraDeiaVXxyE15gii5kfif-220C2tCTIFGzVK-TzqWB7A11DXramAuknRQGP5DMCA-_Z-ai3XnVU0BSGcmJYaSoJbSWTBJbgrqvSwiSZWv84pM";
	                    return admin.messaging().sendToDevice(user.pushToken, payload);
	                });
                	

                }
            }


        });
    });
});


exports.updateCommentInAlgolia = functions.database.ref('/confession-comments/{confessionId}/{commentId}').onCreate(event => {
   var uid = event.auth.variable ? event.auth.variable.uid : null;
   var confessionID = event.params.confessionId;

   return getConfessionDetails(confessionID, uid).then(confession => {
        addOrUpdateIndexRecord(confession, confessionID);
    });

});



//Notification for comments
exports.sendReplyPush = functions.database.ref('/confession-comments/{confessionId}/{commentId}').onCreate(event => {
    var isAdmin = event.auth.admin;
    var uid = event.auth.variable ? event.auth.variable.uid : null;
    console.log('current online user uid', uid);

    let projectStateChanged = false;
    let projectCreated = false;
    let projectData = event.data.val();
    if (!event.data.previous.exists()) {
        projectCreated = true;
    }
    if (!projectCreated && event.data.changed()) {
        projectStateChanged = true;
    }
    let msg = 'A project state was changed';
        if (projectCreated) {
            msg = `The following new project was added to the project: ${projectData.author}`;
        }

    var commentorName = "";
    var confessionID = event.params.confessionId;
    var commentID = event.params.commentId;



    return getCommentDetails(commentID).then(comment => {

        console.log('reply to', comment.replyTo);
        console.log('it zit anonymous', comment.anonymous);

        if( comment.replyTo != "" ) {
        	
	        return getAuthorDetails(comment.replyTo).then(user => {
	        	if( comment.anonymous == true ) {
		            let payload = {
		                notification: {
		                    //title: 'Firebase Notification',
		                    body: 'One student replied on your comment.',
		                    //sound: 'default',
		                    badge: user.notiCount.toString()

		                }
		            };

		            
		            return admin.messaging().sendToDevice(user.pushToken, payload);
	        	} else {
	                let payload = {
	                    notification: {
	                        //title: 'Firebase Notification',
	                        body:  comment.userName + ' replied on your comment.',
	                        //sound: 'default',
		                    badge: user.notiCount.toString()

	                    }
	                };

	                
	                return admin.messaging().sendToDevice(user.pushToken, payload);
	        	}


	        });
        	

        } else {


            return getAuthorDetails(uid).then(commentor => {
                commentorName = commentor.userName;
                return getConfessionDetails(confessionID, uid).then(confession => {

                console.log('author id ', confession.author);
                    
                   let userComment = confession.userComment;
                    //console.log('likes' , userLike[uid]);

                    if( confessionID != 'DISCUSSION' ) {
                        if ( userComment[uid] == true ) {
                          if( uid != confession.author ) {
                              return getAuthorDetails(confession.author).then(user => {

                                if( comment.anonymous == true ) {
                                    let payload = {
                                        notification: {
                                            body: 'One student commented on your confession.',
                                            badge: user.notiCount.toString()

                                        }
                                    };

                                    //var registrationToken = "f2ZbojVi_0Y:APA91bHR5e2p_h5TQGM89TAxwstiKEVraDeiaVXxyE15gii5kfif-220C2tCTIFGzVK-TzqWB7A11DXramAuknRQGP5DMCA-_Z-ai3XnVU0BSGcmJYaSoJbSWTBJbgrqvSwiSZWv84pM";
                                    return admin.messaging().sendToDevice(user.pushToken, payload);
                                } else {
                                    let payload = {
                                        notification: {
                                            body:  commentorName + ' commented on your confession.',
                                            badge: user.notiCount.toString()

                                        }
                                    };

                                    //var registrationToken = "f2ZbojVi_0Y:APA91bHR5e2p_h5TQGM89TAxwstiKEVraDeiaVXxyE15gii5kfif-220C2tCTIFGzVK-TzqWB7A11DXramAuknRQGP5DMCA-_Z-ai3XnVU0BSGcmJYaSoJbSWTBJbgrqvSwiSZWv84pM";
                                    return admin.messaging().sendToDevice(user.pushToken, payload);
                                }

                              });
                          }
                      }

                    }



                });
            });
        	


        }
    });


});

/*
exports.onDeleteConfession = functions.database.ref('/confessions/{confessionId}').onUpdate(event => {

    var confessionID = event.params.confessionId;
    console.log('Deletion of confession', confessionID);
    var uid = event.auth.variable ? event.auth.variable.uid : null;


    removeNotifications(uid, confessionID);
    



});*/

exports.deleteConfessionOnAlgolia = functions.database.ref('/confessions/{confessionId}/').onDelete(event => {

    var confessionID = event.params.confessionId;
    
    var uid = event.auth.variable ? event.auth.variable.uid : null;

    return getConfessionDetails(confessionID, uid).then(confession => {
        deleteIndexRecord(confession, confessionID);
    });
});

exports.onDeletedRow = functions.database.ref('/confessions/{confessionId}/').onDelete(event => {

    var confessionID = event.params.confessionId;
    
    var uid = event.auth.variable ? event.auth.variable.uid : null;



    setTimeout(function (){

        console.log('Deletion', confessionID);
           
        //Delete orphan notification for likes
        admin.database().ref("notification2/"+uid+"/"+confessionID).once("value").then(function(snapshot) {
            snapshot.forEach(function(childSnapshot) {
                //childSnapshot.remove();
                //console.log('inside', childSnapshot);

            var notiID = childSnapshot.key;

            var notiValue = childSnapshot.val();
            console.log('1' , notiID);
            console.log('2' , notiValue);

            admin.database().ref("notification/"+uid+"/"+notiID).remove();
            
           
            }); 
            admin.database().ref("notification2/"+uid+"/"+confessionID).remove();

        });

        //Delete orphan notification for comments
        admin.database().ref("confession-notification/"+confessionID).once("value").then(function(snapshot) {
            snapshot.forEach(function(childSnapshot) {
                //childSnapshot.remove();
                //console.log('inside', childSnapshot);

            var notiID = childSnapshot.key;

            var notiValue = childSnapshot.val();
            console.log('newbie' , notiID);
            console.log('newbie2' , notiValue);

            admin.database().ref("notification/"+uid+"/"+notiID).remove();
            
           
            }); 
            admin.database().ref("confession-notification/"+confessionID).remove();

        });        

    }, 3000);

    


});

// exports.sendPush = functions.database.ref('/confessions/{confessionId}').onWrite(event => {
// 	var isAdmin = event.auth.admin;
//     var uid = event.auth.variable ? event.auth.variable.uid : null;
//     console.log('id', uid);

//     let projectStateChanged = false;
//     let projectCreated = false;
//     let projectData = event.data.val();
//     if (!event.data.previous.exists()) {
//         projectCreated = true;
//     }
//     if (!projectCreated && event.data.changed()) {
//         projectStateChanged = true;
//     }
//     let msg = 'A project state was changed';
// 		if (projectCreated) {
// 			msg = `The following new project was added to the project: ${projectData.author}`;
// 		}
//     return loadUsers().then(users => {
//         let tokens = [];
//         for (let user of users) {
//             tokens.push(user.pushToken);
//             console.log('Uppercasing', user.pushToken, tokens);
//         }
//         let payload = {
//             notification: {
//                 title: 'Firebase Notification',
//                 body: msg,
//                 sound: 'default',
//                 badge: '1'
//             }
//         };

//         var registrationToken = "f2ZbojVi_0Y:APA91bHR5e2p_h5TQGM89TAxwstiKEVraDeiaVXxyE15gii5kfif-220C2tCTIFGzVK-TzqWB7A11DXramAuknRQGP5DMCA-_Z-ai3XnVU0BSGcmJYaSoJbSWTBJbgrqvSwiSZWv84pM";
//         return admin.messaging().sendToDevice(registrationToken, payload);
//     });
// });



function getAuthorDetails(authorID) {
	var ref = '/users/' + authorID
    let dbRef = admin.database().ref(ref);
    let defer = new Promise((resolve, reject) => {
        dbRef.once('value', (snap) => {
            let data = snap.val();
            // let users = [];
            // for (var property in data) {
            //     users.push(data[property]);
            // }
            console.log('The author details' , data);
            resolve(data);
        }, (err) => {
            reject(err);
        });
    });
    return defer;
}

function getMessageDetails(msgID) {
  var ref = '/messages/' + msgID
    let dbRef = admin.database().ref(ref);
    let defer = new Promise((resolve, reject) => {
        dbRef.once('value', (snap) => {
            let data = snap.val();
            // let users = [];
            // for (var property in data) {
            //     users.push(data[property]);
            // }

            resolve(data);
        }, (err) => {
            reject(err);
        });
    });
    return defer;

}

function getConfessionDetails(confessionsID, currentUserID) {
	var ref = '/confessions/' + confessionsID
    let dbRef = admin.database().ref(ref);
    let defer = new Promise((resolve, reject) => {
        dbRef.once('value', (snap) => {
            let data = snap.val();
            // let users = [];
            // for (var property in data) {
            //     users.push(data[property]);
            // }

            console.log('The confession details' , data);
            resolve(data);
        }, (err) => {
            reject(err);
        });
    });
    return defer;

}

function getCommentDetails(commentID, currentUserID) {
    var ref = '/comments/' + commentID
    let dbRef = admin.database().ref(ref);
    let defer = new Promise((resolve, reject) => {
        dbRef.once('value', (snap) => {
            let data = snap.val();
            // let users = [];
            // for (var property in data) {
            //     users.push(data[property]);
            // }

            console.log('The comment details' , data);
            resolve(data);
        }, (err) => {
            reject(err);
        });
    });
    return defer;
}


function removeNotifications(currentUserID, confessionId) {

    var query = admin.database().ref("/notification/"+currentUserID).orderByChild("confessionKey").equalTo(confessionId).on("value").then(function(snapshot) {
        snapshot.forEach(function(childSnapshot) {
          // key will be "ada" the first time and "alan" the second time
          var key = childSnapshot.key;
          // childData will be the actual contents of the child
          var childData = childSnapshot.val();
           console.log('1' , key);
            console.log('2' , childData);

                    // childRef = admin.database().ref("/liker-confession-notification/"+key);
                    // childRef.on('value', function (snapshot2) {
                    //     snapshot2.forEach(function(childSnapshot2) {
                    //         console.log('4', childSnapshot2.key);
                    //         console.log('5', childSnapshot2.val());
                    //     });
                        

                    // });

      });
    });

}
function test(confessionId) {

var query = admin.database().ref("liker-confession-notification");
query.once("value")
  .then(function(snapshot) {
    snapshot.forEach(function(childSnapshot) {
      // key will be "ada" the first time and "alan" the second time
      var key = childSnapshot.key;
      // childData will be the actual contents of the child
      var childData = childSnapshot.val();
       console.log('1' , key);
        console.log('2' , childData);

                childRef = admin.database().ref("/liker-confession-notification/"+key);
                childRef.on('value', function (snapshot2) {
                    snapshot2.forEach(function(childSnapshot2) {
                        console.log('4', childSnapshot2.key);
                        console.log('5', childSnapshot2.val());
                    });
                    

                });

  });
});

    let dbRef = admin.database().ref('/liker-confession-notification');
    let defer = new Promise((resolve, reject) => {
        dbRef.once('value', (snap) => {


            let data = snap.val();
            let users = [];
            //console.log('outside' , data);
            for (var property in data) {
                users.push(data[property]);
                //console.log('nah' , data[property]);
                //console.log('key' , property);
            }
            resolve(users);
        }, (err) => {
            reject(err);
        });
    });
    return defer;


}

function loadUsers() {
    let dbRef = admin.database().ref('/users');
    let defer = new Promise((resolve, reject) => {
        dbRef.once('value', (snap) => {
            let data = snap.val();
            let users = [];
            for (var property in data) {
                users.push(data[property]);
            }
            resolve(users);
        }, (err) => {
            reject(err);
        });
    });
    return defer;
}